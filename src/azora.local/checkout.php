<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Checkout'; ?>

<?php	

	if (!Authentication::isAuthenticated()) {
		header( 'Location: index.php');
	}
	
	$config = Factory::getConfig();
	
	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('validation');
	JSManager::getInstance()->add('numeric');	
	JSManager::getInstance()->add('jquery.ui');			
	CSSManager::getInstance()->add('/js/jqueryui/smoothness/jquery-ui-1.8.16.custom.css');
	
	DomainManager::getInstance()->load('Store');	
	$storeObj = new Store();
	
	DomainManager::getInstance()->load('Point');
	$pointObj = new Point();
	
	$customer_id = Authentication::getUserId();	
	$pointdetatils = $pointObj->getPointDetailsByCustomer($customer_id);
	
	$error = array();	
	
	$validPointsDetails = array();
	foreach($pointdetatils as $detail) {
		if ($detail['accumulated_points'] > 0) {
			array_push($validPointsDetails, $detail);
		}
	}	
		
	require_once dirname(__FILE__) . '/includes/cart.php';			
	$mypoints =	getPointsInCart();
	
	$storeele = 0;
	$dateele = '';
	$timeele = 0;
	$points = array();
	
	$redemption = array();
	
	
	if (!isset($_POST['points'])) {
	
		$tmppoints = $mypoints;
		
		require_once 'checkout.functions.php';	
		
		$calDetails = $validPointsDetails;	
		$values = array();
		
		foreach ($calDetails as $li) {
			$li['accumulated_points'] = 0;
			array_push($values, $li);
		}
		
		while ($tmppoints > 0) {
		
			$valuetodeduct = $tmppoints;
			
			if ($tmppoints >= count($calDetails))
				$valuetodeduct = floor( $tmppoints / count($calDetails) );		
				
			$min = getMinPoint($calDetails);
			
			if ($valuetodeduct > $min['accumulated_points']) {		
				$tmpDetails = $calDetails;
				foreach($tmpDetails as $key => $li) {				
					$tmppoints = $tmppoints - $min['accumulated_points'];
					$calDetails[$key]['accumulated_points'] = $calDetails[$key]['accumulated_points'] - $min['accumulated_points'];
					$values[$key]['accumulated_points'] = $values[$key]['accumulated_points'] + $min['accumulated_points'];
				}
			
				foreach($calDetails as $key => $li) {				
					if($li['accumulated_points'] == 0) 
						unset($calDetails[$key]);
				}
				
			} elseif ($valuetodeduct <= $min['accumulated_points']) {			
				$tmpDetails = $calDetails;
				foreach ($tmpDetails as $key => $li) {				
					if ($tmppoints > 0) {
						$tmppoints = $tmppoints - $valuetodeduct;
						
						$calDetails[$key]['accumulated_points'] = $calDetails[$key]['accumulated_points'] - $valuetodeduct;
						$values[$key]['accumulated_points'] = $values[$key]['accumulated_points'] + $valuetodeduct;
					}
				}
			}
		}	
	} else {		
		$tpoint = 0;
		$valid = true;
		
		if (isset($_POST['points']))
			$points = $_POST['points'];
		
		foreach($points as $point)
			$tpoint += $point;				
			
		if ($tpoint != $mypoints) {
			array_push($error, 'Total deduction points must be same as total redemption points.');
			$valid = false;
		}	
		
		
		$idx = 0;		
		foreach($points as $point) {
			if($validPointsDetails[$idx]['accumulated_points'] < $point) {
				array_push($error, 'Invalid points information found in your changes. Please rectify.');
				$valid = false;
			}
			$idx++;
		}
		
		$date_year = substr($_POST['date'],6,4);
		$date_month = substr($_POST['date'],3,2);
		$date_day = substr($_POST['date'],0,2);
		
		
		if (checkdate($date_month, $date_day, $date_year)) {		
			$date_on = date("Y-m-d", mktime(0,0,0,$date_month, $date_day, $date_year));			
			$cdate = getdate();
			$cdate = new DateTime(date("Y-m-d", mktime(0,0,0,$cdate['mon'],$cdate['mday'],$cdate['year'])));		
			//$cdate->modify('+14 day');
			$cdate->modify('+30 day');
			$cdate = $cdate->format("Y-m-d");
			if ( $date_on <= $cdate ) {
				//array_push($error, 'Collection date should be after two weeks of the submission date.');
				array_push($error, 'Collection date should be after thirty (30) days of the submission date.');
				$valid = false;
			}
		} else {
			array_push($error, 'Invalid collection date. Please rectify.');
			$valid = false;
		}
			
		
		if ($valid) {
		
			$validdetail = $validPointsDetails;	
			$validpoints = array();
			
			$idx = 0;	
			foreach ($validdetail as $li) {
				$li['accumulated_points'] = $points[$idx];
				array_push($validpoints, $li);
				$idx++;
			}
		
			$redemption = array( 'store_id' => $_POST['store'],
									'date' => $_POST['date'],
									'time' => $_POST['time'],
									'points' => $validpoints);
			
			Factory::getSession()->setValue('myredemption', $redemption);
			header( 'Location: coconfirmation.php');exit();
		}
	}
	
	if (Factory::getSession()->isExist('myredemption')) {
		$redemption = Factory::getSession()->getValue('myredemption');		
	} else {
		if (isset($_POST['store'])) 			
			$redemption['store_id'] = $_POST['store'];
		else
			$redemption['store_id'] =  0;
			
		if (isset($_POST['date'])) 			
			$redemption['date'] = $_POST['date'];
		else
			$redemption['date'] =  '';		
		
		if (isset($_POST['time'])) 			
			$redemption['time'] = $_POST['time'];
		else
			$redemption['time'] =  0;	
	}	
?>


<div id="righttitle">Checkout</div>
<div id="contentcontainer">

<?php
	if (isset($error) && count($error) > 0) {
?>
	<div class="error-info form-info">
		<?php foreach ($error as $handle) {
				echo "<p>$handle</p>";
		} ?>
	</div>
<?php
	}
?>


	<form name="checkoutform" id="checkoutform" action="checkout.php" method="post"> 
		<table class="formview" width="100%" border="0" cellspacing="3px" cellpadding="3px">
			<tr>
				<td class="LabelCell Disabled">Total redemption points</td>
				<td><input type="text" name="t_points" id="t_points" maxlength="9" class="input" value="<?php echo $mypoints; ?>" size="20" tabindex="10" disabled /></td>
			</tr>
			<tr>			
				<td class="SectionBar" colspan="2">				
					Please select your prefer location & datetime to collect
				</td>
			</tr>
			<tr>
				<td class="LabelCell Required">Store</td>
				<td>
					<select tabindex="20" name="store" class="GreaterThanZero">						
						<?php
							$stores = $storeObj->getCollectionStores();							
							echo '<option value="0">- Select -</option>';
								
							foreach ($stores as $store) {
								echo '<option value='.$store['store_id'].' '.($store['store_id'] == $redemption['store_id'] ? 'selected' : '') .'>'.$store['store_branch_name'].'</option>';
							}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td class="LabelCell Required">Date <span class="hint">(DD/MM/YYYY)</span></td>
				<td><input type="text" name="date" id="date" maxlength="10" class="input Required ValidDate" value="<?php echo $redemption['date']; ?>" size="20" tabindex="30" /></td>
			</tr>
			<tr>
				<td class="LabelCell Required">Time</td>
				<td>
					<select tabindex="30" name="time" class="GreaterThanZero" tabindex="40">						
						<option value="0" <?php echo (0 == $redemption['time'] ? 'selected' : ''); ?>>- Select -</option>	
						<?php
							$startime = $config['PRMSConfig']->collection_starttime;
							$counter = 0;
							for ($i=0;$i<24;$i++) {
								$time = $startime + $counter;
								if ($time > 23) {
									$time = 0;
									$startime = 0;
									$counter = 0;
								}
								$counter++;
								$timedesc = '';
								if ($time == 0) {
									$timedesc =  '0' . $time . ':00 AM';									
								} else if ($time <= 11) {
									$timedesc =  str_pad($time, 2, "0", STR_PAD_LEFT)  . ':00 AM';
								} else if ($time == 12) {
									$timedesc = $time . ':00 PM';
								} else if ($time > 12) {
									$timedesc = str_pad(($time - 12), 2, "0", STR_PAD_LEFT) . ':00 PM';
								}								
								echo '<option value="'.($time == 0 ? '24' : $time).'" '.(($time == 0 ? 24 : $time) == $redemption['time'] ? 'selected' : '') .'>'.$timedesc.'</option>';								
							}
							
						?>						
					</select>
				</td>
			</tr>
			<tr>			
				<td class="SectionBar" colspan="2">				
					Points will be deducted from
				</td>
			</tr>
			<tr>			
				<td colspan="2">				
					<table width="100%" border="0" cellspacing="3px" cellpadding="3px">						
						<?php							
							$idx = 0;
							$tpoints = 0;
							foreach($validPointsDetails as $detail) {
						?>
							<tr>						
								<td><?php echo $detail['branch_name'] ?></td>
								<td><?php echo $detail['store_name'] ?></td>
								<td><?php echo $detail['accumulated_points'] ?></td>							
								<td width="70px"><input type="text" name="points[]" class="input Required numeric" value="<?php 
										if (isset($redemption['points'])) {																					
											$points = $redemption['points'];
											foreach ($points as $li) {
												if ($li['store_id'] == $detail['store_id']) {
													echo $li['accumulated_points'];
													$tpoints += $li['accumulated_points'];
												}
											}												
										} else {
											if (!isset($_POST['points'])) {
												foreach ($values as $li) {
													if ($li['store_id'] == $detail['store_id']) {
														echo $li['accumulated_points'];
														$tpoints += $li['accumulated_points'];
													}
												}
											} else {
												echo $points[$idx];									
												$tpoints += $points[$idx];
											}
										}
									?>" size="20" tabindex="50" style="width:50px" /></td>
							</tr>
						<?php 
								$idx++;
							} ?>
						<tr>
							<td colspan="2">&nbsp;</td>
							<td><b>Total</b></td>
							<td><span id="tpoints"><?php echo $tpoints; ?></span>&nbsp;pts</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>			
				<td class="BottomToolBar" colspan="2">				
					<input type="submit" name="submit" id="submit" class="button-primary" value="Next" tabindex="60"/>
					<a href="viewcart.php" class="button-secondary" tabindex="70">Cancel</a>					
				</td>			
			</tr>
		</table>
	</form>
	
</div>

<?php Factory::getSession()->removeKey('myredemption'); ?>

<script type="text/javascript">
	$(document).ready(function() { 
		loadValidation('checkoutform');
		$(".numeric").numeric(false);
		$( "#date").datepicker({ dateFormat: 'dd/mm/yy'});
		
		$('input[name$="points[]"]').blur(function() {
			tvalue = 0;
			$('input[name$="points[]"]').each(function() {
				tvalue += parseInt($(this).val());
			});
			
			$('#tpoints').html(tvalue);			
		});
	});
</script>