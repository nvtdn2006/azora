<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Overview'; ?>

<?php	
	require_once './prms/includes/rws_contents.php';
	$cntObj = new RWS_Contents();	
	
	$rwscontents = $cntObj->getContent('Overview');	
?>

<div id="righttitle">Overview</div>
<div class="terms-icon"></div>
<div id="contentcontainer">
<?php
	echo $rwscontents[0]['content'];
?>
<br style="clear:both;" />
</div>
