<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Products'; ?>

<?php
	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('jquery.easing');
	JSManager::getInstance()->add('jquery.cycle');
	JSManager::getInstance()->add('jquery.autoellipsis');
	JSManager::getInstance()->add('fancybox');	
	CSSManager::getInstance()->add('/js/fancybox/jquery.fancybox-1.3.4.css');
	
	$config = Factory::getConfig();
	
	$products  = array();
	$cat_name = 'All Products';
	
	DomainManager::getInstance()->load('Product');
	$productObj = new Product();	
	
	if (isset($_GET['id'])) {		
		$cat_id = $_GET['id'];
		DomainManager::getInstance()->load('Category');
		$categoryObj = new Category();	
		$category =  $categoryObj->getCategory($cat_id);
		$products = $productObj->getProductsByCategory($cat_id);	
		$cat_name = $category['category_name'];
	} else {
		$products = $productObj->getActiveProducts();	
	}
?>

<div id="righttitle"><?php echo $cat_name; ?></div>
<div class="products-listing-icon"></div>
<div id="product-list">
	<?php
		foreach($products as $product) {
			$images = $product['product_images'];
			$image  = null;
			foreach($images as $img) {
				if ($img['image_type'] == 'T') {
					$image = $img;
				}					
			}				
		?>
			<div class="product">
				<div class="image">
					<img src="<?php echo $config['PRMSConfig']->live_site . '/domain/images/products/' . $image['sys_file_name']; ?>" width="155" height="155" alt="<?php echo $product['product']['product_name']; ?>" title="<?php echo $product['product']['product_name']; ?>"/>
					<?php if ($product['product_balance']['product_balance'] <= 0) echo '<div class="outofstock" title="'.$product['product']['product_name'].'"></div>'; ?>
				</div>
				<div class="description">
					<span><?php echo $product['product']['product_name']; ?></span>
					<p title="<?php echo $product['product']['product_description']; ?>"><?php echo $product['product']['product_description']; ?></p>
				</div>
				<div class="controls">
					<span><?php echo number_format($product['product']['product_points']); ?> pts</span>
					<a class="button-primary <?php 						
							if ($product['product_balance']['product_balance'] <= 0) {
								echo 'outofstock';
							} else {
								if (!Authentication::isAuthenticated()) {
									echo 'redeem';
								}
							}							
						?>" href="<?php 
						
							if ($product['product_balance']['product_balance'] <= 0) {
								echo '#outofstock';
							} else {
								if (!Authentication::isAuthenticated()) {
									echo '#pleaselogin';
								} else {
									echo 'productdetail.php?id='. $product['product']['product_id'];
								}
							}
							
						?>" 
						<?php if ($product['product_balance']['product_balance'] <= 0) echo 'disabled'; ?>
						>Redeem</a>
				</div>
			</div>
		<?php
		}
	?>
	<div id="pleaselogin" style="display:none;" class="info-box">	
		Please login first to proceed.
	</div>
	<div id="outofstock" style="display:none;" class="info-box">	
		This item is temporarily out of stock.
	</div>
</div>				

<script type="text/javascript">
	$(document).ready(function() {
		$("div.product div.description").ellipsis();
		
		$("a.redeem").fancybox(
		{
			'scrolling'		: 'no',
			'titleShow'		: false,			
			'onStart' 		: function() {
				$("#pleaselogin").show();
			},
			'onClosed'		: function() {
				$("#pleaselogin").hide();
			}
		});
		
		$("a.outofstock").fancybox(
		{
			'scrolling'		: 'no',
			'titleShow'		: false,			
			'onStart' 		: function() {
				$("#outofstock").show();
			},
			'onClosed'		: function() {
				$("#outofstock").hide();
			}
		});
	});
</script>