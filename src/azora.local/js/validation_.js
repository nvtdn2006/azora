/*
 *  Change history
 *  04-04-2013(PiNT) #FX.1304.001 Fix Multiple Point Submit
 *  21-04-2013(PiNT) #FX.1304.005 Fix SIgn up issue (user cannot sign up thru register screen)
 */
function loadValidation(formname) {

	$('#' + formname + ' input.Required').blur(function()
	{
		setValid($(this).parent());
		
		if( $(this).val().length === 0 ) {			
			setInvalid($(this).parent(),'');
		} else {
			if( $(this).hasClass('GreaterThanOrEqualZero') ) {
				if( parseFloat($(this).val()) < 0 ) {					
					setInvalid($(this).parent(),'Value should be greater than or equal to zero.');
				}				
			}
			
			if( $(this).hasClass('GreaterThanZero') ) {
				if( parseFloat($(this).val()) <= 0 ) {					
					setInvalid($(this).parent(),'Value should be greater than zero.');
				}				
			}
			
			if( $(this).hasClass('ValidDate') ) {
				if( !isValidDate($(this).val()) ) {										
					setInvalid($(this).parent(),'Invalid date format.');
				}				
			}
			
			if( $(this).hasClass('ValidEmail') ) {
				if( !isValidEmail($(this).val()) ) {										
					setInvalid($(this).parent(),'Invalid email address.');
				}				
			}
			
			if( $(this).hasClass('AlphaNumberic') ) {
				if( !isAlphaNumeric($(this).val()) ) {										
					setInvalid($(this).parent(),'Only accept for alphanumeric.');
				}				
			}
		}		
	});
	
	$('#' + formname + ' textarea.Required').blur(function()
	{
		setValid($(this).parent());
		
		if( $(this).val().length === 0 ) {			
			setInvalid($(this).parent(),'');
		}
	});
	
	
	$('#' + formname + ' select').blur(function()
	{		
		setValid($(this).parent());
		if( $(this).hasClass('GreaterThanZero') ) {
			if( parseFloat($(this).val()) <= 0 ) {									
				setInvalid($(this).parent(),'');
			}
		}
	});

	
	$('#' + formname).submit(function ()
	{
		valid = true;

		$('#' + formname + ' input.Required, #' + formname + ' textarea.Required, #' + formname + ' select').each(function () {
			
			if ($(this).is("input")) {
				setValid($(this).parent());
				if ($(this).css('display') != 'none') {
					if( $(this).val().length === 0 ) {						
						setInvalid($(this).parent(),'');
						if (valid) $(this).focus();
						valid = false;
					} else {					
						if( $(this).hasClass('GreaterThanOrEqualZero') ) {
							if( parseFloat($(this).val()) < 0 ) {					
								setInvalid($(this).parent(),'');							
								if (valid) $(this).focus();
								valid = false;
							}				
						}
						
						if( $(this).hasClass('GreaterThanZero') ) {
							if( parseFloat($(this).val()) <= 0 ) {					
								setInvalid($(this).parent(),'Value should be greater than zero.');
								if (valid) $(this).focus();
								valid = false;
							}				
						}
						
						if( $(this).hasClass('ValidDate') ) {
							if( !isValidDate($(this).val()) ) {												
								setInvalid($(this).parent(),'Invalid date format.');
								if (valid) $(this).focus();
								valid = false;
							}				
						}
						
						
						if( $(this).hasClass('ValidEmail') ) {
							if( !isValidEmail($(this).val()) ) {												
								setInvalid($(this).parent(),'Invalid email address.');
								if (valid) $(this).focus();
								valid = false;
							}				
						}
						
						if( $(this).hasClass('AlphaNumberic') ) {
							if( !isAlphaNumeric($(this).val()) ) {												
								setInvalid($(this).parent(),'Only accept for alphanumeric.');
								if (valid) $(this).focus();
								valid = false;
							}				
						}
					}
				}
			}
			
			if ($(this).is("textarea")) {
				setValid($(this).parent());
				if( $(this).val().length === 0 ) {					
					setInvalid($(this).parent(),'');
					if (valid) $(this).focus();
					valid = false;					
				}	
			}
			
			if ($(this).is("select")) {
				setValid($(this).parent());
				if( $(this).hasClass('GreaterThanZero') ) {
					if( parseFloat($(this).val()) <= 0 ) {											
						setInvalid($(this).parent(),'');						
						if (valid) $(this).focus();
						valid = false;					
					}
				}
			}
		});
                
                // 04-04-2013 #FX.1304.001 Fix Multiple Point Submit
                // Disble Button after click
                if (valid == true)
                {
                   // FX.1304.005 (old)
                   // $('input:submit').attr("disabled", true);
                   // FX.1304.005 (new)
                   $('input[name*="save"]').attr("disabled", true);
                }
		
		return valid;
	});

}

function isValidDate(subject){
  if (subject.match(/^(?:(0[1-9]|[12][0-9]|3[01])[\/](0[1-9]|1[012])[\/](19|20)[0-9]{2})$/)){
    return true;
  }else{
    return false;
  }
}

function isValidEmail(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
} 

function setValid(element) {	
	$(element).removeClass('required-warning');
	$(element).children('.invalidtext').remove();
}

function setInvalid(element, text) {	
	$(element).addClass('required-warning');
	$(element).append("<span class='invalidtext'>" + text + "</span>");
}

function isAlphaNumeric(value) {
	var re = /^[A-Za-z0-9]+$/i;
    return re.test(value);
}
