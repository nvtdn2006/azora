function checkSelectAllBox(cssName, isChecked) {

	$('.' + cssName).each(function() {
	   this.checked = isChecked;
	});
	
}        
        
function checkSelectBox(cssNameAll, cssName) {              

	var unCheck = 0;         
	
	$('.' + cssName).each(function() {                                                                                          
	   if (!this.checked){
			unCheck++;
	   }                                             
	}); 
	
	$('.' + cssNameAll).each(function() {                
		this.checked = !(unCheck > 0);
	});	
	
}