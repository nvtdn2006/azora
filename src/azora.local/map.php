<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Map</title>
</head>
<style type="text/css">
	html,
	body {
	   margin:0;
	   padding:0;
	   height:100%;
	}
</style>
<body>
    <div id="map" style="width:500px;height:300px;"></div>
   
    <script type="text/javascript" src="http://gothere.sg/jsapi?sensor=false"></script>
    <script type="text/javascript">
        gothere.load("maps");
        var map, geocoder;
		
		var address = '';
		
		<?php
			if(isset($_GET['address'])) {
				echo 'address = \'' . $_GET['address'] . '\';';
			}
		?>
		
        function initialize() {
            if (GBrowserIsCompatible()) {
                // Create the Gothere map object.
            	map = new GMap2(document.getElementById("map"));
            	// Set the center of the map.
            	map.setCenter(new GLatLng(1.362083, 103.819836), 11);
            	// Add zoom controls on the top left of the map.
            	map.addControl(new GSmallMapControl());
            	// Add a scale bar at the bottom left of the map.
            	map.addControl(new GScaleControl());
            	
            	// Create a geocoder object.
            	geocoder = new GClientGeocoder();
				
				
				if (geocoder) {
					geocoder.getLatLng(address, function(latlng) {
						if (!latlng) {
							alert("Opps, " + address + " not found.");
						} else {
							// Center the map at the address and draw a marker.
							map.setCenter(latlng, 17);
							var marker = new GMarker(latlng);
							map.addOverlay(marker);
							marker.openInfoWindowHtml("<h3>" + address + "</h3>");
						}
					});
				}
				
              }
        }
        gothere.setOnLoadCallback(initialize);
    </script>
</body>
</html>