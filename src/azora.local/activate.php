<?php require_once './includes/application.php'; $this->template = 'registration'; $this->title = 'Activation'; ?>

<?php		
	
	DomainManager::getInstance()->load('Customer');
	$customerObj = new Customer();
	
	$config = Factory::getConfig();
	$cryptographer = Factory::getCryptographer();
	
	$status = 0;
	
	DomainManager::getInstance()->load('Company');
	$companyObj = new Company();
	$company = $companyObj->getCompany();	
	$company = $company[0];
	
	if (isset($_GET['atk']) && isset($_GET['id'])) {
		$activation_key = $_GET['atk'];
		//$id = $cryptographer->Decrypt($_GET['id']);
		$id = $_GET['id'];
		
		$checkingActivation = $customerObj->checkingActivation($activation_key, $id);
		
		if ($checkingActivation == 2) {
			$status = 3;
		} else if ($checkingActivation == 1) {
			if ($customerObj->activateCustomer($activation_key, $id)) {
				$status = 1;
				$customer = $customerObj->getCustomerProfile($id);								
								
				$customer['company_name'] = $company['company_name'];
				
				$mailer = new SiteMailer();				
				$mailer->toMail = $customer['email'] ;
				$mailer->subject = 'Account has been activated at '.$company['company_name'].' redemption website';				
				$mailer->PrepareMail('sendActivatedToCustomer', $customer);
				$mailer->Send();

				//Event Log
				$evtObj = new EventObject();					
				$evtObj->event_id = EventTypes::RWS_UserActivated;
				$evtObj->description = $customer['nric'] . ' activated his/her account.';
				$evtObj->action_by = $customer['nric'];					
				EventLog::Log($evtObj);
				
			} else {
				$status = 2;
			}
		} else if ($checkingActivation == 0) {
			$status = 2;
		}
	}
?>

<div id="registration" >Account activation</div>
<div class="register-icon"></div>

<?php
	if ($status == 1) {
?>

<div id="activation-success">
	<b>Welcome to <?php echo $company['company_name']; ?> redemption website. Your account has been activated.</b><br/><br/><br/><br/>
	You may now try to login with your Login ID and Password.
	<br/><br/><br/><br/><br/><br/>
	<a href="<?php echo $config['PRMSConfig']->live_site; ?>">Back to home</a>
</div>

<?php
	}
	
	if ($status == 2) {
?>

<div id="activation-fail">
	<b>Activation process has failed for your account.</b><br/><br/><br/><br/>
	Please check your email and follow the link to activate your account again.<br/><br/>
	You can also contact to our registration email at <a href="mailto:<?php echo $company['company_reg_email']; ?>"><?php echo $company['company_reg_email']; ?></a>
	<br/><br/><br/><br/><br/><br/>
	<a href="<?php echo $config['PRMSConfig']->live_site; ?>">Back to home</a>
</div>

<?php
	}
	
	if ($status == 0) {
?>

<div id="activation-fail">
	<b>Your activation request is not able to process as the information is not sufficient.</b><br/><br/><br/><br/>
	Please check your email and follow the link to activate your account
	<br/><br/><br/><br/><br/><br/>
	<a href="<?php echo $config['PRMSConfig']->live_site; ?>">Back to home</a>
</div>

<?php
	}
	
	if ($status == 3) {
?>

<div id="activation-success">
	<b>Your account has been already activated.</b><br/><br/><br/><br/>
	Please try login with Login ID and Password. If you forgot password, you can request for a password at login page.
	<br/><br/><br/><br/><br/><br/>
	<a href="<?php echo $config['PRMSConfig']->live_site; ?>">Back to home</a>
</div>

<?php
	}
?>