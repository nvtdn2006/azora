<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Redemption submitted'; ?>


<?php
	Factory::getSession()->removeKey('myredemption');
	require_once dirname(__FILE__) . '/includes/cart.php';			
	clearCart();
?>	

<div id="righttitle" >Redemption submitted</div>
<div id="contentcontainer">

<div class="cprocess">
	Submission process successfully done.<br/><br/><br/><br/>
	Your redemption application has been submitted. Please go and collect on your submitted date and time.
	<br/><br/><br/>
	Thank you for using our redemption online.
	<br/><br/><br/>
	<a href="index.php" class="button-secondary" title="Back to home page.">Go to home</a>
</div>

</div>