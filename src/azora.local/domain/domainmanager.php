<?php
class DomainManager
{	
	private static $instance;
	
	private static $domains;
	
	private function __construct() {
		$this->LoadDomains();
	}
	
	public function LoadDomains() {
		require_once('domains.php');
		self::$domains = getDomains();
	}
	
    public static function getInstance() {
        if (!self::$instance)
            self::$instance = new DomainManager();					
        return self::$instance;
    }
	
	public function load($class) {
		if (file_exists(self::$domains[$class])) {
			require_once(self::$domains[$class]);
		}
		else {
			throw new Exception('Domain could not found.');
		}
	}
}
?>