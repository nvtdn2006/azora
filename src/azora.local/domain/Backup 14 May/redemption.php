<?php
class Redemption
{	
	function submitRedemption(&$redemption) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
		
		try {

			$redemption_id = 0;
			$point_rate = 0;
			
			DomainManager::getInstance()->load('Reward');
			$rewardObj = new Reward();		
			$rewards = $rewardObj->getReward();		
			if (isset($rewards) && count($rewards)){		
				$reward = $rewards[0];					
				$point_rate	= $reward['pt_in_dollars'];
			}			

			$dbh->beginTransaction();
			
			$sth = $dbh->prepare('INSERT INTO redemptions (redemption_no, customer_id, transaction_dt, store_id, collection_date, collection_time, total_points, point_rate, redemption_status) VALUES 
														(:redemption_no, :customer_id, NOW(), :store_id, :collection_date, :collection_time, :total_points, :point_rate, :redemption_status)');		
		
			$redemption_no = $this->getRedemptionNo($redemption['redemption']['customer_id']);
			$sth->bindParam(':redemption_no', $redemption_no);			
			$sth->bindParam(':customer_id', $redemption['redemption']['customer_id']);			
			$sth->bindParam(':store_id', $redemption['redemption']['store_id']);			
			$sth->bindParam(':collection_date', $redemption['redemption']['collection_date']);			
			$sth->bindParam(':collection_time', $redemption['redemption']['collection_time']);			
			$sth->bindParam(':total_points', $redemption['redemption']['total_points']);			
			$sth->bindParam(':point_rate', $point_rate);			
			$sth->bindParam(':redemption_status', $redemption['redemption']['redemption_status']);				
			$sth->execute();
			$redemption_id = $dbh->lastInsertId('redemption_id');
			
			foreach ($redemption['points'] as $point) {			
				$sth = $dbh->prepare('INSERT INTO redemption_pointdetails (redemption_id, store_id, points) VALUES 
															(:redemption_id, :store_id, :points)');		

				$sth->bindParam(':redemption_id', $redemption_id);			
				$sth->bindParam(':store_id', $point['store_id']);		
				$sth->bindParam(':points', $point['points']);		
				$sth->execute();
				
				$sth = $dbh->prepare('UPDATE point_balance SET accumulated_points = accumulated_points - :accumulated_points, last_transaction = NOW() WHERE 
															customer_id = :customer_id AND store_id = :store_id');		

				$sth->bindParam(':accumulated_points', $point['points']);
				$sth->bindParam(':customer_id', $redemption['redemption']['customer_id']);			
				$sth->bindParam(':store_id', $point['store_id']);				
				$sth->execute();			
			}
			
			foreach ($redemption['products'] as $product) {			
				$sth = $dbh->prepare('INSERT INTO redemption_products (redemption_id, product_id, product_cost, product_points, quantity) VALUES 
															(:redemption_id, :product_id, :product_cost, :product_points, :quantity)');		

				$sth->bindParam(':redemption_id', $redemption_id);			
				$sth->bindParam(':product_id', $product['product_id']);		
				$sth->bindParam(':product_cost', $product['product_cost']);		
				$sth->bindParam(':product_points', $product['product_points']);		
				$sth->bindParam(':quantity', $product['quantity']);		
				$sth->execute();
				
				$sth = $dbh->prepare('UPDATE product_balance SET product_balance = product_balance - :product_balance WHERE 
															product_id = :product_id');		

				$sth->bindParam(':product_balance', $product['quantity']);
				$sth->bindParam(':product_id', $product['product_id']);			
				$sth->execute();	
			}
			
			$dbh->commit();							
			$result = true;		

			$redemption['redemption']['redemption_no'] = $redemption_no;
			
			
		} catch(PDOException $e) {
			$dbh->rollback();			
			throw new Exception($e->getMessage());
			exit();
		}
		
		return $result;
	}
	
	
	function getRedemptionNo($customer_id) {
		$no = 'R';
		
		$no .= date('YmdHis');
		$no .=  str_pad($customer_id, 5, "0", STR_PAD_LEFT);
		
		return $no;
	}
	
	function getRedemptionInfo($criteria = null, $limit = 0) {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
		
		$sql = 'SELECT r.*, cp.nric, cp.name, s.store_name, b.branch_name, concat_ws(\' - \', s.store_name,b.branch_name) as store_branch_name, up.full_name as updated_username FROM redemptions r 
								LEFT JOIN customer_profiles cp ON r.customer_id = cp.customer_id
								LEFT JOIN stores s ON r.store_id = s.store_id
								LEFT JOIN branches b ON s.branch_id = b.branch_id
								LEFT JOIN user_profiles up ON r.updated_by = up.user_id';
		
		if ($criteria) {
			$sql .= ' WHERE ';
			$sp = '';
			
			if (isset($criteria['store_id'])) {
				$sql .= $sp . ' r.store_id IN (' .$criteria['store_id']. ')';
				$sp = ' AND ';
			}
			
			if (isset($criteria['submissionfrom']) && isset($criteria['submissionto'])) {
				$start = $criteria['submissionfrom']; $end = $criteria['submissionto'];
				if ($end < $start) {
					$start = $criteria['submissionto'];
					$end = $criteria['submissionfrom'];
				}
				
				$sql .= $sp . ' DATE(r.transaction_dt) BETWEEN \'' .$start. '\' AND \'' .$end. '\'';
				$sp = ' AND ';
			}
			
			if (isset($criteria['nric'])) {
				$sql .= $sp . ' cp.nric LIKE \'%' .$criteria['nric']. '%\'';
				$sp = ' AND ';
			}
			
			if (isset($criteria['name'])) {
				$sql .= $sp . ' cp.name LIKE \'%' .$criteria['name']. '%\'';
				$sp = ' AND ';
			}
			
			if (isset($criteria['collectionfrom']) && isset($criteria['collectionto'])) {
				$start = $criteria['collectionfrom']; $end = $criteria['collectionto'];
				if ($end < $start) {
					$start = $criteria['collectionto'];
					$end = $criteria['collectionfrom'];
				}
				
				$sql .= $sp . ' DATE(r.collection_date) BETWEEN \'' .$start. '\' AND \'' .$end. '\'';
				$sp = ' AND ';
			}
			
			if (isset($criteria['redemption_status'])) {
				$sql .= $sp . ' r.redemption_status = ' .$criteria['redemption_status'];
				$sp = ' AND ';
			}
		}
		
		if ($limit > 0) {
			$sql .= ' ORDER BY r.transaction_dt DESC LIMIT 0, '.$limit;
		}
		
		$sth = $dbh->prepare($sql);			
		$sth->execute();
		$result = $sth->fetchAll(PDO::FETCH_ASSOC);		
		
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function getRedemption($redemption_id) {
		$result = array();
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT r.*, cp.nric, cp.name, s.store_name, b.branch_name, concat_ws(\' - \', s.store_name,b.branch_name) as store_branch_name, up.full_name as updated_username FROM redemptions r 
								LEFT JOIN customer_profiles cp ON r.customer_id = cp.customer_id
								LEFT JOIN stores s ON r.store_id = s.store_id
								LEFT JOIN branches b ON s.branch_id = b.branch_id
								LEFT JOIN user_profiles up ON r.updated_by = up.user_id
								WHERE r.redemption_id = :redemption_id');		

		$sth->bindParam(':redemption_id', $redemption_id);
		$sth->execute();
		$result['redemption'] = $sth->fetch(PDO::FETCH_ASSOC);
		
		
		$sth = $dbh->prepare('SELECT rpt.*, s.store_name, b.branch_name FROM redemption_pointdetails rpt	
								LEFT JOIN stores s ON rpt.store_id = s.store_id
								LEFT JOIN branches b ON s.branch_id = b.branch_id
								WHERE rpt.redemption_id = :redemption_id');		

		$sth->bindParam(':redemption_id', $redemption_id);
		$sth->execute();
		$result['redemption_points'] = $sth->fetchAll(PDO::FETCH_ASSOC);
		
		$sth = $dbh->prepare('SELECT rp.*, p.product_name FROM redemption_products rp 								
								INNER JOIN products p ON rp.product_id = p.product_id
								WHERE rp.redemption_id = :redemption_id');		

		$sth->bindParam(':redemption_id', $redemption_id);
		$sth->execute();
		$result['redemption_products'] = $sth->fetchAll(PDO::FETCH_ASSOC);
				
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function updateRedemptionStatus($update) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('UPDATE redemptions SET redemption_status  = :redemption_status , updated_by = :updated_by, updated_remarks = :updated_remarks, updated_dt = NOW()
								WHERE redemption_id = :redemption_id');			
		
		$sth->bindParam(':redemption_status', $update['redemption_status']);
		$sth->bindParam(':updated_by', $update['updated_by']);
		$sth->bindParam(':updated_remarks', $update['updated_remarks']);		
		$sth->bindParam(':redemption_id', $update['redemption_id']);
			
		$sth->execute();
		$result = ($sth->rowCount() > 0);
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function getRedemptionByCustomer($customer_id, $fromdate, $todate, $status = 0) {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT r.*, cp.nric, cp.name, s.store_name, b.branch_name, concat_ws(\' - \', s.store_name,b.branch_name) as store_branch_name, up.full_name as updated_username FROM redemptions r 
								LEFT JOIN customer_profiles cp ON r.customer_id = cp.customer_id
								LEFT JOIN stores s ON r.store_id = s.store_id
								LEFT JOIN branches b ON s.branch_id = b.branch_id
								LEFT JOIN user_profiles up ON r.updated_by = up.user_id WHERE r.customer_id = :customer_id 
								AND date(r.transaction_dt) BETWEEN :fromdate AND :todate' . ($status > 0 ? ' AND r.redemption_status = '.$status : '') );			
		$from = $fromdate->format('Y-m-d');
		$to = $todate->format('Y-m-d');
		$sth->bindParam(':customer_id', $customer_id);
		$sth->bindParam(':fromdate', $from);
		$sth->bindParam(':todate', $to);
		$sth->execute();		
		$result = $sth->fetchAll(PDO::FETCH_ASSOC);
		
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function getRedemptionStatus($redemption_status) {
		$rvalue = '';
		switch ($redemption_status) {
			case 1:
				$rvalue = 'New redemption'; break;
			case 2:
				$rvalue = 'Collected'; break;
			case 3:
				$rvalue = 'Expired'; break;
			case 4:
				$rvalue = 'Cancelled'; break;
		}
		return $rvalue;
	}

	function getRedemptionForReminder() {	
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
		
		$firstday = getdate();
		$firstday = new DateTime(date("Y-m-d", mktime(0,0,0,$firstday['mon'],$firstday['mday'],$firstday['year'])));			
		$firstday->modify('+5 day');		
		$firstday = $firstday->format("Y-m-d");		
		
		$secondday = getdate();
		$secondday = new DateTime(date("Y-m-d", mktime(0,0,0,$secondday['mon'],$secondday['mday'],$secondday['year'])));			
		$secondday->modify('+3 day');		
		$secondday = $secondday->format("Y-m-d");		
				
		$sth = $dbh->prepare('SELECT distinct store_id AS store_id FROM redemptions WHERE redemption_status = 1 AND (collection_date = :firstday OR collection_date = :secondday)');
		$sth->bindParam(':firstday', $firstday);
		$sth->bindParam(':secondday', $secondday);
		$sth->execute();
		$result = $sth->fetchAll(PDO::FETCH_ASSOC);

		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function getBills($month, $year, $store_id) {		
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		/*$sth = $dbh->prepare('SELECT r.*, cp.nric, cp.name, rp.store_id, rp.points, s.store_name, b.branch_name, concat_ws(\' - \', s.store_name,b.branch_name) as store_branch_name FROM redemption_pointdetails rp LEFT JOIN
								redemptions r ON rp.redemption_id = r.redemption_id LEFT JOIN stores s ON rp.store_id = s.store_id 
								LEFT JOIN branches b ON s.branch_id = b.branch_id 
								LEFT JOIN customer_profiles cp ON r.customer_id = cp.customer_id
								WHERE rp.store_id = :store_id AND r.redemption_status = 2 
								AND month(r.updated_dt) = :month AND year(r.updated_dt) = :year');*/

		$sth = $dbh->prepare('SELECT r.*, cp.nric, cp.name, rp.product_id, rp.product_cost, rp.product_points, rp.quantity, p.product_name, rpd.points, 
								ROUND( (rp.product_cost / rp.product_points) * rpd.points ) AS amount
								, s.store_name, b.branch_name, concat_ws(\' - \', s.store_name,b.branch_name) as store_branch_name 
								FROM redemption_products rp LEFT JOIN
								redemptions r ON rp.redemption_id = r.redemption_id
								LEFT JOIN redemption_pointdetails rpd ON r.redemption_id = rpd.redemption_id
								LEFT JOIN stores s ON rpd.store_id = s.store_id 
								LEFT JOIN branches b ON s.branch_id = b.branch_id 
								LEFT JOIN products p ON rp.product_id = p.product_id
								LEFT JOIN customer_profiles cp ON r.customer_id = cp.customer_id
								WHERE rpd.store_id = :store_id AND r.redemption_status = 2 
								AND month(r.updated_dt) = :month AND year(r.updated_dt) = :year');
		$sth->bindParam(':store_id', $store_id);
		$sth->bindParam(':month', $month);
		$sth->bindParam(':year', $year);
		$sth->execute();		
		$result = $sth->fetchAll(PDO::FETCH_ASSOC);
		
		$sth = null;
		$dbh = null;
		return $result;		
	}
	
	function doRedemptionExpiry() {	
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
		
		$sth = $dbh->prepare('SELECT distinct r.customer_id, cp.nric, cp.name, cp.email FROM redemptions r LEFT JOIN customer_profiles cp ON r.customer_id = cp.customer_id 
							WHERE date_add(r.collection_date, INTERVAL 10 DAY) = date(now())  AND r.redemption_status = 1');		
		$sth->execute();
		$expired = $sth->fetchAll(PDO::FETCH_ASSOC);
		
		if (isset($expired) && count($expired) > 0) {
		
			$result = $expired;
			
			$sth = $dbh->prepare('SELECT redemption_id FROM redemptions 
							WHERE date_add(collection_date, INTERVAL 10 DAY) = date(now())  AND redemption_status = 1');		
			$sth->execute();
			$expired = $sth->fetchAll(PDO::FETCH_ASSOC);
		
			foreach ($expired as $item) {
				$sth = $dbh->prepare('UPDATE redemptions SET redemption_status = 3, updated_dt = NOW(), updated_remarks = \'Expired by system\' WHERE redemption_id = :redemption_id');		
				$sth->bindParam(':redemption_id', $item['redemption_id']);
				$sth->execute();
			}
		
		}
		
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	
}
?>