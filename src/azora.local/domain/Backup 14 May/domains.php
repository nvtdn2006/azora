<?php
function getDomains() {
	return array(	
					'Branch' => WEB_ROOT . '/domain/branch.php',
					'Store' => WEB_ROOT . '/domain/store.php',
					'Category' => WEB_ROOT . '/domain/category.php',
					'Product' => WEB_ROOT . '/domain/product.php',
					'Reward' => WEB_ROOT . '/domain/reward.php',
					'Customer' => WEB_ROOT . '/domain/customer.php',
					'Point' => WEB_ROOT . '/domain/point.php',
					'Redemption' => WEB_ROOT . '/domain/redemption.php',
					'Company' => WEB_ROOT . '/domain/company.php',
					'Marketing' => WEB_ROOT . '/domain/marketing.php',					
				);
}
?>