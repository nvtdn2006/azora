<?php
class Reward
{
	function getReward() {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();			
		$sth = $dbh->prepare('SELECT rrp.*, cu.username as created_username, mu.username as updated_username FROM reward_rule_policies rrp 
								LEFT JOIN users cu ON rrp.created_by = cu.user_id
								LEFT JOIN users mu ON rrp.updated_by = mu.user_id
								LIMIT 1;
								');			
		
		$sth->execute();
		$result = $sth->fetchAll(PDO::FETCH_ASSOC);
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function saveReward($reward) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('INSERT INTO reward_rule_policies (pt_in_dollars, rounding, expiration_required, expiration_month, terms_n_conditions, created_dt, created_by, updated_dt, updated_by) VALUES 
							(:pt_in_dollars, :rounding, :expiration_required, :expiration_month, :terms_n_conditions, NOW(), :created_by, NOW(), :updated_by)');			
		
		$sth->bindParam(':pt_in_dollars', $reward['pt_in_dollars']);
		$sth->bindParam(':rounding', $reward['rounding']);	
		$sth->bindParam(':expiration_required', $reward['expiration_required']);
		$sth->bindParam(':expiration_month', $reward['expiration_month']);
		$sth->bindParam(':terms_n_conditions', $reward['terms_n_conditions']);
		$sth->bindParam(':created_by', $reward['created_by']);	
		$sth->bindParam(':updated_by', $reward['updated_by']);
			
		$sth->execute();
		$result = ($sth->rowCount() > 0);
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function updateReward($reward) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('UPDATE reward_rule_policies SET pt_in_dollars = :pt_in_dollars, rounding = :rounding, expiration_required = :expiration_required, 
						expiration_month = :expiration_month, terms_n_conditions = :terms_n_conditions,
						updated_dt = NOW(), updated_by = :updated_by WHERE id = :id');			
		
		$sth->bindParam(':pt_in_dollars', $reward['pt_in_dollars']);
		$sth->bindParam(':rounding', $reward['rounding']);	
		$sth->bindParam(':expiration_required', $reward['expiration_required']);
		$sth->bindParam(':expiration_month', $reward['expiration_month']);
		$sth->bindParam(':terms_n_conditions', $reward['terms_n_conditions']);		
		$sth->bindParam(':updated_by', $reward['updated_by']);
		$sth->bindParam(':id', $reward['id']);
			
		$sth->execute();
		$result = ($sth->rowCount() > 0);
		$sth = null;
		$dbh = null;
		return $result;
	}
}
?>