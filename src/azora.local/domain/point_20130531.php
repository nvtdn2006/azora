<?php

class Point {

    function registerPoint(&$point, &$firstTimeUsing) {
        $result = false;
        Loader::Load('Database');
        $dbh = Database::getInstance()->getConnection();

        try {

            $startedDate = $this->getPointStartedDate($point['customer_id']);

            if ($startedDate == null || $startedDate == '') {
                $firstTimeUsing = true;
                $startedDate = date('Y-m-d');
            }

            $rewarded_points = $this->getRewardPoints($point['sales_amount']);

            if ($rewarded_points > 0) {

                $dbh->beginTransaction();

                $sth = $dbh->prepare('INSERT INTO point_transactions (customer_id, transacted_date, reference_no, sales_amount, trans_points, transaction_type, remarks, store_id, registered_by) VALUES 
									(:customer_id, NOW(), :reference_no, :sales_amount, :trans_points, :transaction_type, :remarks, :store_id, :registered_by)');

                $sth->bindParam(':customer_id', $point['customer_id']);
                $sth->bindParam(':reference_no', $point['reference_no']);
                $sth->bindParam(':sales_amount', $point['sales_amount']);
                $sth->bindParam(':trans_points', $rewarded_points);
                $sth->bindParam(':transaction_type', $point['transaction_type']);
                $sth->bindParam(':remarks', $point['remarks']);
                $sth->bindParam(':store_id', $point['store_id']);
                $sth->bindParam(':registered_by', $point['registered_by']);
                $sth->execute();

                if ($this->getPointsForAStore($point['customer_id'], $point['store_id']) >= 0) {
                    $sth = $dbh->prepare('UPDATE point_balance SET accumulated_points = accumulated_points + :accumulated_points, last_transaction = NOW()
											WHERE customer_id = :customer_id AND store_id = :store_id');
                } else {
                    $sth = $dbh->prepare('INSERT INTO point_balance (customer_id, store_id, accumulated_points, last_transaction, started_date) VALUES 
											(:customer_id, :store_id, :accumulated_points, NOW(), :started_date)');
                    $sth->bindParam(':started_date', $startedDate);
                }

                $sth->bindParam(':customer_id', $point['customer_id']);
                $sth->bindParam(':store_id', $point['store_id']);
                $sth->bindParam(':accumulated_points', $rewarded_points);

                $sth->execute();

                $dbh->commit();

                $point['rewarded_points'] = $rewarded_points;
                $point['started_date'] = $startedDate;

                $result = true;
            }
        } catch (PDOException $e) {
            $dbh->rollback();
            throw new Exception($e->getMessage());
            exit();
        }

        return $result;
    }

    function getPointStartedDate($customer_id) {
        $startedDate = null;
        Loader::Load('Database');
        $dbh = Database::getInstance()->getConnection();

        $sth = $dbh->prepare('SELECT MIN(started_date) AS started_date FROM point_balance WHERE customer_id = :customer_id');
        $sth->bindParam(':customer_id', $customer_id);
        $sth->execute();
        $result = $sth->fetch(PDO::FETCH_ASSOC);

        if (isset($result)) {
            $startedDate = $result['started_date'];
        }

        $sth = null;
        $dbh = null;
        return $startedDate;
    }

    //+START FX.1304.003
    // Start date based on first transaction
    function getPointStartedDate2($customer_id) {
        $startedDate = null;
        Loader::Load('Database');
        $dbh = Database::getInstance()->getConnection();

        $sth = $dbh->prepare('SELECT MIN( transacted_date ) AS started_date
                    FROM point_transactions
                    WHERE customer_id = :customer_id');
        $sth->bindParam(':customer_id', $customer_id);
        $sth->execute();
        $result = $sth->fetch(PDO::FETCH_ASSOC);

        if (isset($result)) {
            $startedDate = $result['started_date'];
        }

        $sth = null;
        $dbh = null;
        return $startedDate;
    }

    function getPointExpiryDate($customer_id) {
        DomainManager::getInstance()->load('Reward');
        $rewardObj = new Reward();

        $rewards = $rewardObj->getReward();

        $pointExpiryDate = null;

        if (isset($rewards) && count($rewards) > 0) {
            $reward = $rewards[0];

            if ($reward['expiration_required'] == 1) {
                $expiration_month = $reward['expiration_month'];

                $pointStartDate = $this->getPointStartedDate2($customer_id);
                if ($pointStartDate != "") {

                    $pointExpiryDate = new DateTime($pointStartDate);

                    while ($pointExpiryDate <= new DateTime()) {

                        $pointExpiryDate->modify('+' . $expiration_month . ' month');
                    }
                } else {
                    $pointExpiryDate = null;
                }
            }
        }

        return $pointExpiryDate;
    }

    //-END FX.1304.003

    function getPointsForAStore($customer_id, $store_id) {
        $accumulated_points = -1;
        Loader::Load('Database');
        $dbh = Database::getInstance()->getConnection();

        $sth = $dbh->prepare('SELECT accumulated_points FROM point_balance WHERE customer_id = :customer_id AND store_id = :store_id');
        $sth->bindParam(':customer_id', $customer_id);
        $sth->bindParam(':store_id', $store_id);
        $sth->execute();

        if ($sth->rowCount() > 0) {
            $result = $sth->fetch(PDO::FETCH_ASSOC);
            $accumulated_points = $result['accumulated_points'];
        }

        $sth = null;
        $dbh = null;
        return $accumulated_points;
    }

    function getRewardPoints($salesamount) {
        $points = 0;
        $rewards = null;
        DomainManager::getInstance()->load('Reward');
        $rewardObj = new Reward();
        $rewards = $rewardObj->getReward();
        if (isset($rewards) && count($rewards) > 0 && $salesamount > 0) {
            $reward = $rewards[0];
            if ($reward['rounding'] == 0)
                $points = floor($salesamount / $reward['pt_in_dollars']);
            else {
                $points = round($salesamount / $reward['pt_in_dollars']);
            }
        }
        return $points;
    }

    /* function getPointsOnCost($cost) {
      $points = 0;
      $rewards = null;
      DomainManager::getInstance()->load('Reward');
      $rewardObj = new Reward();
      $rewards = $rewardObj->getReward();
      if (isset($rewards) && count($rewards) > 0 && $cost > 0){
      $reward = $rewards[0];
      $points = $cost * $reward['pt_in_dollars'];
      }
      return $points;
      } */

    function getPointByCustomer($customer_id) {
        $points = 0;
        Loader::Load('Database');
        $dbh = Database::getInstance()->getConnection();

        $sth = $dbh->prepare('SELECT SUM(accumulated_points) AS accumulated_points FROM point_balance WHERE customer_id = :customer_id');
        $sth->bindParam(':customer_id', $customer_id);
        $sth->execute();
        $result = $sth->fetch(PDO::FETCH_ASSOC);

        if (isset($result)) {
            $points = $result['accumulated_points'];
        }

        $sth = null;
        $dbh = null;
        return $points;
    }

    function getPointDetailsByCustomer($customer_id) {
        $result = null;
        Loader::Load('Database');
        $dbh = Database::getInstance()->getConnection();

        $sth = $dbh->prepare('SELECT pb.*, s.store_name, b.branch_name FROM point_balance pb LEFT JOIN stores s ON pb.store_id = s.store_id 
								LEFT JOIN branches b ON s.branch_id = b.branch_id WHERE pb.customer_id = :customer_id');
        $sth->bindParam(':customer_id', $customer_id);
        $sth->execute();
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);

        $sth = null;
        $dbh = null;
        return $result;
    }

    function getPurchaseHistoryByCustomer($customer_id, $month = null, $year = null) {
        $result = null;
        Loader::Load('Database');
        $dbh = Database::getInstance()->getConnection();

        if ($month != null && $year != null) {
            $sth = $dbh->prepare('SELECT pt.*, s.store_name, b.branch_name FROM point_transactions pt LEFT JOIN stores s ON pt.store_id = s.store_id 
									LEFT JOIN branches b ON s.branch_id = b.branch_id WHERE pt.customer_id = :customer_id AND transaction_type = 1 
									AND month(transacted_date) = :month AND year(transacted_date) = :year');
            $sth->bindParam(':customer_id', $customer_id);
            $sth->bindParam(':month', $month);
            $sth->bindParam(':year', $year);
        } else { /* #PFX201200014 12/2/2012 */
            $sth = $dbh->prepare('SELECT pt.*, s.store_name, b.branch_name FROM point_transactions pt LEFT JOIN stores s ON pt.store_id = s.store_id
					LEFT JOIN branches b ON s.branch_id = b.branch_id WHERE pt.customer_id = :customer_id AND transaction_type = 1
					');
            $sth->bindParam(':customer_id', $customer_id);
        }
        $sth->execute();
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);

        $sth = null;
        $dbh = null;
        return $result;
    }

    function getPointsTransactionByCustomer($customer_id, $fromdate, $todate, $type) {
        $result = null;
        Loader::Load('Database');
        $dbh = Database::getInstance()->getConnection();

        $sth = $dbh->prepare('SELECT pt.*, s.store_name, b.branch_name FROM point_transactions pt LEFT JOIN stores s ON pt.store_id = s.store_id 
								LEFT JOIN branches b ON s.branch_id = b.branch_id WHERE pt.customer_id = :customer_id 
								AND date(transacted_date) BETWEEN :fromdate AND :todate' . ($type > 0 ? ' AND transaction_type = ' . $type : ''));
        $from = $fromdate->format('Y-m-d');
        $to = $todate->format('Y-m-d');
        $sth->bindParam(':customer_id', $customer_id);
        $sth->bindParam(':fromdate', $from);
        $sth->bindParam(':todate', $to);
        $sth->execute();
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);

        $sth = null;
        $dbh = null;
        return $result;
    }

    function cancelPoint($point) {
        $result = false;
        Loader::Load('Database');
        $dbh = Database::getInstance()->getConnection();

        try {

            $cancel_points = $this->getRewardPoints($point['sales_amount']);

            $dbh->beginTransaction();

            $sth = $dbh->prepare('INSERT INTO point_transactions (customer_id, transacted_date, reference_no, sales_amount, trans_points, transaction_type, remarks, store_id, registered_by) VALUES 
								(:customer_id, NOW(), :reference_no, :sales_amount, :trans_points, :transaction_type, :remarks, :store_id, :registered_by)');

            $sth->bindParam(':customer_id', $point['customer_id']);
            $sth->bindParam(':reference_no', $point['reference_no']);
            $sth->bindParam(':sales_amount', $point['sales_amount']);
            $sth->bindParam(':trans_points', $cancel_points);
            $sth->bindParam(':transaction_type', $point['transaction_type']);
            $sth->bindParam(':remarks', $point['remarks']);
            $sth->bindParam(':store_id', $point['store_id']);
            $sth->bindParam(':registered_by', $point['registered_by']);
            $sth->execute();

            $sth = $dbh->prepare('UPDATE point_balance SET accumulated_points = accumulated_points - :accumulated_points, last_transaction = NOW()
									WHERE customer_id = :customer_id AND store_id = :store_id');


            $sth->bindParam(':customer_id', $point['customer_id']);
            $sth->bindParam(':store_id', $point['store_id']);
            $sth->bindParam(':accumulated_points', $cancel_points);

            $sth->execute();

            $dbh->commit();
            $result = true;
        } catch (PDOException $e) {
            $dbh->rollback();
            throw new Exception($e->getMessage());
            exit();
        }

        return $result;
    }

    function getPurchaseHistory($limit = 5) {
        $result = null;
        Loader::Load('Database');
        $dbh = Database::getInstance()->getConnection();

        $sth = $dbh->prepare('SELECT pt.*, s.store_name, b.branch_name FROM point_transactions pt LEFT JOIN stores s ON pt.store_id = s.store_id 
								LEFT JOIN branches b ON s.branch_id = b.branch_id WHERE transaction_type = 1
								ORDER BY transacted_date DESC LIMIT 0, ' . $limit);
        $sth->execute();
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);

        $sth = null;
        $dbh = null;
        return $result;
    }

    function getPointsTransaction($limit = 5) {
        $result = null;
        Loader::Load('Database');
        $dbh = Database::getInstance()->getConnection();

        $sth = $dbh->prepare('SELECT pt.*, s.store_name, b.branch_name FROM point_transactions pt LEFT JOIN stores s ON pt.store_id = s.store_id 
								LEFT JOIN branches b ON s.branch_id = b.branch_id ORDER BY transacted_date DESC LIMIT 0, ' . $limit);

        $sth->execute();
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);

        $sth = null;
        $dbh = null;
        return $result;
    }

    function expiredPoints($customer_id) {
        $result = false;
        Loader::Load('Database');
        $dbh = Database::getInstance()->getConnection();

        // Fix Expire negative point issue. If below ZERO, no action.
        if ($this->getPointDetailsByCustomer($customer_id) > 0) {

            try {

                $header_id = 0;

                $dbh->beginTransaction();

                $sth = $dbh->prepare('INSERT INTO expiredpoint_headers (customer_id, transaction_on ) VALUES (:customer_id, NOW())');

                $sth->bindParam(':customer_id', $customer_id);
                $sth->execute();
                $header_id = $dbh->lastInsertId('id');

                if ($header_id > 0) {

                    $pd = $this->getPointDetailsByCustomer($customer_id);

                    foreach ($pd as $item) {
                        $sth = $dbh->prepare('INSERT INTO expiredpoint_balance (header_id, store_id, accumulated_points , last_transaction, started_date) VALUES 
											(:header_id, :store_id, :accumulated_points , :last_transaction, :started_date)');

                        $sth->bindParam(':header_id', $header_id);
                        $sth->bindParam(':store_id', $item['store_id']);
                        $sth->bindParam(':accumulated_points', $item['accumulated_points']);
                        $sth->bindParam(':last_transaction', $item['last_transaction']);
                        $sth->bindParam(':started_date', $item['started_date']);
                        $sth->execute();
                    }

                    $sth = $dbh->prepare('DELETE FROM point_balance WHERE customer_id = :customer_id');
                    $sth->bindParam(':customer_id', $customer_id);
                    $sth->execute();
                }

                $dbh->commit();
                $result = true;
            } catch (PDOException $e) {
                $dbh->rollback();
                throw new Exception($e->getMessage());
                exit();
            }
        }

        return $result;
    }

    function getExpiredPointsByCustomer($customer_id, $fromdate, $todate) {

        $result = null;
        Loader::Load('Database');
        $dbh = Database::getInstance()->getConnection();

        $sth = $dbh->prepare('SELECT h.*, c.nric, c.name FROM expiredpoint_headers h LEFT JOIN customer_profiles c ON h.customer_id = c.customer_id 
								WHERE h.customer_id = :customer_id 
								AND date(transaction_on ) BETWEEN :fromdate AND :todate');
        $from = $fromdate->format('Y-m-d');
        $to = $todate->format('Y-m-d');
        $sth->bindParam(':customer_id', $customer_id);
        $sth->bindParam(':fromdate', $from);
        $sth->bindParam(':todate', $to);
        $sth->execute();
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);

        $returnvalue = array();

        foreach ($result as $header) {
            $sth = $dbh->prepare('SELECT pb.*, s.store_name, b.branch_name FROM expiredpoint_balance pb LEFT JOIN stores s ON pb.store_id = s.store_id 
									LEFT JOIN branches b ON s.branch_id = b.branch_id WHERE pb.header_id = :id');
            $sth->bindParam(':id', $header['id']);
            $sth->execute();
            $balance = $sth->fetchAll(PDO::FETCH_ASSOC);

            $temp = array();
            $temp['header'] = $header;
            $temp['balance'] = $balance;

            array_push($returnvalue, $temp);
        }


        $sth = null;
        $dbh = null;
        return $returnvalue;
    }

}

?>