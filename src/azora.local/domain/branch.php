<?php
class Branch
	{
	function getAllBranches() {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT b.*, cu.username as created_username, mu.username as updated_username FROM branches b 
								LEFT JOIN users cu ON b.created_by = cu.user_id
								LEFT JOIN users mu ON b.updated_by = mu.user_id');			
		
		$sth->execute();
		$result = $sth->fetchAll(PDO::FETCH_ASSOC);
		$sth = null;
		$dbh = null;
		return $result;
	}

	function getBranch($branch_id) {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT b.*, cu.username as created_username, mu.username as updated_username FROM branches b 
								LEFT JOIN users cu ON b.created_by = cu.user_id
								LEFT JOIN users mu ON b.updated_by = mu.user_id
								WHERE b.branch_id = :branch_id');		

		$sth->bindParam(':branch_id', $branch_id);
		$sth->execute();
		$result = $sth->fetch(PDO::FETCH_ASSOC);
		$sth = null;
		$dbh = null;
		return $result;
	}

	function saveBranch($branch) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('INSERT INTO branches (branch_name, branch_description, created_dt, created_by, updated_dt, updated_by) VALUES (:branch_name, :branch_description, NOW(), :created_by, NOW(), :updated_by)');			
		
		$sth->bindParam(':branch_name', $branch['branch_name']);
		$sth->bindParam(':branch_description', $branch['branch_description']);	
		$sth->bindParam(':created_by', $branch['created_by']);	
		$sth->bindParam(':updated_by', $branch['updated_by']);
			
		$sth->execute();
		$result = ($sth->rowCount() > 0);
		$sth = null;
		$dbh = null;
		return $result;
	}

	function updateBranch($branch) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('UPDATE branches SET branch_name = :branch_name, branch_description = :branch_description, updated_dt = NOW(), updated_by = :updated_by WHERE branch_id = :branch_id');			
		
		$sth->bindParam(':branch_name', $branch['branch_name']);
		$sth->bindParam(':branch_description', $branch['branch_description']);		
		$sth->bindParam(':updated_by', $branch['updated_by']);
		$sth->bindParam(':branch_id', $branch['branch_id']);
			
		$sth->execute();
		$result = ($sth->rowCount() > 0);
		$sth = null;
		$dbh = null;
		return $result;
	}

	function deleteBranch($branches) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
		
		$branch_ids = implode(',', $branches);
			
		$sth = $dbh->prepare('DELETE FROM branches WHERE branch_id IN (' . $branch_ids . ')');			
		$sth->execute();
		
		$result = ($sth->rowCount() > 0);
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function isExistBranch($branch_name, $branch_id) {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT 1 FROM branches b 								
								WHERE b.branch_name = :branch_name AND b.branch_id <> :branch_id');		

		$sth->bindParam(':branch_name', $branch_name);
		$sth->bindParam(':branch_id', $branch_id);
		$sth->execute();
		$result = ($sth->rowCount() > 0);
		$sth = null;
		$dbh = null;
		return $result;
	}
}
?>