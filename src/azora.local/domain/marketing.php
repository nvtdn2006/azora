<?php
class Marketing
{
	function getAllMarketing() {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT m.*, cu.username as created_username, mu.username as updated_username FROM marketing_mail m 								
								LEFT JOIN users cu ON m.created_by = cu.user_id
								LEFT JOIN users mu ON m.updated_by = mu.user_id ORDER BY updated_dt DESC');			
		
		$sth->execute();
		$result = $sth->fetchAll(PDO::FETCH_ASSOC);
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function getMail($mail_id) {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT m.*, cu.username as created_username, mu.username as updated_username FROM marketing_mail m 									
								LEFT JOIN users cu ON m.created_by = cu.user_id
								LEFT JOIN users mu ON m.updated_by = mu.user_id
								WHERE m.id = :id');		

		$sth->bindParam(':id', $mail_id);
		$sth->execute();
		$result = $sth->fetch(PDO::FETCH_ASSOC);
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function saveMail($mail) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('INSERT INTO marketing_mail (mail_subject, mail_body, created_dt, created_by, updated_dt, updated_by) VALUES 
												(:mail_subject, :mail_body, NOW(), :created_by, NOW(), :updated_by)');			
		
		$sth->bindParam(':mail_subject', $mail['mail_subject']);
		$sth->bindParam(':mail_body', $mail['mail_body']);		
		$sth->bindParam(':created_by', $mail['created_by']);	
		$sth->bindParam(':updated_by', $mail['updated_by']);
			
		$sth->execute();
		$result = ($sth->rowCount() > 0);
		$sth = null;
		$dbh = null;
		return $result;
	}

	function updateMail($mail) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('UPDATE marketing_mail SET mail_subject = :mail_subject, mail_body = :mail_body, updated_dt = NOW(), updated_by = :updated_by WHERE id = :id');			
		
		$sth->bindParam(':mail_subject', $mail['mail_subject']);
		$sth->bindParam(':mail_body', $mail['mail_body']);		
		$sth->bindParam(':updated_by', $mail['updated_by']);
		$sth->bindParam(':id', $mail['id']);
			
		$sth->execute();
		$result = ($sth->rowCount() > 0);
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function deleteMail($mails) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
		
		$mail_ids = implode(',', $mails);	
			
		$sth = $dbh->prepare('DELETE FROM marketing_mail WHERE id IN (' . $mail_ids . ')');			
		$sth->execute();
		
		$result = ($sth->rowCount() > 0);
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function updateLastSent($mail_id) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('UPDATE marketing_mail SET last_sent_on = NOW() WHERE id = :id');			
		
		$sth->bindParam(':id', $mail_id);
			
		$sth->execute();
		$result = ($sth->rowCount() > 0);
		$sth = null;
		$dbh = null;
		return $result;
	}
}
?>