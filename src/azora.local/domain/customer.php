<?php
class Customer
{
	function isExistNRIC($nric, $customer_id = 0) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT 1 FROM customer_profiles cp 								
								WHERE cp.nric = :nric AND cp.customer_id <> :customer_id');		

		$sth->bindParam(':nric', $nric);	
		$sth->bindParam(':customer_id', $customer_id);
		$sth->execute();
		$result = ($sth->rowCount() > 0);
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function isExistEmail($email, $customer_id = 0) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT 1 FROM customer_profiles cp 								
								WHERE cp.email = :email AND cp.customer_id <> :customer_id');		
		
		$sth->bindParam(':email', $email);
		$sth->bindParam(':customer_id', $customer_id);
		$sth->execute();
		$result = ($sth->rowCount() > 0);
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function isExistCustomer($nric, $email, $customer_id = 0) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT 1 FROM customer_profiles cp 								
								WHERE cp.nric = :nric AND cp.email = :email AND cp.customer_id <> :customer_id');		

		$sth->bindParam(':nric', $nric);
		$sth->bindParam(':email', $email);
		$sth->bindParam(':customer_id', $customer_id);
		$sth->execute();
		$result = ($sth->rowCount() > 0);
		$sth = null;
		$dbh = null;
		return $result;
	}

	function checkingActivation($activation_key, $id) {
		$result = 0;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT is_activated FROM customers  								
								WHERE customer_id = :id AND activation_key = :activation_key');		

		$sth->bindParam(':id', $id);
		$sth->bindParam(':activation_key', $activation_key);
		$sth->execute();		
		if ($sth->rowCount() > 0) {
			$user = $sth->fetch(PDO::FETCH_ASSOC);
			if ($user['is_activated']) {
				$result = 2;
			} else {
				$result = 1;
			}
		}
		$sth = null;
		$dbh = null;
		return $result;
	}

	function activateCustomer($activation_key, $id) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('UPDATE customers SET is_activated = 1 
								WHERE customer_id = :id AND activation_key = :activation_key');		

		$sth->bindParam(':id', $id);
		$sth->bindParam(':activation_key', $activation_key);
		$sth->execute();
		$result = ($sth->rowCount() > 0);
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function getCustomerProfile($customer_id) {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT * FROM customer_profiles
								WHERE customer_id = :id');		

		$sth->bindParam(':id', $customer_id);
		$sth->execute();
		$result = $sth->fetch(PDO::FETCH_ASSOC);
		
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function getCustomerProfileByEmail($email) {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT * FROM customer_profiles
								WHERE email = :email');		

		$sth->bindParam(':email', $email);
		$sth->execute();
		$result = $sth->fetch(PDO::FETCH_ASSOC);
		
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function getCustomerByNRIC($nric) {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT * FROM customers
								WHERE login_id = :nric');		

		$sth->bindParam(':nric', $nric);
		$sth->execute();
		$result = $sth->fetch(PDO::FETCH_ASSOC);
		
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function getCustomerProfileByValues($customer_kv = null) {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
		
		$where = '';
		$and = '';
		
		if ($customer_kv != null) {
			foreach ($customer_kv as $key => $value) {
				switch ($key) { /* #PFX20120002 11/02/2012 */
					case 'month':
						$month = $value;
						break;
					case 'year':
						$year = $value;
						break;						
					default:
						$where .= $and . ' ' . $key . ' LIKE \'%' . $value . '%\' ';
						$and = 'AND';
				}
			}		
			/* +Start PFX20120002 11/02/2012 */
                        $month = isset($month)?$month:0;
                        $year = isset($year)?$year:0;
			if ( $month != 0 && $year != 0 ) {
				$from = new DateTime();
				$from->setDate($year, $month, 1);		
				$to = new DateTime();
				$to->setDate($year, $month, 1);	
				$to->modify( 'next month' );
				$to->modify( 'yesterday' );
			
				$where .= $and . 'DATE(updated_dt) between '.'\''. $from->format('Y-m-d') .'\' and '.'\''. $to->format('Y-m-d') .'\'';
			}
			/* +End PFX20120002 */
		}
		
		$sth = $dbh->prepare('SELECT * FROM customer_profiles ' . (strlen($where) > 0 ? 'WHERE ' . $where : ''));		
		
		$sth->execute();
		$result = $sth->fetchAll(PDO::FETCH_ASSOC);
		
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function isValidUser($nric) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
		
		$sth = $dbh->prepare('SELECT 1 FROM customers  								
							WHERE login_id = :nric');		

		$sth->bindParam(':nric', $nric);		
		$sth->execute();		
		
		$result = ($sth->rowCount() > 0);		
			
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function isActivatedUser($nric) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
		
		$sth = $dbh->prepare('SELECT 1 FROM customers  								
							WHERE login_id = :nric AND is_activated = 1');		

		$sth->bindParam(':nric', $nric);		
		$sth->execute();		
		
		$result = ($sth->rowCount() > 0);		
			
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function isAuthorizedUser($nric, $password, &$customer) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
		
		$customer = $this->getCustomerByNRIC($nric);		
		
		if (isset($customer)) {
		
			$cryptographer = Factory::getCryptographer();
			$salted_password = $cryptographer->MD5($password . $customer['salt']);
			
			$sth = $dbh->prepare('SELECT 1 FROM customers  								
								WHERE login_id = :nric AND password = :password');		

			$sth->bindParam(':nric', $nric);
			$sth->bindParam(':password', $salted_password);
			$sth->execute();			
			
			$result = ($sth->rowCount() > 0);
		}
		
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function registerCustomer(&$customer) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		try {
		
			$customer_id = 0;			
			
			$cryptographer = Factory::getCryptographer();
			$salt = $cryptographer->GenerateSalt();
			
			$salted_password = $cryptographer->MD5($customer['password']. $salt);
			$activation_key = $cryptographer->MD5($customer['email']. $salt);
					
			$dbh->beginTransaction();
			
			$sth = $dbh->prepare('INSERT INTO customers (login_id, password, salt, activation_key) VALUES (:login_id, :password, :salt, :activation_key)');		

			$sth->bindParam(':login_id', $customer['nric']);
			$sth->bindParam(':password', $salted_password);
			$sth->bindParam(':salt', $salt);
			$sth->bindParam(':activation_key', $activation_key);
			$sth->execute();
			$customer_id = $dbh->lastInsertId('customer_id');
			
			if ($customer_id > 0) {				
				$sth = $dbh->prepare('INSERT INTO customer_profiles (customer_id, nric, name, date_of_birth, address, company, contact_mobile, contact_home, email, updated_dt) VALUES 
															(:customer_id, :nric, :name, :date_of_birth, :address, :company, :contact_mobile, :contact_home, :email, NOW())');		

				$sth->bindParam(':customer_id', $customer_id);			
				$sth->bindParam(':nric', $customer['nric']);		
				$sth->bindParam(':name', $customer['name']);		
				$sth->bindParam(':date_of_birth', $customer['dob']);		
				$sth->bindParam(':address', $customer['address']);
				$sth->bindParam(':company', $customer['company']);
				$sth->bindParam(':contact_mobile', $customer['mobile']);
				$sth->bindParam(':contact_home', $customer['home']);
				$sth->bindParam(':email', $customer['email']);
				$sth->execute();	
			}		
			
			$dbh->commit();			
			$customer['id'] = $customer_id;
			$customer['activation_key'] = $activation_key;			
			$result = true;
			
		} catch(PDOException $e) {
			$dbh->rollback();			
			throw new Exception($e->getMessage());
		}
		
		$sth = null;
		$dbh = null;
		return $result;
	}
		
	function resetPassword($email, &$resetInfo) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		try {
		
			$customer = $this->getCustomerProfileByEmail($email);
			
			$cryptographer = Factory::getCryptographer();
			$salt = $cryptographer->GenerateSalt();
			$newpassword = $cryptographer->GenerateAlphanumeric(8);
			$salted_password = $cryptographer->MD5($newpassword . $salt);			
					
			$dbh->beginTransaction();
			
			$sth = $dbh->prepare('UPDATE customers SET password = :password, salt = :salt WHERE customer_id = :customer_id');		
			
			$sth->bindParam(':password', $salted_password);
			$sth->bindParam(':salt', $salt);
			$sth->bindParam(':customer_id', $customer['customer_id']);
			$sth->execute();			
			
			$dbh->commit();	
			
			$customer['newpassword'] = $newpassword;	

			$resetInfo = $customer;
			$result = true;
			
		} catch(PDOException $e) {
			$dbh->rollback();			
			throw new Exception($e->getMessage());
		}
		
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function updatePassword($nric, $password) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		try {
			
			$cryptographer = Factory::getCryptographer();
			$salt = $cryptographer->GenerateSalt();			
			$salted_password = $cryptographer->MD5($password . $salt);			
					
			$dbh->beginTransaction();
			
			$sth = $dbh->prepare('UPDATE customers SET password = :password, salt = :salt WHERE login_id = :login_id');		
			
			$sth->bindParam(':password', $salted_password);			
			$sth->bindParam(':salt', $salt);			
			$sth->bindParam(':login_id', $nric);
			$sth->execute();			
			
			$dbh->commit();	
			
			$result = true;
			
		} catch(PDOException $e) {
			$dbh->rollback();			
			throw new Exception($e->getMessage());
		}
		
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function updateProfile($cutomer) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		try {
		
			$dbh->beginTransaction();
			
			$sth = $dbh->prepare('UPDATE customer_profiles SET name = :name, date_of_birth = :date_of_birth, address = :address, company = :company, 
									contact_mobile = :contact_mobile, contact_home = :contact_home, email = :email, updated_dt = NOW() WHERE customer_id = :customer_id AND nric = :nric');		
			
			$sth->bindParam(':name', $cutomer['name']);
			$sth->bindParam(':date_of_birth', $cutomer['date_of_birth']);
			$sth->bindParam(':address', $cutomer['address']);
			$sth->bindParam(':company', $cutomer['company']);
			$sth->bindParam(':contact_mobile', $cutomer['contact_mobile']);
			$sth->bindParam(':contact_home', $cutomer['contact_home']);
			$sth->bindParam(':email', $cutomer['email']);
			$sth->bindParam(':customer_id', $cutomer['customer_id']);
			$sth->bindParam(':nric', $cutomer['nric']);
			$sth->execute();			
			
			$dbh->commit();	
			
			$result = true;
			
		} catch(PDOException $e) {
			$dbh->rollback();			
			throw new Exception($e->getMessage());
		}
		
		$sth = null;
		$dbh = null;
		return $result;
	}
}
?>