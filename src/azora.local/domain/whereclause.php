<?php

/*
 * FX.1305.001 Create WHERECLAUSE 
 */

/*
 * Wrap single quote 
 */

function _sq($string) {
    return "'" . $string . "'";
}

function _sqs($strings = array()) {
    $output = "";
    foreach ($strings as $string) {
        $output .= ($output == "") ? "" : " ,";
        $output .= "'" . $string . "'";
    }
    return $output;
}

class wcSign {

    const like = 'LIKE';
    const in = 'IN';
    const between = 'BETWEEN';
    const equal = '=';

}

class wcOption {

    const incl = 'I';
    const excl = 'E';

}

class wcComponent {

    const sign = 'sign';
    const option = 'option';
    const from = 'from';
    const to = 'to';

}

class WhereClause {

    private $conditions = array();
    private $conversions = array();
    private $count = 0;

    public function add($field, $option, $sign, $from, $to = "") {
        if (!isset($this->conditions)) {
            $this->conditions = array();
        }
        if (!isset($this->conditions[$field])) {
            $this->conditions[$field] = array();
        }
        if (!isset($this->conditions[$field][$option])) {
            $this->conditions[$field][$option] = array();
        }

        $this->conditions[$field][$option][] = array(
            wcComponent::sign => $sign,
            wcComponent::from => $from,
            wcComponent::to   => $to
        );

        $this->count++;
    }

    public function count() {
        return $this->count;
    }

    public function setConv($from, $to) {
        $this->conversions[$from] = $to;
    }

    public function setConvs($conversions = array()) {
        $this->conversions = $conversions;
    }

    public function getConv($from) {
        return isset($this->conversions[$from]) ? $this->conversions[$from] : $from;
    }

    public function getWhereClause() {
        $query = "1";
        foreach ($this->conditions as $field => $options) {

            foreach ($options as $option => $conditions) {

                $sub_query = "";

                foreach ($conditions as $condition) {

                    $sub_query .= ($sub_query == "") ? "" : " OR ";

                    switch ($condition[wcComponent::sign]) {
                        case wcSign::equal:
                        case wcSign::like:
                            $sub_query .= $this->getConv($field) . " "
                                    . $condition[wcComponent::sign] . " "
                                    . $condition[wcComponent::from];
                            break;
                        case wcSign::in:
                            $sub_query .= $this->getConv($field) . " "
                                    . $condition[wcComponent::sign] . " ("
                                    . $condition[wcComponent::from] . ")";
                            break;
                        case wcSign::between:
                            $sub_query .= $this->getConv($field) . " "
                                    . $condition[wcComponent::sign] . " "
                                    . $condition[wcComponent::from] . " AND "
                                    . $condition[wcComponent::to];
                            break;
                        default:
                            break;
                    }
                }

                if ($sub_query != "") {
                    $query .= " AND ";
                    $query .= (($option == wcOption::incl) ? " (" : " !(") . $sub_query . ")";
                }
            }
        }
        return $query;
    }

}

/*
  class UnitTest_WhereClause {

  static function run() {
  $wc = new WhereClause();
  $wc->add('Test', wcOption::incl, wcSign::equal, _sq('INCL0'));
  $wc->add('Test', wcOption::incl, wcSign::equal, _sq('INCL1'));
  $wc->add('Test', wcOption::incl, wcSign::like, _sq('LIKE%'));
  $wc->add('Test', wcOption::excl, wcSign::equal, _sq('ECL0'));
  $wc->add('Test', wcOption::excl, wcSign::in, _sqs(array(1, 2, 3, 4, 5)));
  $wc->add('Date', wcOption::incl, wcSign::between, _sq('20130101'), _sq('20130105'));
  $wc->setConvs(array('Date' => 'Date2', 'Test' => 'Test2'));
  echo $wc->getWhereClause();
  }

  }

  UnitTest_WhereClause::run();

 */
?>
