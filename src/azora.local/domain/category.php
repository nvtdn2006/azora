<?php
class Category
{
	function getAllCategories() {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT c.*, cu.username as created_username, mu.username as updated_username FROM categories c 
								LEFT JOIN users cu ON c.created_by = cu.user_id
								LEFT JOIN users mu ON c.updated_by = mu.user_id ORDER BY c.created_dt');			
		
		$sth->execute();
		$result = $sth->fetchAll(PDO::FETCH_ASSOC);
		$sth = null;
		$dbh = null;
		return $result;
	}

	function getCategory($category_id) {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT c.*, cu.username as created_username, mu.username as updated_username FROM categories c 
								LEFT JOIN users cu ON c.created_by = cu.user_id
								LEFT JOIN users mu ON c.updated_by = mu.user_id
								WHERE c.category_id = :category_id');		

		$sth->bindParam(':category_id', $category_id);
		$sth->execute();
		$result = $sth->fetch(PDO::FETCH_ASSOC);
		$sth = null;
		$dbh = null;
		return $result;
	}

	function saveCategory($category) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('INSERT INTO categories (category_name, category_description, created_dt, created_by, updated_dt, updated_by) VALUES (:category_name, :category_description, NOW(), :created_by, NOW(), :updated_by)');			
		
		$sth->bindParam(':category_name', $category['category_name']);
		$sth->bindParam(':category_description', $category['category_description']);	
		$sth->bindParam(':created_by', $category['created_by']);	
		$sth->bindParam(':updated_by', $category['updated_by']);
			
		$sth->execute();
		$result = ($sth->rowCount() > 0);
		$sth = null;
		$dbh = null;
		return $result;
	}

	function updateCategory($category) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('UPDATE categories SET category_name = :category_name, category_description = :category_description, updated_dt = NOW(), updated_by = :updated_by WHERE category_id = :category_id');			
		
		$sth->bindParam(':category_name', $category['category_name']);
		$sth->bindParam(':category_description', $category['category_description']);		
		$sth->bindParam(':updated_by', $category['updated_by']);
		$sth->bindParam(':category_id', $category['category_id']);
			
		$sth->execute();
		$result = ($sth->rowCount() > 0);
		$sth = null;
		$dbh = null;
		return $result;
	}

	function deleteCategory($categories) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
		
		$category_ids = implode(',', $categories);	
		
		$sth = $dbh->prepare('DELETE FROM categories WHERE category_id IN (' . $category_ids . ')');		
		$sth->execute();
		
		$result = ($sth->rowCount() > 0);
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function isExistCategory($category_name, $category_id) {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT 1 FROM categories c 								
								WHERE c.category_name = :category_name AND c.category_id <> :category_id');		

		$sth->bindParam(':category_name', $category_name);
		$sth->bindParam(':category_id', $category_id);
		$sth->execute();
		$result = ($sth->rowCount() > 0);
		$sth = null;
		$dbh = null;
		return $result;
	}
}
?>