<?php
class Company
{
	function getCompany() {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();			
		$sth = $dbh->prepare('SELECT c.*, cu.username as created_username, mu.username as updated_username FROM company_info c 
								LEFT JOIN users cu ON c.created_by = cu.user_id
								LEFT JOIN users mu ON c.updated_by = mu.user_id
								LIMIT 1;
								');			
		
		$sth->execute();
		$result = $sth->fetchAll(PDO::FETCH_ASSOC);
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function saveCompany($company) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('INSERT INTO company_info (company_name, company_description, company_address, company_phone, company_fax, company_email, company_reg_email, company_contactus_email, company_customersupport_email, created_dt, created_by, updated_dt, updated_by) VALUES 
							(:company_name, :company_description, :company_address, :company_phone, :company_fax, :company_email, :company_reg_email, :company_contactus_email, :company_customersupport_email, NOW(), :created_by, NOW(), :updated_by)');			
		
		$sth->bindParam(':company_name', $company['company_name']);
		$sth->bindParam(':company_description', $company['company_description']);
		$sth->bindParam(':company_address', $company['company_address']);
		$sth->bindParam(':company_phone', $company['company_phone']);
		$sth->bindParam(':company_fax', $company['company_fax']);
		$sth->bindParam(':company_email', $company['company_email']);
		$sth->bindParam(':company_reg_email', $company['company_reg_email']);
		$sth->bindParam(':company_contactus_email', $company['company_contactus_email']);
		$sth->bindParam(':company_customersupport_email', $company['company_customersupport_email']);
		$sth->bindParam(':created_by', $company['created_by']);	
		$sth->bindParam(':updated_by', $company['updated_by']);
			
		$sth->execute();
		$result = ($sth->rowCount() > 0);
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function updateCompany($company) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('UPDATE company_info SET company_name = :company_name, company_description = :company_description, company_address = :company_address, 
						company_phone = :company_phone, company_fax = :company_fax,
						company_email = :company_email, company_reg_email = :company_reg_email,
						company_contactus_email = :company_contactus_email,
						company_customersupport_email = :company_customersupport_email,
						updated_dt = NOW(), updated_by = :updated_by WHERE id = :id');			
		
		$sth->bindParam(':company_name', $company['company_name']);
		$sth->bindParam(':company_description', $company['company_description']);
		$sth->bindParam(':company_address', $company['company_address']);
		$sth->bindParam(':company_phone', $company['company_phone']);
		$sth->bindParam(':company_fax', $company['company_fax']);
		$sth->bindParam(':company_email', $company['company_email']);
		$sth->bindParam(':company_reg_email', $company['company_reg_email']);
		$sth->bindParam(':company_contactus_email', $company['company_contactus_email']);		
		$sth->bindParam(':company_customersupport_email', $company['company_customersupport_email']);
		$sth->bindParam(':updated_by', $company['updated_by']);
		$sth->bindParam(':id', $company['id']);
			
		$sth->execute();
		$result = ($sth->rowCount() > 0);
		$sth = null;
		$dbh = null;
		return $result;
	}
}
?>