<?php
class Store
{
	function getAllStores() {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT s.*, b.branch_name, concat_ws(\' - \', s.store_name,b.branch_name) as store_branch_name, cu.username as created_username, mu.username as updated_username FROM stores s 
								LEFT JOIN branches b ON s.branch_id = b.branch_id
								LEFT JOIN users cu ON s.created_by = cu.user_id
								LEFT JOIN users mu ON s.updated_by = mu.user_id ORDER BY b.branch_name, s.store_name');			
		
		$sth->execute();
		$result = $sth->fetchAll(PDO::FETCH_ASSOC);
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function getCollectionStores() {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT s.*, b.branch_name, concat_ws(\' - \', s.store_name,b.branch_name) as store_branch_name, cu.username as created_username, mu.username as updated_username FROM stores s 
								LEFT JOIN branches b ON s.branch_id = b.branch_id
								LEFT JOIN users cu ON s.created_by = cu.user_id
								LEFT JOIN users mu ON s.updated_by = mu.user_id 
								WHERE s.store_collection = 1
								ORDER BY b.branch_name, s.store_name');			
		
		$sth->execute();
		$result = $sth->fetchAll(PDO::FETCH_ASSOC);
		$sth = null;
		$dbh = null;
		return $result;
	}

	function getStore($store_id) {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT s.*, b.branch_name, concat_ws(\' - \', s.store_name,b.branch_name) as store_branch_name, cu.username as created_username, mu.username as updated_username FROM stores s 
								LEFT JOIN branches b ON s.branch_id = b.branch_id
								LEFT JOIN users cu ON s.created_by = cu.user_id
								LEFT JOIN users mu ON s.updated_by = mu.user_id
								WHERE s.store_id = :store_id');		

		$sth->bindParam(':store_id', $store_id);
		$sth->execute();
		$result = $sth->fetch(PDO::FETCH_ASSOC);
		$sth = null;
		$dbh = null;
		return $result;
	}

	function saveStore($store) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('INSERT INTO stores (branch_id, store_name, store_description, store_address, store_website, store_collection, created_dt, created_by, updated_dt, updated_by) VALUES 
												(:branch_id,  :store_name, :store_description, :store_address, :store_website, :store_collection, NOW(), :created_by, NOW(), :updated_by)');			
		
		$sth->bindParam(':branch_id', $store['branch_id']);
		$sth->bindParam(':store_name', $store['store_name']);
		$sth->bindParam(':store_description', $store['store_description']);	
		$sth->bindParam(':store_address', $store['store_address']);	
		$sth->bindParam(':store_website', $store['store_website']);	
		$sth->bindParam(':store_collection', $store['store_collection']);	
		$sth->bindParam(':created_by', $store['created_by']);	
		$sth->bindParam(':updated_by', $store['updated_by']);
			
		$sth->execute();
		$result = ($sth->rowCount() > 0);
		$sth = null;
		$dbh = null;
		return $result;
	}

	function updateStore($store) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('UPDATE stores SET branch_id = :branch_id, store_name = :store_name, store_description = :store_description, 
								store_address = :store_address, store_website = :store_website, store_collection = :store_collection, updated_dt = NOW(), updated_by = :updated_by WHERE store_id = :store_id');			
		
		$sth->bindParam(':branch_id', $store['branch_id']);
		$sth->bindParam(':store_name', $store['store_name']);
		$sth->bindParam(':store_description', $store['store_description']);		
		$sth->bindParam(':store_address', $store['store_address']);
		$sth->bindParam(':store_website', $store['store_website']);	
		$sth->bindParam(':store_collection', $store['store_collection']);
		$sth->bindParam(':updated_by', $store['updated_by']);
		$sth->bindParam(':store_id', $store['store_id']);
			
		$sth->execute();
		$result = ($sth->rowCount() > 0);
		$sth = null;
		$dbh = null;
		return $result;
	}

	function deleteStore($stores) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
		
		$store_ids = implode(',', $stores);	
			
		$sth = $dbh->prepare('DELETE FROM stores WHERE store_id IN (' . $store_ids . ')');			
		$sth->execute();
		
		$result = ($sth->rowCount() > 0);
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function isExistStore($store_name, $branch_id, $store_id) {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT 1 FROM stores s 								
								WHERE s.store_name = :store_name AND s.branch_id = :branch_id AND s.store_id <> :store_id');		

		$sth->bindParam(':store_name', $store_name);
		$sth->bindParam(':branch_id', $branch_id);
		$sth->bindParam(':store_id', $store_id);
		$sth->execute();
		$result = ($sth->rowCount() > 0);
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function getAssignedStores($user_id, $isSuperUser) {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
		
		if ($isSuperUser) {
			$sth = $dbh->prepare('SELECT s.*, b.branch_name, concat_ws(\' - \', s.store_name,b.branch_name) as store_branch_name FROM stores s 
								LEFT JOIN branches b ON s.branch_id = b.branch_id');			
		} else {
			$sth = $dbh->prepare('SELECT s.*, b.branch_name, concat_ws(\' - \', s.store_name,b.branch_name) as store_branch_name FROM user_stores us LEFT JOIN stores s ON us.store_id = s.store_id
								LEFT JOIN branches b ON s.branch_id = b.branch_id
								WHERE us.user_id = :user_id ORDER BY s.store_name');
			$sth->bindParam(':user_id', $user_id);
		}
		$sth->execute();
		$result = $sth->fetchAll(PDO::FETCH_ASSOC);
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function getUserStoresNotSelected($user_id) {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT s.*, b.branch_name, concat_ws(\' - \', s.store_name,b.branch_name) as store_branch_name FROM stores s 
								LEFT JOIN branches b ON s.branch_id = b.branch_id
								WHERE s.store_id NOT IN (SELECT store_id FROM user_stores us WHERE us.user_id = :user_id )
								ORDER BY s.store_name');		
								
		$sth->bindParam(':user_id', $user_id);		
		
		$sth->execute();
		$result = $sth->fetchAll(PDO::FETCH_ASSOC);
		$sth = null;
		$dbh = null;	
		return $result;
	}
	
	function getUserStores($user_id) {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT us.*, s.store_name, b.branch_name, concat_ws(\' - \', s.store_name,b.branch_name) as store_branch_name FROM user_stores us LEFT JOIN stores s ON us.store_id = s.store_id
								LEFT JOIN branches b ON s.branch_id = b.branch_id
								WHERE us.user_id = :user_id ORDER BY s.store_name');			
		
		$sth->bindParam(':user_id', $user_id);
		$sth->execute();
		$result = $sth->fetchAll(PDO::FETCH_ASSOC);
		$sth = null;
		$dbh = null;	
		return $result;
	}
}
?>