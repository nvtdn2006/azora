<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Point details'; ?>

<?php	

	if (!Authentication::isAuthenticated()) {
		header( 'Location: index.php');
	}
	
	DomainManager::getInstance()->load('Point');
	$pointObj = new Point();
	
	$customer_id = Authentication::getUserId();	
	$pointdetatils = $pointObj->getPointDetailsByCustomer($customer_id);
?>

<div id="righttitle" >Point details</div>
<div id="contentcontainer">


<?php 

	DomainManager::getInstance()->load('Reward');
	$rewardObj = new Reward();
	
	$rewards = $rewardObj->getReward();
	$expiry_on = null;
	if (isset($rewards) && count($rewards) > 0) {
		$reward = $rewards[0];
		if ($reward['expiration_required'] == 1) {
			if (isset($pointdetatils) && count($pointdetatils) > 0) {
				$pd = $pointdetatils[0];
				
				$expiry_on = new DateTime($pd['started_date']);
				$expiry_on->modify('+'.$reward['expiration_month'].' month');
				
				echo '<div class="warning-info">The expiration date of following points will be on '.$expiry_on->format('j, M Y').'.</div><br/>';				
			}			
		}
	}

?>
<table class="tabular">
	<thead>
		<tr>			
			<th>Branch</th>
			<th>Store</th>
			<th>Accumulated Points</th>
			<th>Last transaction</th>			
		</tr>
	</thead>
	<tbody>
		<?php
			foreach($pointdetatils as $detail) {
		?>
		<tr>						
			<td><?php echo $detail['branch_name'] ?></td>
			<td><?php echo $detail['store_name'] ?></td>
			<td><?php echo $detail['accumulated_points'] ?></td>
			<td><?php echo $detail['last_transaction'] ?></td>			
		</tr>
		<?php } ?>
	</tbody>
</table>

</div>