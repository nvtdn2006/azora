<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?php $this->theTitle(); ?> &lsaquo; Azora - Redemption</title>
<link rel="stylesheet" type="text/css" media="all" href="<?php $this->theThemeURL(); ?>/css/style.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php $this->theThemeURL(); ?>/css/default.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php $this->theThemeURL(); ?>/css/registration.css" />
<?php	
	$this->setPlaceHolder('css');
	$this->setPlaceHolder('js');
?>
</head>
<body>
	<div id="container">
		<div id="userpanel">
			<?php $this->loadModule('mod_userpanel'); ?>
		</div>
		<div id="wrapper">
			<div id="header" class="bottomspacing">
				<div id="logo"><a href="<?php $this->theSiteURL(); ?>"></a></div>
				<div id="prms"><a href="<?php $this->theSiteURL(); ?>"></a></div>
			</div>			
			<?php $this->theContent(); ?>									
			<br style="clear:both;" />
			<div class="push"></div>
		</div>
		<div id="footer">
			<div id="container">
				<div id="section"><?php $this->loadModule('mod_footersection'); ?></div>
				<div id="powered"><a href="http://www.bluecube.com.sg" target="_blank" title="Visit to BlueCube.">Powered by BlueCube Pte Ltd.</a></div>
			</div>
		</div>
	</div>	
</html>