<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Our stores'; ?>

<?php	

	JSManager::getInstance()->add('jquery');	
	JSManager::getInstance()->add('fancybox');	
	CSSManager::getInstance()->add('/js/fancybox/jquery.fancybox-1.3.4.css');	
	
	DomainManager::getInstance()->load('Store');
	$storeObj = new Store();
	
	$stores = $storeObj->getAllStores();
	
?>

<div id="righttitle" >Our stores</div>
<div class="store-icon"></div>
<div id="contentcontainer">

	<table class="formview" width="100%" border="0" cellspacing="3px" cellpadding="3px">
	
		<?php
			$branch = '';
			
			foreach ($stores as $store) {
				if ($branch != $store['branch_name']) {					
					echo '<tr>			
							<td class="SectionBar" colspan="3">				
								'. $store['branch_name'].'
							</td>
						</tr>';			
				}
				echo '<tr>			
						<td><b>'. $store['store_name'].'</b></td>
						<td><a target="_blank" href="'. $store['store_website'].'">'. $store['store_website'].'</a></td>
						<td width="50px"><a class="fancybox" href="map.php?address='. $store['store_address'].'">Map</a></td>
					</tr><tr><td>Address</td><td colspan="2">'. $store['store_address'].'</td>';
						
				$branch = $store['branch_name'];
			}
		?>
		
	</table>

</div>

<script type="text/javascript">
	$(document).ready(function() { 		
		$('a.fancybox').fancybox({type: 'iframe', width : 500, height: 300});
	});	
</script>