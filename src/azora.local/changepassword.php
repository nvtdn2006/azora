<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Change password'; ?>

<?php	

	if (!Authentication::isAuthenticated()) {
		header( 'Location: index.php');
	}
	
	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('validation');		
	
	DomainManager::getInstance()->load('Customer');
	$customerObj = new Customer();
	
	$status = true;
	$info = array();
	
	if (isset($_POST['submit']) && $_POST['submit'] == 'Save'
                || (isset($_POST['hidden_submit']) && $_POST['hidden_submit'] == 'Save') // FX.1304.005  - if not find submit, check hidden submit
                ) {			
		$old_password = $_POST['old_password'];	
		$new_password = $_POST['new_password'];	
		$reenter_password = $_POST['reenter_password'];	
		
		if ($old_password != '' && $new_password != '' && $reenter_password != '') {
			if ($new_password == $reenter_password) {
				$nric = Authentication::getAttribute('login_id');
				
				if ($customerObj->isAuthorizedUser($nric, $old_password, $customer)) {
					if ($customerObj->updatePassword($nric, $new_password)) {										
						array_push($info, 'New password has been successfully saved.');
						
						//Event Log
						$evtObj = new EventObject();					
						$evtObj->event_id = EventTypes::RWS_ChangePassword;
						$evtObj->description = Authentication::getAttribute('login_id') . ' changed his/her password.';
						$evtObj->action_by = Authentication::getAttribute('login_id');					
						EventLog::Log($evtObj);
						
					} else {
						$status = false;
						array_push($info, 'Failed to update passwords.');
					}
				} else {
					$status = false;
					array_push($info, 'Invalid old password.');
				}
			} else {
				$status = false;
				array_push($info, 'New and Re-enter passwords do not match.');
			}	
		} else {
			$status = false;
			array_push($info, 'Please enter required fields.');
		}
	}
	
?>


<div id="righttitle">Change password</div>
<div id="contentcontainer">

	<?php
		$infoCss = $status ? 'success-info' : 'error-info';
		if (isset($info) && count($info) > 0) {
	?>
		<div class="<?php echo $infoCss; ?> form-info">
			<?php foreach ($info as $handle) {
					echo "<p>$handle</p>";
			} ?>
		</div>
	<?php
		}
	?>
	
	<form name="changepwdform" id="changepwdform" action="changepassword.php" method="post"> 
		<table class="formview" width="100%" border="0" cellspacing="3px" cellpadding="3px">
			<tr>
				<td class="LabelCell Required">Old password</td>
				<td><input type="password" name="old_password" id="old_password" maxlength="50" class="input Required" size="20" tabindex="10" /></td>
			</tr>
			<tr>
				<td class="LabelCell Required">New password</td>
				<td><input type="password" name="new_password" id="new_password" maxlength="50" class="input Required" size="20" tabindex="20" /></td>
			</tr>
			<tr>
				<td class="LabelCell Required">Re-enter password</td>
				<td><input type="password" name="reenter_password" id="reenter_password" maxlength="50" class="input Required" size="20" tabindex="30" /></td>
			</tr>
			<tr>			
				<td class="BottomToolBar" colspan="2">				
					<input type="submit" name="submit" id="submit" class="button-primary" value="Save" tabindex="30"/>
					<a href="index.php" class="button-secondary" tabindex="40">Cancel</a>					
				</td>			
			</tr>
		</table>
	</form>
</div>

<script type="text/javascript">
	$(document).ready(function() { 
		loadValidation('changepwdform');
	});
</script>