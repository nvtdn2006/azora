<?php require_once './includes/application.php'; $this->template = 'login'; $this->title = 'Login'; ?>

<?php	
	//Todo
?>

<div id="login-wrapper">
	
	<form name="loginform" id="loginform" action="authentication.php" method="post"> 
		<p> 
			<label>NRIC / FIN<br /> 
			<input type="text" name="user_login" id="user_login" class="input" value="" size="20" tabindex="1000" /></label> 
		</p> 
		<p> 
			<label>Password<br /> 
			<input type="password" name="user_pass" id="user_pass" class="input" value="" size="20" tabindex="1010" /></label> 
		</p> 		
		<p class="submit"> 
			<input type="submit" name="submit" id="submit" class="button-primary" value="Login" title="Login" tabindex="1020"/><div class="indicator"></div>
		</p> 
		<p id="nav"> 
			<a href="#" id="forgot" title="Password Lost and Found" tabindex="1030">Lost your password?</a> 
		</p> 
	</form>
	
	<form name="forgotform" id="forgotform" action="authentication.php" method="post"> 
		<p> 
			<label>Email<br /> 
			<input type="text" name="user_email" id="user_email" class="input" value="" size="20" tabindex="1000" /></label> 
		</p> 		
		<p class="submit"> 
			<input type="submit" name="submit" id="submit" class="button-primary" value="Submit" tabindex="1010"/><div class="indicator"></div> 			
		</p> 
		<p id="nav"> 
			<a href="#" id="cancel" title="Cancel">Cancel</a> 
		</p> 
	</form>
	
	<div id="login_info" class="login_error">		
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() { 
		$('#forgot').click(function(e) {
			e.preventDefault();
			$('#loginform').hide();
			$('#forgotform').show();
			$('#user_email').focus();
		});
		$('#cancel').click(function(e) {
			e.preventDefault();
			$('#loginform').show();
			$('#forgotform').hide();
			$('#user_login').focus();
		});
				
		$("#loginform").submit(function(event) {
			
			/* stop form from submitting normally */
			event.preventDefault(); 
			
			$( "#login_info" ).hide();
				
			/* get some values from elements on the page: */
			var $form = $( this ),
				username = $form.find( 'input[name="user_login"]' ).val(),
				password = $form.find( 'input[name="user_pass"]' ).val(),
				url = $form.attr( 'action' );

			indicator = $form.find('.indicator');
			indicator.show();
			
			submit = $form.find('#submit');
			submit.attr("disabled", "disabled");
				
			/* Send the data using post and put the results in a div */
			$.post( url, { username: username, password: password, form: 'login' },
				function(data) {
					//var content = $( data ).find( '#content' );					
					if (data.status) {
						window.location.href = data.url;
					} else {
						$( "#login_info" ).empty().append(data.message);
						$( "#login_info" ).show();					
					}
					indicator.hide();
					submit.removeAttr("disabled");
				}
			);
		});
		
		$("#forgotform").submit(function(event) {
			
			/* stop form from submitting normally */
			event.preventDefault(); 
			
			$( "#login_info" ).hide();
				
			/* get some values from elements on the page: */
			var $form = $( this ),
				email = $form.find( 'input[name="user_email"]' ).val(),				
				url = $form.attr( 'action' );

			indicator = $form.find('.indicator');
			indicator.show();
			
			submit = $form.find('#submit');
			submit.attr("disabled", "disabled");
				
			/* Send the data using post and put the results in a div */
			$.post( url, { email: email, form: 'forgot' },
				function(data) {
					//var content = $( data ).find( '#content' );					
					if (data.status) {
						//window.location.href = data.url;
						$( "#login_info" ).removeClass('login_error');
						$( "#login_info" ).addClass('login_success');
						$( "#login_info" ).empty().append(data.message);
						$( "#login_info" ).show();	
					} else {
						$( "#login_info" ).addClass('login_error');
						$( "#login_info" ).removeClass('login_success');
						$( "#login_info" ).empty().append(data.message);
						$( "#login_info" ).show();					
						submit.removeAttr("disabled");
					}
					indicator.hide();					
				}
			);
		});

	});	
</script>