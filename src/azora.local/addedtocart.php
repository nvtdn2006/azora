<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Added to cart' ?>

<?php	

	if (!Authentication::isAuthenticated()) {
		header( 'Location: index.php');
	}

?>


<div id="righttitle">Added to cart</div>
<div id="contentcontainer">

	<div class="cprocess">
		Product has been added to your cart.<br/><br/><br/>
		You can view your items in your cart from <b>Manage Your Account</b> menu.
		<br/><br/><br/><br/>
		<a class="button-primary" href="products.php">Continue</a>
		<a class="button-primary" href="checkout.php">Check out</a>
	</div>
	
</div>