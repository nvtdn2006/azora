<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Update your profile'; ?>

<?php	

	if (!Authentication::isAuthenticated()) {
		header( 'Location: index.php');
	}
	
	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('validation');	
	
	$config = Factory::getConfig();
	$cryptographer = Factory::getCryptographer();
	
	$error = array();
	
	DomainManager::getInstance()->load('Customer');
	$customerObj = new Customer();
	
	$customer_id = Authentication::getUserId();
	$nric = Authentication::getAttribute('login_id');
	$customer = $customerObj->getCustomerProfile($customer_id);
		
	if (isset($_POST['update']) && $_POST['update'] == 'Update') {
		
		$isValid = true;
		
		if ($isValid) {
			if ($customerObj->isExistEmail($_POST['email'], $customer_id)) {				
				$isValid = false;
				array_push($error, 'Your email is already exist in system, please re-enter.');	
			}
		}
		
		if ($isValid) {
			if ($customerObj->isExistCustomer($nric, $_POST['email'], $customer_id)) {				
				$isValid = false;
				array_push($error, 'User already exists in system, please check your NRIC/FIN or Email.');	
			}
		}		
		
		if ($isValid) {
		
			$dob_year = substr($_POST['date_of_birth'],6,4);
			$dob_month = substr($_POST['date_of_birth'],3,2);
			$dob_day = substr($_POST['date_of_birth'],0,2);
			$dob = date("Y-m-d", mktime(0,0,0,$dob_month,$dob_day,$dob_year));
			
			$customer = array('name' => $_POST['name'],
									'date_of_birth' => $dob,
									'address' => $_POST['address'],
									'company' => $_POST['company'],
									'contact_mobile' => $_POST['contact_mobile'],
									'contact_home' => $_POST['contact_home'],
									'email' => $_POST['email'],
									'customer_id' => $customer_id,
									'nric' => $nric);			
			
			if ($customerObj->updateProfile($customer)) {
			
				//Event Log
				$evtObj = new EventObject();					
				$evtObj->event_id = EventTypes::RWS_UpdateProfile;
				$evtObj->description = Authentication::getAttribute('login_id') . ' updated his/her profile.';
				$evtObj->action_by = Authentication::getAttribute('login_id');					
				EventLog::Log($evtObj);
						
				header( 'Location: profile.php?rst=1');
			}
		}
	
	}
	
?>

<div id="righttitle" >Update your profile</div>
<div id="contentcontainer">
<?php
	if (isset($error) && count($error) > 0) {
?>
	<div class="error-info form-info">
		<?php foreach ($error as $handle) {
				echo "<p>$handle</p>";
		} ?>
	</div>
<?php
	}
?>

<?php
	if (isset($_GET['rst']) && $_GET['rst'] == 1) {
?>
	<div class="success-info form-info">
		Profile has been updated.
	</div>
<?php
	}
?>

<form name="profileform" id="profileform" action="profile.php" method="post"> 
	<table class="formview" width="100%" border="0" cellspacing="3px" cellpadding="3px">
		<tr>
			<td class="LabelCell Disabled">NRIC / FIN</td>
			<td><input type="text" name="nric" id="nric" maxlength="9" class="input" value="<?php echo $customer['nric']; ?>" size="20" tabindex="10" disabled /></td>
		</tr>
		<tr>
			<td class="LabelCell Required">Name</td>
			<td><input type="text" name="name" id="name" maxlength="255" class="input Required" value="<?php echo $customer['name']; ?>" size="20" tabindex="20" /></td>
		</tr>
		<tr>
			<td class="LabelCell Required">Date of Birth <span class="hint">(DD/MM/YYYY)</span></td>
			<td><input type="text" name="date_of_birth" id="date_of_birth" maxlength="10" class="input Required ValidDate" value="<?php echo date("d/m/Y", strtotime($customer['date_of_birth'])); ?>" size="20" tabindex="30" /></td>
		</tr>
		<tr>
			<td class="LabelCell">Address</td>
			<td><input type="text" name="address" id="address" maxlength="500" class="input" value="<?php echo $customer['address']; ?>" size="20" tabindex="40" /></td>
		</tr>		
		<tr>
			<td class="LabelCell Required">Company <span class="hint">(N.A. if not applicable)</span></td>
			<td><input type="text" name="company" id="company" maxlength="500" class="input Required" value="<?php echo $customer['company']; ?>" size="20" tabindex="50" /></td>
		</tr>
		<tr>			
			<td class="SectionBar" colspan="2">				
				Contacts
			</td>
		</tr>
		<tr>
			<td class="LabelCell Required">Mobile</td>
			<td><input type="text" name="contact_mobile" id="contact_mobile" maxlength="50" class="input Required" value="<?php echo $customer['contact_mobile']; ?>" size="20" tabindex="60" /></td>
		</tr>
		<tr>
			<td class="LabelCell">Home</td>
			<td><input type="text" name="contact_home" id="contact_home" maxlength="50" class="input" value="<?php echo $customer['contact_home']; ?>" size="20" tabindex="70" /></td>
		</tr>	
		<tr>			
			<td class="SectionBar" colspan="2">				
				Email
			</td>
		</tr>
		<tr>
			<td class="LabelCell Required">Your Email</td>
			<td><input type="text" name="email" id="email" maxlength="50" class="input Required ValidEmail" value="<?php echo $customer['email']; ?>" size="20" tabindex="80" /></td>
		</tr>		
		<tr>			
			<td class="BottomToolBar" colspan="2">				
				<input type="submit" name="update" id="update" class="button-primary" value="Update" tabindex="90"/>
				<a href="index.php" class="button-secondary" tabindex="100">Cancel</a>				
			</td>			
		</tr>
	</table>	
</form>

</div>

<script type="text/javascript">
	$(document).ready(function() { 
		loadValidation('profileform');
		<?php if ($_SERVER['REQUEST_METHOD'] != "POST") { ?> 
			$("#name").focus();
		<?php } ?>
	});	
</script>