<?php
class PRMSConfig
{
	public $live_site = 'http://azora.local';
	
	public $cryptography_key = '8f20a5403da7cfdf606e56118295e116'; //md5('cryptography key for PRMS')
	
	public $DB_Driver = 'mysql';
	public $DB_DSN = 'mysql:host=localhost;dbname=dev_azora';
	public $DB_Host = 'localhost';
	public $DB_Name = 'dev_azora';
	public $DB_User = 'dev_azora';
	public $DB_Password = '';
	
	public $timezone = 'Asia/Singapore';

	public $front_theme = 'default';
	public $back_theme = 'default';

	public $noreply_mail = 'no-reply@azorarewards.com.sg';	
        
        // 05-04-2013 FX.1304.002 Add CC email option
        public $PRMS_default_cc_mail = 'rewards@azorarewards.com.sg'; // '';
        public $PRMS_redemption_cc_mail = 'azoragroup@singnet.com.sg'; // '';
	

        // 24-04-2013 FX.1304.002 Add CC email option
        public $smtp_host = "mail.azorarewards.com.sg";
        public $smtp_username = "azorareward@azorarewards.com.sg";
        public $smtp_password = "P@ssw0rd";
	
	public $collection_starttime = 9;
}
?>