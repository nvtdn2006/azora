<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Your current cart'; ?>

<?php	

	if (!Authentication::isAuthenticated()) {
		header( 'Location: index.php');
	}
	
	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('checkboxlist');
	JSManager::getInstance()->add('messagebox');
	JSManager::getInstance()->add('validation');	
	JSManager::getInstance()->add('numeric');
	
	require_once dirname(__FILE__) . '/includes/cart.php';			
	$mycart = getCart();
	$mypoints =	getPointsInCart();
	
	$error = array();
	
	if (isset($_POST['update']) && $_POST['update'] == 'Update') {			
		$quantities = array();
		
		if (isset($_POST['quantity'])) 
			$quantities = $_POST['quantity'];		
		
		if (isset($quantities) && count($quantities) > 0) {
			$idx = 0;			
			$tmpCart = array();
			$pts = 0;
			foreach ($mycart as $item) {
				$item['qty'] = $quantities[$idx];			
				$pts += $item['qty'] * $item['points'];
				array_push($tmpCart, $item);
				$idx++;
			}
			
			DomainManager::getInstance()->load('Point');
			$pointObj = new Point();	
			$points = $pointObj->getPointByCustomer(Authentication::getUserId());
			
			if ($points >= $pts) {
				$mypoints = $pts;
				$mycart = $tmpCart;
				setCart($mycart);
			} else {
				array_push($error, 'Your have no sufficient points for your changes. Please try again.');
			}
			
		}
	}
	
	if (isset($_POST['delete']) && $_POST['delete'] == 'Remove') {			
		$items = array();
		
		if (isset($_POST['items'])) 
			$items = $_POST['items'];		
		
		if (isset($items) && count($items) > 0) {			
			$tmpCart = array();	
			$pts = 0;
			foreach ($mycart as $item) {
				$add = true;
				$tpt = $item['qty'] * $item['points'];
				foreach($items as $itm) {
					if ($itm == $item['id']) {
						$add = false;
						break;
					}
				}				
				if ($add)  {
					array_push($tmpCart, $item);
					$pts += $tpt;
				}
			}
			$mycart = $tmpCart;
			setCart($mycart);
			$mypoints = $pts;
		}
	}
	
?>

<div id="righttitle">Your current cart</div>
<div id="contentcontainer">

	<?php
	if (isset($error) && count($error) > 0) {
	?>
		<div class="error-info">
			<?php foreach ($error as $handle) {
					echo "<p>$handle</p>";
			} ?>
		</div>
	<?php
	}
	?>

	<?php if ($mycart != null) { ?>
	
		<form name="cartform" id="cartform" action="viewcart.php" method="post"> 
			<table class="tabular">				
				<tfoot>
					<tr>
						<th colspan="4">						
							<input type="submit" name="delete" id="delete" class="button-secondary" value="Remove" onclick="javascript:return confirmBox('Are you sure you want to delete selected item?');"/>
							<input type="submit" name="update" id="update" class="button-primary" value="Update" />
							<a class="button-primary" href="checkout.php">Check out</a>
						</th>		
						<th colspan="2">						
							<b><?php echo number_format($mypoints); ?> pts</b>
						</th>
					</tr>
				</tfoot>
				<tbody>		
					<?php
						$config = Factory::getConfig();
						
						foreach($mycart as $item) {
							DomainManager::getInstance()->load('Product');
							$productObj = new Product();	
							$product = $productObj->getProduct($item['id']);
							
							$images = $product['product_images'];
							$image  = null;

							foreach($images as $img) {
								if ($img['image_type'] == 'S') {
									$image = $img;
								}													
							}
							
							if (!$image) {
								foreach($images as $img) {
									if ($img['image_type'] == 'T') {
										$image = $img;
									}													
								}
							}
							
					?>
					<tr>
						<td width="20px"><input type="checkbox" name="items[]" value="<?php echo $item['id']; ?>"/></td>
						<td width="60px">
							<img src="<?php echo $config['PRMSConfig']->live_site . '/domain/images/products/' . $image['sys_file_name']; ?>" width="50" height="50" alt="<?php echo $product['product']['product_name']; ?>" title="<?php echo $product['product']['product_name']; ?>"/></td>
						<td><b><?php echo $product['product']['product_name']; ?></b></td>
						<td><?php echo $product['product']['product_description']; ?></td>
						<td width="80px">&nbsp;<?php echo number_format($product['product']['product_points']); ?> pts</td>
						<td width="50px"><input type="text" name="quantity[]" class="input Required numeric" value="<?php echo $item['qty'] ?>" size="20" tabindex="10" style="width:30px" /></td>						
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</form>
	
	<?php }  else { ?>
		<div style="margin: 10px; padding: 25px 5px 25px 5px;text-align: center;">
			<b>Currently, your cart is empty.</b>
		</div>	
	<?php } ?>
	
</div>

<?php
	
?>

<script type="text/javascript">
	$(document).ready(function() { 
		loadValidation('cartform');		
		$(".numeric").numeric(false);
	});	
</script>