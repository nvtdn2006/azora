<?php require_once './includes/application.php'; $this->template = 'registration'; $this->title = 'Registration Complete'; ?>

<?php		
	$config = Factory::getConfig();
?>

<div id="registration" >Registration Complete</div>
<div class="register-icon"></div>


<div id="complete">
	<b>Thank you for registering at redemption website.</b><br/><br/><br/><br/>
	You will receive an email in your inbox. You MUST follow the link in that email to activate your account.
	<br/><br/><br/><br/><br/><br/>
	<a href="<?php echo $config['PRMSConfig']->live_site; ?>">Back to home</a>
</div>