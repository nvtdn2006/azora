<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Contact us'; ?>

<?php	

	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('validation');
	
	DomainManager::getInstance()->load('Company');
	$companyObj = new Company();
	
	$company = $companyObj->getCompany();
	$company = $company[0];
	
	$status = true;
	$sent = false;
	$info = array();
	
	$config = Factory::getConfig();
	
	if (isset($_POST['submit']) && $_POST['submit'] == 'Send') {			
		$name = $_POST['name'];	
		$email = $_POST['email'];	
		$phone = $_POST['phone'];	
		$message = $_POST['message'];	
		
		if ($name != '' && $email != '' && $message != '') {
			
			$to      = $company['company_contactus_email'];
			
			$subject = 'Message from Redemption Contact Form';
			
			$message = 'The message as following' . "\r\n\r\n" . 
			'Name : ' . $name  . "\r\n" .
			'Email : ' . $email  . "\r\n" .
			'Phone : ' . $phone  . "\r\n\r\n" .
			'Message :' . "\r\n" . $message  . "\r\n"
			;
			
			$headers = 'From: ' . $config['PRMSConfig']->noreply_mail . "\r\n" .				
				'X-Mailer: PHP/' . phpversion();

			if (mail($to, $subject, $message, $headers)) {	
				$sent = true;
				array_push($info, 'Your message has been sent.');
			} else {
				$status = false;
				array_push($info, 'Failed to send an email. Please try again.');
			}						
		} else {
			$status = false;
			array_push($info, 'Please enter required fields.');
		}
	}
	
	$formv = array();
	
	if (isset($_REQUEST['name'])) {	
		if ($sent) {
			$formv['name'] = '';
		} else {
		$formv['name'] = $_REQUEST['name']; }
	}
	else
		$formv['name'] =  '';
		
	if (isset($_REQUEST['email'])) {	
		if ($sent) {
			$formv['email'] = '';
		} else {
		$formv['email'] = $_REQUEST['email']; }
	}
	else
		$formv['email'] =  '';
		
	if (isset($_REQUEST['phone'])) {	
		if ($sent) {
			$formv['phone'] = '';
		} else {
		$formv['phone'] = $_REQUEST['phone']; }
	}
	else
		$formv['phone'] =  '';
		
	if (isset($_REQUEST['message']))  {	
		if ($sent) {
			$formv['message'] = '';
		} else {
		$formv['message'] = $_REQUEST['message']; }
	}
	else
		$formv['message'] =  '';

	
?>

<div id="righttitle" >Contact us</div>
<div class="contact-icon"></div>
<div id="contentcontainer">

	<div class="form-info">
	
	<br/>
	<h1><?php echo $company['company_name']; ?></h1>	
	<br/>
	<b>Address : </b><?php echo $company['company_address']; ?>
	<?php if ($company['company_phone'] != '') 
	echo '<br/><br/>
	<b>Tel : </b>' . $company['company_phone'];
	?>	
	<?php if ($company['company_fax'] != '') 
	echo '<br/><br/>
	<b>Fax : </b>' . $company['company_fax'];
	?>
	<br/><br/>	
	
	</div>
	
	<?php
		$infoCss = $status ? 'success-info' : 'error-info';
		if (isset($info) && count($info) > 0) {
	?>
		<div class="<?php echo $infoCss; ?> form-info">
			<?php foreach ($info as $handle) {
					echo "<p>$handle</p>";
			} ?>
		</div>
	<?php
		}
	?>
	
	<form name="contactusform" id="contactusform" action="contactus.php" method="post"> 
		<table class="formview" width="100%" border="0" cellspacing="3px" cellpadding="3px">		
			<tr>			
				<td class="SectionBar" colspan="2">				
					Write to us.
				</td>
			</tr>
			<tr>
				<td class="LabelCell Required">Name</td>
				<td><input type="text" name="name" id="name" maxlength="255" class="input Required" size="20" tabindex="10" value="<?php echo $formv['name']; ?>" /></td>
			</tr>		
			<tr>
				<td class="LabelCell Required">Email</td>
				<td><input type="text" name="email" id="email" maxlength="255" class="input Required ValidEmail" size="20" tabindex="20" value="<?php echo $formv['email']; ?>" /></td>
			</tr>	
			<tr>
				<td class="LabelCell">Phone</td>
				<td><input type="text" name="phone" id="phone" maxlength="255" class="input" size="20" tabindex="30" value="<?php echo $formv['phone']; ?>" /></td>
			</tr>
			<tr>			
				<td class="LabelCell Required">Message</td>
				<td><textarea name="message" id="message" class="Required" maxlength="2000" tabindex="40" rows="5" ><?php echo $formv['message']; ?></textarea></td>
			</tr>
			<tr>			
				<td class="BottomToolBar" colspan="2">				
					<input type="submit" name="submit" id="submit" class="button-primary" value="Send" tabindex="50"/>					
				</td>			
			</tr>
		</table>
	</table>
	
</div>


<script type="text/javascript">
	$(document).ready(function() { 
		loadValidation('contactusform');
	});
</script>