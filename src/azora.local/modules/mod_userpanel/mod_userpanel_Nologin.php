<?php
	$config = Factory::getConfig();
	
	JSManager::getInstance()->add('jquery');		
	JSManager::getInstance()->add('fancybox');	
	CSSManager::getInstance()->add('/js/fancybox/jquery.fancybox-1.3.4.css');
	
	DomainManager::getInstance()->load('Customer');
	$customerObj = new Customer();	
	$customer_id = Authentication::getUserId();
	$customer_id = $customer_id == '' ? 0 : $customer_id;	
	$customer = $customerObj->getCustomerProfile($customer_id);	
	
	$username = 'Guest';
	$lastlogin = '';
	$userbutton = '<a href="login.php" id="login" title="Click here to login to your account." style="display:none;">Login</a>';
	if ($customer != null && count($customer) > 0) {
		$username = $customer['name'];	
		$lastlogin = '(Logged in '.Authentication::getLoggedInDateTime().')';
		$userbutton = '<a href="logout.php" id="logout" title="Click here to logout from your account.">Logout</a>';
	}
	
?>

<div id="userpanel-wrapper">				
	<div id="user"><ul><li><a href="<?php echo $config['PRMSConfig']->live_site; ?>" id="home"></a></li><li><span id="username">Welcome, <?php echo $username; ?></span></li><li><span id="lastlogin"><?php echo $lastlogin; ?></span></li></ul></div>
	<div id="controls"><ul><li><?php echo $userbutton; ?></li></ul></div>				
</div>

<script type="text/javascript">
	$(document).ready(function() {		
		$('a#login').live('click', function(e){
			e.preventDefault();
			
			$.fancybox({
				'scrolling'		  : 'no',
				'titleShow'		  : false,
				'autoDimensions'  : false,
				'width'           : 270,
				'height'          : 200,
				'href'            : $(this).attr('href'),
				'onComplete' 	  : function() {
					$("#user_login").focus();
				}
			});
			return false;
		});
		
		$('a#logout').live('click', function(e){
			e.preventDefault();
			
			$.post( 'authentication.php', { form: 'logout' },
				function(data) {					
					if (data.status) {
						window.location.href = data.url;
					} 
				}
			);
			
			return false;
		});
	});
</script>