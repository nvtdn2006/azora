<?php	
	$result = null;
	Loader::Load('Database');
	$dbh = Database::getInstance()->getConnection();
		
	$sth = $dbh->prepare('SELECT * FROM mod_slider WHERE image_status = 1 AND in_trash = 0');			
	
	$sth->execute();
	$result = $sth->fetchAll(PDO::FETCH_ASSOC);
	$sth = null;
	$dbh = null;
	
	$config = Factory::getConfig();
?>
	
<div id="slider">	
	<?php
		foreach($result as $image) {
	?>	
		<img src="<?php echo $config['PRMSConfig']->live_site . '/modules/mod_slider/images/' . $image['sys_file_name'] ; ?>" width="950" height="300" alt="<?php echo $image['description']; ?>" title="<?php echo $image['description']; ?>"/>	
	<?php } ?>	
</div>
<div id="nav-container">
	<ul id="slides-nav">
	</ul>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$("#slider").cycle({
			fx: 'scrollLeft',
			pause: 1,
			pager:  '#slides-nav', 
			timeout: 7000,
		 
			// callback fn that creates a thumbnail to use as pager anchor 
			pagerAnchorBuilder: function(idx, slide) { 
				return '<li><a href="#"></a></li>'; 
			} 
		});
	});
</script>