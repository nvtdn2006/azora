<div id="user-menus" class="left">
	<ul class="main-menu">
	<?php	
		DomainManager::getInstance()->load('Point');
		$pointObj = new Point();
		$points	= 0;
		$points = $pointObj->getPointByCustomer(Authentication::getUserId());
                
                //+START FX.1304.003 Show Expiry Date below Customer Point
                
                $pointExpiryDate = $pointObj->getPointExpiryDate(Authentication::getUserId());
                
                //-END FX.1304.003
                
                
		
		require_once dirname(__FILE__) . '/../../includes/cart.php';
		$qtyInCart = getQuantityInCart();
		
		echo '<li class="menu-group"><div class="menu-group-panel-outer">					
							<div class="menu-group-panel-bar user-menus">
								<span>Manage your account</span>
								<div id="user-menu-icon"></div>
							</div>
							<div id="pointnumberbox">
								'.number_format($points).' pts	
                                                                '. 
                                                                        ((!is_null($pointExpiryDate))?"<br/><font style='font-size:14px'>Expiry date: ". $pointExpiryDate->format('d-M-Y')."</font>":"") //FX.1304.003
                                                                .'
							</div>
							<ul class="menu-item-list">';
		
		echo '<li><a href="viewcart.php" title="View your items in cart">View cart [ <b>'.$qtyInCart.'</b> ]</a></li>';		
		echo '<li><a href="redemptions.php" title="View your redemption history">Redemptions</a></li>';		
		echo '<li><a href="pointdetails.php" title="Check your point details">Point details</a></li>';		
		echo '<li><a href="purchasehistory.php" title="View your purchase history">Purchase history</a></li>';		
		echo '<li><a href="pointstransaction.php" title="View your points transaction">Points transaction</a></li>';		
		echo '<li><a href="profile.php" title="Update your profile">Update profile</a></li>';		
		echo '<li><a href="changepassword.php" title="Change new password">Change password</a></li>';						
		echo '</ul></div></li>';
	?>
	</ul>
</div>