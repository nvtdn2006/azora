<div id="product-categories" class="left">
	<ul class="main-menu">
	<?php
		DomainManager::getInstance()->load('Category');
		$categoryObj = new Category();
		$categories = $categoryObj->getAllCategories();
		
		echo '<li class="menu-group"><div class="menu-group-panel-outer">					
							<div class="menu-group-panel-bar">
								<span>Product Category</span>
								<div id="product-menu-icon"></div>
							</div>
							<ul class="menu-item-list">';
		echo '<li><a href="products.php" title="All products">All Products</a></li>';		
		foreach ($categories as $category) {	
			echo '<li><a href="products.php?id='.$category['category_id'].'" title="'.$category['category_description'].'">'.$category['category_name'].'</a></li>';		
		}
		echo '</ul></div></li>';
	?>
	</ul>
</div>