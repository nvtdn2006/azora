<?php
require_once dirname(__FILE__) . '/../libraries/application.php';

class WebSite extends Application
{	
	public $service = false;
	public $template;
	public $title;
	public $feature_id;
	private $page_content;

	function __construct() {
		parent::__construct();	
		$this->LoadDomains();		
		$this->LoadMailer();
		$this->LoadEventLog();
	}

	public function run() {	
		$this->setAuthenticationProcess();
		$this->getPageContent();
		if (!$this->service) {			
			$this->setTheme();
		} else { 
			echo $this->page_content;
		}
	}
	
	private function setAuthenticationProcess() {		
		Authentication::$process = 'front';	
	}
	
	private function LoadDomains() {
		if (!class_exists('DomainManager')) {
			require_once dirname(__FILE__) . '/../domain/domainmanager.php';			
		}	
	}

	private function LoadMailer() {
		if (!class_exists('SiteMailer')) {
			require_once dirname(__FILE__) . '/sitemailer.php';
		}
	}
	
	private function LoadEventLog() {
		if (!class_exists('EventLog')) {
			require_once dirname(__FILE__) . '/eventlog.php';
		}
	}
	
	private function getPageContent() {		
		ob_start();			
		include(DOC_ROOT.$_SERVER['PHP_SELF']);
		$this->page_content = ob_get_contents();
		ob_end_clean(); 
	}
	
	private function setTheme() {								
		Loader::load('Theme');		
		$theme = new Theme();
		$theme->initWithThemeOptions(array('content' => $this->page_content, 'template' => $this->template, 'theme_side' => 'front', 'title' => $this->title));
		$theme->applyTheme();
	}
}

$application = new WebSite();
$application->run();
exit();
?>