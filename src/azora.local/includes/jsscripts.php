<?php
function getScripts() {
	return array(
					'jquery' => '../js/jquery-1.6.4.min.js',
					'checkboxlist' => '../js/checkboxlist.js',					
					'messagebox' => '../js/messagebox.js',					
					'validation' => '../js/validation.js',	
					'mousewheel' => '../js/fancybox/jquery.mousewheel-3.0.4.pack.js',	
					'fancybox' => '../js/fancybox/jquery.fancybox-1.3.4.pack.js',	
					'numeric' => '../js/jquery.numeric.js',					
					'jquery.easing' => '../js/jquery.easing.1.3.js',					
					'jquery.cycle' => '../js/jquery.cycle.all.min.js',		
					'jquery.autoellipsis' => '../js/jquery.autoellipsis.js',		
					'jquery.ui' => '../js/jquery-ui-1.8.16.custom.min.js',	
				);
}
?>