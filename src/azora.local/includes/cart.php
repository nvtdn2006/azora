<?php

	function getPointsInCart() {
		$rvalue = 0;
		if (Factory::getSession()->isExist('mycart')) {
			$cart = Factory::getSession()->getValue('mycart');
			foreach ($cart as $item) {
				$rvalue	+= $item['qty'] * $item['points'];
			}
		}		
		return $rvalue;
	}
	
	function getQuantityInCart() {
		$rvalue = 0;
		if (Factory::getSession()->isExist('mycart')) {
			$cart = Factory::getSession()->getValue('mycart');
			foreach ($cart as $item) {
				$rvalue	+= $item['qty'];
			}
		}		
		return $rvalue;
	}
	
	
	function getCart() {
		$rvalue = null;
		if (Factory::getSession()->isExist('mycart')) {
			$rvalue = Factory::getSession()->getValue('mycart');			
		}		
		return $rvalue;
	}
	
	function setCart($cart) {		
		Factory::getSession()->setValue('mycart', $cart);			
	}
	
	function clearCart() {		
		Factory::getSession()->removeKey('mycart');			
	}
	
	function addToCart($item) {	
		$rvalue = true;
		if (Factory::getSession()->isExist('mycart')) {			
			if (isExistItemInCart($item)) {
				updateToCart($item);
			} else {
				$cartim = Factory::getSession()->getValue('mycart');
				array_push($cartim, $item);
				
				Factory::getSession()->setValue('mycart', $cartim);
			}
		} else {
			$cart = array();
			array_push($cart, $item);
			Factory::getSession()->setValue('mycart', $cart);
		}		
		return $rvalue;
	}
	
	function isExistItemInCart($item) {
		$rvalue = false;
		
		if (Factory::getSession()->isExist('mycart')) {
			$cart = Factory::getSession()->getValue('mycart');
			foreach ($cart as $li) {				
				if ($item['id'] == $li['id']) {
					$rvalue	= true;
					break;
				}
			}
		}
		
		return $rvalue;
	}
	
	function updateToCart($item) {
		$rvalue = true;
		
		if (Factory::getSession()->isExist('mycart')) {
			$cart = Factory::getSession()->getValue('mycart');
			$newcart = array();
			foreach ($cart as $li) {				
				if ($item['id'] == $li['id']) {
					$li['qty'] += $item['qty'];					
				}
				array_push($newcart, $li);
			}
			Factory::getSession()->setValue('mycart', $newcart);
		}
		
		return $rvalue;
	}
?>