<?php
require_once dirname(__FILE__) . '/../libraries/application.php';

class WebSiteService extends Application
{	
	private $page_content;

	function __construct() {
		parent::__construct();	
		$this->LoadDomains();		
		$this->LoadMailer();
		$this->LoadEventLog();
	}

	public function run() {		
		$this->setAuthenticationProcess();
		$this->getPageContent();
		echo $this->page_content;
	}
	
	private function setAuthenticationProcess() {		
		Authentication::$process = 'front';	
	}
	
	private function LoadDomains() {
		if (!class_exists('DomainManager')) {
			require_once dirname(__FILE__) . '/../domain/domainmanager.php';			
		}	
	}

	private function LoadMailer() {
		if (!class_exists('SiteMailer')) {
			require_once dirname(__FILE__) . '/sitemailer.php';
		}
	}
	
	private function LoadEventLog() {
		if (!class_exists('EventLog')) {
			require_once dirname(__FILE__) . '/eventlog.php';
		}
	}
	
	private function getPageContent() {		
		ob_start();			
		include(DOC_ROOT.$_SERVER['PHP_SELF']);
		$this->page_content = ob_get_contents();
		ob_end_clean(); 
	}
}

$application = new WebSiteService();
$application->run();
exit();
?>