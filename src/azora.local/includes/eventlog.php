<?php

class EventTypes {

	const PRMS_UserLogin = 1;
	const PRMS_UserLogout = 2;
	const PRMS_UpdateProfile = 3;
	const PRMS_ChangePassword = 4;
	const PRMS_ReqForgotPwd = 5;
	const PRMS_RegisterPoints = 6;
	const PRMS_CancelPoints = 7;
	const PRMS_UpdateRedemptionStatus = 8;
	const PRMS_AddCategory = 9;
	const PRMS_UpdateCategory = 10;
	const PRMS_DeleteCategory = 11;
	const PRMS_AddProduct = 12;
	const PRMS_UpdateProduct = 13;
	const PRMS_DeleteProduct = 14;
	const PRMS_UpdateRulePolicies = 15;	
	const PRMS_AddSliderImage = 16;
	const PRMS_UpdateSliderImage = 17;
	const PRMS_DeleteSliderImage = 18;	
	const PRMS_UpdateCompanyInfo = 19;	
	const PRMS_AddBranch = 20;
	const PRMS_UpdateBranch = 21;
	const PRMS_DeleteBranch = 22;
	const PRMS_AddStore = 23;
	const PRMS_UpdateStore = 24;
	const PRMS_DeleteStore = 25;
	const PRMS_AddMarketing = 26;
	const PRMS_UpdateMarketing = 27;
	const PRMS_DeleteMarketing = 28;
	const PRMS_SendMarketing = 29;
	const PRMS_AddRole = 30;
	const PRMS_UpdateRole = 31;
	const PRMS_DeleteRole = 32;
	const PRMS_AddUser = 33;
	const PRMS_UpdateUser = 34;
	const PRMS_DeleteUser = 35;
	const PRMS_ResetUserPassword = 36;
	const PRMS_UpdateOverviewContent = 37;
	const PRMS_UpdateFAQContent = 38;
	
	const RWS_UserLogin = 1001;
	const RWS_UserLogout = 1002;
	const RWS_UpdateProfile = 1003;
	const RWS_ChangePassword = 1004;
	const RWS_ReqForgotPwd = 1005;
	const RWS_UserActivated = 1006;
	const RWS_SubmitRedemption = 1007;
	
	
	
}

class EventObject {
	public $event_id;
	public $description;
	public $action_by;	
}

class EventLog {

	public static function Log($event) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('INSERT INTO event_log (event_id, description, action_by, action_on) VALUES 
												(:event_id, :description, :action_by, NOW())');			
		
		$sth->bindParam(':event_id', $event->event_id);
		$sth->bindParam(':description', $event->description);
		$sth->bindParam(':action_by', $event->action_by);		
			
		$sth->execute();
		
		$result = ($sth->rowCount() > 0);
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	public static function GetEventTypes() {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT * FROM events ORDER BY event_name');			
		
		$sth->execute();
		$result = $sth->fetchAll(PDO::FETCH_ASSOC);
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	public static function GetEventLogs($criteria = null, $limit = 0) {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
		
		$sql = 'SELECT el.*, e.event_name FROM event_log el  
								LEFT JOIN events e ON el.event_id = e.id								
								';
		
		if ($criteria) {
			$sql .= ' WHERE ';
			$sp = '';
			
			if (isset($criteria['event_id'])) {
				$sql .= $sp . ' el.event_id = ' .$criteria['event_id'];
				$sp = ' AND ';
			}
			
			if (isset($criteria['eventfrom']) && isset($criteria['eventto'])) {
				$start = $criteria['eventfrom']; $end = $criteria['eventto'];
				if ($end < $start) {
					$start = $criteria['eventto'];
					$end = $criteria['eventfrom'];
				}
				
				$sql .= $sp . ' DATE(el.action_on) BETWEEN \'' .$start. '\' AND \'' .$end. '\'';
				$sp = ' AND ';
			}			
		}
		
		$sql .= ' ORDER BY el.action_on DESC';
		
		if ($limit > 0) {
			$sql .= ' LIMIT 0, '.$limit;
		}
		
		$sth = $dbh->prepare($sql);			
		$sth->execute();
		$result = $sth->fetchAll(PDO::FETCH_ASSOC);		
		
		$sth = null;
		$dbh = null;
		return $result;
	}

}


class SchedulerLog {
	
	public static function Log($log) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('INSERT INTO scheduler_log (scheduler_name, status, is_manual, last_run) VALUES 
												(:scheduler_name, :status, :is_manual, NOW())');			
		
		$sth->bindParam(':scheduler_name', $log['scheduler_name']);
		$sth->bindParam(':status', $log['status']);
		$sth->bindParam(':is_manual', $log['is_manual']);		
			
		$sth->execute();
		
		$result = ($sth->rowCount() > 0);
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	public static function GetSchedulerLogs() {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
		
		$sql = 'SELECT * FROM scheduler_log WHERE last_run BETWEEN last_run - INTERVAL 10 DAY AND NOW() ORDER BY last_run DESC';
		
		$sth = $dbh->prepare($sql);			
		$sth->execute();
		$result = $sth->fetchAll(PDO::FETCH_ASSOC);		
		
		$sth = null;
		$dbh = null;
		return $result;
	}

}

?>
