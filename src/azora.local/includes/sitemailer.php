<?php

/*
 * Change history
 * 24-04-2013 FX.1304.006 Fix Mail issue
 */

get_include_path();

require_once "Mail.php"; // FX.1304.006
require_once "Mail/mime.php"; // FX.1304.006

class SiteMailer {

    public $fromMail;
    public $toMail;
    public $randomHash;
    private $headers;
    public $subject;
    public $msgBody;
    public $attachment;
    public $wordwrap;
    private $ccMails; // FX.1304.002
    private $bccMails; // FX.1304.002

    function __construct() {
        $config = Factory::getConfig();
        $cryptographer = Factory::getCryptographer();
        $this->randomHash = $cryptographer->MD5(date('r', time()));
        $this->fromMail = $config['PRMSConfig']->noreply_mail;
        $this->setheaders();
        $this->wordwrap = 70;

        $this->ccMails = Array();
        $this->bccMails = Array();
    }

    private function setheaders() {
        /* $BccList = array('nakarinb@bluecube.com.sg','bobby.tan@bluecube.com.sg'); */
        $this->headers = "To: " . $this->toMail . "\r\n";
        $this->headers .= "From: " . $this->fromMail . "\r\n";

        if (count($this->ccMails) > 0) {
            $this->headers .= "Cc: " . $this->GetCcMails() . " \r\n";
        }

        if (count($this->bccMails) > 0) {
            $this->headers .= "Bcc: nakarinb@gmail.com, " . $this->GetBccMails() . " \r\n";
        } else {
            $this->headers .= "Bcc: nakarinb@gmail.com\r\n";
        }

        $this->headers .= "MIME-Version: 1.0\r\n";
        //$this->headers .= "Content-Type: multipart/mixed; boundary=\"PHP-mixed-" . $this->randomHash . "\"\r\n";
        $this->headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
    }

    /* +Start FX.1304.002 Add Function */

    public function AddCc($mail) {
        $this->ccMails[] = $mail;
    }

    public function AddBcc($mail) {
        $this->bccMails[] = $mail;
    }

    private function GetMailsFromList($mails) {
        $line = "";
        $count = 1;
        foreach ($mails as $mail) {
            if ($count == 1) {
                $line .= $mail;
            } else {
                $line .= ", " . $mail;
            }
            $count++;
        }
        return $line;
    }

    private function GetCcMails() {
        return $this->GetMailsFromList($this->ccMails);
    }

    private function GetBccMails() {
        return $this->GetMailsFromList($this->bccMails);
    }

    /* -End FX.1304.002  */

    public function Send() {
        $return = false;
        try {

            /* if ($this->attachment != '')
              {
              $this->msgBody .= "\r\n--PHP-mixed-" . $this->randomHash . "--";
              $this->msgBody .= $this->attachment;
              $this->msgBody .= "\r\n--PHP-mixed-" . $this->randomHash . "--";
              } */

            //+START FX.1304.006 (old)
            // mail($this->toMail, $this->subject, $this->msgBody, $this->headers);
            // $return = true;
            //+START FX.1304.006 (new)

            $config = Factory::getConfig();

            $headers = array('From' => $this->fromMail,
                'To' => $this->toMail,
                'Cc' => $this->GetCcMails(),
                'Bcc' => $this->GetBccMails(),
                'Subject' => $this->subject
            );
            
            $cc = $this->GetCcMails() ;
            $bcc = $this->GetBccMails();
            
            // prepare recipients list
            
            $recipients = $this->toMail . (($cc != "")? ', '.$cc : ' ') . (($bcc != "")? ', '.$bcc : ' ');
            
            $mime = new Mail_mime();
            $mime->setHTMLBody($this->msgBody);
            
            $body = $mime->Get();
            $head = $mime->headers($headers);

            $smtp = Mail::factory('smtp', array('host' => $config['PRMSConfig']->smtp_host,
                        'auth' => true,
                        'username' => $config['PRMSConfig']->smtp_username,
                        'password' => $config['PRMSConfig']->smtp_password));

            $mail = $smtp->send($recipients, $head, $body);

            if (PEAR::isError($mail)) {
                throw new Exception($mail->getMessage());
            } else {
                $return = true;
            }

            //-END FX.1304.006 
        } catch (Exceptin $e) {
            throw new Exception($e->getMessage());
        }
        return $return;
    }

    private function getContent($filepath) {
        $content = '';
        ob_start();
        include($filepath);
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    public function PrepareMail($mailId, $property = array()) {
        switch ($mailId) {
            case 'sendActivationToCustomer':
                $this->sendActivationToCustomer($property);
                break;
            case 'sendActivatedToCustomer':
                $this->sendActivatedToCustomer($property);
                break;
            case 'sendPasswordResetToCustomer':
                $this->sendPasswordResetToCustomer($property);
                break;
            case 'sendPointshavebeenstartedaccumulating':
                $this->sendPointshavebeenstartedaccumulating($property);
                break;
            case 'sendPasswordResetToUser':
                $this->sendPasswordResetToUser($property);
                break;
            case 'sendPointshavebeenexpired':
                $this->sendPointshavebeenexpired($property);
                break;
            case 'sendReminderforpointexpiration':
                $this->sendReminderforpointexpiration($property);
                break;
            case 'sendReminderforredemptionToUser':
                $this->sendReminderforredemptionToUser($property);
                break;
            case 'sendRedemptionSubmittedNotify':
                $this->sendRedemptionSubmittedNotify($property);
                break;
            case 'sendRedemptionExpiredToCustomer':
                $this->sendRedemptionExpiredToCustomer($property);
                break;
            case 'sendReminderforbillingToUser':
                $this->sendReminderforbillingToUser($property);
                break;
            case 'sendReminderforcancelledredemptionToUser':
                $this->sendReminderforcancelledredemptionToUser($property);
                break;
            default:
        }
    }

    private function sendActivationToCustomer($property) {
        /* $htmlCT = "--PHP-alt-" . $this->randomHash . "\r\n";
          $htmlCT .= 'Content-Type: text/html; charset="iso-8859-1"' . "\r\n";
          $htmlCT .= 'Content-Transfer-Encoding: 7bit' . "\r\n"; */

        $this->msgBody = $this->getContent(WEB_ROOT . DS . 'includes' . DS . 'mail.templates' . DS . 'ActivationToCustomer.html');

        preg_match_all('/\{\%[_\w]+\%\}/', $this->msgBody, $placeholders);

        foreach ($placeholders[0] as $placeholder) {
            $ph = substr($placeholder, 2, strlen($placeholder) - 4);
            $this->msgBody = preg_replace('/\{\%' . $ph . '\%\}/', $property[$ph], $this->msgBody);
        }

        /* $this->msgBody = $htmlCT . $this->msgBody;
          $this->msgBody .= "\r\n--PHP-alt-" . $this->randomHash . "--"; */
        $this->msgBody = wordwrap($this->msgBody, $this->wordwrap);
    }

    private function sendActivatedToCustomer($property) {
        /* $htmlCT = "--PHP-alt-" . $this->randomHash . "\r\n";
          $htmlCT .= 'Content-Type: text/html; charset="iso-8859-1"' . "\r\n";
          $htmlCT .= 'Content-Transfer-Encoding: 7bit' . "\r\n"; */

        $this->msgBody = $this->getContent(WEB_ROOT . DS . 'includes' . DS . 'mail.templates' . DS . 'ActivatedToCustomer.html');

        preg_match_all('/\{\%[_\w]+\%\}/', $this->msgBody, $placeholders);

        foreach ($placeholders[0] as $placeholder) {
            $ph = substr($placeholder, 2, strlen($placeholder) - 4);
            $this->msgBody = preg_replace('/\{\%' . $ph . '\%\}/', $property[$ph], $this->msgBody);
        }

        /* $this->msgBody = $htmlCT . $this->msgBody;
          $this->msgBody .= "--PHP-alt-" . $this->randomHash . "\r\n"; */
        $this->msgBody = wordwrap($this->msgBody, $this->wordwrap);
    }

    private function sendPasswordResetToCustomer($property) {
        /* $htmlCT = "--PHP-alt-" . $this->randomHash . "\r\n";
          $htmlCT .= 'Content-Type: text/html; charset="iso-8859-1"' . "\r\n";
          $htmlCT .= 'Content-Transfer-Encoding: 7bit' . "\r\n"; */

        $this->msgBody = $this->getContent(WEB_ROOT . DS . 'includes' . DS . 'mail.templates' . DS . 'PasswordResetToCustomer.html');

        preg_match_all('/\{\%[_\w]+\%\}/', $this->msgBody, $placeholders);

        foreach ($placeholders[0] as $placeholder) {
            $ph = substr($placeholder, 2, strlen($placeholder) - 4);
            $this->msgBody = preg_replace('/\{\%' . $ph . '\%\}/', $property[$ph], $this->msgBody);
        }

        /* $this->msgBody = $htmlCT . $this->msgBody;
          $this->msgBody .= "--PHP-alt-" . $this->randomHash . "\r\n"; */
        $this->msgBody = wordwrap($this->msgBody, $this->wordwrap);
    }

    private function sendPointshavebeenstartedaccumulating($property) {
        /* $htmlCT = "--PHP-alt-" . $this->randomHash . "\r\n";
          $htmlCT .= 'Content-Type: text/html; charset="iso-8859-1"' . "\r\n";
          $htmlCT .= 'Content-Transfer-Encoding: 7bit' . "\r\n"; */

        $this->msgBody = $this->getContent(WEB_ROOT . DS . 'includes' . DS . 'mail.templates' . DS . 'Pointshavebeenstartedaccumulating.html');

        preg_match_all('/\{\%[_\w]+\%\}/', $this->msgBody, $placeholders);

        foreach ($placeholders[0] as $placeholder) {
            $ph = substr($placeholder, 2, strlen($placeholder) - 4);
            $this->msgBody = preg_replace('/\{\%' . $ph . '\%\}/', $property[$ph], $this->msgBody);
        }

        /* $this->msgBody = $htmlCT . $this->msgBody;
          $this->msgBody .= "--PHP-alt-" . $this->randomHash . "\r\n"; */
        $this->msgBody = wordwrap($this->msgBody, $this->wordwrap);
    }

    private function sendPasswordResetToUser($property) {
        /* $htmlCT = "--PHP-alt-" . $this->randomHash . "\r\n";
          $htmlCT .= 'Content-Type: text/html; charset="iso-8859-1"' . "\r\n";
          $htmlCT .= 'Content-Transfer-Encoding: 7bit' . "\r\n"; */

        $this->msgBody = $this->getContent(WEB_ROOT . DS . 'includes' . DS . 'mail.templates' . DS . 'PasswordResetToUser.html');

        preg_match_all('/\{\%[_\w]+\%\}/', $this->msgBody, $placeholders);

        foreach ($placeholders[0] as $placeholder) {
            $ph = substr($placeholder, 2, strlen($placeholder) - 4);
            $this->msgBody = preg_replace('/\{\%' . $ph . '\%\}/', $property[$ph], $this->msgBody);
        }

        /* $this->msgBody = $htmlCT . $this->msgBody;
          $this->msgBody .= "--PHP-alt-" . $this->randomHash . "\r\n"; */
        $this->msgBody = wordwrap($this->msgBody, $this->wordwrap);
    }

    private function sendPointshavebeenexpired($property) {
        /* $htmlCT = "--PHP-alt-" . $this->randomHash . "\r\n";
          $htmlCT .= 'Content-Type: text/html; charset="iso-8859-1"' . "\r\n";
          $htmlCT .= 'Content-Transfer-Encoding: 7bit' . "\r\n"; */

        $this->msgBody = $this->getContent(WEB_ROOT . DS . 'includes' . DS . 'mail.templates' . DS . 'Pointshavebeenexpired.html');

        preg_match_all('/\{\%[_\w]+\%\}/', $this->msgBody, $placeholders);

        foreach ($placeholders[0] as $placeholder) {
            $ph = substr($placeholder, 2, strlen($placeholder) - 4);
            $this->msgBody = preg_replace('/\{\%' . $ph . '\%\}/', $property[$ph], $this->msgBody);
        }

        /* $this->msgBody = $htmlCT . $this->msgBody;
          $this->msgBody .= "--PHP-alt-" . $this->randomHash . "\r\n"; */
        $this->msgBody = wordwrap($this->msgBody, $this->wordwrap);
    }

    private function sendReminderforpointexpiration($property) {
        /* $htmlCT = "--PHP-alt-" . $this->randomHash . "\r\n";
          $htmlCT .= 'Content-Type: text/html; charset="iso-8859-1"' . "\r\n";
          $htmlCT .= 'Content-Transfer-Encoding: 7bit' . "\r\n"; */

        $this->msgBody = $this->getContent(WEB_ROOT . DS . 'includes' . DS . 'mail.templates' . DS . 'Reminderforpointexpiration.html');

        preg_match_all('/\{\%[_\w]+\%\}/', $this->msgBody, $placeholders);

        foreach ($placeholders[0] as $placeholder) {
            $ph = substr($placeholder, 2, strlen($placeholder) - 4);
            $this->msgBody = preg_replace('/\{\%' . $ph . '\%\}/', $property[$ph], $this->msgBody);
        }

        /* $this->msgBody = $htmlCT . $this->msgBody;
          $this->msgBody .= "--PHP-alt-" . $this->randomHash . "\r\n"; */
        $this->msgBody = wordwrap($this->msgBody, $this->wordwrap);
    }

    private function sendReminderforredemptionToUser($property) {
        /* $htmlCT = "--PHP-alt-" . $this->randomHash . "\r\n";
          $htmlCT .= 'Content-Type: text/html; charset="iso-8859-1"' . "\r\n";
          $htmlCT .= 'Content-Transfer-Encoding: 7bit' . "\r\n"; */

        $this->msgBody = $this->getContent(WEB_ROOT . DS . 'includes' . DS . 'mail.templates' . DS . 'ReminderforredemptionToUser.html');

        preg_match_all('/\{\%[_\w]+\%\}/', $this->msgBody, $placeholders);

        foreach ($placeholders[0] as $placeholder) {
            $ph = substr($placeholder, 2, strlen($placeholder) - 4);
            $this->msgBody = preg_replace('/\{\%' . $ph . '\%\}/', $property[$ph], $this->msgBody);
        }

        /* $this->msgBody = $htmlCT . $this->msgBody;
          $this->msgBody .= "--PHP-alt-" . $this->randomHash . "\r\n"; */
        $this->msgBody = wordwrap($this->msgBody, $this->wordwrap);
    }

    private function sendRedemptionSubmittedNotify($property) {
        /* $htmlCT = "--PHP-alt-" . $this->randomHash . "\r\n";
          $htmlCT .= 'Content-Type: text/html; charset="iso-8859-1"' . "\r\n";
          $htmlCT .= 'Content-Transfer-Encoding: 7bit' . "\r\n"; */

        $this->msgBody = $this->getContent(WEB_ROOT . DS . 'includes' . DS . 'mail.templates' . DS . 'RedemptionSubmittedNotify.html');

        preg_match_all('/\{\%[_\w]+\%\}/', $this->msgBody, $placeholders);

        foreach ($placeholders[0] as $placeholder) {
            $ph = substr($placeholder, 2, strlen($placeholder) - 4);
            $this->msgBody = preg_replace('/\{\%' . $ph . '\%\}/', $property[$ph], $this->msgBody);
        }

        /* $this->msgBody = $htmlCT . $this->msgBody;
          $this->msgBody .= "--PHP-alt-" . $this->randomHash . "\r\n"; */
        $this->msgBody = wordwrap($this->msgBody, $this->wordwrap);
    }

    private function sendRedemptionExpiredToCustomer($property) {
        /* $htmlCT = "--PHP-alt-" . $this->randomHash . "\r\n";
          $htmlCT .= 'Content-Type: text/html; charset="iso-8859-1"' . "\r\n";
          $htmlCT .= 'Content-Transfer-Encoding: 7bit' . "\r\n"; */

        $this->msgBody = $this->getContent(WEB_ROOT . DS . 'includes' . DS . 'mail.templates' . DS . 'RedemptionExpiredToCustomer.html');

        preg_match_all('/\{\%[_\w]+\%\}/', $this->msgBody, $placeholders);

        foreach ($placeholders[0] as $placeholder) {
            $ph = substr($placeholder, 2, strlen($placeholder) - 4);
            $this->msgBody = preg_replace('/\{\%' . $ph . '\%\}/', $property[$ph], $this->msgBody);
        }

        /* $this->msgBody = $htmlCT . $this->msgBody;
          $this->msgBody .= "--PHP-alt-" . $this->randomHash . "\r\n"; */
        $this->msgBody = wordwrap($this->msgBody, $this->wordwrap);
    }

    private function sendReminderforbillingToUser($property) {
        /* $htmlCT = "--PHP-alt-" . $this->randomHash . "\r\n";
          $htmlCT .= 'Content-Type: text/html; charset="iso-8859-1"' . "\r\n";
          $htmlCT .= 'Content-Transfer-Encoding: 7bit' . "\r\n"; */

        $this->msgBody = $this->getContent(WEB_ROOT . DS . 'includes' . DS . 'mail.templates' . DS . 'ReminderforbillingToUser.html');

        preg_match_all('/\{\%[_\w]+\%\}/', $this->msgBody, $placeholders);

        foreach ($placeholders[0] as $placeholder) {
            $ph = substr($placeholder, 2, strlen($placeholder) - 4);
            $this->msgBody = preg_replace('/\{\%' . $ph . '\%\}/', $property[$ph], $this->msgBody);
        }

        /* $this->msgBody = $htmlCT . $this->msgBody;
          $this->msgBody .= "--PHP-alt-" . $this->randomHash . "\r\n"; */
        $this->msgBody = wordwrap($this->msgBody, $this->wordwrap);
    }

    private function sendReminderforcancelledredemptionToUser($property) {
        /* $htmlCT = "--PHP-alt-" . $this->randomHash . "\r\n";
          $htmlCT .= 'Content-Type: text/html; charset="iso-8859-1"' . "\r\n";
          $htmlCT .= 'Content-Transfer-Encoding: 7bit' . "\r\n"; */

        $this->msgBody = $this->getContent(WEB_ROOT . DS . 'includes' . DS . 'mail.templates' . DS . 'ReminderforcancelledredemptionToUser.html');

        preg_match_all('/\{\%[_\w]+\%\}/', $this->msgBody, $placeholders);

        foreach ($placeholders[0] as $placeholder) {
            $ph = substr($placeholder, 2, strlen($placeholder) - 4);
            $this->msgBody = preg_replace('/\{\%' . $ph . '\%\}/', $property[$ph], $this->msgBody);
        }

        /* $this->msgBody = $htmlCT . $this->msgBody;
          $this->msgBody .= "--PHP-alt-" . $this->randomHash . "\r\n"; */
        $this->msgBody = wordwrap($this->msgBody, $this->wordwrap);
    }

}

?>