<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Purchase history'; ?>

<?php	

	if (!Authentication::isAuthenticated()) {
		header( 'Location: index.php');
	}
	
	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('validation');
	
	DomainManager::getInstance()->load('Point');
	$pointObj = new Point();
	
	$customer_id = Authentication::getUserId();		
	
	$purchasehistory = null;
	
	if (isset($_GET['action']) && $_GET['action'] == 'search') {
		if ($customer_id > 0) {
			$month = date('n');
			$year = date('Y');
			
			if (isset($_POST['month'])) 
				$month = $_POST['month'];
				
			if (isset($_POST['year'])) 
				$year = $_POST['year'];
				
			$purchasehistory = $pointObj->getPurchaseHistoryByCustomer($customer_id, $month, $year);			
		}
	}
	
	$formvalues = array();
	
	if (isset($_REQUEST['month'])) 			
		$formvalues['month'] = $_REQUEST['month'];
	else
		$formvalues['month'] =  0;
	
	if (isset($_REQUEST['year'])) 			
		$formvalues['year'] = $_REQUEST['year'];
	else
		$formvalues['year'] =  0;
?>

<div id="righttitle">Purchase history</div>
<div id="contentcontainer">


<form name="historyform" id="historyform" action="purchasehistory.php?action=search" method="post"> 
	<table class="formview" width="100%" border="0">		
		<tr>
			<td>Month</td>		
			<td>				
				<select tabindex="50" name="month" class="GreaterThanZero" tabindex="10">						
					<option value="0" <?php echo $formvalues['month'] == 0 ? 'selected' : ''; ?>>- Select -</option>
					<option value="1" <?php echo $formvalues['month'] == 1 ? 'selected' : ''; ?>>Jan</option>
					<option value="2" <?php echo $formvalues['month'] == 2 ? 'selected' : ''; ?>>Feb</option>
					<option value="3" <?php echo $formvalues['month'] == 3 ? 'selected' : ''; ?>>Mar</option>
					<option value="4" <?php echo $formvalues['month'] == 4 ? 'selected' : ''; ?>>Apr</option>
					<option value="5" <?php echo $formvalues['month'] == 5 ? 'selected' : ''; ?>>May</option>
					<option value="6" <?php echo $formvalues['month'] == 6 ? 'selected' : ''; ?>>Jun</option>
					<option value="7" <?php echo $formvalues['month'] == 7 ? 'selected' : ''; ?>>Jul</option>
					<option value="8" <?php echo $formvalues['month'] == 8 ? 'selected' : ''; ?>>Aug</option>
					<option value="9" <?php echo $formvalues['month'] == 9 ? 'selected' : ''; ?>>Sep</option>
					<option value="10" <?php echo $formvalues['month'] == 10 ? 'selected' : ''; ?>>Oct</option>
					<option value="11" <?php echo $formvalues['month'] == 11 ? 'selected' : ''; ?>>Nov</option>
					<option value="12" <?php echo $formvalues['month'] == 12 ? 'selected' : ''; ?>>Dec</option>						
				</select>
			</td>			
			<td>Year</td>		
			<td>
				<select tabindex="50" name="year" class="GreaterThanZero" tabindex="20">				
					<option value="0">- Select -</option>
					<?php
						$year = date('Y');
						for($i=0;$i<3;$i++) {
							echo '<option value="'.($year - $i).'" '.($formvalues['year'] == ($year - $i) ? 'selected' : '').'  >'.($year - $i).'</option>';							
						}
					?>
				</select>
			</td>			
		</tr>				
		<tr>			
			<td colspan="4">				
				<input type="submit" name="search" id="search" class="button-primary" value="Search" tabindex="30"/>				
			</td>			
		</tr>
	</table>
</form>

<div class="SectionTitle" ><b><?php echo ($purchasehistory != null && count($purchasehistory) > 0) ? count($purchasehistory) : '0'; ?> </b> Record(s) found.</div>

<table class="tabular">
	<thead>
		<tr>			
			<th>Branch</th>
			<th>Store</th>
			<th>Reference No.</th>
			<th>Purchase Amount</th>
			<th>Reward Points</th>
			<th>Transaction On</th>			
		</tr>
	</thead>
	<tbody>
		<?php
			if ($purchasehistory != null) {
			foreach($purchasehistory as $history) {
		?>
		<tr>						
			<td><?php echo $history['branch_name'] ?></td>
			<td><?php echo $history['store_name'] ?></td>
			<td><?php echo $history['reference_no'] ?></td>
			<td><?php echo $history['sales_amount'] ?></td>
			<td><?php echo $history['trans_points'] ?></td>			
			<td><?php echo $history['transacted_date'] ?></td>			
		</tr>
		<?php } } ?>
	</tbody>
</table>

</div>


<script type="text/javascript">
	$(document).ready(function() { 
		loadValidation('historyform');			
	});	
</script>