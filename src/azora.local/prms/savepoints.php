<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Register points'?>

<?php		

	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('validation');
	JSManager::getInstance()->add('numeric');
	
	DomainManager::getInstance()->load('Customer');
	DomainManager::getInstance()->load('Store');
	
	$customerObj = new Customer();
	$storeObj = new Store();
	
	$regcustomer = null;	
	
	$error = array();
	
	if (isset($_GET['action']) && $_GET['action'] == 'reg') {
		$customer_id = 0;
		
		if (isset($_GET['action']))
			$customer_id = $_GET['id'];
		
		if ($customer_id > 0) {
			$regcustomer = $customerObj->getCustomerProfile($customer_id);
		}
	}
	
	$point = null;
	
	if (isset($_POST['customer_id']) && $_POST['customer_id'] != 0) {
		$point = array('customer_id' => $_POST['customer_id'],
						'reference_no' => $_POST['referenceno'],
						'sales_amount' => $_POST['salesamount'],
						'transaction_type' => 1,
						'remarks' => $_POST['remarks'],
						'store_id' => $_POST['store'],								
						'registered_by' => Authentication::getUserId());
		$firstTimeUsing = false;
		
		DomainManager::getInstance()->load('Point');
		$pointObj = new Point();
		
		if ($pointObj->registerPoint($point, $firstTimeUsing)) {
			if ($firstTimeUsing) {
				DomainManager::getInstance()->load('Reward');
				$rewardObj = new Reward();		
				$rewards = $rewardObj->getReward();	
				if (isset($rewards) && count($rewards) > 0) {
					$reward = $rewards[0];
					
					$expiry_on = new DateTime($point['started_date']);
					$expiry_on->modify('+'.$reward['expiration_month'].' month');
					$config = Factory::getConfig();

					$expiry = '';
					if ($reward['expiration_required'] == 1) {
						$expiry = 'The new expiry date will be on ' . $expiry_on->format('Y-m-d') . ' according to our current terms & conditions.';
					}

					DomainManager::getInstance()->load('Company');
					$companyObj = new Company();
					$company = $companyObj->getCompany();	
					$company = $company[0];
						
					$property = array('expiry_on' => $expiry ,
										'name' => $regcustomer['name'],
										'link' => $config['PRMSConfig']->live_site,
										'company_name' => $company['company_name']);
					$mailer = new SiteMailer();
					$mailer->toMail = $regcustomer['email'];
					$mailer->subject = 'Points are started accumulating at '.$company['company_name'].' redemption website';				
					$mailer->PrepareMail('sendPointshavebeenstartedaccumulating', $property);
					$mailer->Send();					
				}				
			}

			//Event Log
			$evtObj = new EventObject();					
			$evtObj->event_id = EventTypes::PRMS_RegisterPoints;
			$evtObj->description = Authentication::getAttribute('login_id') . ' registered points ('.$point['rewarded_points'].') for account ('.$regcustomer['nric'].').';
			$evtObj->action_by = Authentication::getAttribute('login_id');					
			EventLog::Log($evtObj);
			
			header( 'Location: savedpoints.php?nric='.$regcustomer['nric'].'&points='.$point['rewarded_points']);
		}
		else {
			array_push($error, 'Saving process failed. Please try again.');					
		}
	}
	
	
	if (!isset($point)) {		
		if (isset($_REQUEST['referenceno'])) 			
			$point['reference_no'] = $_REQUEST['referenceno'];
		else
			$point['reference_no'] =  '';
		
		if (isset($_REQUEST['salesamount'])) 			
			$point['sales_amount'] = $_REQUEST['salesamount'];
		else
			$point['sales_amount'] =  '';
			
		if (isset($_REQUEST['remarks'])) 			
			$point['remarks'] = $_REQUEST['remarks'];
		else
			$point['remarks'] =  '';
			
		if (isset($_REQUEST['store'])) 			
			$point['store_id'] = $_REQUEST['store'];
		else
			$point['store_id'] =  0;			
	}
	
?>


<?php
	if (isset($error) && count($error) > 0) {
?>
	<div class="error-info">
		<?php foreach ($error as $handle) {
				echo "<p>$handle</p>";
		} ?>
	</div>
<?php
	}
?>


<div class="SectionTitle" >Please enter required information to register the points</div>

<form name="regpointsform" id="regpointsform" action="savepoints.php?action=reg&id=<?php echo $regcustomer['customer_id']; ?>" method="post"> 
	<table class="formview" width="100%" border="0" cellspacing="3px" cellpadding="3px">			
		<tr>
			<td class="LabelCell Disabled">NRIC / FIN</td>
			<td><input id="customer_id" name="customer_id" type="hidden" value="<?php echo $regcustomer['customer_id']; ?>"/>
			<input type="text" name="nric" id="nric" maxlength="9" class="input" value="<?php echo $regcustomer['nric']; ?>" size="20" tabindex="10" disabled /></td>
		</tr>			
		<tr>
			<td class="LabelCell Disabled">Name</td>
			<td><input type="text" name="name" id="name" maxlength="9" class="input" value="<?php echo $regcustomer['name']; ?>" size="20" tabindex="20" disabled /></td>
		</tr>			
		<tr>
			<td class="LabelCell Disabled">Email</td>
			<td><input type="text" name="email" id="email" maxlength="9" class="input" value="<?php echo $regcustomer['email']; ?>" size="20" tabindex="30" disabled /></td>
		</tr>			
		<tr>
			<td class="LabelCell Disabled">Mobile</td>
			<td><input type="text" name="mobile" id="mobile" maxlength="9" class="input" value="<?php echo $regcustomer['contact_mobile']; ?>" size="20" tabindex="40" disabled /></td>
		</tr>
		
		<tr>
			<td class="LabelCell Required">Store</td>
			<td>
				<select tabindex="50" name="store" class="GreaterThanZero">						
					<?php
						$stores = $storeObj->getAssignedStores(Authentication::getUserId(), Authentication::getAttribute('su'));						
						echo '<option value="0">- Select -</option>';
						
						foreach ($stores as $store) {
							echo '<option value='.$store['store_id'].' '.($store['store_id'] == $point['store_id'] ? 'selected' : '') .' >'.$store['store_branch_name'].'</option>';
						}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td class="LabelCell Required">Reference no.</td>
			<td><input type="text" name="referenceno" id="referenceno" class="input Required" value="<?php echo $point['reference_no']; ?>" maxlength="50" size="20" tabindex="60" /></td>
		</tr>
		<tr>
			<td class="LabelCell Required">Sales amount</td>
			<td><input type="text" name="salesamount" id="salesamount" class="input Required GreaterThanZero numeric" value="" size="20" tabindex="70" /></td>
		</tr>
		<tr>
			<td class="LabelCell">Reward points</td>
			<td><input type="text" name="points" id="points" class="input" value="" size="20" tabindex="80" disabled /></td>
		</tr>
		<tr>
			<td class="LabelCell">Sale person</td>
			<td><input type="text" name="remarks" id="remarks" maxlength="1000" class="input" value="<?php echo $point['remarks']; ?>" size="20" tabindex="90" /></td>
		</tr>
		<tr>			
			<td class="BottomToolBar" colspan="2">				
				<input type="submit" name="save" id="save" class="button-primary" value="Save" tabindex="100"/>
				<a href="customersearch.php?d=register" class="button-secondary" tabindex="110">Cancel</a>				
			</td>			
		</tr>
	</table>
</form>

<script type="text/javascript">
	$(document).ready(function() { 
		
		loadValidation('regpointsform');
		
		$('#salesamount').blur(function() {
			$.ajax({
				url: 'pointcalcsvc.php',
				data: { "amount": $('#salesamount').val(), "for": "pcalsvc" }, 
				success: function(data) {				
					$('#points').val(data.value);
				},		
			});
		});		
		
		$(".numeric").numeric(false);		
	});	
</script>