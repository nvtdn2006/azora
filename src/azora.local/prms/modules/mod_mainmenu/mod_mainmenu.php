<ul id="main-menu">
<?php
	require_once dirname(__FILE__) . '/../../includes/menu.php';	
	$menu = new Menu();
	$menus = $menu->getMenuItems(Authentication::getUserId(), Authentication::getAttribute('su'));	
	$group = '';
	$cryptographer = Factory::getCryptographer();

	$feature_id = 0;
	if (Factory::getSession()->isExist('fid'))
		$feature_id = Factory::getSession()->getValue('fid');
		
	foreach ($menus as $menu) {
		if ($group != $menu['menu_group_name']) {
			if ($group != '') 
				echo '</ul></div></li>';
			echo '<li class="menu-group"><div class="menu-group-panel-outer">					
						<div class="menu-group-panel-bar">
							<span>'.$menu['menu_group_name'].'</span>
							<!--<a class="menu-group-toggle" href="#"></a>-->
						</div>
						<ul class="menu-item-list">';			
		}
		$concatChar = '?';
		if (strpos($menu['menu_url'], '?') > 0)
			$concatChar = '&';
			
		echo '<li><a href="'.$menu['menu_url'].$concatChar.'fid='.$cryptographer->Encrypt($menu['feature_id']).'"  '.($feature_id == $menu['feature_id'] ? 'class="selected"' : '').' title="'.$menu['menu_description'].'">'.$menu['menu_name'].'</a></li>';
		$group = $menu['menu_group_name'];
	}
	echo '</ul></div></li>';
?>
</ul>