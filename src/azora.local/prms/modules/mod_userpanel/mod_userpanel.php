<div id="upcontainer">
	<?php		
	
		JSManager::getInstance()->add('jquery');
		
		$cryptographer = Factory::getCryptographer();
		
		require_once dirname(__FILE__) . '/../../includes/user.php';				
		$user = new User();
		$userprofile = $user->getUserProfile(Authentication::getUserId());
		$username = '';
		if (isset($userprofile))
			$username = $userprofile['full_name'];		
	?>
	<div id="user"><ul><li><span id="username"><?php echo $username ?></span></li><li><span id="lastlogin">(Logged in <?php echo Authentication::getLoggedInDateTime() ?>)</span></li></ul></div>
	<div id="controls"><ul id="top-menu"><li>
	
		<a href="#" id="account">Account Setting</a>
		<ul>
            <li><a href="updateprofile.php?fid=<?php echo $cryptographer->Encrypt('uptpf'); ?>">Update profile</a></li>
            <li><a href="changepwd.php?fid=<?php echo $cryptographer->Encrypt('chgpwd'); ?>">Change password</a></li>            
        </ul>
	</li><li><a href="logout.php" id="logout">Logout</a></li></ul></div>
</div>

<script type="text/javascript">

	var timeout    = 500;
	var closetimer = 0;
	var ddmenuitem = 0;

	function jsddm_open()
	{  jsddm_canceltimer();
	   jsddm_close();
	   ddmenuitem = $(this).find('ul').css('visibility', 'visible');}

	function jsddm_close()
	{  if(ddmenuitem) ddmenuitem.css('visibility', 'hidden');}

	function jsddm_timer()
	{  closetimer = window.setTimeout(jsddm_close, timeout);}

	function jsddm_canceltimer()
	{  if(closetimer)
	   {  window.clearTimeout(closetimer);
		  closetimer = null;}}

	  
	$(document).ready(function() {
		$('#top-menu > li').bind('mouseover', jsddm_open);
		$('#top-menu > li').bind('mouseout',  jsddm_timer);
		
		$('#account').click(function (){
			event.preventDefault();
		});
	});
	
	document.onclick = jsddm_close;
</script>