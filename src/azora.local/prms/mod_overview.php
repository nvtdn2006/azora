<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Content of Overview'?>

<?php		
	$config = Factory::getConfig();	
	
	require_once './includes/rws_contents.php';
	$cntObj = new RWS_Contents();
	
	$error = array();
	
	if (isset($_GET['action'])) {		
		switch ($_GET['action']){
			case 'new':							
				$content = array('content' => $_POST['content'],
								'content_type' => 'Overview',								
								'created_by' => Authentication::getUserId(),								
								'updated_by' => Authentication::getUserId());								
				if ($cntObj->saveContent($content)) {					
					
					//Event Log
					$evtObj = new EventObject();					
					$evtObj->event_id = EventTypes::PRMS_UpdateOverviewContent;
					$evtObj->description = Authentication::getAttribute('login_id') . ' updated content of overview.';
					$evtObj->action_by = Authentication::getAttribute('login_id');					
					EventLog::Log($evtObj);
					
					header( 'Location: mod_overview.php?rst=1');				
				}
			break;
			case 'update':				
				$content = array('content' => $_POST['content'],
								'content_type' => 'Overview',																	
								'updated_by' => Authentication::getUserId(),
								'id' => $_GET['id']);
				if ($cntObj->updateContent($content)) {
					
					//Event Log
					$evtObj = new EventObject();					
					$evtObj->event_id = EventTypes::PRMS_UpdateOverviewContent;
					$evtObj->description = Authentication::getAttribute('login_id') . ' updated content of overview.';
					$evtObj->action_by = Authentication::getAttribute('login_id');					
					EventLog::Log($evtObj);
					
					header( 'Location: mod_overview.php?rst=1');									
				}
			break;
		}		
	}
	
	$rwscontents = $cntObj->getContent('Overview');
	
	if ((count($rwscontents) == 0) || isset($_GET['action'])) {		
		$rwscontents['id'] = 0 ;
			
		if (isset($_REQUEST['content'])) 			
			$rwscontents['content'] = $_REQUEST['content'];
		else
			$rwscontents['content'] =  '';		
	} else {
		$rwscontents = $rwscontents[0];		
	}
?>

<?php
	if (isset($error) && count($error) > 0) {
?>
	<div class="error-info form-info">
		<?php foreach ($error as $handle) {
				echo "<p>$handle</p>";
		} ?>
	</div>
<?php
	}
?>

<?php
	if (isset($_GET['rst']) && $_GET['rst'] == 1) {
?>
	<div class="success-info form-info">
		Saving process successfully done.
	</div><br/>
<?php
	}
?>

<form name="overviewform" id="overviewform" action="mod_overview.php?action=<?php echo $rwscontents['id'] > 0 ? 'update&id='.$rwscontents['id'] : 'new'; ?>" method="post"> 
	<table class="formview" width="100%" border="0" cellspacing="3px" cellpadding="3px">		
		<tr>
			<td colspan="2">
				<div style="width: 635px; height: 500px; overflow: auto;">
					<textarea name="content" id="content" class="Required" tabindex="50" style="width: 635px"><?php echo $rwscontents['content']; ?></textarea>					
				</div>				
			</td>
		</tr>			
		<tr>			
			<td class="BottomToolBar" colspan="2">				
				<input type="submit" name="submit" id="submit" class="button-primary" value="Save" tabindex="100"/>				
				<a href="rwssettings.php" class="button-secondary" tabindex="60">Cancel</a>
			</td>			
		</tr>
	</table>
</form>
<?php
	
?>
<!-- TinyMCE -->
<script type='text/javascript' src='<?php echo $config['PRMSConfig']->live_site; ?>/js/tiny_mce/tiny_mce.js'></script>

<script type="text/javascript">

	tinyMCE.init({
		// General options
		mode : "exact",
		elements : "content",
		theme : "advanced",
		plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave",

		// Theme options
		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr",
		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
		theme_advanced_buttons5 : ",|,link,unlink,anchor,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor,|,print,|,ltr,rtl,|,fullscreen",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : false,
		editor_content_width : 500,
		width: "635",
		height: "480",

	});
	
</script>
<!-- /TinyMCE -->