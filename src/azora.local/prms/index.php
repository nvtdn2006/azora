<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Dashboard'?>

<?php
	
	DomainManager::getInstance()->load('Point');
	$pointObj = new Point();
	
	$purchasehistory = $pointObj->getPurchaseHistory(3);
	
	$pointhistory = $pointObj->getPointsTransaction(3);
	
	DomainManager::getInstance()->load('Redemption');
	$redemptionObj = new Redemption();
	
	DomainManager::getInstance()->load('Store');
	$storeObj = new Store();
	
	$assignedStores = $storeObj->getAssignedStores(Authentication::getUserId(), Authentication::getAttribute('su'));
	
	$store_ids = '';
	$sp = '';
	foreach($assignedStores as $as){
		$store_ids .= $sp . $as['store_id'];
		$sp = ',';
	}
	$criteria = array('store_id' => $store_ids);
	$criteria['redemption_status'] = 1;
	$redemptions = $redemptionObj->getRedemptionInfo($criteria , 5);
	
?>

<div class="SectionTitle" >Recent customer purchases</div>

<table class="tabular">
<thead>
	<tr>			
		<th>Branch</th>
		<th>Store</th>
		<th>Reference No.</th>
		<th>Purchase Amount</th>
		<th>Reward Points</th>
		<th>Transaction On</th>			
	</tr>
</thead>
<tbody>
	<?php
		if ($purchasehistory != null) {
		foreach($purchasehistory as $history) {
	?>
	<tr>						
		<td><?php echo $history['branch_name'] ?></td>
		<td><?php echo $history['store_name'] ?></td>
		<td><?php echo $history['reference_no'] ?></td>
		<td><?php echo $history['sales_amount'] ?></td>
		<td><?php echo $history['trans_points'] ?></td>			
		<td><?php echo $history['transacted_date'] ?></td>			
	</tr>
	<?php } } ?>
</tbody>
</table>

<br/><br/>
<div class="SectionTitle" >Recent point transactions</div>

<table class="tabular">
	<thead>
		<tr>			
			<th>Branch</th>
			<th>Store</th>
			<th>Reference No.</th>
			<th>Purchase Amount</th>
			<th>Points</th>
			<th>Transaction On</th>			
			<th>Type</th>			
		</tr>
	</thead>
	<tbody>
		<?php
			if ($purchasehistory != null) {
			foreach($purchasehistory as $history) {
		?>
		<tr>						
			<td><?php echo $history['branch_name']; ?></td>
			<td><?php echo $history['store_name']; ?></td>
			<td><?php echo $history['reference_no']; ?></td>
			<td><?php echo $history['sales_amount']; ?></td>
			<td><?php echo $history['trans_points']; //($history['transaction_type'] == 1 ? '' : '-') .  ?></td>			
			<td><?php echo $history['transacted_date']; ?></td>			
			<td><?php echo $history['transaction_type'] == 1 ? 'Accumulation' : 'Cancellation'; ?></td>			
		</tr>
		<?php } } ?>
	</tbody>
</table>

<br/><br/>
<div class="SectionTitle" >Recent submitted redemption</div>

<table class="tabular">
	<thead>
		<tr>			
			<th>NRIC</th>
			<th>Name</th>
			<th>Collection</th>
			<th>Store</th>
			<th>Points</th>			
			<th>Submitted</th>
		</tr>
	</thead>
	<tbody>
		<?php
			foreach($redemptions as $redemption) {
		?>
		<tr>			
			<td><a class="detailLink" href="redemptiondetail.php?m=<?php echo Factory::getCryptographer()->Encrypt('write'); ?>&id=<?php echo $redemption['redemption_id']; ?>" alt="Select" title="Select"><?php echo $redemption['nric']; ?></a></td>
			<td><?php echo $redemption['name']; ?></td>
			<td><?php echo $redemption['collection_date'] . ' ' .  date('h:i:s  A', strtotime($redemption['collection_time'])); ?></td>
			<td><?php echo $redemption['store_branch_name']; ?></td>
			<td><?php echo $redemption['total_points']; ?></td>			
			<td><?php echo date('Y-m-d h:i:s  A', strtotime($redemption['transaction_dt'])); ?></td>
		</tr>
		<?php } ?>
	</tbody>
</table>