<?php
/*
 * 09-05-2013 FX.1305.002 Add Redeem
 */
require_once './includes/application.php';
$this->template = '';
$this->title = 'Search customer'
?>

<?php
JSManager::getInstance()->add('jquery');
JSManager::getInstance()->add('validation');
JSManager::getInstance()->add('numeric');

DomainManager::getInstance()->load('Customer');
DomainManager::getInstance()->load('Store');

$customerObj = new Customer();
$storeObj = new Store();
$cutomers = null;
$formvalues = null;

if (isset($_POST['search']) && $_POST['search'] == 'Search') {

    $customer_kv = array();

    if (isset($_POST['nric']) && $_POST['nric'] != '') {
        $customer_kv['nric'] = $_POST['nric'];
    }

    if (isset($_POST['name']) && $_POST['name'] != '') {
        $customer_kv['name'] = $_POST['name'];
    }

    if (isset($_POST['email']) && $_POST['email'] != '') {
        $customer_kv['email'] = $_POST['email'];
    }

    if (isset($_POST['company']) && $_POST['company'] != '') {
        $customer_kv['company'] = $_POST['company'];
    }

    if (isset($_POST['contact_mobile']) && $_POST['contact_mobile'] != '') {
        $customer_kv['contact_mobile'] = $_POST['contact_mobile'];
    }

    if (count($customer_kv) > 0) {
        $cutomers = $customerObj->getCustomerProfileByValues($customer_kv);
    }
}


if (isset($_REQUEST['nric']))
    $formvalues['nric'] = $_REQUEST['nric'];
else
    $formvalues['nric'] = '';

if (isset($_REQUEST['name']))
    $formvalues['name'] = $_REQUEST['name'];
else
    $formvalues['name'] = '';

if (isset($_REQUEST['email']))
    $formvalues['email'] = $_REQUEST['email'];
else
    $formvalues['email'] = '';

if (isset($_REQUEST['company']))
    $formvalues['company'] = $_REQUEST['company'];
else
    $formvalues['company'] = '';

if (isset($_REQUEST['contact_mobile']))
    $formvalues['contact_mobile'] = $_REQUEST['contact_mobile'];
else
    $formvalues['contact_mobile'] = '';


$destination = 'savepoints.php?action=reg';
if (isset($_POST['destination'])) {
    $destination = $_POST['destination'];
} else {
    if (isset($_GET['d']) && $_GET['d'] == 'register')
        $destination = 'savepoints.php?action=reg';

    if (isset($_GET['d']) && $_GET['d'] == 'cancel')
        $destination = 'cancelpoints.php?action=cancel';

    if (isset($_GET['d']) && $_GET['d'] == 'custpurrpt')
        $destination = 'custpurrpt.php?action=custpurrpt';

    if (isset($_GET['d']) && $_GET['d'] == 'pttrans')
        $destination = 'pttrans.php?action=pttrans';

    if (isset($_GET['d']) && $_GET['d'] == 'ptexpired')
        $destination = 'ptexpired.php?action=ptexpired';
    // FX.1305.002 Add Redeem Page 
    if (isset($_GET['d']) && $_GET['d'] == 'redeem')
        $destination = 'redeem.php?action=redeem';
}
?>

<form name="customersearchform" id="customersearchform" action="customersearch.php?action=search" method="post"> 
    <table class="formview" width="100%" border="0">		
        <tr>
            <td>NRIC</td>		
            <td><input type="text" name="nric" id="nric" class="input" value="<?php echo $formvalues['nric']; ?>" size="20" tabindex="10" /></td>			
            <td>Name</td>		
            <td><input type="text" name="name" id="name" class="input" value="<?php echo $formvalues['name']; ?>" size="20" tabindex="20" /></td>			
        </tr>		
        <tr>
            <td>Email</td>		
            <td><input type="text" name="email" id="email" class="input" value="<?php echo $formvalues['email']; ?>" size="20" tabindex="30" /></td>	
            <td>Company</td>		
            <td><input type="text" name="company" id="company" class="input" value="<?php echo $formvalues['company']; ?>" size="20" tabindex="40" /></td>						
        </tr>		
        <tr>
            <td>Mobile</td>		
            <td><input type="text" name="contact_mobile" id="contact_mobile" class="input" value="<?php echo $formvalues['contact_mobile']; ?>" size="20" tabindex="50" /></td>	
            <td>&nbsp;</td>		
            <td>&nbsp;</td>						
        </tr>
        <tr>			
            <td colspan="4">
                <input type="hidden" name="destination" id="destination" value="<?php echo $destination; ?>">
                <input type="submit" name="search" id="search" class="button-primary" value="Search" tabindex="50"/>				
            </td>			
        </tr>
    </table>
</form>

<?php if (isset($_POST['search']) && $_POST['search'] == 'Search') { ?>
    <div class="SectionTitle" ><b><?php echo ($cutomers != null && count($cutomers) > 0) ? count($cutomers) : '0'; ?> </b> Record(s) found.</div>
    <?php
    if ($cutomers != null && count($cutomers) > 0) {
        ?>
        <table class="tabular">
            <thead>
                <tr>			
                    <th>NRIC</th>
                    <th>Name</th>
                    <th>Company</th>
                    <th>Mobile</th>
                    <th>Email</th>			
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($cutomers as $cutomer) {
                    ?>
                    <tr>			
                        <td><a class="detailLink" href="<?php echo $destination; ?>&id=<?php echo $cutomer['customer_id'] ?>" alt="Select" title="Select"><?php echo $cutomer['nric'] ?></a></td>
                        <td><?php echo $cutomer['name'] ?></td>
                        <td><?php echo $cutomer['company'] ?></td>
                        <td><?php echo $cutomer['contact_mobile'] ?></td>
                        <td><?php echo $cutomer['email'] ?></td>			
                    </tr>
        <?php } ?>
            </tbody>
        </table>

    <?php
    }
}
?>