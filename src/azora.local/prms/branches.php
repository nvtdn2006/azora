<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Manage branches'?>

<?php		

	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('checkboxlist');
	JSManager::getInstance()->add('messagebox');
	
	DomainManager::getInstance()->load('Branch');
	$branchObj = new Branch();
	
	$error = array();
	
	if (isset($_POST['delete']) && $_POST['delete'] == 'Remove') {					
		$branches = null;
		if (isset($_POST['branches']))
			$branches = $_POST['branches'];
		if (isset($branches) && count($branches) > 0) {
		
			$_names = '';
			$sp = '';
			foreach($branches as $item) {
				$nobj = $branchObj->getBranch($item);
				$_names .= $sp . $nobj['branch_name'];
				$sp = ', ';
			}
			
			if ($branchObj->deleteBranch($branches)) {
			
				//Event Log
				$evtObj = new EventObject();					
				$evtObj->event_id = EventTypes::PRMS_DeleteBranch;
				$evtObj->description = Authentication::getAttribute('login_id') . ' deleted branches ('.$_names.').';
				$evtObj->action_by = Authentication::getAttribute('login_id');					
				EventLog::Log($evtObj);
				
				header( 'Location: branches.php');
				
			}
		} else {
			array_push($error, 'Please select at least one item to proceed.');	
		}
	}
	
	$branches = $branchObj->getAllBranches();
?>

<?php
	if (isset($error) && count($error) > 0) {
?>
	<div class="error-info tabular-info">
		<?php foreach ($error as $handle) {
				echo "<p>$handle</p>";
		} ?>
	</div>
<?php
	}
?>

<form name="branchlist" id="branchlist" action="branches.php" method="post"> 
	<table class="tabular">
		<thead>
			<tr>			
				<th><input type="checkbox" name="listselectall" class="listselectall" onclick="javascript:checkSelectAllBox('listselect', this.checked);" /></th>
				<th>Branch</th>
				<th>Description</th>
				<th>Updated By</th>
				<th>Updated On</th>			
			</tr>
		</thead>
		<tfoot>
			<tr>
				<th colspan="5"><a class="button-primary" href="branch-edit.php">New</a>
				<input type="submit" name="delete" id="submit" class="button-secondary" value="Remove" onclick="javascript:return confirmBox('Are you sure you want to delete selected item?');"/></th>		
			</tr>
		</tfoot>
		<tbody>		
			<?php
				foreach($branches as $branch) {
			?>
			<tr>
				<td><input type="checkbox" name="branches[]" class="listselect" onclick="javascript:checkSelectBox('listselectall', 'listselect');" value="<?php echo $branch['branch_id']; ?>" /></td>
				<td><a class="detailLink" href="<?php echo 'branch-edit.php?branch='.$branch['branch_id'] ?>" alt="Edit" title="Edit"><?php echo $branch['branch_name'] ?></a></td>
				<td><?php echo $branch['branch_description'] ?></td>
				<td><?php echo $branch['updated_username'] ?></td>
				<td><?php echo $branch['updated_dt'] ?></td>				
			</tr>
			<?php } ?>
		</tbody>
	</table>
</form>
<?php
	$branchObj = null;
?>