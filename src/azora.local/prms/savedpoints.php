<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Points saved'; ?>

<?php
	$points = 0;
	$nric = '';
	if (isset($_GET['nric'])) {
		$nric = $_GET['nric'];
	}
	
	if (isset($_GET['points'])) {
		$points = $_GET['points'];
	}
?>	
 <div class="cprocess">
	Saving process successfully done.<br/><br/><br/><br/>
	Points <b><?php echo $points; ?></b> have been accumulated to account <b><?php echo $nric; ?></b>.
	<br/><br/><br/><br/><br/><br/>
	<a href="customersearch.php?d=register" class="button-secondary" title="Back to register points.">Done</a>
</div>