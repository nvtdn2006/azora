<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Manage roles'?>

<?php

	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('checkboxlist');
	JSManager::getInstance()->add('messagebox');
	
	require_once './includes/role.php';
	$roleObj = new Role();
	
	$error = array();

	if (isset($_POST['delete']) && $_POST['delete'] == 'Remove') {
		$roles = null;
		if (isset($_POST['roles']))
			$roles = $_POST['roles'];	
		if (isset($roles) && count($roles) > 0) {
		
			$_names = '';
			$sp = '';
			foreach($roles as $item) {
				$nobj = $roleObj->getRole($item);
				$_names .= $sp . $nobj['role_name'];
				$sp = ', ';
			}
		
			if ($roleObj->deleteRole($roles)) {		
				
				//Event Log
				$evtObj = new EventObject();					
				$evtObj->event_id = EventTypes::PRMS_DeleteRole;
				$evtObj->description = Authentication::getAttribute('login_id') . ' deleted roles ('.$_names.').';
				$evtObj->action_by = Authentication::getAttribute('login_id');					
				EventLog::Log($evtObj);
				
				header( 'Location: roles.php');
			}
		} else {
			array_push($error, 'Please select at least one item to proceed.');	
		}
	}
	
	$roles = $roleObj->getAllRoles();
?>

<?php
	if (isset($error) && count($error) > 0) {
?>
	<div class="error-info tabular-info">
		<?php foreach ($error as $handle) {
				echo "<p>$handle</p>";
		} ?>
	</div>
<?php
	}
?>

<form name="rolelist" id="rolelist" action="roles.php" method="post"> 
	<table class="tabular">
		<thead>
			<tr>
				<th><input type="checkbox" name="listselectall" class="listselectall" onclick="javascript:checkSelectAllBox('listselect', this.checked);"/></th>
				<th>Role</th>
				<th style="width: 200px;">Description</th>
				<th>Updated By</th>
				<th>Updated On</th>			
			</tr>
		</thead>
		<tfoot>
			<tr>			
				<th colspan="5"><a class="button-primary" href="role-edit.php">New</a>
				<input type="submit" name="delete" id="submit" class="button-secondary" value="Remove" onclick="javascript:return confirmBox('Are you sure you want to delete selected item?');"/></th>				
			</tr>
		</tfoot>
		<tbody>		
			<?php
				foreach($roles as $role) {
			?>
			<tr>
				<td><?php echo $role['role_is_sys'] ? '<input type="checkbox" name="roles_disabled" disabled />' : '<input type="checkbox" name="roles[]" class="listselect" onclick="javascript:checkSelectBox(\'listselectall\', \'listselect\');" value="'.$role['role_id'].'"/>'; ?> 
				</td>
				<td><?php echo $role['role_is_sys'] ? '<span>'.$role['role_name'].'</span>' :
				'<a class="detailLink" href="role-edit.php?role='.$role['role_id'].'" alt="Edit" title="Edit">'.$role['role_name'].'</a>'; ?></td>
				<td><?php echo $role['role_description'] ?></td>
				<td><?php echo $role['role_updated_username'] ?></td>
				<td><?php echo $role['role_updated_dt'] ?></td>				
			</tr>
			<?php } ?>
		</tbody>
	</table>
</form>
<?php
	$roleObj = null;
?>