<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Company Information'?>

<?php		
	$config = Factory::getConfig();	

	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('validation');
	
	DomainManager::getInstance()->load('Company');
	$companyObj = new Company();
	
	$error = array();
	
	
	if (isset($_GET['action'])) {		
		switch ($_GET['action']){
			case 'new':							
				$company = array('company_name' => $_POST['company_name'],
								'company_description' => $_POST['company_description'],	
								'company_address' => $_POST['company_address'],	
								'company_phone' => $_POST['company_phone'],	
								'company_fax' => $_POST['company_fax'],	
								'company_email' => $_POST['company_email'],
								'company_reg_email' => $_POST['company_reg_email'],
								'company_contactus_email' => $_POST['company_contactus_email'],
								'company_customersupport_email' => $_POST['company_customersupport_email'],
								'created_by' => Authentication::getUserId(),								
								'updated_by' => Authentication::getUserId());								
				if ($companyObj->saveCompany($company)) {	

					//Event Log
					$evtObj = new EventObject();					
					$evtObj->event_id = EventTypes::PRMS_UpdateCompanyInfo;
					$evtObj->description = Authentication::getAttribute('login_id') . ' updated company information.';
					$evtObj->action_by = Authentication::getAttribute('login_id');					
					EventLog::Log($evtObj);
				
					header( 'Location: companyinfo.php?rst=1');				
				}
			break;
			case 'update':				
				$company = array('company_name' => $_POST['company_name'],
								'company_description' => $_POST['company_description'],	
								'company_address' => $_POST['company_address'],	
								'company_phone' => $_POST['company_phone'],	
								'company_fax' => $_POST['company_fax'],	
								'company_email' => $_POST['company_email'],
								'company_reg_email' => $_POST['company_reg_email'],
								'company_contactus_email' => $_POST['company_contactus_email'],															
								'company_customersupport_email' => $_POST['company_customersupport_email'],															
								'updated_by' => Authentication::getUserId(),
								'id' => $_GET['id']);
				if ($companyObj->updateCompany($company)) {

					//Event Log
					$evtObj = new EventObject();					
					$evtObj->event_id = EventTypes::PRMS_UpdateCompanyInfo;
					$evtObj->description = Authentication::getAttribute('login_id') . ' updated company information.';
					$evtObj->action_by = Authentication::getAttribute('login_id');					
					EventLog::Log($evtObj);
				
					header( 'Location: companyinfo.php?rst=1');									
				}
			break;
		}		
	}
	
	$company = $companyObj->getCompany();
	
	if ((count($company) == 0) || isset($_GET['action'])) {		
		$company['id'] = 0 ;
		
		if (isset($_REQUEST['company_name'])) 			
			$company['company_name'] = $_REQUEST['company_name'];
		else
			$company['company_name'] =  '';
			
		if (isset($_REQUEST['company_description'])) 			
			$company['company_description'] = $_REQUEST['company_description'];
		else
			$company['company_description'] =  '';
			
		if (isset($_REQUEST['company_address'])) 			
			$company['company_address'] = $_REQUEST['company_address'];
		else
			$company['company_address'] =  '';
			
		if (isset($_REQUEST['company_phone'])) 			
			$company['company_phone'] = $_REQUEST['company_phone'];
		else
			$company['company_phone'] =  '';
			
		if (isset($_REQUEST['company_fax'])) 			
			$company['company_fax'] = $_REQUEST['company_fax'];
		else
			$company['company_fax'] =  '';

		if (isset($_REQUEST['company_email'])) 			
			$company['company_email'] = $_REQUEST['company_email'];
		else
			$company['company_email'] =  '';
			
		if (isset($_REQUEST['company_reg_email'])) 			
			$company['company_reg_email'] = $_REQUEST['company_reg_email'];
		else
			$company['company_reg_email'] =  '';
			
		if (isset($_REQUEST['company_contactus_email'])) 			
			$company['company_contactus_email'] = $_REQUEST['company_contactus_email'];
		else
			$company['company_contactus_email'] =  '';
			
		if (isset($_REQUEST['company_customersupport_email'])) 			
			$company['company_customersupport_email'] = $_REQUEST['company_customersupport_email'];
		else
			$company['company_customersupport_email'] =  '';


	} else {
		$company = $company[0];
	}
?>

<?php
	if (isset($error) && count($error) > 0) {
?>
	<div class="error-info form-info">
		<?php foreach ($error as $handle) {
				echo "<p>$handle</p>";
		} ?>
	</div>
<?php
	}
?>

<?php
	if (isset($_GET['rst']) && $_GET['rst'] == 1) {
?>
	<div class="success-info form-info">
		Saving process successfully done.
	</div>
<?php
	}
?>

<form name="companyinfoform" id="companyinfoform" action="companyinfo.php?action=<?php echo $company['id'] > 0 ? 'update&id='.$company['id'] : 'new'; ?>" method="post"> 
	<table class="formview" width="100%" border="0" cellspacing="3px" cellpadding="3px">
		<tr>
			<td class="LabelCell Required">Name</td>
			<td><input type="text" name="company_name" id="company_name" class="input Required " value="<?php echo $company['company_name']; ?>" size="20" tabindex="10" /></td>
		</tr>
		<tr>
			<td class="LabelCell Required">Description</td>
			<td><input type="text" name="company_description" id="company_description" class="input Required " value="<?php echo $company['company_description']; ?>" size="20" tabindex="20" /></td>
		</tr>
		<tr>
			<td class="LabelCell Required">Address</td>
			<td><input type="text" name="company_address" id="company_address" class="input Required " value="<?php echo $company['company_address']; ?>" size="20" tabindex="30" /></td>
		</tr>
		<tr>
			<td class="LabelCell">Phone</td>
			<td><input type="text" name="company_phone" id="company_phone" class="input" value="<?php echo $company['company_phone']; ?>" size="20" tabindex="40" /></td>
		</tr>
		<tr>
			<td class="LabelCell">Fax</td>
			<td><input type="text" name="company_fax" id="company_fax" class="input" value="<?php echo $company['company_fax']; ?>" size="20" tabindex="50" /></td>
		</tr>
		<tr>
			<td class="LabelCell Required">Email</td>
			<td><input type="text" name="company_email" id="company_email" class="input Required ValidEmail" value="<?php echo $company['company_email']; ?>" size="20" tabindex="60" /></td>
		</tr>
		<tr>			
			<td class="SectionBar" colspan="2">				
				Email addresses <i>(purpose for support)</i>
			</td>
		</tr>
		<tr>
			<td class="LabelCell Required">Registration</td>
			<td><input type="text" name="company_reg_email" id="company_reg_email" class="input Required ValidEmail" value="<?php echo $company['company_reg_email']; ?>" size="20" tabindex="70" /></td>
		</tr>		
		<tr>
			<td class="LabelCell Required">Contact us</td>
			<td><input type="text" name="company_contactus_email" id="company_contactus_email" class="input Required ValidEmail" value="<?php echo $company['company_contactus_email']; ?>" size="20" tabindex="80" /></td>
		</tr>		
		<tr>
			<td class="LabelCell Required">Customer support</td>
			<td><input type="text" name="company_customersupport_email" id="company_customersupport_email" class="input Required ValidEmail" value="<?php echo $company['company_customersupport_email']; ?>" size="20" tabindex="90" /></td>
		</tr>
		<tr>			
			<td class="BottomToolBar" colspan="2">				
				<input type="submit" name="submit" id="submit" class="button-primary" value="Save" tabindex="100"/>				
			</td>			
		</tr>
	</table>
</form>

<script type="text/javascript">
	$(document).ready(function() { 
		loadValidation('companyinfoform');						
	});		
</script>