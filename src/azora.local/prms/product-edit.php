<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'New product'?>

<?php
	
	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('validation');		
	JSManager::getInstance()->add('fancybox');	
	CSSManager::getInstance()->add('/js/fancybox/jquery.fancybox-1.3.4.css');
	JSManager::getInstance()->add('numeric');	
	
	DomainManager::getInstance()->load('Product');
	DomainManager::getInstance()->load('Category');
	$productObj = new Product();	
	$categoryObj = new Category();
	
	$product_id = 0;
	$product = null;
	
	$error = array();
	
	$imagetypeName = array('product_thumbnailimage' => 'Thumbnail', 'product_smallimage' => 'Small', 'product_largeimage' => 'Large');
	$imagetypeKey = array('product_thumbnailimage' => 'T', 'product_smallimage' => 'S', 'product_largeimage' => 'L');
	
	if (isset($_GET['product'])) {		
		$product_id = $_GET['product'];
		$product = $productObj->getProduct($product_id);	
		$this->title = 'Update product';
	}
	
	if (isset($_GET['action'])) {		
		switch ($_GET['action']){
			case 'new':
			
				$isValid = true;
				
				if ($productObj->isExistProductCode($_POST['product_code'], 0)) {				
					$isValid = false;
					array_push($error, 'Product code already exists in system.');	
				}
				
				if (isset($_FILES)) {										
					foreach ($_FILES as $key => $file) {				
						$filecheck = basename($file['name']);
						if ($filecheck != '') {
							$ext = strtolower(substr($filecheck, strrpos($filecheck, '.') + 1));

							if (!(($ext == "jpg" || $ext == "gif" || $ext == "png") && ($file["type"] == "image/jpeg" || $file["type"] == "image/gif" || $file["type"] == "image/png") && 
								($file["size"] < 2120000))) {
								$isValid = false;
								array_push($error, $imagetypeName[$key] . ' image : jpg, gif, png file types are only allowed to upload. File size must be less than 2MB.');	
							}
						}
					}
				}
				
				if ($isValid) {				
					
					$product = array();
					
					$product_info = array('product_code' => $_POST['product_code'],
											'product_name' => $_POST['product_name'],
											'product_description' => $_POST['product_description'],
											'product_status' => $_POST['product_status'],
											'product_cost' => $_POST['product_cost'],
											'product_points' => $_POST['product_points'],
											'created_by' => Authentication::getUserId(),								
											'updated_by' => Authentication::getUserId());	
					$product['product'] = $product_info;
					
					$product_balance = array('product_balance' => $_POST['product_balance']);	
					$product['product_balance'] = $product_balance;
					
					$categories = array();
					$category = array();			
					foreach ($_POST['product_category'] as $handle) {
						$category['category_id'] = $handle;
						array_push($categories, $category);
					}
					$product['product_categories'] = $categories;
					
					$images = array();
					$image = array();			
					foreach ($_FILES as $key => $file) {
						if ($file['name'] != '') {
							$image['image_name'] = $file['name'];
							$image['image_type'] = $imagetypeKey[$key];
							
							$sys_file_name = date('YmdHisu') . '_' . $file['name'];							
							move_uploaded_file($file["tmp_name"], '../domain/images/products/'.$sys_file_name);
							
							$image['sys_file_name'] = $sys_file_name;
							array_push($images, $image);
						}
					}
					$product['product_images'] = $images;
					
					if ($productObj->saveProduct($product)) {
					
						//Event Log
						$evtObj = new EventObject();					
						$evtObj->event_id = EventTypes::PRMS_AddProduct;
						$evtObj->description = Authentication::getAttribute('login_id') . ' added new product ('.$_POST['product_code'].').';
						$evtObj->action_by = Authentication::getAttribute('login_id');					
						EventLog::Log($evtObj);
					
						header( 'Location: products.php');					
					} else {
						//If failed, delete images here.
					}
				}				
				
			break;
			case 'update':				
			
				$isValid = true;
				
				if ($productObj->isExistProductCode($_POST['product_code'], $product_id)) {				
					$isValid = false;
					array_push($error, 'Product code already exists in system.');	
				}
				
				if (isset($_FILES)) {										
					foreach ($_FILES as $key => $file) {				
						$filecheck = basename($file['name']);
						if ($filecheck != '') {
							$ext = strtolower(substr($filecheck, strrpos($filecheck, '.') + 1));

							if (!(($ext == "jpg" || $ext == "gif" || $ext == "png") && ($file["type"] == "image/jpeg" || $file["type"] == "image/gif" || $file["type"] == "image/png") && 
								($file["size"] < 2120000))) {
								$isValid = false;
								array_push($error, $imagetypeName[$key] . ' image : jpg, gif, png file types are only allowed to upload. File size must be less than 2MB.');	
							}
						}
					}
				}
								
				if ($isValid) {	
				
					$product = array();
						
					$product_info = array('product_code' => $_POST['product_code'],
											'product_name' => $_POST['product_name'],
											'product_description' => $_POST['product_description'],
											'product_status' => $_POST['product_status'],
											'product_cost' => $_POST['product_cost'],
											'product_points' => $_POST['product_points'],
											'updated_by' => Authentication::getUserId(),
											'product_id' => $product_id);	
					$product['product'] = $product_info;
					
					$product_balance = array('product_balance' => $_POST['product_balance']);	
					$product['product_balance'] = $product_balance;
					
					$categories = array();
					$category = array();			
					foreach ($_POST['product_category'] as $handle) {
						$category['category_id'] = $handle;
						array_push($categories, $category);
					}
					$product['product_categories'] = $categories;
					
					if ($_POST['fileRemoved'] != '') {
						$removedFiles = explode(',', $_POST['fileRemoved']);
						$removedImages = array();
						foreach($removedFiles as $removedFile) {
							if ($removedFile != '')
								array_push($removedImages, $imagetypeKey[$removedFile]);
						}						
						$product['product_imagesTBD'] = $removedImages;
					}
					
					$images = array();
					$image = array();			
					foreach ($_FILES as $key => $file) {
						if ($file['name'] != '') {
							$image['image_name'] = $file['name'];
							$image['image_type'] = $imagetypeKey[$key];
							
							$sys_file_name = date('YmdHisu') . '_' . $file['name'];							
							move_uploaded_file($file["tmp_name"], '../domain/images/products/'.$sys_file_name);
							
							$image['sys_file_name'] = $sys_file_name;
							array_push($images, $image);
						}
					}
					$product['product_images'] = $images;
									
					if ($productObj->updateProduct($product)) {
					
						//Event Log
						$evtObj = new EventObject();					
						$evtObj->event_id = EventTypes::PRMS_UpdateProduct;
						$evtObj->description = Authentication::getAttribute('login_id') . ' updated product ('.$_POST['product_code'].').';
						$evtObj->action_by = Authentication::getAttribute('login_id');					
						EventLog::Log($evtObj);
					
						header( 'Location: products.php');					
					} else {
						//If failed, delete images here.
					}
				}
				
			break;
		}		
	}
	
	if (!isset($product) || isset($_GET['action'])) {		
		if (isset($_REQUEST['product_code'])) 			
			$product['product']['product_code'] = $_REQUEST['product_code'];
		else
			$product['product']['product_code'] =  '';
			
		if (isset($_REQUEST['product_name'])) 			
			$product['product']['product_name'] = $_REQUEST['product_name'];
		else
			$product['product']['product_name'] =  '';
			
		if (isset($_REQUEST['product_description'])) 			
			$product['product']['product_description'] = $_REQUEST['product_description'];
		else
			$product['product']['product_description'] =  '';
		
		if (isset($_REQUEST['product_status'])) 			
			$product['product']['product_status'] = $_REQUEST['product_status'];
		else
			$product['product']['product_status'] =  1;
			
		if (isset($_REQUEST['product_cost'])) 			
			$product['product']['product_cost'] = $_REQUEST['product_cost'];
		else
			$product['product']['product_cost'] =  '';
			
		if (isset($_REQUEST['product_points'])) 			
			$product['product']['product_points'] = $_REQUEST['product_points'];
		else
			$product['product']['product_points'] =  '';
			
		if (isset($_REQUEST['product_balance'])) 			
			$product['product_balance']['product_balance'] = $_REQUEST['product_balance'];
		else
			$product['product_balance']['product_balance'] =  '';
			
		if (isset($_REQUEST['product_category'])) {			
			$categories = array();
			$category = array();			
			foreach ($_REQUEST['product_category'] as $handle) {
				$category['category_id'] = $handle;
				array_push($categories, $category);
			}

			$product['product_categories'] = $categories;
		}
		
		$product['product_images'] = array();
	}
		
?>


<?php
	if (isset($error) && count($error) > 0) {
?>
	<div class="error-info form-info">
		<?php foreach ($error as $handle) {
				echo "<p>$handle</p>";
		} ?>
	</div>
<?php
	}
?>

<form name="productform" id="productform" action="product-edit.php?action=<?php echo $product_id > 0 ? 'update&product='.$product_id : 'new'; ?>" method="post" enctype="multipart/form-data"> 
	<table class="formview" width="100%" border="0" cellspacing="3px" cellpadding="3px">
		<tr>
			<td class="LabelCell Required">Code</td>
			<td><input type="text" name="product_code" id="product_code" maxlength="6" class="input Required" value="<?php echo $product['product']['product_code']; ?>" size="20" tabindex="10" /></td>
		</tr>
		<tr>
			<td class="LabelCell Required">Name</td>
			<td><input type="text" name="product_name" id="product_name" maxlength="50" class="input Required" value="<?php echo $product['product']['product_name']; ?>" size="20" tabindex="20" /></td>
		</tr>
		<tr>
			<td class="LabelCell">Description</td>
			<td><input type="text" name="product_description" id="product_description" maxlength="255" class="input" value="<?php echo $product['product']['product_description']; ?>" size="20" tabindex="30" /></td>
		</tr>
		<tr>
			<td class="LabelCell">Status</td>
			<td><input type="radio" name="product_status" value="1" tabindex="40" <?php echo $product['product']['product_status'] ? 'checked' : ''; ?>/> Active 
			<input type="radio" name="product_status" value="0" tabindex="40" <?php echo $product['product']['product_status'] ? '' : 'checked'; ?>/> In-active</td>
		</tr>
		<tr>
			<td class="LabelCell Required">Cost in dollars</td>
			<td><input type="text" name="product_cost" id="product_cost" class="input Required GreaterThanOrEqualZero numeric" value="<?php echo $product['product']['product_cost']; ?>" size="20" tabindex="50" /></td>
		</tr>
		<tr>			
			<td class="SectionBar" colspan="2">				
				Points to be redeemed
			</td>
		</tr>
		<tr>
			<td class="LabelCell Required">Points</td>
			<td><input type="text" name="product_points" id="product_points" class="input Required GreaterThanOrEqualZero numeric" value="<?php echo $product['product']['product_points']; ?>" size="20" tabindex="60" /></td>
		</tr>
		<tr>			
			<td class="SectionBar" colspan="2">				
				Product Balance
			</td>
		</tr>
		<tr>
			<td class="LabelCell Required">Balance</td>
			<td><input type="text" name="product_balance" id="product_balance" class="input Required GreaterThanOrEqualZero numeric" value="<?php echo $product['product_balance']['product_balance']; ?>" size="20" tabindex="70" /></td>
		</tr>
		<tr>			
			<td class="SectionBar" colspan="2">				
				Product Categories
			</td>
		</tr>
		<tr>
			<td class="LabelCell">Categories</td>
			<td>
				<?php					
					$categories = $categoryObj->getAllCategories();
					foreach ($categories as $category) {												
						$checked = '';
						if (isset($product['product_categories']))
						{
							foreach ($product['product_categories'] as $productcategory) {
								if ($productcategory['category_id'] == $category['category_id']) {
									$checked = 'checked';
									break;
								}								
							}
						}							
						echo '<input type="checkbox" name="product_category[]" value='.$category['category_id'].' tabindex="80" '.$checked.'/>'.$category['category_name'].'<br/>';
					}
				?>				
			</td>
		</tr>
		<tr>			
			<td class="SectionBar" colspan="2">				
				Product Images
			</td>
		</tr>
		<tr>
			<td class="LabelCell Required">Thumbnail (155x155)</td>
			<td>
				<?php
					$display = '';
					foreach ($product['product_images'] as $productimage) {
						if ($productimage['image_type'] == 'T') {
							echo '<div><a class="fancybox" href="../domain/images/products/'.$productimage['sys_file_name'].'">'.$productimage['image_name'].'</a>
							&nbsp;[&nbsp;<a href="#div_product_thumbnailimage" class="imagechange">Change</a>&nbsp;&#8226;&nbsp;<a href="#div_product_thumbnailimage" class="imageremove">Remove</a>]<br/><br/></div>';
							$display = 'style="display:none;"';
							break;
						}								
					}
				?>
				<div id="div_product_thumbnailimage" <?php echo $display; ?>>
					<span id="product_thumbnailimage_file">
						<input id="product_thumbnailimage" name="product_thumbnailimage" type="file" tabindex="90" class="Required" <?php echo $display; ?> />
					</span> [&nbsp;<a href="#product_thumbnailimage_file" class="imageclear" >Clear</a>&nbsp;]
				</div>
			</td>
		</tr>
		<tr>
			<td class="LabelCell">Small  (50x50)</td>
			<td>
				<?php
					$display = '';
					foreach ($product['product_images'] as $productimage) {
						if ($productimage['image_type'] == 'S') {
							echo '<div><a class="fancybox" href="../domain/images/products/'.$productimage['sys_file_name'].'">'.$productimage['image_name'].'</a>
							&nbsp;[&nbsp;<a href="#div_product_smallimage" class="imagechange">Change</a>&nbsp;&#8226;&nbsp;<a href="#div_product_smallimage" class="imageremove">Remove</a>]<br/><br/></div>';
							$display = 'style="display:none;"';
							break;
						}								
					}
				?>
				<div id="div_product_smallimage" <?php echo $display; ?>>
					<span id="product_smallimage_file">
						<input id="product_smallimage"  name="product_smallimage" type="file" tabindex="100" <?php echo $display; ?> />
					</span> [&nbsp;<a href="#product_smallimage_file" class="imageclear">Clear</a>&nbsp;]
				</div>
			</td>
		</tr>		
		<tr>
			<td class="LabelCell">Large (Any)</td>
			<td>
				<?php
					$display = '';
					foreach ($product['product_images'] as $productimage) {
						if ($productimage['image_type'] == 'L') {
							echo '<div><a class="fancybox" href="../domain/images/products/'.$productimage['sys_file_name'].'">'.$productimage['image_name'].'</a>
							&nbsp;[&nbsp;<a href="#div_product_thumbnailimage" class="imagechange">Change</a>&nbsp;&#8226;&nbsp;<a href="#div_product_largeimage" class="imageremove">Remove</a>]<br/><br/></div>';
							$display = 'style="display:none;"';
							break;
						}								
					}
				?>
				<div id="div_product_largeimage" <?php echo $display; ?>>
					<span id="product_largeimage_file">
						<input id="product_largeimage"  name="product_largeimage" type="file" tabindex="110" <?php echo $display; ?> />
					</span> [&nbsp;<a href="#product_largeimage_file" class="imageclear">Clear</a>&nbsp;]
				</div>
			</td>
		</tr>
		<tr>			
			<td class="BottomToolBar" colspan="2">				
				<input type="submit" name="submit" id="submit" class="button-primary" value="Save" tabindex="120"/>
				<a href="products.php" class="button-secondary" tabindex="130">Cancel</a>				
			</td>			
		</tr>
	</table>
	<input id="fileRemoved" name="fileRemoved" type="hidden" value=""/>
</form>
<?php
	$productObj = null;
?>
<script type="text/javascript">
	$(document).ready(function() { 
		loadValidation('productform');
		$('a.fancybox').fancybox();
		
		$('a.imagechange').click(function (event){
			event.preventDefault();
			e = $(this).attr('href');		
			fi = '#' + e.substring(5);
			if ($(this).html() == 'Change') {
				$(this).html('Cancel');				
				$(e).css('display','');
				$(fi).css('display','');
			} else {
				$(this).html('Change');
				$(e).css('display','none');
				$(fi).css('display','none');
			}			
		});
		
		$('a.imageremove').click(function (event){
			event.preventDefault();
			e = $(this).attr('href');		
			fi = '#' + e.substring(5);
						
			$(e).css('display','');
			$(fi).css('display','');
			
			$(this).parent().remove();
			
			$('#fileRemoved').val($('#fileRemoved').val() + e.substring(5) + ',');			
		});
		
		$(".imageclear").click(function(event){
			event.preventDefault();
			e = $(this).attr('href');
			html = $(e).html();
			$(e).html(html);			
		});
		
		/*
		$('#product_cost').blur(function() {
			$.ajax({
				url: 'pointcalcsvc.php',
				data: { "amount": $('#product_cost').val(), "for": "pcalsvc" }, 
				success: function(data) {					
					$('#product_points').val(data.value);
				},		
			});
		});
		*/
		
		$(".numeric").numeric(false);		
	});	
</script>