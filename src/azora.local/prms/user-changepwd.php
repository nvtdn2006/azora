<?php require_once './includes/application.php'; $this->template = 'blank'; $this->title = 'Change password'?>

<?php	

	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('validation');
	
	$user_id= 0;
	
	if (isset($_GET['id'])) {		
		$user_id = $_GET['id'];		
	}
	
?>

<div style="width: 680px;">
<form name="changepwdform" id="changepwdform" action="passwordsvc.php" method="post"> 
	<table class="formview" width="100%" border="0" cellspacing="3px" cellpadding="3px">		
		<tr>
			<td class="LabelCell">New password</td>
			<td><input type="password" name="new_password" id="new_password" maxlength="50" class="input Required" size="20" tabindex="20" /></td>
		</tr>
		<tr>
			<td class="LabelCell">Re-enter password</td>
			<td><input type="password" name="reenter_password" id="reenter_password" maxlength="50" class="input Required" size="20" tabindex="30" /></td>
		</tr>
		<tr>			
			<td class="BottomToolBar" colspan="2">	
				<input type="hidden" name="uid" value="<?php echo $user_id; ?>" />
				<input type="submit" name="submit" id="submit" class="button-primary" value="Save" tabindex="30"/><div class="indicator"></div> 				
			</td>			
		</tr>
	</table>
</form>
<div id="_info" class="error-info">		
</div>
</div>

<script type="text/javascript">
	$(document).ready(function() { 
		
		$( "#_info" ).hide();
		
		$("#changepwdform").submit(function(event) {
			
			/* stop form from submitting normally */
			event.preventDefault(); 
			
			$( "#_info" ).hide();
				
			/* get some values from elements on the page: */
			var $form = $( this ),
				password = $form.find( 'input[name="new_password"]' ).val(),
				rtpassword = $form.find( 'input[name="reenter_password"]' ).val(),
				uid = $form.find( 'input[name="uid"]' ).val(),
				url = $form.attr( 'action' );

			indicator = $form.find('.indicator');
			indicator.show();

			submit = $form.find('#submit');
			submit.attr("disabled", "disabled");
				
			/* Send the data using post and put the results in a div */
			$.post( url, { password: password, rtpassword: rtpassword,  uid: uid, form: 'change' },
				function(data) {
					//var content = $( data ).find( '#content' );					
					if (data.status) {
						window.location.href = 'user-edit.php?user=' + uid;
					} else {
						$( "#_info" ).empty().append(data.message);
						$( "#_info" ).show();					
					}	
					indicator.hide();					
					submit.removeAttr("disabled");
				}
			);
		});
		
	});
</script>