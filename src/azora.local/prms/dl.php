<?PHP
session_start();

function cleanData(&$str)
{
	$str = preg_replace("/\t/", "\\t", $str);
	$str = preg_replace("/\r?\n/", "\\n", $str);
	if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
}

if (isset($_GET['p']) && isset($_SESSION[$_GET['p']])) {

	$report = $_SESSION[$_GET['p']];

	//print_r($report);

	switch ($report['report']) {
		case 'CUSTPURRPT':
		case 'CUSTRPT':

			# filename for download
			$filename = $report['report_name'];

			header("Content-Disposition: attachment; filename=\"$filename\"");
			header("Content-Type: application/vnd.ms-excel");

			print_r($purchasehistory);

			foreach($report['content'] as $row) {
				array_walk($row, 'cleanData');
				echo implode("\t", array_values($row)) . "\r\n";
			}

	}

	exit;

}

?>

<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Download'?>

<?php echo "Unable to download file. It may be becasue your current session expired or file doesn't exist. Please retry again!"; ?>
