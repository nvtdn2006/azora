<?php require_once './includes/application.php'; $this->template = 'print'; $this->title = 'Invoice'?>

<?php		

	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('validation');
	
	DomainManager::getInstance()->load('Company');
	$companyObj = new Company();
	$company = $companyObj->getCompany();
	$company = $company[0];
	
	DomainManager::getInstance()->load('Store');	
	$storeObj = new Store();
	
	
	
	$month = date('n');
	$year = date('Y');
	$store = 0;
	
	if (isset($_GET['month'])) 
		$month = $_GET['month'];
		
	if (isset($_GET['year'])) 
		$year = $_GET['year'];
		
	if (isset($_GET['store'])) 
		$store = $_GET['store'];

	
	$sobj = $storeObj->getStore($store);
	
	DomainManager::getInstance()->load('Redemption');
	$redemptionObj = new Redemption();
	
	$redemptions = $redemptionObj->getBills($month, $year, $store);			
	
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td style="width: 170px;">
			<img src="<?php $config = Factory::getConfig();	 echo $config['PRMSConfig']->live_site . '/prms/themes/' . $config['PRMSConfig']->back_theme . '/images/azora.png'; ?>" width="160" height="100" />
			<br/><br/>
		</td>
		<td>
			<h2><?php echo $company['company_name']; ?></h2>
			<?php echo $company['company_address']; ?><br/><br/>			
		</td>
		<td align="right">
			<h2>Invoice</h2>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			Tel: <?php echo $company['company_phone']; ?><br/>
			Fax: <?php echo $company['company_fax']; ?><br/>
		</td>
		<td align="right">
			Date: <?php echo date('d-M-Y'); ?><br/>
		</td>
	</tr>
</table>
<hr/>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="150px">
			<b>Bill To</b>
		</td>
		<td>
			<b><?php echo $sobj['store_name']; ?></b> (Branch : <?php echo $sobj['branch_name']; ?>) <br/>			
		</td>
	</tr>	
	<tr>
		<td>&nbsp;</td>
		<td><?php echo $sobj['store_address']; ?></td>
	</tr>
</table>
<br/><br/><br/><br/>
<hr/>

<table width="100%" border="0" cellspacing="3" cellpadding="3" style="border: 1px solid #000;border-collapse: collapse;">
	<thead>
		<tr>
			<th style="border: 1px solid #000;">No.</th>
			<th style="border: 1px solid #000;">Redemption No.</th>
			<th style="border: 1px solid #000;">NRIC</th>
			<th style="border: 1px solid #000;">Product</th>
			<th style="border: 1px solid #000;">Product Cost</th>
			<th style="border: 1px solid #000;">Product Points</th>			
			<th style="border: 1px solid #000;">Quantity</th>
			<th style="border: 1px solid #000;">Redeemed Points</th>						
			<th style="border: 1px solid #000;">Amount</th>			
		</tr>
	</thead>
	<tbody>
		<?php
			if ($redemptions != null) {
			$sr = 1;
			$total = 0;
			foreach($redemptions as $redemption) {
		?>
		<tr>						
			<td style="border: 1px solid #000;"><?php echo $sr; ?></td>
			<td style="border: 1px solid #000;"><?php echo $redemption['redemption_no'];?></td>
			<td style="border: 1px solid #000;"><?php echo $redemption['nric']; ?></td>
			<td style="border: 1px solid #000;"><?php echo $redemption['product_name']; ?></td>
			<td style="border: 1px solid #000;"><?php echo $redemption['product_cost']; ?></td>
			<td style="border: 1px solid #000;"><?php echo $redemption['product_points']; ?></td>
			<td style="border: 1px solid #000;"><?php echo $redemption['quantity']; ?></td>
			<td style="border: 1px solid #000;"><?php echo $redemption['points']; ?></td>	
			<td style="border: 1px solid #000;"><?php echo $redemption['amount']; ?></td>				
		</tr>
		<?php 
			$sr++;
			$total += $redemption['amount'];
		}?>
		<tr>
			<td colspan="7" style="border: 1px solid #000;">&nbsp;</td>
			<td style="border: 1px solid #000;"><b>Total</b></td>
			<td style="border: 1px solid #000;"><?php echo $total; ?></td>
		</tr>
		<?php } ?>
		
	</tbody>
</table>

<br/><br/><br/>
<div style="color:#888;font-size: 12px;">Computer generated invoice<br/>PRMS - Powered by BlueCube Pte Ltd</div>