<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Update profile'?>

<?php	

	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('validation');	
	
	require_once './includes/user.php';
	$userObj = new User();
	
	$user_id = Authentication::getUserId();
	$user = null;
	
	$error = array();
	
	$user = $userObj->getUser($user_id);
	
	if (isset($_GET['action'])) {		
		$isValid = true;
		
		switch ($_GET['action']){			
			case 'update':
				
				if ($isValid) {
					if ($userObj->isExistEmail($_POST['email'], $user_id)) {				
						$isValid = false;
						array_push($error, 'Email is already exist in system, please re-enter.');	
					}
				}
				
				if ($isValid) {
					if ($userObj->isExistUser($_POST['username_hf'], $_POST['email'], $user_id)) {				
						$isValid = false;
						array_push($error, 'User already exists in system, please check your username or Email.');	
					}
				}
					
				if ($isValid) {
					$user = array('user_id' => $user_id,
									'full_name' => $_POST['full_name'],
									'email' => $_POST['email'],							
									'updated_by' => Authentication::getUserId()
									);	
					if ($userObj->updateProfile($user)) { 
					
						//Event Log
						$evtObj = new EventObject();					
						$evtObj->event_id = EventTypes::PRMS_UpdateProfile;
						$evtObj->description = Authentication::getAttribute('login_id') . ' updated his/her profile.';
						$evtObj->action_by = Authentication::getAttribute('login_id');					
						EventLog::Log($evtObj);

						header( 'Location: updateprofile.php?rst=1');
						
					}
					else 
						array_push($error, 'Saving process failed. Please try again.');
				}
			break;
		}		
	}
	
	if (!isset($user) || isset($_GET['action'])) {		
		if (isset($_REQUEST['full_name'])) 			
			$user['user_profiles']['full_name'] = $_REQUEST['full_name'];
		else
			$user['user_profiles']['full_name'] =  '';
			
		if (isset($_REQUEST['email'])) 			
			$user['user_profiles']['email'] = $_REQUEST['email'];
		else
			$user['user_profiles']['email'] =  '';
			
		if (isset($_REQUEST['username'])) 			
			$user['user']['username'] = $_REQUEST['username'];
		else
			$user['user']['username'] =  '';			
	}
	
?>

<?php
	if (isset($error) && count($error) > 0) {
?>
	<div class="error-info form-info">
		<?php foreach ($error as $handle) {
				echo "<p>$handle</p>";
		} ?>
	</div>
<?php
	}
?>

<?php
	if (isset($_GET['rst']) && $_GET['rst'] == 1) {
?>
	<div class="success-info form-info">
		Saving process successfully done.
	</div>
<?php
	}
?>

<form name="userform" id="userform" action="updateprofile.php?action=<?php echo 'update&user='; ?>" method="post"> 
	<table class="formview" width="100%" border="0" cellspacing="3px" cellpadding="0">
		<tr>
			<td class="LabelCell Required">Full name</td>
			<td><input type="text" name="full_name" id="full_name" maxlength="50" class="input Required" value="<?php echo isset($user) ? $user['user_profiles']['full_name'] : '' ?>" size="20" tabindex="10" /></td>
		</tr>
		<tr>
			<td class="LabelCell Required">Email</td>
			<td><input type="text" name="email" id="email" maxlength="50" class="input Required ValidEmail" value="<?php echo isset($user) ? $user['user_profiles']['email'] : '' ?>" size="20" tabindex="20" /></td>
		</tr>
		
		
		<tr>			
			<td class="SectionBar" colspan="2">				
				User information
			</td>
		</tr>
		<tr>
			<td class="LabelCell Required">Username</td>
			<td><input type="hidden" name="username_hf" id="username_hf" value="<?php echo isset($user) ? $user['user']['username'] : '' ?>"/>
			<input type="text" name="username" id="username" maxlength="50" class="input Required" value="<?php echo isset($user) ? $user['user']['username'] : '' ?>" size="20" tabindex="30" 
			<?php echo ($user_id == 0) ? '' : 'disabled'; ?> /></td>
		</tr>
		
		<tr>
			<td class="BottomToolBar" colspan="2">	
				<input type="submit" name="submit" id="submit" class="button-primary" value="Save" tabindex="40"/>				
			</td>			
		</tr>
	</table>
</form>
<?php
	$roleObj = null;
?>
<script type="text/javascript">
	$(document).ready(function() { 
		loadValidation('userform');
	});
</script>