<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'New user'?>

<?php	

	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('validation');
	JSManager::getInstance()->add('fancybox');	
	CSSManager::getInstance()->add('/js/fancybox/jquery.fancybox-1.3.4.css');

	require_once './includes/role.php';
	$roleObj = new Role();
	
	require_once './includes/user.php';
	$userObj = new User();
	
	DomainManager::getInstance()->load('Store');
	$storeObj = new Store();
	
	$user_id = 0;
	$user = null;
	
	$error = array();
	
	if (isset($_GET['user'])) {		
		$user_id = $_GET['user'];
		$user = $userObj->getUser($user_id);
		$this->title = 'Update user';
	}
	
	if (isset($_GET['action'])) {		
		$isValid = true;
		
		switch ($_GET['action']){
			case 'new':	
				$roles = null;
				if (isset($_POST['select_to']))
					$roles = $_POST['select_to'];
					
				$stores = null;
				if (isset($_POST['select_to_store']))
					$stores = $_POST['select_to_store'];
					
				if ($roles == null) {
					array_push($error, 'Please select roles.');
					$isValid = false;
				}
				
				if ($isValid) {
					if ($_POST['password'] != $_POST['reenterpassword']) {
						$isValid = false;
						array_push($error, 'Passwords do not match.');
					}
				}
				
				if ($isValid) {
					if ($userObj->isExistUsername($_POST['username'])) {				
						$isValid = false;
						array_push($error, 'Username is already exist in system, please re-enter.');	
					}
				}
				
				if ($isValid) {
					if ($userObj->isExistEmail($_POST['email'])) {				
						$isValid = false;
						array_push($error, 'Email is already exist in system, please re-enter.');	
					}
				}
				
				if ($isValid) {
					if ($userObj->isExistUser($_POST['username'], $_POST['email'])) {				
						$isValid = false;
						array_push($error, 'User already exists in system, please check your username or Email.');	
					}
				}
					
				if ($isValid) {
					$user = array('full_name' => $_POST['full_name'],
									'email' => $_POST['email'],								
									'username' => $_POST['username'],	
									'password' => $_POST['password'],
									'user_status' => $_POST['user_status'],								
									'created_by' => Authentication::getUserId(),								
									'updated_by' => Authentication::getUserId(),
									'roles' => $roles,
									'stores' => $stores);								
					if ($userObj->saveUser($user)) {
					
						//Event Log
						$evtObj = new EventObject();					
						$evtObj->event_id = EventTypes::PRMS_AddUser;
						$evtObj->description = Authentication::getAttribute('login_id') . ' added new user ('.$_POST['username'].').';
						$evtObj->action_by = Authentication::getAttribute('login_id');					
						EventLog::Log($evtObj);
					
						header( 'Location: users.php');
					}
					else 
						array_push($error, 'Saving process failed. Please try again.');
				}
			break;
			case 'update':
				$roles = null;
				if (isset($_POST['select_to']))
					$roles = $_POST['select_to'];
				
				$stores = null;
				if (isset($_POST['select_to_store']))
					$stores = $_POST['select_to_store'];
					
				if ($roles == null) {
					array_push($error, 'Please select roles.');
					$isValid = false;
				}
				
				if ($isValid) {
					if ($userObj->isExistEmail($_POST['email'], $user_id)) {				
						$isValid = false;
						array_push($error, 'Email is already exist in system, please re-enter.');	
					}
				}
				
				if ($isValid) {
					if ($userObj->isExistUser($_POST['username_hf'], $_POST['email'], $user_id)) {				
						$isValid = false;
						array_push($error, 'User already exists in system, please check your username or Email.');	
					}
				}
					
				if ($isValid) {
					$user = array('user_id' => $user_id,
									'full_name' => $_POST['full_name'],
									'email' => $_POST['email'],		
									'user_status' => $_POST['user_status'],																	
									'updated_by' => Authentication::getUserId(),
									'roles' => $roles,
									'stores' => $stores);	
					if ($userObj->updateUser($user)) {
					
						//Event Log
						$evtObj = new EventObject();					
						$evtObj->event_id = EventTypes::PRMS_UpdateUser;
						$evtObj->description = Authentication::getAttribute('login_id') . ' updated user ('.$_POST['username_hf'].').';
						$evtObj->action_by = Authentication::getAttribute('login_id');					
						EventLog::Log($evtObj);
						
						header( 'Location: users.php');					
					}
					else 
						array_push($error, 'Saving process failed. Please try again.');
				}
			break;
		}		
	}
	
	if (!isset($user) || isset($_GET['action'])) {		
		if (isset($_REQUEST['full_name'])) 			
			$user['user_profiles']['full_name'] = $_REQUEST['full_name'];
		else
			$user['user_profiles']['full_name'] =  '';
			
		if (isset($_REQUEST['email'])) 			
			$user['user_profiles']['email'] = $_REQUEST['email'];
		else
			$user['user_profiles']['email'] =  '';
			
		if (isset($_REQUEST['username'])) 			
			$user['user']['username'] = $_REQUEST['username'];
		else
			$user['user']['username'] =  '';

		if (isset($_REQUEST['user_status'])) 			
			$user['user']['user_status'] = $_REQUEST['user_status'];
		else
			$user['user']['user_status'] =  1;
	}
	
?>

<?php
	if (isset($error) && count($error) > 0) {
?>
	<div class="error-info form-info">
		<?php foreach ($error as $handle) {
				echo "<p>$handle</p>";
		} ?>
	</div>
<?php
	}
?>

<form name="userform" id="userform" action="user-edit.php?action=<?php echo $user_id > 0 ? 'update&user='.$user_id : 'new'; ?>" method="post"> 
	<table class="formview" width="100%" border="0" cellspacing="3px" cellpadding="0">
		<tr>
			<td class="LabelCell Required">Full name</td>
			<td><input type="text" name="full_name" id="full_name" maxlength="50" class="input Required" value="<?php echo isset($user) ? $user['user_profiles']['full_name'] : '' ?>" size="20" tabindex="10" /></td>
		</tr>
		<tr>
			<td class="LabelCell Required">Email</td>
			<td><input type="text" name="email" id="email" maxlength="50" class="input Required ValidEmail" value="<?php echo isset($user) ? $user['user_profiles']['email'] : '' ?>" size="20" tabindex="20" /></td>
		</tr>
		
		
		<tr>			
			<td class="SectionBar" colspan="2">				
				User information
			</td>
		</tr>
		<tr>
			<td class="LabelCell Required">Username</td>
			<td><input type="hidden" name="username_hf" id="username_hf" value="<?php echo isset($user) ? $user['user']['username'] : '' ?>"/>
			<input type="text" name="username" id="username" maxlength="50" class="input Required" value="<?php echo isset($user) ? $user['user']['username'] : '' ?>" size="20" tabindex="30" 
			<?php echo ($user_id == 0) ? '' : 'disabled'; ?> /></td>
		</tr>
		
		<?php 
			if ($user_id == 0) {
			?>
		<tr>
			<td class="LabelCell Required">Your Password</td>
			<td><input type="password" name="password" id="password" maxlength="50" class="input Required" value="" size="20" tabindex="40" /></td>
		</tr>
		<tr>
			<td class="LabelCell Required">Re-enter Password</td>
			<td><input type="password" name="reenterpassword" id="reenterpassword" maxlength="50" class="input Required" value="" size="20" tabindex="50" /></td>
		</tr>
			<?php
			}
		?>		

		<tr>
			<td class="LabelCell">Status</td>
			<td><input type="radio" name="user_status" value="1" tabindex="60" <?php echo ( (isset($user) && $user['user']['user_status'] == true)  ? 'checked' : (isset($user) ? '' : 'checked')); ?>/> Enable 
			<input type="radio" name="user_status" value="0" tabindex="60" <?php echo ( (isset($user) && $user['user']['user_status'] == false)  ? 'checked' : ''); ?>/> Disable</td>
		</tr>	
		
		<tr>			
			<td colspan="2">				
				<table width="100%" border="0" cellspacing="3px" cellpadding="3px">
					<tr>
						<td width="40%">
							<b>Roles</b>
						</td>
						<td width="20%">
							
						</td>
						<td width="40%">
							<b>Selected roles</b>
						</td>
					</tr>
					<tr>
						<td>
							<select id="select_from" name="select_from[]" size="7" multiple tabindex="70">							
								<?php
									$roles = $roleObj->getUserRolesNotSelected($user_id);
									
									foreach($roles as $role) {
										echo '<option value="'.$role['role_id'].'">'.$role['role_name'].'</option>';
									}
								?>							
							</select>
						</td>
						<td align="center">
							<a id="btn_add" href="#" class="button-secondary">&gt;</a><br/><br/>
							<a id="btn_remove" href="#" class="button-secondary">&lt;</a>
						</td>
						<td>
							<select id="select_to" name="select_to[]" size="7" multiple tabindex="80">
								<?php
									$roles = $roleObj->getUserRoles($user_id);
									
									foreach($roles as $role) {
										echo '<option value="'.$role['role_id'].'">'.$role['role_name'].'</option>';
									}
								?>
							</select>
						</td>
					</tr>
				</table>
			</td>
		</tr>

		
		<tr>			
			<td class="SectionBar" colspan="2">				
				Store Assignment
			</td>
		</tr>
		<tr>			
			<td colspan="2">				
				<table width="100%" border="0" cellspacing="3px" cellpadding="3px">
					<tr>
						<td width="40%">
							<b>Stores</b>
						</td>
						<td width="20%">
							
						</td>
						<td width="40%">
							<b>Selected stores</b>
						</td>
					</tr>
					<tr>
						<td>
							<select id="select_from_store" name="select_from_store[]" size="7" multiple tabindex="90">							
								<?php
									$stores = $storeObj->getUserStoresNotSelected($user_id);
									
									foreach($stores as $store) {
										echo '<option value="'.$store['store_id'].'">'.$store['store_branch_name'].'</option>';
									}
								?>							
							</select>
						</td>
						<td align="center">
							<a id="btn_add_store" href="#" class="button-secondary">&gt;</a><br/><br/>
							<a id="btn_remove_store" href="#" class="button-secondary">&lt;</a>
						</td>
						<td>
							<select id="select_to_store" name="select_to_store[]" size="7" multiple tabindex="100">
								<?php
									$stores = $storeObj->getUserStores($user_id);
									
									foreach($stores as $store) {
										echo '<option value="'.$store['store_id'].'">'.$store['store_branch_name'].'</option>';
									}
								?>
							</select>
						</td>
					</tr>
				</table>
			</td>
		</tr>

		<?php 
			if ($user_id > 0) {
			?>
		<tr>
			<td class="LabelCell">Change Password</td>
			<td><a class="fancybox" href="user-changepwd.php?id=<?php echo $user_id; ?>">Click Here</a></td>
		</tr>
			<?php
			}
		?>			
		
		<tr>
			<td class="BottomToolBar" colspan="2">	
				<input type="submit" name="submit" id="submit" class="button-primary" value="Save" tabindex="110"/>
				<a href="users.php" class="button-secondary" tabindex="120">Cancel</a>
			</td>			
		</tr>
	</table>
</form>
<?php
	$roleObj = null;
?>
<script type="text/javascript">
	$(document).ready(function() { 
		loadValidation('userform');	
		
		$('#btn_add').click(function(){						
			$('#select_from option:selected').each( function() {				
				$('#select_to').append("<option value='"+$(this).val()+"'>"+$(this).text()+"</option>");
				$(this).remove();
			});
			return false;
		});
		$('#btn_remove').click(function(){			
			$('#select_to option:selected').each( function() {
				$('#select_from').append("<option value='"+$(this).val()+"'>"+$(this).text()+"</option>");
				$(this).remove();
			});
			return false;
		});
		
		
		$('#btn_add_store').click(function(){			
			$('#select_from_store option:selected').each( function() {
					$('#select_to_store').append("<option value='"+$(this).val()+"'>"+$(this).text()+"</option>");
				$(this).remove();
			});
			return false;
		});
		$('#btn_remove_store').click(function(){			
			$('#select_to_store option:selected').each( function() {
				$('#select_from_store').append("<option value='"+$(this).val()+"'>"+$(this).text()+"</option>");
				$(this).remove();
			});
			return false;
		});

		
		$('#submit').click(function(){			
			$('#select_to option').each(function() {				
				$(this).attr('selected', 'selected');
			});
			
			$('#select_to_store option').each(function() {				
				$(this).attr('selected', 'selected');
			});
		});
		
		
		$('a.fancybox').fancybox();
	});
</script>