<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'New branch'?>

<?php	

	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('validation');	
	
	DomainManager::getInstance()->load('Branch');
	$branchObj = new Branch();
	
	$branch_id = 0;
	$branch = null;
	
	$error = array();
	
	if (isset($_GET['branch'])) {		
		$branch_id = $_GET['branch'];
		$branch = $branchObj->getBranch($branch_id);	
		$this->title = 'Update branch';
	}
	
	if (isset($_GET['action'])) {		
		switch ($_GET['action']){
			case 'new':		
				if (!$branchObj->isExistBranch($_POST['branch_name'], 0)) {			
					$branch = array('branch_name' => $_POST['branch_name'],
									'branch_description' => $_POST['branch_description'],								
									'created_by' => Authentication::getUserId(),								
									'updated_by' => Authentication::getUserId());								
					if ($branchObj->saveBranch($branch)) {
						
						//Event Log
						$evtObj = new EventObject();					
						$evtObj->event_id = EventTypes::PRMS_AddBranch;
						$evtObj->description = Authentication::getAttribute('login_id') . ' added new branch ('.$_POST['branch_name'].').';
						$evtObj->action_by = Authentication::getAttribute('login_id');					
						EventLog::Log($evtObj);
						
						header( 'Location: branches.php');
					}
				} else {
					array_push($error, 'Branch name already exists in system.');	
				}
			break;
			case 'update':
				if (!$branchObj->isExistBranch($_POST['branch_name'], $branch_id)) {			
					$branch = array('branch_name' => $_POST['branch_name'],
									'branch_description' => $_POST['branch_description'],																
									'updated_by' => Authentication::getUserId(),
									'branch_id' => $branch_id);
					if ($branchObj->updateBranch($branch)) {
					
						//Event Log
						$evtObj = new EventObject();					
						$evtObj->event_id = EventTypes::PRMS_UpdateBranch;
						$evtObj->description = Authentication::getAttribute('login_id') . ' updated branch ('.$_POST['branch_name'].').';
						$evtObj->action_by = Authentication::getAttribute('login_id');					
						EventLog::Log($evtObj);
						
						header( 'Location: branches.php');
					}
				} else {
					array_push($error, 'Branch name already exists in system.');
				}
			break;
		}		
	}
	
	if (!isset($branch) || isset($_GET['action'])) {		
		if (isset($_REQUEST['branch_name'])) 			
			$branch['branch_name'] = $_REQUEST['branch_name'];
		else
			$branch['branch_name'] =  '';
			
		if (isset($_REQUEST['branch_description'])) 			
			$branch['branch_description'] = $_REQUEST['branch_description'];
		else
			$branch['branch_description'] =  '';
	}
	
?>

<?php
	if (isset($error) && count($error) > 0) {
?>
	<div class="error-info form-info">
		<?php foreach ($error as $handle) {
				echo "<p>$handle</p>";
		} ?>
	</div>
<?php
	}
?>

<form name="branchform" id="branchform" action="branch-edit.php?action=<?php echo $branch_id > 0 ? 'update&branch='.$branch_id : 'new'; ?>" method="post"> 
	<table class="formview" width="100%" border="0" cellspacing="3px" cellpadding="3px">
		<tr>
			<td class="LabelCell Required">Branch</td>
			<td><input type="text" name="branch_name" id="branch_name" maxlength="50" class="input Required" value="<?php echo $branch['branch_name'];  ?>" size="20" tabindex="10" /></td>
		</tr>
		<tr>
			<td class="LabelCell">Description</td>
			<td><input type="text" name="branch_description" id="branch_description" maxlength="255"  class="input" value="<?php echo $branch['branch_description']; ?>" size="20" tabindex="20" /></td>
		</tr>
		<tr>			
			<td class="BottomToolBar" colspan="2">				
				<input type="submit" name="submit" id="submit" class="button-primary" value="Save" tabindex="30"/>
				<a href="branches.php" class="button-secondary" tabindex="40">Cancel</a>				
			</td>			
		</tr>
	</table>
</form>
<?php
	$branchObj = null;
?>
<script type="text/javascript">
	$(document).ready(function() { 
		loadValidation('branchform');
	});
</script>