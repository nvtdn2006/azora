<?php require_once './includes/service.php'; ?>

<?php

	$return = array();
	if (isset($_GET['for']) && $_GET['for'] == 'pcalsvc') {
		$salesamount = 0;
		
		if (isset($_GET['amount']))
			$salesamount = $_GET['amount'];
		
		$points = 0;	
		
		if ($salesamount > 0) {
			DomainManager::getInstance()->load('Point');
			$pointObj = new Point();	
			$points = $pointObj->getRewardPoints($salesamount);
		}
		$return['value'] = $points;
	} else {
		//Nothing
	}
		
	header('Content-type: application/json');
	echo json_encode($return);
	
?>