<?php
/* 
 * 19-05-2013 FX.1305.002 Add Admin Redeem feature
 */
require_once './includes/application.php'; $this->template = ''; $this->title = 'Redemption submitted'; ?>


<?php
	Factory::getSession()->removeKey('myredemption');
	require_once dirname(__FILE__) . '/../includes/cart.php';			
	clearCart();
?>	

<div id="righttitle" >Customer's Redemption submitted</div>
<div id="contentcontainer">

<div class="cprocess">
	Submission process successfully done.<br/><br/><br/><br/>
	Customer's redemption application has been submitted. Please inform Customer to go and collect on submitted date and time.
	<br/><br/><br/>
	<a href="customersearch.php?d=redeem" class="button-secondary" title="Back to search.">Go to redeem</a>
</div>

</div>