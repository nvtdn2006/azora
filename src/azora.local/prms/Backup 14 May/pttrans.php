<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Point transactions'?>

<?php		

	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('validation');
	
	DomainManager::getInstance()->load('Point');
	$pointObj = new Point();
	
	DomainManager::getInstance()->load('Customer');
	$customerObj = new Customer();
	
	$customer_id = 0;
	$customer = null;
	
	if (isset($_GET['id'])) {		
		$customer_id = $_GET['id'];		
	}
	
	if (isset($_POST['customer_id'])) {		
		$customer_id = $_POST['customer_id'];		
	}
	
	if ($customer_id  > 0 ) {
		$customer = $customerObj->getCustomerProfile($customer_id);
	
		$purchasehistory = null;
		
		if (isset($_GET['action']) && $_GET['action'] == 'search') {
			if ($customer_id > 0) {
				$frommonth = date('n');
				$fromyear = date('Y');
				
				$tomonth = date('n');
				$toyear = date('Y');
				
				$type = 0;
				
				if (isset($_POST['frommonth'])) 
					$frommonth = $_POST['frommonth'];
					
				if (isset($_POST['fromyear'])) 
					$fromyear = $_POST['fromyear'];
					
				if (isset($_POST['tomonth'])) 
					$tomonth = $_POST['tomonth'];
					
				if (isset($_POST['toyear'])) 
					$toyear = $_POST['toyear'];
					
				if (isset($_POST['type'])) 
					$type = $_POST['type'];
					
				$fromdate = new DateTime($fromyear . '-' . $frommonth . '-' . '1');
				$todate = new DateTime($toyear . '-' . $tomonth . '-' . '1');
				$todate->modify('+1 month');
				$todate->modify('-1 day');
							
				$purchasehistory = $pointObj->getPointsTransactionByCustomer($customer_id, $fromdate, $todate, $type);			
			}
		}
		
		$formvalues = array();
		
		if (isset($_REQUEST['frommonth'])) 			
			$formvalues['frommonth'] = $_REQUEST['frommonth'];
		else
			$formvalues['frommonth'] =  0;
		
		if (isset($_REQUEST['fromyear'])) 			
			$formvalues['fromyear'] = $_REQUEST['fromyear'];
		else
			$formvalues['fromyear'] =  0;
			
		if (isset($_REQUEST['tomonth'])) 			
			$formvalues['tomonth'] = $_REQUEST['tomonth'];
		else
			$formvalues['tomonth'] =  0;
			
		if (isset($_REQUEST['toyear'])) 			
			$formvalues['toyear'] = $_REQUEST['toyear'];
		else
			$formvalues['toyear'] =  0;
			
		if (isset($_REQUEST['type'])) 			
			$formvalues['type'] = $_REQUEST['type'];
		else
			$formvalues['type'] =  0;
			
	} else {
		header( 'Location: customersearch.php?d=pttrans');
		exit();
	}
		
?>

<form name="historyform" id="historyform" action="pttrans.php?action=search&id=<?php echo $customer['customer_id']; ?>" method="post"> 
	
	<table class="formview" width="100%" border="0" cellspacing="3px" cellpadding="3px">
		<tr>
			<td class="LabelCell Disabled">NRIC / FIN</td>
			<td><input type="text" name="nric" id="nric" maxlength="9" class="input" value="<?php echo $customer['nric']; ?>" size="20" tabindex="10" disabled /></td>
		</tr>
		<tr>
			<td class="LabelCell Disabled">Name</td>
			<td><input type="text" name="name" id="name" maxlength="255" class="input" value="<?php echo $customer['name']; ?>" size="20" tabindex="20" disabled /></td>
		</tr>
		
	</table>
	<table class="formview" width="100%" border="0">		
		<tr>
			<td><b>From</b></td>		
			<td>Month</td>		
			<td>				
				<select tabindex="50" name="frommonth" class="GreaterThanZero" tabindex="10">						
					<option value="0" <?php echo $formvalues['frommonth'] == 0 ? 'selected' : ''; ?>>- Select -</option>
					<option value="1" <?php echo $formvalues['frommonth'] == 1 ? 'selected' : ''; ?>>Jan</option>
					<option value="2" <?php echo $formvalues['frommonth'] == 2 ? 'selected' : ''; ?>>Feb</option>
					<option value="3" <?php echo $formvalues['frommonth'] == 3 ? 'selected' : ''; ?>>Mar</option>
					<option value="4" <?php echo $formvalues['frommonth'] == 4 ? 'selected' : ''; ?>>Apr</option>
					<option value="5" <?php echo $formvalues['frommonth'] == 5 ? 'selected' : ''; ?>>May</option>
					<option value="6" <?php echo $formvalues['frommonth'] == 6 ? 'selected' : ''; ?>>Jun</option>
					<option value="7" <?php echo $formvalues['frommonth'] == 7 ? 'selected' : ''; ?>>Jul</option>
					<option value="8" <?php echo $formvalues['frommonth'] == 8 ? 'selected' : ''; ?>>Aug</option>
					<option value="9" <?php echo $formvalues['frommonth'] == 9 ? 'selected' : ''; ?>>Sep</option>
					<option value="10" <?php echo $formvalues['frommonth'] == 10 ? 'selected' : ''; ?>>Oct</option>
					<option value="11" <?php echo $formvalues['frommonth'] == 11 ? 'selected' : ''; ?>>Nov</option>
					<option value="12" <?php echo $formvalues['frommonth'] == 12 ? 'selected' : ''; ?>>Dec</option>					
				</select>
			</td>			
			<td>Year</td>		
			<td>
				<select tabindex="50" name="fromyear" class="GreaterThanZero" tabindex="20">				
					<option value="0">- Select -</option>
					<?php
						$year = date('Y');
						for($i=0;$i<3;$i++) {
							echo '<option value="'.($year - $i).'"  '.($formvalues['fromyear'] == ($year - $i) ? 'selected' : '').'  >'.($year - $i).'</option>';							
						}
					?>
				</select>
			</td>			
		</tr>
		<tr>
			<td><b>To</b></td>		
			<td>Month</td>		
			<td>				
				<select tabindex="50" name="tomonth" class="GreaterThanZero" tabindex="30">						
					<option value="0" <?php echo $formvalues['tomonth'] == 0 ? 'selected' : ''; ?>>- Select -</option>
					<option value="1" <?php echo $formvalues['tomonth'] == 1 ? 'selected' : ''; ?>>Jan</option>
					<option value="2" <?php echo $formvalues['tomonth'] == 2 ? 'selected' : ''; ?>>Feb</option>
					<option value="3" <?php echo $formvalues['tomonth'] == 3 ? 'selected' : ''; ?>>Mar</option>
					<option value="4" <?php echo $formvalues['tomonth'] == 4 ? 'selected' : ''; ?>>Apr</option>
					<option value="5" <?php echo $formvalues['tomonth'] == 5 ? 'selected' : ''; ?>>May</option>
					<option value="6" <?php echo $formvalues['tomonth'] == 6 ? 'selected' : ''; ?>>Jun</option>
					<option value="7" <?php echo $formvalues['tomonth'] == 7 ? 'selected' : ''; ?>>Jul</option>
					<option value="8" <?php echo $formvalues['tomonth'] == 8 ? 'selected' : ''; ?>>Aug</option>
					<option value="9" <?php echo $formvalues['tomonth'] == 9 ? 'selected' : ''; ?>>Sep</option>
					<option value="10" <?php echo $formvalues['tomonth'] == 10 ? 'selected' : ''; ?>>Oct</option>
					<option value="11" <?php echo $formvalues['tomonth'] == 11 ? 'selected' : ''; ?>>Nov</option>
					<option value="12" <?php echo $formvalues['tomonth'] == 12 ? 'selected' : ''; ?>>Dec</option>					
				</select>
			</td>			
			<td>Year</td>		
			<td>
				<select tabindex="50" name="toyear" class="GreaterThanZero" tabindex="40">				
					<option value="0">- Select -</option>
					<?php
						$year = date('Y');
						for($i=0;$i<3;$i++) {
							echo '<option value="'.($year - $i).'"  '.($formvalues['toyear'] == ($year - $i) ? 'selected' : '').' >'.($year - $i).'</option>';							
						}
					?>
				</select>
			</td>			
		</tr>
		<tr>		
			<td>Transaction type</td>
			<td colspan="4">				
				<select tabindex="50" name="type" tabindex="30">						
					<option value="0" <?php echo $formvalues['type'] == 0 ? 'selected' : ''; ?>>- All -</option>
					<option value="1" <?php echo $formvalues['type'] == 1 ? 'selected' : ''; ?>>Accumulation</option>
					<option value="2" <?php echo $formvalues['type'] == 2 ? 'selected' : ''; ?>>Cancellation</option>
				</select>
			</td>			
		</tr>		
		<tr>			
			<td colspan="5">				
				<input type="hidden" name="customer_id" id="customer_id" value="<?php echo $customer['customer_id']; ?>">		
				<input type="submit" name="search" id="search" class="button-primary" value="Search" tabindex="30"/>				
			</td>			
		</tr>
	</table>
</form>

<div class="SectionTitle" ><b><?php echo ($purchasehistory != null && count($purchasehistory) > 0) ? count($purchasehistory) : '0'; ?> </b> Record(s) found.</div>

<table class="tabular">
	<thead>
		<tr>			
			<th>Branch</th>
			<th>Store</th>
			<th>Reference No.</th>
			<th>Purchase Amount</th>
			<th>Points</th>
			<th>Transaction On</th>			
			<th>Type</th>			
		</tr>
	</thead>
	<tbody>
		<?php
			if ($purchasehistory != null) {
			foreach($purchasehistory as $history) {
		?>
		<tr>						
			<td><?php echo $history['branch_name']; ?></td>
			<td><?php echo $history['store_name']; ?></td>
			<td><?php echo $history['reference_no']; ?></td>
			<td><?php echo $history['sales_amount']; ?></td>
			<td><?php echo $history['trans_points']; //($history['transaction_type'] == 1 ? '' : '-') .  ?></td>			
			<td><?php echo $history['transacted_date']; ?></td>			
			<td><?php echo $history['transaction_type'] == 1 ? 'Accumulation' : 'Cancellation'; ?></td>			
		</tr>
		<?php } } ?>
	</tbody>
</table>


<script type="text/javascript">
	$(document).ready(function() { 
		loadValidation('historyform');			
	});	
</script>