<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Cancel points'?>

<?php		

	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('validation');
	JSManager::getInstance()->add('numeric');
	
	DomainManager::getInstance()->load('Customer');
	DomainManager::getInstance()->load('Store');
	
	$customerObj = new Customer();
	$storeObj = new Store();
	
	$regcustomer = null;	
	
	$error = array();
	
	if (isset($_GET['action']) && $_GET['action'] == 'cancel') {
		$customer_id = 0;
		
		if (isset($_GET['action']))
			$customer_id = $_GET['id'];
		
		if ($customer_id > 0) {
			$regcustomer = $customerObj->getCustomerProfile($customer_id);
		}
	}
	
	$point = null;
	
	if (isset($_POST['customer_id']) && $_POST['customer_id'] != 0) {
	
		$isValid = true;
		
		DomainManager::getInstance()->load('Point');
		$pointObj = new Point();
		
		$store_id = 0;
		if (isset($_POST['store'])) 
			$store_id = $_POST['store'];
			
		$points = 0;
		if (isset($_POST['points'])) 
			$points = $_POST['points'];
			
		$currentpoints = $pointObj->getPointsForAStore($_POST['customer_id'], $store_id);
				
		if (isset($_POST['salesamount'])) 
			$points = $pointObj->getRewardPoints($_POST['salesamount']);
		
		if ($currentpoints < $points) {
			array_push($error, 'Sorry, cancellation process cannot be done as current points in system is insufficient to deduct.');	
			$isValid = false;
		}
	
		if ($isValid) {
			$point = array('customer_id' => $_POST['customer_id'],	
							'reference_no' => $_POST['referenceno'],
							'sales_amount' => $_POST['salesamount'],										
							'transaction_type' => 2,
							'remarks' => $_POST['remarks'],
							'store_id' => $_POST['store'],								
							'registered_by' => Authentication::getUserId());
			
			if ($pointObj->cancelPoint($point)) {

				//Event Log
				$evtObj = new EventObject();					
				$evtObj->event_id = EventTypes::PRMS_CancelPoints;
				$evtObj->description = Authentication::getAttribute('login_id') . ' cancelled points ('.$points.') from account ('.$regcustomer['nric'].').';
				$evtObj->action_by = Authentication::getAttribute('login_id');					
				EventLog::Log($evtObj);
			
				header( 'Location: cancelledpoints.php?nric='.$regcustomer['nric'].'&points='.$points);
			}
			else {
				array_push($error, 'Saving process failed. Please try again.');					
			}
		}
	}
	
	
	if (!isset($point)) {		
		if (isset($_REQUEST['referenceno'])) 			
			$point['reference_no'] = $_REQUEST['referenceno'];
		else
			$point['reference_no'] =  '';
		
		if (isset($_REQUEST['salesamount'])) 			
			$point['sales_amount'] = $_REQUEST['salesamount'];
		else
			$point['sales_amount'] =  '';
			
		if (isset($_REQUEST['remarks'])) 			
			$point['remarks'] = $_REQUEST['remarks'];
		else
			$point['remarks'] =  '';
			
		if (isset($_REQUEST['store'])) 			
			$point['store_id'] = $_REQUEST['store'];
		else
			$point['store_id'] =  0;			
	}
	
?>


<?php
	if (isset($error) && count($error) > 0) {
?>
	<div class="error-info">
		<?php foreach ($error as $handle) {
				echo "<p>$handle</p>";
		} ?>
	</div>
<?php
	}
?>


<div class="SectionTitle" >Please enter required information to cancel</div>

<form name="cancelpointsform" id="cancelpointsform" action="cancelpoints.php?action=cancel&id=<?php echo $regcustomer['customer_id']; ?>" method="post"> 
	<table class="formview" width="100%" border="0" cellspacing="3px" cellpadding="3px">			
		<tr>
			<td class="LabelCell Disabled">NRIC / FIN</td>
			<td><input id="customer_id" name="customer_id" type="hidden" value="<?php echo $regcustomer['customer_id']; ?>"/>
			<input type="text" name="nric" id="nric" maxlength="9" class="input" value="<?php echo $regcustomer['nric']; ?>" size="20" tabindex="10" disabled /></td>
		</tr>			
		<tr>
			<td class="LabelCell Disabled">Name</td>
			<td><input type="text" name="name" id="name" maxlength="9" class="input" value="<?php echo $regcustomer['name']; ?>" size="20" tabindex="20" disabled /></td>
		</tr>			
		<tr>
			<td class="LabelCell Disabled">Email</td>
			<td><input type="text" name="email" id="email" maxlength="9" class="input" value="<?php echo $regcustomer['email']; ?>" size="20" tabindex="30" disabled /></td>
		</tr>			
		<tr>
			<td class="LabelCell Disabled">Mobile</td>
			<td><input type="text" name="mobile" id="mobile" maxlength="9" class="input" value="<?php echo $regcustomer['contact_mobile']; ?>" size="20" tabindex="40" disabled /></td>
		</tr>
		
		<tr>
			<td class="LabelCell Required">Store</td>
			<td>
				<select tabindex="50" name="store" class="GreaterThanZero">						
					<?php
						$stores = $storeObj->getAssignedStores(Authentication::getUserId(), Authentication::getAttribute('su'));						
						echo '<option value="0">- Select -</option>';
							
						foreach ($stores as $store) {
							echo '<option value='.$store['store_id'].' '.($store['store_id'] == $point['store_id'] ? 'selected' : '') .' >'.$store['store_branch_name'].'</option>';
						}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td class="LabelCell">Reference no.</td>
			<td><input type="text" name="referenceno" id="referenceno" class="input" value="<?php echo $point['reference_no']; ?>" maxlength="50" size="20" tabindex="60" /></td>
		</tr>
		<tr>
			<td class="LabelCell Required">Amount</td>
			<td><input type="text" name="salesamount" id="salesamount" class="input Required GreaterThanZero numeric" value="" size="20" tabindex="70" /></td>
		</tr>
		<tr>
			<td class="LabelCell">Points to be cancelled</td>
			<td><input type="text" name="points" id="points" class="input Required GreaterThanZero numeric" value="" size="20" tabindex="80" disabled/></td>
		</tr>
		<tr>
			<td class="LabelCell">Remarks</td>
			<td><input type="text" name="remarks" id="remarks" maxlength="1000" class="input" value="<?php echo $point['remarks']; ?>" size="20" tabindex="90" /></td>
		</tr>
		<tr>			
			<td class="BottomToolBar" colspan="2">				
				<input type="submit" name="save" id="save" class="button-primary" value="Save" tabindex="100"/>
				<a href="customersearch.php?d=cancel" class="button-secondary" tabindex="110">Cancel</a>				
			</td>			
		</tr>
	</table>
</form>

<script type="text/javascript">
	$(document).ready(function() { 		
		loadValidation('cancelpointsform');		
		
		$('#salesamount').blur(function() {
			$.ajax({
				url: 'pointcalcsvc.php',
				data: { "amount": $('#salesamount').val(), "for": "pcalsvc" }, 
				success: function(data) {				
					$('#points').val(data.value);
				},		
			});
		});		
		
		$(".numeric").numeric(false);		
	});	
</script>