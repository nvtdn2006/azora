<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Redemption website settings'?>

<?php		
	$config = Factory::getConfig();
?>

<div class="rws-tool">	
	<a href="mod_slider_images.php" title="Manage slider images">
		<div class="rws-tool-slider"></div>
		<span>Slider</span>
	</a>
</div>

<div class="rws-tool">	
	<a href="mod_overview.php" title="Edit content of Overview">
		<div class="rws-tool-document"></div>
		<span>Overview</span>
	</a>
</div>


<div class="rws-tool">	
	<a href="mod_faq.php" title="Edit content of FAQ">
		<div class="rws-tool-document"></div>
		<span>FAQ</span>
	</a>
</div>
