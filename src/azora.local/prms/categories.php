<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Manage categories'?>

<?php		
		
	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('checkboxlist');
	JSManager::getInstance()->add('messagebox');
	
	DomainManager::getInstance()->load('Category');
	$categoryObj = new Category();
	
	$error = array();
		
	if (isset($_POST['delete']) && $_POST['delete'] == 'Remove') {					
		$categories = null;
		if (isset($_POST['categories']))
			$categories = $_POST['categories'];
		if (isset($categories) && count($categories) > 0) {
		
			$_names = '';
			$sp = '';
			foreach($categories as $item) {
				$nobj = $categoryObj->getCategory($item);
				$_names .= $sp . $nobj['category_name'];
				$sp = ', ';
			}
			
			if ($categoryObj->deleteCategory($categories)) {
			
				//Event Log
				$evtObj = new EventObject();					
				$evtObj->event_id = EventTypes::PRMS_DeleteCategory;
				$evtObj->description = Authentication::getAttribute('login_id') . ' deleted categories ('.$_names.').';
				$evtObj->action_by = Authentication::getAttribute('login_id');					
				EventLog::Log($evtObj);
				
				header( 'Location: categories.php');
			}
		} else {
			array_push($error, 'Please select at least one item to proceed.');	
		}
	}
	
	$categories = $categoryObj->getAllCategories();
?>

<?php
	if (isset($error) && count($error) > 0) {
?>
	<div class="error-info tabular-info">
		<?php foreach ($error as $handle) {
				echo "<p>$handle</p>";
		} ?>
	</div>
<?php
	}
?>

<form name="categorylist" id="categorylist" action="categories.php" method="post"> 
	<table class="tabular">
		<thead>
			<tr>			
				<th><input type="checkbox" name="listselectall" class="listselectall" onclick="javascript:checkSelectAllBox('listselect', this.checked);"/></th>
				<th>Category</th>
				<th>Description</th>
				<th>Updated By</th>
				<th>Updated On</th>			
			</tr>
		</thead>
		<tfoot>
			<tr>
				<th colspan="5"><a class="button-primary" href="category-edit.php">New</a>
				<input type="submit" name="delete" id="submit" class="button-secondary" value="Remove" onclick="javascript:return confirmBox('Are you sure you want to delete selected item?');"/>
				</th>		
			</tr>
		</tfoot>
		<tbody>		
			<?php
				foreach($categories as $category) {
			?>
			<tr>
				<td><input type="checkbox" name="categories[]" class="listselect" onclick="javascript:checkSelectBox('listselectall', 'listselect');" value="<?php echo $category['category_id']; ?>" /></td>
				<td><a class="detailLink" href="<?php echo 'category-edit.php?category='.$category['category_id'] ?>" alt="Edit" title="Edit"><?php echo $category['category_name'] ?></a></td>
				<td><?php echo $category['category_description'] ?></td>
				<td><?php echo $category['updated_username'] ?></td>
				<td><?php echo $category['updated_dt'] ?></td>				
			</tr>
			<?php } ?>
		</tbody>
	</table>
</form>
<?php
	$categoryObj = null;
?>