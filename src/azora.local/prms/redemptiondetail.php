<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Redemption Detail'?>

<?php		

	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('validation');
		
	DomainManager::getInstance()->load('Redemption');
	$redemptionObj = new Redemption();
	
	DomainManager::getInstance()->load('Company');
	$companyObj = new Company();
	$company = $companyObj->getCompany();
	$company = $company[0];
	
	$error = array();
	
	$config = Factory::getConfig();
	$readonly = true;
	
	$redemption_id = 0;
	
	if(isset($_POST['redemption_id']) && $_POST['redemption_id'] > 0) {
	
		if(isset($_POST['status']) && $_POST['status'] == 2) {
		
			$update = array('redemption_id' => $_POST['redemption_id'],
						'redemption_status' => $_POST['status'],						
						'updated_remarks' => $_POST['remarks'],						
						'updated_by' => Authentication::getUserId());
			if ($redemptionObj->updateRedemptionStatus($update)) {
			
				$redemption = $redemptionObj->getRedemption($_POST['redemption_id']);
				
				//Event Log
				$evtObj = new EventObject();					
				$evtObj->event_id = EventTypes::PRMS_UpdateRedemptionStatus;
				$evtObj->description = Authentication::getAttribute('login_id') . ' updated redemption status (Collected) for no. '.$redemption['redemption']['redemption_no'].' submitted by ('.$redemption['redemption']['nric'].') on ('.$redemption['redemption']['transaction_dt'].').';
				$evtObj->action_by = Authentication::getAttribute('login_id');					
				EventLog::Log($evtObj);
			
				header( 'Location: redemptiondone.php?id='.$_POST['redemption_id']);
				exit();
			} else {
				array_push($error, 'Saving process failed. Please try again.');
			}
		
		} else if (isset($_POST['status']) && $_POST['status'] == 4) {
		
		
			$update = array('redemption_id' => $_POST['redemption_id'],
						'redemption_status' => $_POST['status'],						
						'updated_remarks' => $_POST['remarks'],						
						'updated_by' => Authentication::getUserId());
			if ($redemptionObj->updateRedemptionStatus($update)) {
			
				$redemption = $redemptionObj->getRedemption($_POST['redemption_id']);
				
				//Event Log
				$evtObj = new EventObject();					
				$evtObj->event_id = EventTypes::PRMS_UpdateRedemptionStatus;
				$evtObj->description = Authentication::getAttribute('login_id') . ' updated redemption status (Cancelled) for no. '.$redemption['redemption']['redemption_no'].' submitted by ('.$redemption['redemption']['nric'].') on ('.$redemption['redemption']['transaction_dt'].').';
				$evtObj->action_by = Authentication::getAttribute('login_id');					
				EventLog::Log($evtObj);								
		
				require_once './includes/user.php';
				$userObj = new User();
				
				$users = $userObj->getUserForRedemptionReminder($redemption['redemption']['store_id']);
			
				if (isset($users)) {
				
					foreach($users as $user) {					
						
						$property = array ('full_name' => $user['full_name'],
											'company_name' => $company['company_name'],
											'redemption_no' => $redemption['redemption']['redemption_no'],
											'transaction_dt' => $redemption['redemption']['transaction_dt']);

						$mailer = new SiteMailer();
						$mailer->toMail = $user['email'];
						$mailer->subject = 'Reminder for cancelled redemption at '.$company['company_name'].' PRMS';				
						$mailer->PrepareMail('sendReminderforcancelledredemptionToUser', $property);
						$mailer->Send();
					}
					
				}
				
				
			
				header( 'Location: redemptiondone.php?id='.$_POST['redemption_id']);
				exit();
			} else {
				array_push($error, 'Saving process failed. Please try again.');
			}
		
		}
		
	}
	
	if(isset($_GET['id'])) {
	
		//$redemption_id = Factory::getCryptographer()->Decrypt($_GET['id']);
		$redemption_id = $_GET['id'];
		
		if(isset($_GET['m'])) {			
			if (Factory::getCryptographer()->Decrypt($_GET['m']) == 'write')
				$readonly = false;
		}
		
		$redemption = $redemptionObj->getRedemption($redemption_id);
		
		if ($redemption == null) {
			header( 'Location: redemptions.php');
			exit();
		}			
		
	} else {
		header( 'Location: redemptions.php');
		exit();
	}	
	
?>

<?php
	if (isset($error) && count($error) > 0) {
?>
	<div class="error-info">
		<?php foreach ($error as $handle) {
				echo "<p>$handle</p>";
		} ?>
	</div>
<?php
	}
?>

<form name="rdetailform" id="rdetailform" action="redemptiondetail.php" method="post"> 
	<table class="formview" width="100%" border="0" cellspacing="3px" cellpadding="3px">
		<tr>
			<td >NRIC</td>
			<td><input type="text" name="nric" id="nric"class="input" value="<?php echo $redemption['redemption']['nric']; ?>" size="20" tabindex="10" disabled /></td>
		</tr>
		<tr>
			<td >Name</td>
			<td><input type="text" name="name" id="name"class="input" value="<?php echo $redemption['redemption']['name']; ?>" size="20" tabindex="20" disabled /></td>
		</tr>
		
		<tr>			
			<td class="SectionBar" colspan="2">				
				Redemption
			</td>
		</tr>
		
		<tr>
			<td >Total redemption points</td>
			<td><input type="text" name="t_points" id="t_points"class="input" value="<?php echo $redemption['redemption']['total_points']; ?>" size="20" tabindex="30" disabled /></td>
		</tr>
		<tr>
			<td >Store</td>
			<td><input type="text" name="Store" id="Store" class="input" value="<?php echo $redemption['redemption']['store_branch_name']; ?>" size="20" tabindex="40" disabled /></td>			
		</tr>
		<tr>
			<td >Date <span class="hint">(DD/MM/YYYY)</span></td>
			<td><input type="text" name="date" id="date" class="input " value="<?php echo date('d/m/Y', strtotime($redemption['redemption']['collection_date']));  ?>" size="20" tabindex="50" disabled /></td>
		</tr>
		<tr>
			<td >Time</td>
			<td><input type="text" name="Time" id="Time" class="input " value="<?php echo date('h:i A', strtotime($redemption['redemption']['collection_time']));  ?>" size="20" tabindex="60" disabled /></td>
		</tr>
		<tr>			
			<td class="SectionBar" colspan="2">				
				Points
			</td>
		</tr>
		<tr>			
			<td colspan="2">				
				<table width="100%" border="0" cellspacing="3px" cellpadding="3px">						
					<?php							
						$tpoints = 0;
						foreach($redemption['redemption_points'] as $detail) {
					?>
						<tr>						
							<td><?php echo $detail['branch_name']; ?></td>
							<td><?php echo $detail['store_name']; ?></td>								
							<td width="70px"><input type="text" name="points[]" class="input" value="<?php 
									echo $detail['points'];
								?>" size="20" tabindex="70" style="width:50px" disabled /></td>
						</tr>
					<?php 
							$tpoints += $detail['points'];
						} ?>
					<tr>
						<td>&nbsp;</td>
						<td><b>Total</b></td>
						<td><span id="tpoints"><?php echo $tpoints; ?></span>&nbsp;pts</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr>			
			<td class="SectionBar" colspan="2">				
				Products
			</td>
		</tr>
		
		<tr>
			<td colspan="2">
				<table width="100%" border="0" cellspacing="3px" cellpadding="3px">							
					<tbody>		
						<?php							
							foreach($redemption['redemption_products'] as $item) {
								DomainManager::getInstance()->load('Product');
								$productObj = new Product();	
								$product = $productObj->getProduct($item['product_id']);
								
								$images = $product['product_images'];
								$image  = null;

								foreach($images as $img) {
									if ($img['image_type'] == 'S') {
										$image = $img;
									}													
								}
								
								if (!$image) {
									foreach($images as $img) {
										if ($img['image_type'] == 'T') {
											$image = $img;
										}													
									}
								}									
						?>
						<tr>								
							<td width="60px">
								<img src="<?php echo $config['PRMSConfig']->live_site . '/domain/images/products/' . $image['sys_file_name']; ?>" width="50" height="50" alt="<?php echo $product['product']['product_name']; ?>" title="<?php echo $product['product']['product_name']; ?>"/></td>
							<td><b><?php echo $product['product']['product_name']; ?></b></td>
							<td><?php echo $product['product']['product_description']; ?></td>
							<td width="80px">&nbsp;<?php echo number_format($item['product_points']); ?> pts</td>
							<td width="50px"><input type="text" name="quantity[]" class="input Required numeric" value="<?php echo $item['quantity'] ?>" size="20" tabindex="80" style="width:30px" disabled /></td>						
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</td>
		</tr>
		
		<?php if(!$readonly) { ?>
		<tr>			
			<td class="SectionBar" colspan="2">				
				Redemption status
			</td>
		</tr>
		
		
		<tr>		
			<td class="LabelCell Required">Status</td>
			<td>				
				<select tabindex="50" name="status" tabindex="90" class="GreaterThanZero">						
					<option value="2">Collected</option>					
					<option value="4">Cancelled</option>					
				</select>
			</td>			
		</tr>
		
		<tr>		
			<td class="LabelCell">Remarks</td>
			<td><input type="text" name="remarks" id="remarks" maxlength="1000" class="input" value="" size="20" tabindex="100" /></td>			
		</tr>
		<?php } else { ?>
		
		<tr>			
			<td class="SectionBar" colspan="2">				
				Redemption Status
			</td>
		</tr>
		
		<tr>		
			<td>Status</td>
			<td><?php echo $redemptionObj->getRedemptionStatus($redemption['redemption']['redemption_status']) ?></td>			
		</tr>
		<tr>		
			<td>Remark</td>
			<td><?php echo $redemption['redemption']['updated_remarks']; ?></td>			
		</tr>
		<tr>		
			<td>Updated By</td>
			<td><?php echo $redemption['redemption']['updated_username']; ?></td>			
		</tr>
		<tr>		
			<td>Updated On</td>
			<td><?php echo isset($redemption['redemption']['updated_dt']) ? date('Y-m-d h:i A', strtotime($redemption['redemption']['updated_dt'])) : '';  ?></td>			
		</tr>
		
		<?php } ?>
		
		<tr>			
			<td class="BottomToolBar" colspan="2">				
				<?php if(!$readonly) { ?>
				<input id="redemption_id" name="redemption_id" type="hidden" value="<?php echo $redemption_id; ?>"/>
				<input type="submit" name="submit" id="submit" class="button-primary" value="Save" tabindex="110"/>
				<a href="redemptions.php" class="button-secondary" tabindex="120">Back</a>
				<?php } else { ?>
				<a href="redemptionrpt.php" class="button-secondary" tabindex="120">Back</a>				
				<?php } ?>				
			</td>			
		</tr>
	</table>
</form>


<script type="text/javascript">
	$(document).ready(function() { 
		loadValidation('rdetailform');
	});
</script>