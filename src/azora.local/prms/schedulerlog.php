<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Scheduler log'?>

<?php	
	$logs = null;
	$logs = SchedulerLog::GetSchedulerLogs();
?>

<table class="tabular">
	<thead>
		<tr>			
			<th style="width: 150px;">Scheduler</th>
			<th>Status</th>
			<th>Last run</th>
			<?php //<th>By manual</th>			?>
		</tr>
	</thead>
	<tbody>
		<?php
			if ($logs != null) {
			foreach($logs as $log) {
		?>
		<tr>						
			<td><?php echo $log['scheduler_name'];?></td>
			<td><?php echo $log['status'] ? 'Success' : 'Failed'; ?></td>
			<td><?php echo $log['last_run']; ?></td>
			<?php //<td> echo $log['is_manual'] ? 'Yes' : 'No'; </td>		?>	
		</tr>
		<?php } } ?>
	</tbody>
</table>