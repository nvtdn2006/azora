<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Redemption done'; ?>

<?php
	$redemption_id = 0;
	
	DomainManager::getInstance()->load('Redemption');
	$redemptionObj = new Redemption();
	
	if(isset($_GET['id'])) {
	
		$redemption_id = $_GET['id'];				
		
		$redemption = $redemptionObj->getRedemption($redemption_id);
		
		if ($redemption == null) {
			header( 'Location: redemptions.php');
			exit();
		}					
	} else {
		header( 'Location: redemptions.php');
		exit();
	}	
?>	
 <div class="cprocess">
	Redemption process successfully done.<br/><br/><br/><br/>
	Points <b><?php echo $redemption['redemption']['total_points']; ?></b> have been deducted from account <b><?php echo $redemption['redemption']['nric']; ?></b>.		
	<br/><br/><br/><br/><br/><br/>
	<a href="redemptions.php" class="button-secondary" title="Back to redemption.">Done</a>
</div>