<?php require_once './includes/service.php'; ?>

<?php

	$return = array();
	if (isset($_POST['form']) && $_POST['form'] == 'change'
		&& $_POST['uid'] != '' 
		&& $_POST['password'] != ''
		&& $_POST['rtpassword'] != '') {
		
		if ($_POST['password'] == $_POST['rtpassword']) {
			
			require_once './includes/user.php';
			$userObj = new User();
			
			$newpassword = array('user_id' => $_POST['uid'],
									'password' => $_POST['password'],
									'updated_by' => Authentication::getUserId());
									
			if($userObj->resetPassword($newpassword)) {
				$return['status'] = true;
				
				$nobj = $userObj->getUser($_POST['uid']);
				
				//Event Log
				$evtObj = new EventObject();					
				$evtObj->event_id = EventTypes::PRMS_ResetUserPassword;
				$evtObj->description = Authentication::getAttribute('login_id') . ' changed password for user ('.$nobj['user']['username'].').';
				$evtObj->action_by = Authentication::getAttribute('login_id');					
				EventLog::Log($evtObj);
				
			} else {
				$return['status'] = false;
				$return['message'] = 'Process failed. Please try again.';
			}
			
		} else {
			$return['status'] = false;
			$return['message'] = 'Passwords do not match.';
		}				
	} else {
		$return['status'] = false;
		$return['message'] = 'Process failed. Please try again.';
	}
		
	header('Content-type: application/json');
	echo json_encode($return);
	
?>