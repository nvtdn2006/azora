<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Customer report'?>

<?php		

/*
 * 11/02/2012 PFX20120001 Add Seacrch by Mobile
 * 06/04/2013 FX.1304.004 Add in 2 columns Expiry date & Total available points
 */

	include 'connectionstring.php';

	$checksearch = $_GET['chksearch'];
	$customercount = $_GET['custcount'];
	$frmnric = $_GET['fromnric'];
	$frmname = $_GET['fromname'];
	$frmemail = $_GET['fromemail'];
	$frmcompany = $_GET['fromcompany'];
	$frmmonth = $_GET['frommonth'];	  /* #PFX20120002 11/02/2012 */
	$frmyear= $_GET['fromyear'];	  /* #PFX20120002 11/02/2012 */
	$frmmobile = $_GET['frommobile']; /* #PFX20120001 11/02/2012 */

	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('validation');
	JSManager::getInstance()->add('numeric');
	
	DomainManager::getInstance()->load('Customer');	
        
        //+START FX.1304.004 Load Point & Reward Classs
        DomainManager::getInstance()->load('Point');
	$pointObj = new Point();
                       
        DomainManager::getInstance()->load('Reward');
        $rewardObj = new Reward();
        //-END FX.1304.004 
                
	
	$customerObj = new Customer();	
	$cutomers = null;	
	$formvalues = null;

	if ($checksearch == 0 && $customercount != null) {		
		$cutomers = $customerObj->getCustomerProfileByValues();
		$checksearch = 0;
	}

	if ($checksearch == 1 && $customercount != null) {
			
		$customer_kv = array();

		$customer_kv['nric'] = $frmnric;
		$searchnric = $frmnric;
		$customer_kv['name'] = $frmname;
		$searchname = $frmname;
		$customer_kv['email'] = $frmemail;
		$searchemail = $frmemail;
		$customer_kv['company'] = $frmcompany;
		$searchcompany = $frmcompany;
		$customer_kv['month'] = $frmmonth; /* #PFX20120002 11/02/2012 */
		$searchmonth = $frmmonth;          /* #PFX20120002 11/02/2012 */
		$customer_kv['year'] = $frmyear;   /* #PFX20120002 11/02/2012 */
		$searchyear = $frmyear;            /* #PFX20120002 11/02/2012 */
		$customer_kv['contact_mobile'] = $frmmobile; /* #PFX20120001 11/02/2012 */
		$searchmobile = $frmmobile;		   /* #PFX20120001 11/02/2012 */
		
		if (count($customer_kv) > 0) {
			$cutomers = $customerObj->getCustomerProfileByValues($customer_kv);
			$checksearch = 1;
		}	
	}
	
	if (isset($_POST['search']) && $_POST['search'] == 'Search') {
			
		$customer_kv = array();
		
		if (isset($_POST['nric']) && $_POST['nric'] != '') {	
			$searchnric = $_POST['nric'] ;
			$customer_kv['nric'] = $_POST['nric'] ;
		}
		
		if (isset($_POST['name']) && $_POST['name'] != '') {	
			$searchname = $_POST['name'] ;		
			$customer_kv['name'] = $_POST['name'] ;
		}
		
		if (isset($_POST['email']) && $_POST['email'] != '') {	
			$searchemail = $_POST['email'] ;				
			$customer_kv['email'] = $_POST['email'] ;
		}
	
		if (isset($_POST['company']) && $_POST['company'] != '') {	
			$searchcompany = $_POST['company'] ;				
			$customer_kv['company'] = $_POST['company'] ;
		}
		
		/* +Start PFX20120002 11/02/2012 */
		if (isset($_POST['month']) && ( $_POST['month'] != '0' || $_POST['month'] != '' ) ) {
			$searchmobile = $_POST['month'] ;
			$customer_kv['month'] = $_POST['month'] ;
		}
		if (isset($_POST['year']) && ( $_POST['year'] != '0' || $_POST['year'] != '' ) ) {
			$searchmobile = $_POST['year'] ;
			$customer_kv['year'] = $_POST['year'] ;
		}
		/* -End PFX20120002 11/02/2012 */		
		
		/* +Start PFX20120001 11/02/2012 */		
		if (isset($_POST['mobile']) && $_POST['mobile'] != '') {
			$searchmobile = $_POST['mobile'] ;
			$customer_kv['contact_mobile'] = $_POST['mobile'] ;
		}
		/* -End PFX20120001 11/02/2012 */
		
		if (count($customer_kv) > 0) {
			$cutomers = $customerObj->getCustomerProfileByValues($customer_kv);
			$checksearch = 1;
		}	
	}

	if (isset($_POST['showall']) && $_POST['showall'] == 'Show all') {		
		$cutomers = $customerObj->getCustomerProfileByValues();
		$checksearch = 0;
	}
	
	if (isset($_REQUEST['nric'])) 			
		$formvalues['nric'] = $_REQUEST['nric'];
	else
		$formvalues['nric'] =  '';
				
	if (isset($_REQUEST['name'])) 			
		$formvalues['name'] = $_REQUEST['name'];
	else
		$formvalues['name'] =  '';
		
	if (isset($_REQUEST['email'])) 			
		$formvalues['email'] = $_REQUEST['email'];
	else
		$formvalues['email'] =  '';
		
	if (isset($_REQUEST['company'])) 			
		$formvalues['company'] = $_REQUEST['company'];
	else
		$formvalues['company'] =  '';
	
	/* +Start PFX20120002 11/02/2012 */
	if (isset($_REQUEST['month']))
		$formvalues['month'] = $_REQUEST['month'];
	else
		$formvalues['month'] =  '0';
	if (isset($_REQUEST['year']))
		$formvalues['year'] = $_REQUEST['year'];
	else
		$formvalues['year'] =  '0';	
	/* -End PFX20120002 11/02/2012 */	
	
	/* +Start PFX20120001 11/02/2012 */	
	if (isset($_REQUEST['mobile']))
		$formvalues['mobile'] = $_REQUEST['mobile'];
	else
		$formvalues['mobile'] =  '';	
	/* -End PFX20120001 11/02/2012 */
		
?>

<form name="customerrptform" id="customerrptform" action="customerrpt.php?action=search" method="post"> 
	<table class="formview" width="100%" border="0">		
		<tr>
			<td>NRIC</td>
			<?
			if ($frmnric != null) {
			?>
			<td><input type="text" name="nric" id="nric" class="input" value="<?php echo $frmnric; ?>" size="20" tabindex="10" /></td>			
			<?
			}
			else {
			?>
			<td><input type="text" name="nric" id="nric" class="input" value="<?php echo $formvalues['nric']; ?>" size="20" tabindex="10" /></td>
			<?
			}
			?>			

			<td>Name</td>	
			<?
			if ($frmname != null) {
			?>
			<td><input type="text" name="name" id="name" class="input" value="<?php echo $frmname; ?>" size="20" tabindex="20" /></td>	
			<?
			}
			else {
			?>	
			<td><input type="text" name="name" id="name" class="input" value="<?php echo $formvalues['name']; ?>" size="20" tabindex="20" /></td>			
			<?
			}
			?>

		</tr>		
		<tr>
			<td>Email</td>	
			<?
			if ($frmemail != null) {
			?>
			<td><input type="text" name="email" id="email" class="input" value="<?php echo $frmemail; ?>" size="20" tabindex="30" /></td>
			<?
			}
			else {
			?>		
			<td><input type="text" name="email" id="email" class="input" value="<?php echo $formvalues['email']; ?>" size="20" tabindex="30" /></td>
			<?
			}
			?>

			<td>Company</td>
			<?
			if ($frmcompany != null) {
			?>
			<td><input type="text" name="company" id="company" class="input" value="<?php echo $frmcompany; ?>" size="20" tabindex="40" /></td>		
			<?
			}
			else {
			?>		
			<td><input type="text" name="company" id="company" class="input" value="<?php echo $formvalues['company']; ?>" size="20" tabindex="40" /></td>		
			<?
			}
			?>
				
		</tr>	

<!-- +Start PFX20120002 11/02/2012 -->			
		<tr>
			<td>Month</td>		
			<td>			
				<select id="s_month" tabindex="50" name="month" class="GreaterThanZero" tabindex="30">						
					<option value="0" <?php echo $formvalues['month'] == 0 ? 'selected' : ''; ?>>- Select -</option>
					<option value="1" <?php echo $formvalues['month'] == 1 ? 'selected' : ''; ?>>Jan</option>
					<option value="2" <?php echo $formvalues['month'] == 2 ? 'selected' : ''; ?>>Feb</option>
					<option value="3" <?php echo $formvalues['month'] == 3 ? 'selected' : ''; ?>>Mar</option>
					<option value="4" <?php echo $formvalues['month'] == 4 ? 'selected' : ''; ?>>Apr</option>
					<option value="5" <?php echo $formvalues['month'] == 5 ? 'selected' : ''; ?>>May</option>
					<option value="6" <?php echo $formvalues['month'] == 6 ? 'selected' : ''; ?>>Jun</option>
					<option value="7" <?php echo $formvalues['month'] == 7 ? 'selected' : ''; ?>>Jul</option>
					<option value="8" <?php echo $formvalues['month'] == 8 ? 'selected' : ''; ?>>Aug</option>
					<option value="9" <?php echo $formvalues['month'] == 9 ? 'selected' : ''; ?>>Sep</option>
					<option value="10" <?php echo $formvalues['month'] == 10 ? 'selected' : ''; ?>>Oct</option>
					<option value="11" <?php echo $formvalues['month'] == 11 ? 'selected' : ''; ?>>Nov</option>
					<option value="12" <?php echo $formvalues['month'] == 12 ? 'selected' : ''; ?>>Dec</option>						
				</select>			
			</td>			
			<td>Year</td>		
			<td>
				<select id="s_year" tabindex="50" name="year" class="GreaterThanZero" tabindex="40">				
					<option value="0">- Select -</option>
					<?php
						if ( $formvalues['year'] != 0 || $formvalues['year'] != '' ) {
							$year = $formvalues['year'];
						} else {
							$year = 0; 
						}
						for($i=0;$i<3;$i++) {
							echo '<option value="'.($year - $i).'" '.($formvalues['year'] == ($year - $i) ? 'selected' : '').'  >'.($year - $i).'</option>';							
						}
					?>
				</select>				
			</td>			
		</tr>				
<!-- +End PFX20120002  -->				
<!-- +Start PFX20120001 11/02/2012 -->		
		<tr>
			<td>Mobile</td>		
			<?
			if ($frmmobile != null) {
			?>					
			<td><input type="text" name="mobile" id="mobile" class="input" value="<?php echo $frmmobile; ?>" size="20" tabindex="49" /></td>
			<?
			}
			else {
			?>		
			<td><input type="text" name="mobile" id="mobile" class="input" value="<?php echo $formvalues['mobile']; ?>" size="20" tabindex="49" /></td>
			<?
			}
			?>						
			<td>&nbsp;</td>		
			<td>&nbsp;</td>						
		</tr>
<!-- +End PFX20120001  -->			
		<tr>					
			<td colspan="4">
				<input type="submit" name="search" id="search" class="button-primary" value="Search" tabindex="50"/>				
				<input type="submit" name="showall" id="showall" class="button-primary" value="Show all" tabindex="60"/>
			</td>			
		</tr>
	</table>
</form>

<script>
<?php 
$this_year = date('Y'); 
?>

$("#s_year").change(function () {

	this_year = <?php echo $this_year ?>;
	cutoff = 2012;
	
	sel_year = $("#s_year option:selected").val();
	year = sel_year;
	if ( year == 0 | year == '' ) {
		year = this_year;
	}
	year_to = parseInt(year) + 5;
	
	if ( year_to > this_year ) { year_to = this_year; }

	year_fr = parseInt(year_to) - 11;
	 
	if ( year_fr < cutoff ) { year_fr = cutoff; }	

	if ( year_fr > year_to ) {	year_fr = year_to;	}		

	$("#s_year option").remove();
	$("#s_year").append("<option value='0'>- Select -</option>");
	for(i=year_to; i>=year_fr; i--) {
		$("#s_year").append("<option value='"+ i +"'>"+ i +"</option>");
	}	

	$("#s_year").val(sel_year);
	
  })
  .trigger('change');
</script>

<?php 
/* #PFX201200014 - removed now required
$cutomer_all = $customerObj->getCustomerProfileByValues();
*/

$rewards = $rewardObj->getReward(); //FX.1304.004 

if ($cutomers != null && count($cutomers) > 0) { 
	$timestamp = time();
$report['report'] = 'CUSTRPT';
$report['report_name'] = 'cust_report_' . date('Ymd') . '.xls'; 

// Create Download file
$i = 0; 
$content[$i][0] = 'NRIC/FIN';
$content[$i][1] = 'Name';
$content[$i][2] = 'Date of Birth (DD/MM/YYYY)';
$content[$i][3] = 'Address';
$content[$i][4] = 'Company';
$content[$i][5] = 'Mobile';
$content[$i][6] = 'Home';
$content[$i][7] = 'Email';
$content[$i][8] = 'Activated';
$content[$i][9] = 'Total Pts'; //FX.1304.004
$content[$i][10] = 'Expiry date'; //FX.1304.004

foreach($cutomers as $customer) { 
	$i++;	
        
        //+START FX.1304.004    
        // Retrieve Customer Point & Expiry Date
        $points = $pointObj->getPointByCustomer($customer['customer_id']);

        $pointExpiryDate = $pointObj->getPointExpiryDate($customer['customer_id']);
        
        //-END FX.1304.004
                        
	$content[$i][0] = $customer['nric'];
	$content[$i][1] = $customer['name'];
	$content[$i][2] = date("d/m/Y", strtotime($customer['date_of_birth']));
	$content[$i][3] = $customer['address'];
	$content[$i][4] = $customer['company'];
	$content[$i][5] = $customer['contact_mobile'];
	$content[$i][6] = $customer['contact_home'];
	$content[$i][7] = $customer['email'];
	$content[$i][8] = ($customerObj->isActivatedUser($customer['nric']) ? 'Yes' : 'No');
        $content[$i][9] = $points;   //FX.1304.004
	$content[$i][10] = !is_null($pointExpiryDate)? $pointExpiryDate->format("d/m/Y") : "" ;//FX.1304.004
}
$report['content'] = $content;
$session_id = 'c'.$timestamp;
$_SESSION[$session_id] = $report;

/* #PFX201200014 - removed now required
// Create Download for All Customers
$report2['report'] = 'CUSTRPT';
$report2['report_name'] = 'allcust_report_' . date('Ymd') . '.xls';
$i = 0;
$content2[$i][0] = 'NRIC';
$content2[$i][1] = 'Name';
$content2[$i][2] = 'Company';
$content2[$i][3] = 'Mobile';
$content2[$i][4] = 'Email';
$content2[$i][5] = 'Activated';
foreach($cutomer_all as $cutomer) {
	$i++;
	$content2[$i][0] = $cutomer['nric'];
	$content2[$i][1] = $cutomer['name'];
	$content2[$i][2] = $cutomer['company'];
	$content2[$i][3] = $cutomer['contact_mobile'];
	$content2[$i][4] = $cutomer['email'];
	$content2[$i][5] = ($customerObj->isActivatedUser($cutomer['nric']) ? 'Yes' : 'No');
}
$report2['content'] = $content2;
$session_id2 = 'ca'.$timestamp;
$_SESSION[$session_id2] = $report2;
*/

?>
	<div class="SectionTitle" ><b><?php echo ($cutomers != null && count($cutomers) > 0) ? count($cutomers) : '0'; ?> </b> Record(s) found.
		  <?php if ($cutomers != null && count($cutomers) > 0)  { ?>
	( <a href="dl.php?p=<?php echo $session_id ?>" class="downlink"> download </a> )
	  <?php } ?> 
	</div>
	
<?php		
	if ($cutomers != null && count($cutomers) > 0) {		
?>

<?php
// find out how many rows are in the table 
$numrows = count($cutomers);

// number of rows to show per page
$rowsperpage = 20;
// find out total pages
$totalpages = ceil($numrows / $rowsperpage);

// get the current page or set a default
if (isset($_GET['currentpage']) && is_numeric($_GET['currentpage'])) {
   // cast var as int
   $currentpage = (int) $_GET['currentpage'];
} else {
   // default page num
   $currentpage = 1;
}

// if current page is greater than total pages...
if ($currentpage > $totalpages) {
   // set current page to last page
   $currentpage = $totalpages;
}
// if current page is less than first page...
if ($currentpage < 1) {
   // set current page to first page
   $currentpage = 1;
}

// the offset of the list, based on current page 
$offset = ($currentpage - 1) * $rowsperpage;

$display_cutomers = $customerObj->getCustomerProfileByValues($customer_kv, $offset, $rowsperpage); /* #PFX20120002 11/02/2012 */
/* #PFX20120002 11/02/2012 
if ($checksearch == 0) {
$custinfo = mysql_query("SELECT * FROM customer_profiles LIMIT $offset, $rowsperpage");
}
else if ($checksearch == 1) {
$custinfo = mysql_query("SELECT * FROM customer_profiles WHERE (nric LIKE '%$searchnric%') AND (name LIKE '%$searchname%') AND (email LIKE '%$searchemail%') AND (company LIKE '%$searchcompany%') AND (contact_mobile LIKE '%$searchmobile%')LIMIT $offset, $rowsperpage");
}
*/
?>

<table class="tabular">
	<thead>
		<tr>			
			<th>NRIC</th>
			<th>Name</th>
			<th>Company</th>
			<th>Mobile</th>
			<th>Email</th>			
			<th>Activated</th>
                        <th>Total Pts</th>   <? //FX.1304.004 ?>
                        <th>Expiry date</th> <? //FX.1304.004 ?>
		</tr>
	</thead>
	<tbody>
		<?php
                        $rewards = $rewardObj->getReward();
                                        
			foreach ($display_cutomers as $key => $getcustinfo ) { /* #PFX20120002 11/02/2012 */
			// while ($getcustinfo = mysql_fetch_assoc($cutomers_display)) { /* #PFX20120002 11/02/2012 */
                           
                        //+START FX.1304.004    
                        // Retrieve Customer Point & Expiry Date
                        $points = $pointObj->getPointByCustomer($getcustinfo['customer_id']);
                
                        $pointExpiryDate = $pointObj->getPointExpiryDate($getcustinfo['customer_id']);
                        $pointExpiryDate = !is_null($pointExpiryDate)? $pointExpiryDate->format("d/m/Y") : "" ;
                        //-END FX.1304.004
		?>
		<tr>			
			<td><a class="detailLink" href="customerrptdetail.php?id=<?php echo $getcustinfo['customer_id'] ?>" alt="Select" title="Select"><?php echo $getcustinfo['nric'] ?></a></td>
			<td><?php echo $getcustinfo['name'] ?></td>
			<td><?php echo $getcustinfo['company'] ?></td>
			<td><?php echo $getcustinfo['contact_mobile'] ?></td>
			<td><?php echo $getcustinfo['email'] ?></td>
			<td><?php echo ($customerObj->isActivatedUser($getcustinfo['nric']) ? 'Yes' : 'No'); ?></td>	
                        <td><?php echo $points ?></td> <? //FX.1304.004 ?>
                        <td><?php echo $pointExpiryDate ?></td>   <? //FX.1304.004 ?>
		</tr>
		<?php } ?>
	</tbody>
</table>

<?php
if ($checksearch == 0) //means Show All clicked
{

/******  build the pagination links ******/
// range of num links to show
$range = 5;
?>

<table class="formview" width="100%" border="0" cellspacing="3px" cellpadding="3px">
<tr>
<td class="BottomToolBar" align="right">
Page <?php echo $currentpage;?> of <?php echo $totalpages;?>

<?php
if ($totalpages != 1) {
?>

&nbsp;&nbsp;&nbsp;&nbsp;

<?php
// if not on page 1, don't show back links
if ($currentpage > 1) {
   // show << link to go back to page 1
   echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=1&chksearch=$checksearch&custcount=$numrows' style='text-decoration:none' class='button-primary'><< First</a> ";
   // get previous page num
   $prevpage = $currentpage - 1;
   // show < link to go back to 1 page
   echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=$prevpage&chksearch=$checksearch&custcount=$numrows' style='text-decoration:none' class='button-primary'>< Prev</a>&nbsp; ";
}

// loop to show links to range of pages around current page
for ($x = ($currentpage - $range); $x < (($currentpage + $range) + 1); $x++) {
   // if it's a valid page number...
   if (($x > 0) && ($x <= $totalpages)) {
      // if we're on current page...
      if ($x == $currentpage) {
         // 'highlight' it but don't make a link
         echo " <b><font size='2'>$x</font></b> ";
      // if not current page...
      } else {
         // make it a link
         echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=$x&chksearch=$checksearch&custcount=$numrows'><font size='2'>$x</font></a> ";
      }
   }
}
                 
// if not on last page, show forward and last page links        
if ($currentpage != $totalpages) {
   // get next page
   $nextpage = $currentpage + 1;
    // echo forward link for next page 
   echo " &nbsp;<a href='{$_SERVER['PHP_SELF']}?currentpage=$nextpage&chksearch=$checksearch&custcount=$numrows' style='text-decoration:none' class='button-primary'>Next ></a> ";
   // echo forward link for lastpage
   echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=$totalpages&chksearch=$checksearch&custcount=$numrows' style='text-decoration:none' class='button-primary'>Last >></a> ";
}

}
?>

</td>
</tr>
</table>

<?php
}
else if ($checksearch == 1) //means Search clicked
{

/******  build the pagination links ******/
// range of num links to show
$range = 10;
?>

<table class="formview" width="100%" border="0" cellspacing="3px" cellpadding="3px">
<tr>
<td class="BottomToolBar" align="right">
Page <?php echo $currentpage;?> of <?php echo $totalpages;?>

<?php
if ($totalpages != 1) {
?>

&nbsp;&nbsp;&nbsp;&nbsp;

<?php
// if not on page 1, don't show back links
if ($currentpage > 1) {
   // show << link to go back to page 1
   echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=1&chksearch=$checksearch&custcount=$numrows&fromnric=$searchnric&fromname=$searchname&fromemail=$searchemail&fromcompany=$searchcompany&frommobile=$searchmobile&frommonth=$searchmonth&fromyear=$searchyear' style='text-decoration:none' class='button-primary'><< First</a> ";
   // get previous page num
   $prevpage = $currentpage - 1;
   // show < link to go back to 1 page
   echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=$prevpage&chksearch=$checksearch&custcount=$numrows&fromnric=$searchnric&fromname=$searchname&fromemail=$searchemail&fromcompany=$searchcompany&frommobile=$searchmobile&frommonth=$searchmonth&fromyear=$searchyear' style='text-decoration:none' class='button-primary'>< Prev</a>&nbsp; ";
}

// loop to show links to range of pages around current page
for ($x = ($currentpage - $range); $x < (($currentpage + $range) + 1); $x++) {
   // if it's a valid page number...
   if (($x > 0) && ($x <= $totalpages)) {
      // if we're on current page...
      if ($x == $currentpage) {
         // 'highlight' it but don't make a link
         echo " <b><font size='2'>$x</font></b> ";
      // if not current page...
      } else {
         // make it a link
         echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=$x&chksearch=$checksearch&custcount=$numrows&fromnric=$searchnric&fromname=$searchname&fromemail=$searchemail&fromcompany=$searchcompany&frommobile=$searchmobile&frommonth=$searchmonth&fromyear=$searchyear'><font size='2'>$x</font></a> ";
      }
   }
}
                 
// if not on last page, show forward and last page links        
if ($currentpage != $totalpages) {
   // get next page
   $nextpage = $currentpage + 1;
    // echo forward link for next page 
   echo " &nbsp;<a href='{$_SERVER['PHP_SELF']}?currentpage=$nextpage&chksearch=$checksearch&custcount=$numrows&fromnric=$searchnric&fromname=$searchname&fromemail=$searchemail&fromcompany=$searchcompany&frommobile=$searchmobile&frommonth=$searchmonth&fromyear=$searchyear' style='text-decoration:none' class='button-primary'>Next ></a> ";
   // echo forward link for lastpage
   echo " <a href='{$_SERVER['PHP_SELF']}?currentpage=$totalpages&chksearch=$checksearch&custcount=$numrows&fromnric=$searchnric&fromname=$searchname&fromemail=$searchemail&fromcompany=$searchcompany&frommobile=$searchmobile&frommonth=$searchmonth&fromyear=$searchyear' style='text-decoration:none' class='button-primary'>Last >></a> ";
}

}
?>

</td>
</tr>
</table>

<?php
}
?>

<?php } 
}	
?>