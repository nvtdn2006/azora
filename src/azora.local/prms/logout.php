<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Logout'?>

<?php 

//Event Log
$evtObj = new EventObject();					
$evtObj->event_id = EventTypes::PRMS_UserLogout;
$evtObj->description = Authentication::getAttribute('login_id') . ' logged out from PRMS.';
$evtObj->action_by = Authentication::getAttribute('login_id');					
EventLog::Log($evtObj);

Authentication::SignOut(); 
header( 'Location: index.php'); 

?>