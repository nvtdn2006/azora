<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Customer detail report / Update Customer Profile'?>

<?php		

	include 'connectionstring.php';

	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('validation');	
	
	$config = Factory::getConfig();
	$cryptographer = Factory::getCryptographer();
	
	$error = array();
	
	DomainManager::getInstance()->load('Customer');
	$customerObj = new Customer();
	
	$customer_id = 0;
	
	if(isset($_GET['id']) && $_GET['id'] > 0) {
		$customer_id = $_GET['id'];
	} else {
		header( 'Location: customerrpt.php');
		exit();
	}
		
	$customer = $customerObj->getCustomerProfile($customer_id);
	
	DomainManager::getInstance()->load('Point');
	$pointObj = new Point();
	$pointdetatils = $pointObj->getPointDetailsByCustomer($customer_id);

	$chkactivatecustid = $customer['customer_id'];
	$chkactivate = mysql_query("SELECT * FROM customers WHERE customer_id='$chkactivatecustid'"); 
	$getchkactivate = mysql_fetch_array($chkactivate);
	$cfmactivate = $getchkactivate['is_activated'];

	require_once './includes/user.php';			
	$user = new User();
	$userprofile = $user->getUserProfile(Authentication::getUserId());
	$useruserid = $userprofile['user_id'];	

	$chkrole = mysql_query("SELECT * FROM user_roles WHERE user_id='$useruserid' AND role_id='1'");
	$chkrole_count = mysql_numrows($chkrole);

?>

<?
if($_GET['fup'] == "1")
{
?>
<div class="success-info form-info">
	Profile has been updated.
</div>
<?
}
else if($_GET['fup'] == "2")
{
?>
<div class="error-info form-info">
	Email already exists in system, please re-enter.
</div>
<?
}
else if($_GET['fup'] == "3")
{
?>
<div class="success-info form-info">
	Activation email has been resend.
</div>
<?
}
?>

<form name="profileform" id="profileform" action="customerrptdetailprocess.php" method="post"> 

<input type="hidden" name="hiddenchkrole" id="hiddenchkrole" class="input" value="<?php echo $chkrole_count; ?>" />
<input type="hidden" name="hiddencustid" id="hiddencustid" class="input" value="<?php echo $customer['customer_id']; ?>" />
<input type="hidden" name="hiddennric" id="hiddennric" maxlength="9" class="input" value="<?php echo $customer['nric']; ?>" size="20" tabindex="10" />

	<table class="formview" width="100%" border="0" cellspacing="3px" cellpadding="3px">
		<tr>
			<td class="LabelCell Disabled">NRIC / FIN</td>
			<td><input type="text" name="nric" id="nric" maxlength="9" class="input" value="<?php echo $customer['nric']; ?>" size="20" tabindex="10" Disabled /></td>
		</tr>
		<tr>
			<td class="LabelCell Required">Name</td>
			<td><input type="text" name="name" id="name" maxlength="255" class="input Required" value="<?php echo $customer['name']; ?>" size="20" tabindex="20" /></td>
		</tr>
		<tr>
			<td class="LabelCell Required">Date of Birth <span class="hint">(DD/MM/YYYY)</span></td>
			<td><input type="text" name="date_of_birth" id="date_of_birth" maxlength="10" class="input Required ValidDate" value="<?php echo date("d/m/Y", strtotime($customer['date_of_birth'])); ?>" size="20" tabindex="30" /></td>
		</tr>
		<tr>
			<td class="LabelCell">Address</td>
			<td><input type="text" name="address" id="address" maxlength="500" class="input" value="<?php echo $customer['address']; ?>" size="20" tabindex="40" /></td>
		</tr>		
		<tr>
			<td class="LabelCell Required">Company <span class="hint">(N.A. if not applicable)</span></td>
			<td><input type="text" name="company" id="company" maxlength="500" class="input Required" value="<?php echo $customer['company']; ?>" size="20" tabindex="50" /></td>
		</tr>
		<tr>			
			<td class="SectionBar" colspan="2">				
				Contacts
			</td>
		</tr>
		<tr>
			<td class="LabelCell Required">Mobile</td>
			<td><input type="text" name="contact_mobile" id="contact_mobile" maxlength="50" class="input Required" value="<?php echo $customer['contact_mobile']; ?>" size="20" tabindex="60" /></td>
		</tr>
		<tr>
			<td class="LabelCell">Home</td>
			<td><input type="text" name="contact_home" id="contact_home" maxlength="50" class="input" value="<?php echo $customer['contact_home']; ?>" size="20" tabindex="70" /></td>
		</tr>	
		<tr>			
			<td class="SectionBar" colspan="2">				
				Email
			</td>
		</tr>
		<tr>
			<td class="LabelCell Required">Your Email</td>
			<td><input type="text" name="email" id="email" maxlength="50" class="input Required ValidEmail" value="<?php echo $customer['email']; ?>" size="20" tabindex="80" /></td>
		</tr>

<?php
if($chkrole_count == "1")
{
?>

		<tr>			
			<td class="SectionBar" colspan="2">				
				Activation
			</td>
		</tr>
		<tr>
			<td class="LabelCell Required">Activate Customer</td>

<?
if($cfmactivate == "1")
{
?>
			<td><input type="radio" name="activate" value="1" checked> Yes&nbsp;&nbsp;&nbsp;<input type="radio" name="activate" value="0"> No
			</td>
<?
}
else
{
?>
			<td><input type="radio" name="activate" value="1"> Yes&nbsp;&nbsp;&nbsp;<input type="radio" name="activate" value="0" checked> No
			</td>
<?
}

}
?>

		</tr>
		<tr>			
			<td class="SectionBar" colspan="2">				
				Points
			</td>
		</tr>
		
		<tr>			
			<td colspan="2">				
				
				<table width="100%" border="0" cellspacing="3px" cellpadding="3px">
					<tr>			
						<td><b>Branch</b></td>
						<td><b>Store</b></td>
						<td><b>Accumulated Points</b></td>
						<td><b>Last transaction</b></td>			
					</tr>
					<?php
						$t_points = 0;
						foreach($pointdetatils as $detail) {
					?>
					<tr>						
						<td><?php echo $detail['branch_name']; ?></td>
						<td><?php echo $detail['store_name']; ?></td>
						<td><?php echo $detail['accumulated_points']; ?></td>
						<td><?php echo $detail['last_transaction']; ?></td>			
					</tr>
					<?php $t_points += $detail['accumulated_points'];
					} ?>
					<tr>			
						<td>&nbsp;</td>
						<td><b>Total</b></td>
						<td><b><?php echo $t_points; ?> pts</b></td>
						<td>&nbsp;</td>			
					</tr>
				</table>
				
			</td>
		</tr>
		
		<tr>			
			<td class="BottomToolBar">		
				<input type="submit" name="update" id="update" class="button-primary" value="Update" tabindex="90"/>
				<a href="customerrpt.php" class="button-secondary" tabindex="100">Back</a>				
			</td>
			<td class="BottomToolBar" align="right">
			<a href="sendactivation.php?custid=<?echo $chkactivatecustid;?>" class="button-primary">Resend Activation Email</a>
			</td>
		</tr>
	</table>	
</form>

<script type="text/javascript">
	$(document).ready(function() { 
		loadValidation('profileform');
		<?php if ($_SERVER['REQUEST_METHOD'] != "POST") { ?> 
			$("#name").focus();
		<?php } ?>
	});	
</script>