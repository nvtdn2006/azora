<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Manage marketing mail'?>

<?php	

	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('checkboxlist');
	JSManager::getInstance()->add('messagebox');
	JSManager::getInstance()->add('fancybox');	
	CSSManager::getInstance()->add('/js/fancybox/jquery.fancybox-1.3.4.css');
	
	DomainManager::getInstance()->load('Marketing');
	$marketingObj = new Marketing();
	
	$error = array();
	
	if (isset($_POST['delete']) && $_POST['delete'] == 'Remove') {			
		$mails = null;
		if (isset($_POST['mails']))
			$mails = $_POST['mails'];
		if (isset($mails) && count($mails) > 0) {
			
			$_names = '';
			$sp = '';
			foreach($mails as $item) {
				$nobj = $marketingObj->getMail($item);
				$_names .= $sp . $nobj['mail_subject'];
				$sp = ', ';
			}
			
			if ($marketingObj->deleteMail($mails)) {
			
				//Event Log
				$evtObj = new EventObject();					
				$evtObj->event_id = EventTypes::PRMS_DeleteMarketing;
				$evtObj->description = Authentication::getAttribute('login_id') . ' deleted marketing mails ('.$_names.').';
				$evtObj->action_by = Authentication::getAttribute('login_id');					
				EventLog::Log($evtObj);
				
				header( 'Location: marketing.php');
			}
		} else {
			array_push($error, 'Please select at least one item to proceed.');	
		}
	}
	
	$mails = $marketingObj->getAllMarketing();
?>

<?php
	if (isset($error) && count($error) > 0) {
?>
	<div class="error-info tabular-info">
		<?php foreach ($error as $handle) {
				echo "<p>$handle</p>";
		} ?>
	</div>
<?php
	}
?>

<form name="maillist" id="maillist" action="marketing.php" method="post"> 
	<table class="tabular">
		<thead>
			<tr>
				<th><input type="checkbox" name="listselectall" class="listselectall" onclick="javascript:checkSelectAllBox('listselect', this.checked);"/></th>
				<th>Subject</th>
				<th>Last Sent</th>				
				<th>Updated By</th>
				<th>Updated On</th>			
				<th>Action</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<th colspan="7"><a class="button-primary" href="marketing-edit.php">New</a>
				<input type="submit" name="delete" id="submit" class="button-secondary" value="Remove" onclick="javascript:return confirmBox('Are you sure you want to delete selected item?');"/></th>		
			</tr>
		</tfoot>
		<tbody>		
			<?php
				foreach($mails as $mail) {
			?>
			<tr>
				<td><input type="checkbox" name="mails[]" class="listselect" onclick="javascript:checkSelectBox('listselectall', 'listselect');" value="<?php echo $mail['id']; ?>" /></td>
				<td><a class="detailLink" href="<?php echo 'marketing-edit.php?mail='.$mail['id'] ?>" alt="Edit" title="Edit"><?php echo $mail['mail_subject'] ?></a></td>
				<td><?php echo $mail['last_sent_on'] ?></td>				
				<td><?php echo $mail['updated_username'] ?></td>
				<td><?php echo $mail['updated_dt'] ?></td>				
				<td><a class="button-primary fancybox" href="marketing-preview.php?id=<?php echo $mail['id']; ?>">Preview</a>&nbsp;<a class="button-primary sending" href="<?php echo $mail['id']; ?>">Send</a></td>	
			</tr>
			<?php } ?>
		</tbody>
	</table>	
	
</form>
<?php
	$marketingObj = null;
?>

<script type="text/javascript">
	
	$(document).ready(function() { 
		$('a.fancybox').fancybox();	
		
		$('.sending').click(function() {
			if (confirmBox('Are you sure you want to send?')) {
				$.fancybox.showActivity();
				id = $(this).attr('href');
				$.ajax({
					type	: "GET",
					cache	: false,
					url		: "marketing-sending.php?id=" + id,					
					success : function(data) {
						$.fancybox({content : data, 'onClosed'		: function() {
						window.location.href = 'marketing.php';
					}});
					}					
				});
			}
			return false;
		});
	});	
	
</script>
<!-- /TinyMCE -->