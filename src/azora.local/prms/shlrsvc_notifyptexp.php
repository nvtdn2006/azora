<?php 
/*
 * 05-04-2013 FX.1304.002 Read Default CC email
 */
require_once './includes/service.php'; 
?>

<?php

	$return = array();
	$return['status'] = true;
	$return['message'] = 'Process done.';
	
	$manual = false;
	
	$config = Factory::getConfig(); //FX.1304.002 
        
	try {
	
		if (isset($_GET['m']) && $_GET['m'] == 1) {		
			$manual = true;		
		}
		
		
		DomainManager::getInstance()->load('Reward');
		$rewardObj = new Reward();
		
		DomainManager::getInstance()->load('Company');
		$companyObj = new Company();
		$company = $companyObj->getCompany();
		$company = $company[0];
		
		$rewards = $rewardObj->getReward();
		$expiry_on = null;
		if (isset($rewards) && count($rewards) > 0) {
			$reward = $rewards[0];
			if ($reward['expiration_required'] == 1) {
				
				$config = Factory::getConfig();
				$expiration_month = $reward['expiration_month']; 
				
				DomainManager::getInstance()->load('Customer');
				$customerObj = new Customer();
				
				$customers = $customerObj->getCustomerProfileByValues();
				
				foreach ($customers as $customer) {
					$customer_id = $customer['customer_id'];
					
					DomainManager::getInstance()->load('Point');
					$pointObj = new Point();
				
					$startdate = new DateTime($pointObj->getPointStartedDate($customer_id));
					$startdate->modify('+'.$expiration_month.' month');
					
					$startdate = $startdate->format("Y-m-d");
					
					$today = getdate();
					$today = date("Y-m-d", mktime(0,0,0,$today['mon'],$today['mday'],$today['year']));		
				
					$day = 0;
					$date_diff = strtotime($startdate) - strtotime($today);
					if ($date_diff/(60 * 60 * 24) == 30) {
						$day = 30;
					} if ($date_diff/(60 * 60 * 24) == 10) {
						$day = 10;
					}
					
					if ($day > 0) {
						//Send Mail						
						$property = array ('name' => $customer['name'],
											'days' => $day,											
											'company_name' => $company['company_name'],
											'link' => $config['PRMSConfig']->live_site);

						$mailer = new SiteMailer();
						$mailer->toMail = $customer['email'];
                                                
                                                //+START FX.1304.002 Add Default CC email 
                                                if (isset($config['PRMSConfig']->PRMS_default_cc_mail) 
                                                        && $config['PRMSConfig']->PRMS_default_cc_mail != "") {
                                                    if (is_array($config['PRMSConfig']->PRMS_default_cc_mail)) {
                                                        foreach ( $config['PRMSConfig']->PRMS_default_cc_mail as $ccMail) {
                                                            $mailer->AddCc($ccMail);
                                                        }
                                                    } else {
                                                        $mailer->AddCc($config['PRMSConfig']->PRMS_default_cc_mail);
                                                    }
                                                }
                                                //-END FX.1304.002
                                        
						$mailer->subject = 'Reminder for point expiration at '.$company['company_name'].' redemption website';				
						$mailer->PrepareMail('sendReminderforpointexpiration', $property);
						$mailer->Send();
					}
				}
			}
		}
	
	} catch(PDOException $e) {
		$return['status'] = false;	
	}

		
	$log = array( 'scheduler_name' => 'Point expiry notification',
					'status' => $return['status'],
					'is_manual' => $manual);
	SchedulerLog::Log($log);
	
	if ($manual) {
		header('Content-type: application/json');
		echo json_encode($return);
	}	
	
?>