<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Manage users'?>

<?php	
	
	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('checkboxlist');
	JSManager::getInstance()->add('messagebox');
	
	require_once './includes/user.php';	
	$userObj = new User();
	
	$error = array();

	if (isset($_POST['delete']) && $_POST['delete'] == 'Remove') {
		$users = null;
		if (isset($_POST['users']))
			$users = $_POST['users'];	
		if (isset($users) && count($users) > 0) {
		
			$_names = '';
			$sp = '';
			foreach($users as $item) {
				$nobj = $userObj->getUser($item);
				$_names .= $sp . $nobj['user']['username'];
				$sp = ', ';
			}
			
			if ($userObj->deleteUser($users)) {
			
				//Event Log
				$evtObj = new EventObject();					
				$evtObj->event_id = EventTypes::PRMS_DeleteUser;
				$evtObj->description = Authentication::getAttribute('login_id') . ' deleted users ('.$_names.').';
				$evtObj->action_by = Authentication::getAttribute('login_id');					
				EventLog::Log($evtObj);
				
				header( 'Location: users.php');
			}
		} else {
			array_push($error, 'Please select at least one item to proceed.');	
		}
	}
	
	$users = $userObj->getAllUsers();
?>

<?php
	if (isset($error) && count($error) > 0) {
?>
	<div class="error-info tabular-info">
		<?php foreach ($error as $handle) {
				echo "<p>$handle</p>";
		} ?>
	</div>
<?php
	}
?>

<form name="rolelist" id="rolelist" action="users.php" method="post"> 

	<table class="tabular">
		<thead>
			<tr>
				<th><input type="checkbox" name="listselectall" class="listselectall" onclick="javascript:checkSelectAllBox('listselect', this.checked);"/></th>
				<th>User</th>
				<th>Name</th>
				<th>Email</th>
				<th>Status</th>
				<th>Updated By</th>
				<th>Updated On</th>			
			</tr>
		</thead>
		<tfoot>
			<tr>
				<th colspan="7"><a class="button-primary" href="user-edit.php">New</a>
				<input type="submit" name="delete" id="submit" class="button-secondary" value="Remove" onclick="javascript:return confirmBox('Are you sure you want to delete selected item?');"/></th>				
			</tr>
		</tfoot>
		<tbody>
			<?php
				foreach($users as $user) {
			?>
			<tr>				
				<td><?php echo $user['user_is_sys'] ? '<input type="checkbox" name="users_disabled" disabled />' : '<input type="checkbox" name="users[]" class="listselect" onclick="javascript:checkSelectBox(\'listselectall\', \'listselect\');" value="'.$user['user_id'].'"/>'; ?> 
				</td>
				<td><?php echo $user['user_is_sys'] ? '<span>'.$user['username'].'</span>' :
				'<a class="detailLink" href="user-edit.php?user='.$user['user_id'].'" alt="Edit" title="Edit">'.$user['username'].'</a>'; ?></td>
				<td><?php echo $user['full_name'] ?></td>
				<td><?php echo $user['email'] ?></td>
				<td><?php echo $user['user_status'] ? 'Enable' : 'Disable'; ?></td>
				<td><?php echo $user['user_updated_username'] ?></td>
				<td><?php echo $user['updated_dt'] ?></td>
				
			</tr>
			<?php } ?>
		</tbody>
	</table>
	
</form>
<?php
	$userObj = null;
?>