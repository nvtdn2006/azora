<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'New category'?>

<?php	

	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('validation');	
	
	DomainManager::getInstance()->load('Category');
	$categoryObj = new Category();
	
	$category_id = 0;
	$category = null;
	
	$error = array();
	
	if (isset($_GET['category'])) {		
		$category_id = $_GET['category'];
		$category = $categoryObj->getCategory($category_id);	
		$this->title = 'Update category';
	}
	
	if (isset($_GET['action'])) {
		switch ($_GET['action']){
			case 'new':
				if (!$categoryObj->isExistCategory($_POST['category_name'], 0)) {
					$category = array('category_name' => $_POST['category_name'],
								'category_description' => $_POST['category_description'],								
								'created_by' => Authentication::getUserId(),								
								'updated_by' => Authentication::getUserId());								
					if ($categoryObj->saveCategory($category)) {
						
						//Event Log
						$evtObj = new EventObject();					
						$evtObj->event_id = EventTypes::PRMS_AddCategory;
						$evtObj->description = Authentication::getAttribute('login_id') . ' added new category ('.$_POST['category_name'].').';
						$evtObj->action_by = Authentication::getAttribute('login_id');					
						EventLog::Log($evtObj);
						
						header( 'Location: categories.php');
					}
				} else {
					array_push($error, 'Category name already exists in system.');					
				}
			break;
			case 'update':		
				if (!$categoryObj->isExistCategory($_POST['category_name'], $category_id)) {
					$category = array('category_name' => $_POST['category_name'],
									'category_description' => $_POST['category_description'],																
									'updated_by' => Authentication::getUserId(),
									'category_id' => $category_id);
					if ($categoryObj->updateCategory($category)) {
					
						//Event Log
						$evtObj = new EventObject();					
						$evtObj->event_id = EventTypes::PRMS_UpdateCategory;
						$evtObj->description = Authentication::getAttribute('login_id') . ' updated category ('.$_POST['category_name'].').';
						$evtObj->action_by = Authentication::getAttribute('login_id');					
						EventLog::Log($evtObj);
					
						header( 'Location: categories.php');
					}
				} else {
					array_push($error, 'Category name already exists in system.');					
				}
			break;
		}		
	}
	
	if (!isset($category) || isset($_GET['action'])) {		
		if (isset($_REQUEST['category_name'])) 			
			$category['category_name'] = $_REQUEST['category_name'];
		else
			$category['category_name'] =  '';
			
		if (isset($_REQUEST['category_description'])) 			
			$category['category_description'] = $_REQUEST['category_description'];
		else
			$category['category_description'] =  '';
	}
	
?>


<?php
	if (isset($error) && count($error) > 0) {
?>
	<div class="error-info form-info">
		<?php foreach ($error as $handle) {
				echo "<p>$handle</p>";
		} ?>
	</div>
<?php
	}
?>

<form name="categoryform" id="categoryform" action="category-edit.php?action=<?php echo $category_id > 0 ? 'update&category='.$category_id : 'new'; ?>" method="post"> 
	<table class="formview" width="100%" border="0" cellspacing="3px" cellpadding="3px">
		<tr>
			<td class="LabelCell Required">Category</td>
			<td><input type="text" name="category_name" id="category_name" maxlength="50" class="input Required" value="<?php echo $category['category_name'];  ?>" size="20" tabindex="10" /></td>
		</tr>
		<tr>
			<td class="LabelCell">Description</td>
			<td><input type="text" name="category_description" id="category_description" maxlength="255" class="input" value="<?php echo $category['category_description']; ?>" size="20" tabindex="20" /></td>
		</tr>
		<tr>			
			<td class="BottomToolBar" colspan="2">				
				<input type="submit" name="submit" id="submit" class="button-primary" value="Save" tabindex="30"/>
				<a href="categories.php" class="button-secondary" tabindex="40">Cancel</a>				
			</td>			
		</tr>
	</table>
</form>
<?php
	$categoryObj = null;
?>
<script type="text/javascript">
	$(document).ready(function() { 
		loadValidation('categoryform');
	});
</script>