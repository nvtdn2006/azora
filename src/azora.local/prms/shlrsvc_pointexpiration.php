<?php 
/*
 * 05-04-2013 FX.1304.002 Read Default CC email
 */
require_once './includes/service.php';
?>

<?php

	$return = array();
	$return['status'] = true;
	$return['message'] = 'Process done.';
	
	$manual = false;
        
        $config = Factory::getConfig(); //FX.1304.002 
	
	try {
	
		if (isset($_GET['m']) && $_GET['m'] == 1) {		
			$manual = true;		
		}
		
		
		DomainManager::getInstance()->load('Reward');
		$rewardObj = new Reward();
		
		DomainManager::getInstance()->load('Company');
		$companyObj = new Company();
		$company = $companyObj->getCompany();
		$company = $company[0];
		
		
		$rewards = $rewardObj->getReward();
		$expiry_on = null;
		if (isset($rewards) && count($rewards) > 0) {
			$reward = $rewards[0];
			if ($reward['expiration_required'] == 1) {
				
				
				$expiration_month = $reward['expiration_month']; 
				
				DomainManager::getInstance()->load('Customer');
				$customerObj = new Customer();
				
				$customers = $customerObj->getCustomerProfileByValues();
				
				foreach ($customers as $customer) {
					$customer_id = $customer['customer_id'];
					
					DomainManager::getInstance()->load('Point');
					$pointObj = new Point();
				
					$startdate = new DateTime($pointObj->getPointStartedDate($customer_id));
					$startdate->modify('+'.$expiration_month.' month');
					
					$today = getdate();
					$today = new DateTime(date("Y-m-d", mktime(0,0,0,$today['mon'],$today['mday'],$today['year'])));		
				
					if ($startdate < $today) {
						
						$pointdetatils = $pointObj->getPointDetailsByCustomer($customer_id);
						
						
						$pdTable = '<table border="1">
										<thead>
											<tr>			
												<th><b>Branch</b></th>
												<th><b>Store</b></th>
												<th><b>Accumulated Points</b></th>
												<th><b>Last transaction</b></th>			
											</tr>
										</thead>
										<tbody>';
			
						foreach($pointdetatils as $detail) {		
							$pdTable .= '<tr>';
							$pdTable .= '<td>'. $detail['branch_name'] .'</td>';
							$pdTable .= '<td>'. $detail['store_name'] .'</td>';
							$pdTable .= '<td>'. $detail['accumulated_points'] .'</td>';
							$pdTable .= '<td>'. $detail['last_transaction'] .'</td>';
							$pdTable .= '</tr>';
						} 
						$pdTable .= '</tbody></table>';
						
						if ($pointObj->expiredPoints($customer_id)) {
							//Send Mail							
							
							$property = array ('name' => $customer['name'],
												'points' => $pdTable,
												'csmail' => $company['company_customersupport_email'],
												'company_name' => $company['company_name']);

							$mailer = new SiteMailer();
							$mailer->toMail = $customer['email'];
                                                        
                                                         //+START FX.1304.002 Add Default CC email 
                                                        if (isset($config['PRMSConfig']->PRMS_default_cc_mail) 
                                                                && $config['PRMSConfig']->PRMS_default_cc_mail != "") {
                                                            if (is_array($config['PRMSConfig']->PRMS_default_cc_mail)) {
                                                                foreach ( $config['PRMSConfig']->PRMS_default_cc_mail as $ccMail) {
                                                                    $mailer->AddCc($ccMail);
                                                                }
                                                            } else {
                                                                $mailer->AddCc($config['PRMSConfig']->PRMS_default_cc_mail);
                                                            }
                                                        }
                                                        //-END FX.1304.002
                        
							$mailer->subject = 'Your reward points have been expired at '.$company['company_name'].' redemption website';				
							$mailer->PrepareMail('sendPointshavebeenexpired', $property);
							$mailer->Send();
						}					
						
					}
					
				}
			}
		}
	
	} catch(PDOException $e) {
		$return['status'] = false;	
	}

		
	$log = array( 'scheduler_name' => 'Point expiration',
					'status' => $return['status'],
					'is_manual' => $manual);
	SchedulerLog::Log($log);
	
	if ($manual) {
		header('Content-type: application/json');
		echo json_encode($return);
	}	
	
?>