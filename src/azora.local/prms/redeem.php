<?php
/*
 * 19-05-2013 FX.1305.002 Add Admin Redeem feature
 */
require_once './includes/application.php';
$this->template = '';
$this->title = 'Redeem'
?>

<?php
JSManager::getInstance()->add('jquery');
JSManager::getInstance()->add('validation');
JSManager::getInstance()->add('numeric');
JSManager::getInstance()->add('jquery.ui');
CSSManager::getInstance()->add('/js/jqueryui/smoothness/jquery-ui-1.8.16.custom.css');


DomainManager::getInstance()->load('Customer');
DomainManager::getInstance()->load('Store');

$config = Factory::getConfig();

$customerObj = new Customer();
$storeObj = new Store();

$regcustomer = null;

$isValid = true;
$isReadyToConfirm = false;
$error = array();
$warning = array();

if (isset($_GET['action']) && $_GET['action'] == 'redeem') {
    $customer_id = 0;

    if (isset($_GET['action']))
        $customer_id = $_GET['id'];

    // @Customer Profile
    if ($customer_id > 0) {
        $regcustomer = $customerObj->getCustomerProfile($customer_id);
    }

    // @Products
    DomainManager::getInstance()->load('Product');
    $productObj = new Product();

    $t_products = $productObj->getActiveProducts();

    $products = array();

    // First line of selection 
    $products[] = array(
        'product_name'   => '-- select --',
        'product_id'     => 'X',
        'product_points' => '0',
        'product_image'  => ''
    );

    // Product Items
    foreach ($t_products as $t_product) {
        $images = $t_product['product_images'];
        $image = null;
        foreach ($images as $img) {
            if ($img['image_type'] == 'T') {
                $image = $img;
            }
        }

        $img_path = $config['PRMSConfig']->live_site . '/domain/images/products/' . $image['sys_file_name'];

        $products[] = array(
            'product_name'   => $t_product['product']['product_name'],
            'product_id'     => $t_product['product']['product_id'],
            'product_points' => $t_product['product']['product_points'],
            'product_image'  => $img_path
        );
    }

    sort($products);

    // @Customer's Points
    DomainManager::getInstance()->load('Point');
    $pointObj = new Point();
    $points = 0;
    if ($customer_id > 0) {
        $points = $pointObj->getPointByCustomer($customer_id);
    }
}

$point = null;
$items = array();
$items_points = array();
$items_qty = array();
$items_total_points = 0;
$items_total_qty = 0;
$items_total_products = 0;

$form_hidden = '';
$form_readonly = '';

$custCart = array();


if (!isset($point)) {
    if (isset($_REQUEST['referenceno']))
        $point['reference_no'] = $_REQUEST['referenceno'];
    else
        $point['reference_no'] = '';

    if (isset($_REQUEST['salesamount']))
        $point['sales_amount'] = $_REQUEST['salesamount'];
    else
        $point['sales_amount'] = '';

    if (isset($_REQUEST['remarks']))
        $point['remarks'] = $_REQUEST['remarks'];
    else
        $point['remarks'] = '';

    if (isset($_REQUEST['store']))
        $point['store_id'] = $_REQUEST['store'];
    else
        $point['store_id'] = 0;

    // Get Line Item
    for ($i = 1; $i <= 5; $i++) {
        $items[$i] = isset($_REQUEST['item' . $i]) ? $_REQUEST['item' . $i] : '';
    }

    for ($i = 1; $i <= 5; $i++) {
        $items_point[$i] = 0;
        if (isset($products) && is_array($products)) {
            foreach ($products as $p) {
                if ($items[$i] != 'X' && $items[$i] == $p['product_id']) {
                    $items_point[$i] = $p['product_points'];
                    $items_total_products++;
                }
            }
        }
    }

    for ($i = 1; $i <= 5; $i++) {
        // Quantity
        $items_qty[$i] = isset($_REQUEST['item' . $i . '-qty']) ? $_REQUEST['item' . $i . '-qty'] : '0';
        // Total Redeem Points
        $items_total_points += $items_qty[$i] * $items_point[$i];
        // $items_total_qty
        $items_total_qty += $items_qty[$i];

        // Add Cart
        if ($items[$i] != 'X' && $items_qty[$i] > 0)
            $custCart[] = array(
                'product_id'     => $items[$i],
                'product_points' => $items_point[$i],
                'product_qty'    => $items_qty[$i]
            );
    }
}

if (isset($_POST['hidden_submit']) &&
        ( $_POST['hidden_submit'] == 'Next' || $_POST['hidden_submit'] == 'Back' || $_POST['hidden_submit'] == 'Confirm' )) {

    // validation check

    if (isset($items_total_points) && $items_total_points > $points) {
        array_push($error, 'Not enough point.');
        $isValid = false;
    }

    if ($items_total_products == 0) {
        array_push($error, 'Please select products.');
        $isValid = false;
    } else {
        if ($items_total_qty == 0) {
            array_push($error, 'Please input redeem quantity.');
            $isValid = false;
        }
    }

    if ($isValid == true && $_POST['hidden_submit'] != 'Back') {
        $isReadyToConfirm = true;
        $form_hidden = 'hidden';
        $form_readonly = 'readonly';
    }
}

$redemption = array();
if ((isset($_POST['hidden_submit']) && $_POST['hidden_submit'] == 'Confirm') // FX.1304.005  - if not find submit, check hidden submit
) {
    $redemptionToSave = array();

    $redemption['date'] = $_REQUEST['date'];
    $redemption['store_id'] = $_REQUEST['store'];
    $redemption['time'] = $_REQUEST['time'];

    $cdate_year = substr($redemption['date'], 6, 4);
    $cdate_month = substr($redemption['date'], 3, 2);
    $cdate_day = substr($redemption['date'], 0, 2);
    $cdate = date("Y-m-d", mktime(0, 0, 0, $cdate_month, $cdate_day, $cdate_year));

    $header = array('customer_id'       => $customer_id,
        'store_id'          => $redemption['store_id'],
        'collection_date'   => $cdate,
        'collection_time'   => $redemption['time'] . ':00:00',
        'total_points'      => $items_total_points,
        'redemption_status' => 1);

    $redemptionToSave['redemption'] = $header;

    $pointList = array();

    // Get Point List
    $items_total_points_tmp = $items_total_points;
    $pointdetatils = $pointObj->getPointDetailsByCustomer($customer_id);
    foreach ($pointdetatils as $detail) {
        if ($detail['accumulated_points'] > 0) {
            if ($items_total_points_tmp <= $detail['accumulated_points']) {
                $p = array('store_id' => $detail['store_id'],
                    'points'   => $items_total_points_tmp);
                $items_total_points_tmp = 0;
            } else {
                $items_total_points_tmp = $items_total_points_tmp - $detail['accumulated_points'];
                $p = array('store_id' => $detail['store_id'],
                    'points'   => $detail['accumulated_points']);
            }
            array_push($pointList, $p);
        }
    }
    // Error Exception: if Redeem point is not enough
    if ($items_total_points_tmp > 0) {
        array_push($error, 'Not enough point.');
    } else {
        $redemptionToSave['points'] = $pointList;
    }

    $productList = array();

// Get Product List
    if (isset($custCart) && is_array($custCart)) {
        foreach ($custCart as $pd) {
            $pObj = $productObj->getProduct($pd['product_id']);
            $p = array('product_id'     => $pd['product_id'],
                'product_points' => $pObj['product']['product_points'],
                'product_cost'   => $pObj['product']['product_cost'],
                'quantity'       => $pd['product_qty']);
            array_push($productList, $p);
        }
    }
    $redemptionToSave['products'] = $productList;

    DomainManager::getInstance()->load('Redemption');
    $redemptionObj = new Redemption();
    if ($redemptionObj->submitRedemption($redemptionToSave)) {

        //Event Log
        $evtObj = new EventObject();
        $evtObj->event_id = EventTypes::RWS_SubmitRedemption;
        $evtObj->description = Authentication::getAttribute('login_id') . ' submitted redemption application (No. : ' . $redemptionToSave['redemption']['redemption_no'] . ').';
        $evtObj->action_by = Authentication::getAttribute('login_id');
        EventLog::Log($evtObj);

        $itemTable = '<table border="1" cellspacing="3px" cellpadding="3px">							
						<thead>
							<tr>			
								<th>Product Name</th>
								<th>Points</th>
								<th>Quantity</th>								
							</tr>
						</thead>
						<tbody>';


        DomainManager::getInstance()->load('Product');
        $productObj = new Product();
        foreach ($productList as $item) {

            $product = $productObj->getProduct($item['product_id']);

            $images = $product['product_images'];
            $image = null;

            foreach ($images as $img) {
                if ($img['image_type'] == 'S') {
                    $image = $img;
                }
            }

            if (!$image) {
                foreach ($images as $img) {
                    if ($img['image_type'] == 'T') {
                        $image = $img;
                    }
                }
            }


            $itemTable .= '<tr>																
					<td><b>' . $product['product']['product_name'] . '</b></td>								
					<td width="80px">' . number_format($product['product']['product_points']) . 'pts</td>
					<td width="50px">' . $item['quantity'] . '</td>						
				</tr>';
        }

        $itemTable .= '</tbody>
				</table>';


        DomainManager::getInstance()->load('Company');
        $companyObj = new Company();
        $company = $companyObj->getCompany();
        $company = $company[0];

        DomainManager::getInstance()->load('Customer');
        $customerObj = new Customer();
        $customer = $customerObj->getCustomerProfile($customer_id);

        $store = $storeObj->getStore($redemption['store_id']);
        //Send Mail							
        $property = array('name'         => $customer['name'],
            'cdate'        => date('j-M-Y, h:i:s A', strtotime($cdate . $redemption['time'] . ':00:00')),
            'store'        => $store['store_branch_name'],
            'items'        => $itemTable,
            'csmail'       => $company['company_customersupport_email'],
            'company_name' => $company['company_name']);

        $mailer = new SiteMailer();
        $mailer->toMail = $customer['email'];

        //+START FX.1304.002 Add Default CC email 
        if (isset($config['PRMSConfig']->PRMS_redemption_cc_mail) && $config['PRMSConfig']->PRMS_redemption_cc_mail != "") {
            if (is_array($config['PRMSConfig']->PRMS_redemption_cc_mail)) {
                foreach ($config['PRMSConfig']->PRMS_redemption_cc_mail as $ccMail) {
                    $mailer->AddCc($ccMail);
                }
            } else {
                $mailer->AddCc($config['PRMSConfig']->PRMS_default_cc_mail);
            }
        }
        //-END FX.1304.002

        $mailer->subject = 'You have submitted a new redemption application';
        $mailer->PrepareMail('sendRedemptionSubmittedNotify', $property);
        $mailer->Send();


        header('Location: redeem-submitted.php');
    } else {
        array_push($error, 'Falied to submit redemption application. Please try again.');
    }
}
?>

<?php
// Show error 
if (isset($error) && count($error) > 0) {
    ?>
    <div class="error-info">
        <?php
        foreach ($error as $handle) {
            echo "<p>$handle</p>";
        }
        ?>
    </div>
    <?php
}
?>

<div class="SectionTitle" >Customer Information</div>

<form name="redeemsform" id="redeemsform" action="redeem.php?action=redeem&id=<?php echo $regcustomer['customer_id']; ?>" method="post"> 
    <table class="formview" width="100%" border="0" cellspacing="3px" cellpadding="3px">			
        <tr>
            <td class="LabelCell Disabled">NRIC / FIN</td>
            <td><input id="customer_id" name="customer_id" type="hidden" value="<?php echo $regcustomer['customer_id']; ?>"/>
                <input type="text" name="nric" id="nric" maxlength="9" class="input" value="<?php echo $regcustomer['nric']; ?>" size="20" tabindex="10" disabled /></td>
        </tr>			
        <tr>
            <td class="LabelCell Disabled">Name</td>
            <td><input type="text" name="name" id="name" maxlength="9" class="input" value="<?php echo $regcustomer['name']; ?>" size="20" tabindex="20" disabled /></td>
        </tr>			
        <tr>
            <td class="LabelCell Disabled">Email</td>
            <td><input type="text" name="email" id="email" maxlength="9" class="input" value="<?php echo $regcustomer['email']; ?>" size="20" tabindex="30" disabled /></td>
        </tr>			
        <tr>
            <td class="LabelCell Disabled">Mobile</td>
            <td><input type="text" name="mobile" id="mobile" maxlength="9" class="input" value="<?php echo $regcustomer['contact_mobile']; ?>" size="20" tabindex="40" disabled /></td>
        </tr>

    </table>
    <table  class="formview" width="100%">

        <tr>
            <td class="LabelCell Disabled">Available Points</td>
            <td><input type="text" name="t_points" id="t_points" maxlength="9" class="input" value="<?php echo $points; ?>" size="20" tabindex="10" disabled /></td>
        </tr>
        <tr>			
            <td class="SectionBar" colspan="2">				
                Please select prefer location & datetime to collect
            </td>
        </tr>
        <tr>
            <td class="LabelCell Required">Store</td>
            <td>
                <select tabindex="20" name="store" class="GreaterThanZero">
                    <?php
                    $stores = $storeObj->getCollectionStores();
                    echo '<option value="0">- Select -</option>';

                    foreach ($stores as $store) {
                        echo '<option value=' . $store['store_id'] . ((isset($_REQUEST['store']) && $_REQUEST['store'] == $store['store_id'] ) ? ' selected' : '' ) . '>' . $store['store_branch_name'] . '</option>';
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="LabelCell Required">Date <span class="hint">(DD/MM/YYYY)</span></td>
            <td><input type="text" name="date" id="date" maxlength="10" class="input Required ValidDate" value="<?php echo isset($_REQUEST['date']) ? $_REQUEST['date'] : ''; ?>" size="20" tabindex="30" /></td>
        </tr>
        <tr>
            <td class="LabelCell Required">Time</td>
            <td>
                <select tabindex="30" name="time" class="GreaterThanZero" tabindex="40">						
                    <option value="0" selected>- Select -</option>	
                    <?php
                    $startime = $config['PRMSConfig']->collection_starttime;
                    $counter = 0;
                    for ($i = 0; $i < 24; $i++) {
                        $time = $startime + $counter;
                        if ($time > 23) {
                            $time = 0;
                            $startime = 0;
                            $counter = 0;
                        }
                        $counter++;
                        $timedesc = '';
                        if ($time == 0) {
                            $timedesc = '0' . $time . ':00 AM';
                        } else if ($time <= 11) {
                            $timedesc = str_pad($time, 2, "0", STR_PAD_LEFT) . ':00 AM';
                        } else if ($time == 12) {
                            $timedesc = $time . ':00 PM';
                        } else if ($time > 12) {
                            $timedesc = str_pad(($time - 12), 2, "0", STR_PAD_LEFT) . ':00 PM';
                        }
                        echo '<option value="' . ($time == 0 ? '24' : $time) . '" ' .
                        ((isset($_REQUEST['time']) && $_REQUEST['time'] == ($time == 0 ? '24' : $time) ) ? ' selected' : '' )
                        . '>' . $timedesc . '</option>';
                    }
                    ?>	

                </select>
            </td>
        </tr>
        <tr>			
            <td class="SectionBar" colspan="2">				
                Please select Products 
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table width="100%" border="0" cellspacing="3px" cellpadding="3px">							
                    <tbody>	
                        <tr>								
                            <td width="5px"></td>
                            <td>Products</td>
                            <td width="80px">Points</td>
                            <td width="50px">Qty</td>						
                        </tr>

                        <tr>								
                            <td width="5px">
                            </td> 
                            <td><b><?php
                                    if ($isReadyToConfirm) {
                                        foreach ($products as $product) {
                                            if (isset($items[1]) && $items[1] == $product['product_id']) {
                                                echo $product['product_name'];
                                            }
                                        }
                                    }
                                    ?>
                                    <select tabindex="20" id="item1" name="item1" class="product-item <?php echo $form_hidden; ?>">
                                        <?php
                                        foreach ($products as $product) {
                                            ?>
                                            <option value="<?php echo $product['product_id']; ?>"
                                                    <?php echo (isset($items[1]) && $items[1] == $product['product_id']) ? 'selected' : ''; ?>>
                                                <?php echo $product['product_name']; ?></option>
                                        <?php } ?>
                                    </select>

                                </b></td>
                            <td width="70px"><input type="text" id="item1-point" name="item1-point" class="input" value="<?php echo isset($items_point[1]) ? $items_point[1] : '0'; ?>" size="30" tabindex="10" style="width:60px" disabled /></td>
                            <td width="50px"><input type="text" id="item1-qty" name="item1-qty" class="input numeric product-item-qty" value="<?php echo isset($items_qty[1]) ? $items_qty[1] : '0'; ?>" size="20" tabindex="10" style="width:30px" <?php echo $form_readonly; ?> /></td>						
                        </tr>

                        <tr>								
                            <td width="5px">
                            </td> 
                            <td><b><?php
                                    if ($isReadyToConfirm) {
                                        foreach ($products as $product) {
                                            if (isset($items[2]) && $items[2] == $product['product_id']) {
                                                echo $product['product_name'];
                                            }
                                        }
                                    }
                                    ?>
                                    <select tabindex="20" id="item2" name="item2" class="product-item <?php echo $form_hidden; ?>">
                                        <?php
                                        foreach ($products as $product) {
                                            ?>
                                            <option value="<?php echo $product['product_id']; ?>"
                                                    <?php echo (isset($items[2]) && $items[2] == $product['product_id']) ? 'selected' : ''; ?>>
                                                <?php echo $product['product_name']; ?></option>
                                        <?php } ?>
                                    </select></b></td>
                            <td width="70px"><input type="text" id="item2-point" name="item2-point" class="input" value="<?php echo isset($items_point[2]) ? $items_point[2] : '0'; ?>" size="30" tabindex="10" style="width:60px" disabled /></td>
                            <td width="50px"><input type="text" id="item2-qty" name="item2-qty" class="input numeric product-item-qty" value="<?php echo isset($items_qty[2]) ? $items_qty[2] : '0'; ?>" size="20" tabindex="10" style="width:30px" <?php echo $form_readonly; ?> /></td>						
                        </tr>

                        <tr>								
                            <td width="5px">
                            </td> 
                            <td><b><?php
                                    if ($isReadyToConfirm) {
                                        foreach ($products as $product) {
                                            if (isset($items[3]) && $items[3] == $product['product_id']) {
                                                echo $product['product_name'];
                                            }
                                        }
                                    }
                                    ?>
                                    <select tabindex="20" id="item3" name="item3" class="product-item <?php echo $form_hidden; ?>">
                                        <?php
                                        foreach ($products as $product) {
                                            ?>
                                            <option value="<?php echo $product['product_id']; ?>"
                                            <?php echo (isset($items[3]) && $items[3] == $product['product_id']) ? 'selected' : ''; ?>
                                                    ><?php echo $product['product_name']; ?></option>
                                                <?php } ?>
                                    </select></b></td>
                            <td width="70px"><input type="text" id="item3-point" name="item3-point" class="input" value="<?php echo isset($items_point[3]) ? $items_point[3] : '0'; ?>" size="30" tabindex="10" style="width:60px" disabled /></td>
                            <td width="50px"><input type="text" id="item3-qty" name="item3-qty" class="input numeric product-item-qty" value="<?php echo isset($items_qty[3]) ? $items_qty[3] : '0'; ?>" size="20" tabindex="10" style="width:30px" <?php echo $form_readonly; ?> /></td>						
                        </tr>

                        <tr>								
                            <td width="5px">
                            </td> 
                            <td><b><?php
                                    if ($isReadyToConfirm) {
                                        foreach ($products as $product) {
                                            if (isset($items[4]) && $items[4] == $product['product_id']) {
                                                echo $product['product_name'];
                                            }
                                        }
                                    }
                                    ?>
                                    <select tabindex="20" id="item4" name="item4" class="product-item <?php echo $form_hidden; ?>">
                                        <?php
                                        foreach ($products as $product) {
                                            ?>
                                            <option value="<?php echo $product['product_id']; ?>"
                                                    <?php echo (isset($items[4]) && $items[4] == $product['product_id']) ? 'selected' : ''; ?>>
                                                <?php echo $product['product_name']; ?></option>
                                        <?php } ?>
                                    </select></b></td>
                            <td width="70px"><input type="text" id="item4-point" name="item4-point" class="input" value="<?php echo isset($items_point[4]) ? $items_point[4] : '0'; ?>" size="30" tabindex="10" style="width:60px" disabled /></td>
                            <td width="50px"><input type="text" id="item4-qty" name="item4-qty" class="input numeric product-item-qty" value="<?php echo isset($items_qty[4]) ? $items_qty[4] : '0'; ?>" size="20" tabindex="10" style="width:30px" <?php echo $form_readonly; ?> /></td>						
                        </tr>

                        <tr>								
                            <td width="5px">
                            </td> 
                            <td><b><?php
                                    if ($isReadyToConfirm) {
                                        foreach ($products as $product) {
                                            if (isset($items[5]) && $items[5] == $product['product_id']) {
                                                echo $product['product_name'];
                                            }
                                        }
                                    }
                                    ?>
                                    <select tabindex="20" id="item5" name="item5" class="product-item <?php echo $form_hidden; ?>">
                                        <?php
                                        foreach ($products as $product) {
                                            ?>
                                            <option value="<?php echo $product['product_id']; ?>"
                                                    <?php echo (isset($items[5]) && $items[5] == $product['product_id']) ? 'selected' : ''; ?>>
                                                <?php echo $product['product_name']; ?></option>
                                        <?php } ?>
                                    </select></b></td>
                            <td width="70px"><input type="text" id="item5-point" name="item5-point" class="input" value="<?php echo isset($items_point[5]) ? $items_point[5] : '0'; ?>" size="30" tabindex="10" style="width:60px" disabled /></td>
                            <td width="50px"><input type="text" id="item5-qty" name="item5-qty" class="input numeric product-item-qty" value="<?php echo isset($items_qty[5]) ? $items_qty[5] : '0'; ?>" size="20" tabindex="10" style="width:30px" <?php echo $form_readonly; ?> /></td>						
                        </tr>

                    </tbody>
                </table>
            </td>
        </tr>
        <tr>			
            <td colspan="2">				
                <table width="100%" border="0" cellspacing="3px" cellpadding="3px">						
                    <tr>
                        <td colspan="2">&nbsp;</td>
                        <td><b>Total Redemption Points</b></td>
                        <td><span id="tpoints"><input type="text" id="total-point" name="total-point"  class="input Required" value="<?php echo isset($items_total_points) ? $items_total_points : 0; ?>" size="30" tabindex="10" style="width:80px" disabled /></span>&nbsp;pts</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>			
            <td class="BottomToolBar" colspan="2">			
                <?php if ($isReadyToConfirm) { ?>
                    <input type="submit" name="submit" id="submit" class="button-primary" value="Back" tabindex="60"/>
                    <input type="submit" name="submit2" id="submit2" class="button-primary" value="Confirm" tabindex="60"/>
                <?php } else { ?>
                    <input type="submit" name="submit" id="submit" class="button-primary" value="Next" tabindex="60"/>
                <?php } ?>
                <a href="customersearch.php?d=redeem" class="button-secondary" tabindex="70">Cancel</a>					
            </td>			
        </tr>
    </table>
</form>

<script type="text/javascript">

    $(document).ready(function() {

        loadValidation('redeemsform');

        $("#date").datepicker({dateFormat: 'dd/mm/yy'});
        /*
         $('#salesamount').blur(function() {
         $.ajax({
         url: 'pointcalcsvc.php',
         data: {"amount": $('#salesamount').val(), "for": "pcalsvc"},
         success: function(data) {
         $('#points').val(data.value);
         },
         });
         });
         */
        $(".numeric").numeric(false);



    });

    var JSONproducts = {"products": [
<?
foreach ($products as $product) {
    echo '{"id": "' . $product['product_id'] . '", "name": "' . $product['product_name'] . '", "points": "' . $product['product_points'] . '", "img": "' . $product['product_image'] . '"},';
}
?>
        ]
    };


    $('.product-item').change(function() {
        ObjID = $(this).attr('id');
        ObjVal = $(this).val();

        for (var key in JSONproducts.products) {
            if (ObjVal === JSONproducts.products[key].id) {
                $('#' + ObjID + '-point').val(JSONproducts.products[key].points);
            }
        }

        recal_total();

    });

    $('.product-item-qty').change(function() {

        recal_total();

    });

    function recal_total() {
        total = $('#item1-point').val() * $('#item1-qty').val() + $('#item2-point').val() * $('#item2-qty').val()
                + $('#item3-point').val() * $('#item3-qty').val() + $('#item4-point').val() * $('#item4-qty').val()
                + $('#item5-point').val() * $('#item5-qty').val();
        $('#total-point').val(total);
    }

</script>