<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Customer purchase history'?>

<?php		
ini_set('display_errors', 'On');
error_reporting(E_ALL);

	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('validation');
	
	DomainManager::getInstance()->load('Point');
	$pointObj = new Point();
	
	DomainManager::getInstance()->load('Customer');
	$customerObj = new Customer();
	
	$customer_id = 0;
	$customer = null;
	
	if (isset($_GET['id'])) {		
		$customer_id = $_GET['id'];		
	}
	
	if (isset($_POST['customer_id'])) {		
		$customer_id = $_POST['customer_id'];		
	}
	
	if ($customer_id  > 0 ) {
		$customer = $customerObj->getCustomerProfile($customer_id);
		
		$purchasehistory = null;
		
		if (isset($_GET['action']) && $_GET['action'] == 'search') {
			if ($customer_id > 0) {
				$month = date('n');
				$year = date('Y');
				
				if (isset($_POST['month'])) 
					$month = $_POST['month'];
					
				if (isset($_POST['year'])) 
					$year = $_POST['year'];
					
				$purchasehistory = $pointObj->getPurchaseHistoryByCustomer($customer_id, $month, $year);
			}
		}
		
		$formvalues = array();
		
		if (isset($_REQUEST['month'])) 			
			$formvalues['month'] = $_REQUEST['month'];
		else
			$formvalues['month'] =  0;
		
		if (isset($_REQUEST['year'])) 			
			$formvalues['year'] = $_REQUEST['year'];
		else
			$formvalues['year'] =  0;
	} else {
		header( 'Location: customersearch.php?d=custpurrpt');
		exit();
	}
		
?>

<form name="historyform" id="historyform" action="custpurrpt.php?action=search&id=<?php echo $customer['customer_id']; ?>" method="post"> 

	<table class="formview" width="100%" border="0" cellspacing="3px" cellpadding="3px">
		<tr>
			<td class="LabelCell Disabled">NRIC / FIN</td>
			<td><input type="text" name="nric" id="nric" maxlength="9" class="input" value="<?php echo $customer['nric']; ?>" size="20" tabindex="10" disabled /></td>
		</tr>
		<tr>
			<td class="LabelCell Disabled">Name</td>
			<td><input type="text" name="name" id="name" maxlength="255" class="input" value="<?php echo $customer['name']; ?>" size="20" tabindex="20" disabled /></td>
		</tr>
		
	</table>
	<table class="formview" width="100%" border="0">		
		<tr>
			<td>Month</td>		
			<td>				
				<select tabindex="50" name="month" class="GreaterThanZero" tabindex="30">						
					<option value="0" <?php echo $formvalues['month'] == 0 ? 'selected' : ''; ?>>- Select -</option>
					<option value="1" <?php echo $formvalues['month'] == 1 ? 'selected' : ''; ?>>Jan</option>
					<option value="2" <?php echo $formvalues['month'] == 2 ? 'selected' : ''; ?>>Feb</option>
					<option value="3" <?php echo $formvalues['month'] == 3 ? 'selected' : ''; ?>>Mar</option>
					<option value="4" <?php echo $formvalues['month'] == 4 ? 'selected' : ''; ?>>Apr</option>
					<option value="5" <?php echo $formvalues['month'] == 5 ? 'selected' : ''; ?>>May</option>
					<option value="6" <?php echo $formvalues['month'] == 6 ? 'selected' : ''; ?>>Jun</option>
					<option value="7" <?php echo $formvalues['month'] == 7 ? 'selected' : ''; ?>>Jul</option>
					<option value="8" <?php echo $formvalues['month'] == 8 ? 'selected' : ''; ?>>Aug</option>
					<option value="9" <?php echo $formvalues['month'] == 9 ? 'selected' : ''; ?>>Sep</option>
					<option value="10" <?php echo $formvalues['month'] == 10 ? 'selected' : ''; ?>>Oct</option>
					<option value="11" <?php echo $formvalues['month'] == 11 ? 'selected' : ''; ?>>Nov</option>
					<option value="12" <?php echo $formvalues['month'] == 12 ? 'selected' : ''; ?>>Dec</option>						
				</select>
			</td>			
			<td>Year</td>		
			<td>
				<select id="s_year" tabindex="50" name="year" class="GreaterThanZero" tabindex="40">				
					<option value="0">- Select -</option>
					<?php
						if ( $formvalues['year'] != 0 || $formvalues['year'] != '' ) { /* #PFX201200013 12/2/2012 */
							$year = $formvalues['year'];
						} else {
							$year = date('Y');
						}
						
						for($i=0;$i<3;$i++) {
							echo '<option value="'.($year - $i).'" '.($formvalues['year'] == ($year - $i) ? 'selected' : '').'  >'.($year - $i).'</option>';							
						}
					?>
				</select>
			</td>			
		</tr>				
		<tr>			
			<td colspan="4">
				<input type="hidden" name="customer_id" id="customer_id" value="<?php echo $customer['customer_id']; ?>">			
				<input type="submit" name="search" id="search" class="button-primary" value="Search" tabindex="50"/>				
			</td>			
		</tr>
	</table>
</form>
<script>
<?php 
/* #PFX201200013 12/2/2012 */
$this_year = date('Y'); 

?>

$("#s_year").change(function () {

	this_year = <?php echo $this_year ?>;
	cutoff = 2012;
	
	sel_year = $("#s_year option:selected").val();
	year = sel_year;
	if ( year == 0 | year == '' ) {
		year = this_year;
	}
	year_to = parseInt(year) + 5;
	
	if ( year_to > this_year ) { year_to = this_year; }

	year_fr = parseInt(year_to) - 11;
	 
	if ( year_fr < cutoff ) { year_fr = cutoff; }	

	if ( year_fr > year_to ) {	year_fr = year_to;	}		

	$("#s_year option").remove();
	$("#s_year").append("<option value='0'>- Select -</option>");
	for(i=year_to; i>=year_fr; i--) {
		$("#s_year").append("<option value='"+ i +"'>"+ i +"</option>");
	}	

	$("#s_year").val(sel_year);
	
  })
  .trigger('change');
</script>

<?php  /* #PFX201200016 12/2/2012 */ 

if ($customer_id != null) {
DomainManager::getInstance()->load('Redemption');
$redemptionObj = new Redemption();

$fromdate = new DateTime('1999-01-01');
$todate = new DateTime(Date('Y-m-d'));
$purchasehistory_all = $pointObj->getPurchaseHistoryByCustomer($customer_id);
$redemption = $redemptionObj->getRedemptionByCustomer($customer_id, $fromdate, $todate, 0);
	
$accum_purchase = 0;
$accum_reward = 0; 

// collect reward
if ($purchasehistory_all != null && count($purchasehistory_all) > 0) {
	foreach($purchasehistory_all as $history) {
		$tran_date = substr($history['transacted_date'], 0, 10);
	
		$accum_purchase += $history['sales_amount'];
		$accum_reward += $history['trans_points'];
	
	}
}
// collect redeem
$accum_redeem = 0;
if ($redemption != null && count($redemption) > 0) {
	foreach($redemption as $history) {
		$tran_date = substr($history['transaction_dt'], 0, 10);
		
		$accum_redeem += $history['total_points'];
	
	}
}

}
?>

<?php /* #PFX201200016 */
$timestamp = time();

$report['report'] = 'CUSTPURRPT';
$report['report_name'] = 'cust_purchase_report_' . date('Ymd') . '.xls'; 

// Create Download file
$i = 0; 
$content[$i][0] = 'Branch';
$content[$i][1] = 'Store';
$content[$i][2] = 'Reference No.';
$content[$i][3] = 'Purchase Amount';
$content[$i][4] = 'Reward Points';
$content[$i][5] = 'Transaction Date';
$content[$i][6] = 'Transaction Time';
$content[$i][7] = 'Sale person';
if ($purchasehistory != null && count($purchasehistory) > 0) {
	foreach($purchasehistory as $history) { 
		$i++;	
		$t_date = new DateTime($history['transacted_date']);
		$content[$i][0] = $history['branch_name'];
		$content[$i][1] = $history['store_name'];
		$content[$i][2] = $history['reference_no'];
		$content[$i][3] = $history['sales_amount'];
		$content[$i][4] = $history['trans_points'];
		$content[$i][5] = $t_date->format('Y-m-d');
		$content[$i][6] = $t_date->format('H:i:s');
		$content[$i][7] = $history['remarks'];
	}
}
$i++;
$content[$i][0] = ' ';
$i++;
$content[$i][0] = 'Accum. Reward Points = ';
$content[$i][1] = $accum_reward;
$content[$i][2] = 'Accum. Redeem Points =';
$content[$i][3] = $accum_redeem;

$report['content'] = $content; 
$session_id = 'd'.$timestamp;
$_SESSION[$session_id] = $report;

// Create Download all by customer
$report['report_name'] = 'cust_allpurchase_report_' . date('Ymd') . '.xls';
$i = 0;
$content_all[$i][0] = 'Branch';
$content_all[$i][1] = 'Store';
$content_all[$i][2] = 'Reference No.';
$content_all[$i][3] = 'Purchase Amount';
$content_all[$i][4] = 'Reward Points';
$content_all[$i][5] = 'Transaction Date';
$content_all[$i][6] = 'Transaction Time';
$content_all[$i][7] = 'Sale person';
if ($purchasehistory_all != null && count($purchasehistory_all) > 0) {
foreach($purchasehistory_all as $history) {
	$i++;
	$t_date = new DateTime($history['transacted_date']);
	$content_all[$i][0] = $history['branch_name'];
	$content_all[$i][1] = $history['store_name'];
	$content_all[$i][2] = $history['reference_no'];
	$content_all[$i][3] = $history['sales_amount'];
	$content_all[$i][4] = $history['trans_points'];
	$content_all[$i][5] = $t_date->format('Y-m-d');
	$content_all[$i][6] = $t_date->format('H:i:s');
	$content_all[$i][7] = $history['remarks'];
}
}
$i++;
$content_all[$i][0] = ' ';
$i++;
$content_all[$i][0] = 'Accum. Reward Points = ';
$content_all[$i][1] = $accum_reward;
$content_all[$i][2] = 'Accum. Redeem Points =';
$content_all[$i][3] = $accum_redeem;

$report['content'] = $content_all;
$session_id2 = 'all'.$timestamp;
$_SESSION[$session_id2] = $report;

?>
<div class="SectionTitle" ><b><?php echo ($purchasehistory != null && count($purchasehistory) > 0) ? count($purchasehistory) : '0'; ?> </b> Record(s) found. 

 (<?php if ($purchasehistory != null && count($purchasehistory) > 0) { ?><a href="dl.php?p=<?php echo $session_id ?>" class="downlink"> download </a> 
 |<?php } ?> <a href="dl.php?p=<?php echo $session_id2 ?>" class="downlink"> download all </a>)

 </div>

<table class="tabular">
	<thead>
		<tr>			
			<th>Branch</th>
			<th>Store</th>
			<th>Reference No.</th>
			<th>Purchase Amount</th>
			<th>Reward Points</th>
			<th>Transaction On</th>			
			<th>Sale person</th>			
		</tr>
	</thead>
	<tbody>
		<?php
			if ($purchasehistory != null) {
			foreach($purchasehistory as $history) {
		?>
		<tr>						
			<td><?php echo $history['branch_name'] ?></td>
			<td><?php echo $history['store_name'] ?></td>
			<td><?php echo $history['reference_no'] ?></td>
			<td><?php echo $history['sales_amount'] ?></td>
			<td><?php echo $history['trans_points'] ?></td>			
			<td><?php echo $history['transacted_date'] ?></td>			
			<td><?php echo $history['remarks'] ?></td>
		</tr>
		<?php } } ?>
	</tbody>
</table>

<table class="formview" width="100%" border="0">		
	<tr>
		<td>Accum. Reward Points</td>		
		<td> <?php echo number_format($accum_reward, 2, '.', ','); ?>			
		</td>			
		<td>Accum. Redeem Points</td>		
		<td> <?php echo number_format($accum_redeem, 2, '.', ','); ?>
		</td>			
	</tr>				
</table>

<script type="text/javascript">
	$(document).ready(function() { 
		loadValidation('historyform');			
	});	
</script>