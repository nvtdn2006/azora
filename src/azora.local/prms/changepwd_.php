<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Change password'?>

<?php	

	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('validation');		
	
	require_once './includes/user.php';
	$userObj = new User();
	
	$status = true;
	$info = array();
	
	if (isset($_POST['submit']) && $_POST['submit'] == 'Save') {			
		$old_password = $_POST['old_password'];	
		$new_password = $_POST['new_password'];	
		$reenter_password = $_POST['reenter_password'];	
		
		if ($old_password != '' && $new_password != '' && $reenter_password != '') {
			if ($new_password == $reenter_password) {
				$user_id = Authentication::getUserId();
				
				if ($userObj->isAuthorizedUser($user_id, $old_password)) {
					$newpassword = array('user_id' => $user_id,
									'password' => $new_password,
									'updated_by' => $user_id);
					
					if ($userObj->resetPassword($newpassword)) {
						array_push($info, 'New password has been successfully saved.');
						
						//Event Log
						$evtObj = new EventObject();					
						$evtObj->event_id = EventTypes::PRMS_ChangePassword;
						$evtObj->description = Authentication::getAttribute('login_id') . ' changed his/her password.';
						$evtObj->action_by = Authentication::getAttribute('login_id');					
						EventLog::Log($evtObj);
						
					} else {
						$status = false;
						array_push($info, 'Failed to update passwords.');
					}
				} else {
					$status = false;
					array_push($info, 'Invalid old password.');
				}
			} else {
				$status = false;
				array_push($info, 'New and Re-enter passwords do not match.');
			}	
		} else {
			$status = false;
			array_push($info, 'Please enter required fields.');
		}
	}
	
?>

<?php
	$infoCss = $status ? 'success-info' : 'error-info';
	if (isset($info) && count($info) > 0) {
?>
	<div class="<?php echo $infoCss; ?> form-info">
		<?php foreach ($info as $handle) {
				echo "<p>$handle</p>";
		} ?>
	</div>
<?php
	}
?>

<form name="changepwdform" id="changepwdform" action="changepwd.php" method="post"> 
	<table class="formview" width="100%" border="0" cellspacing="3px" cellpadding="3px">
		<tr>
			<td class="LabelCell Required">Old password</td>
			<td><input type="password" name="old_password" id="old_password" maxlength="50" class="input Required" size="20" tabindex="10" /></td>
		</tr>
		<tr>
			<td class="LabelCell Required">New password</td>
			<td><input type="password" name="new_password" id="new_password" maxlength="50" class="input Required" size="20" tabindex="20" /></td>
		</tr>
		<tr>
			<td class="LabelCell Required">Re-enter password</td>
			<td><input type="password" name="reenter_password" id="reenter_password" maxlength="50" class="input Required" size="20" tabindex="30" /></td>
		</tr>
		<tr>			
			<td class="BottomToolBar" colspan="2">				
				<input type="submit" name="submit" id="submit" class="button-primary" value="Save" tabindex="30"/>
			</td>			
		</tr>
	</table>
</form>
	
<script type="text/javascript">
	$(document).ready(function() { 
		loadValidation('changepwdform');
	});
</script>