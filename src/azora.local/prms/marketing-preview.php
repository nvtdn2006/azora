<?php require_once './includes/application.php'; $this->template = 'blank'; $this->title = 'Preview'?>

<?php	

	DomainManager::getInstance()->load('Marketing');
	$marketingObj = new Marketing();
	
	$mail_id = 0;
	$mail = null;
	
	if (isset($_GET['id'])) {		
		$mail_id = $_GET['id'];
		$mail = $marketingObj->getMail($mail_id);				
	}
	
?>

<?php if($mail_id > 0) { ?>
<div class="SectionTitle" ><?php echo $mail['mail_subject']; ?></div>
<br/><br/>
<?php echo $mail['mail_body']; ?>

<?php } else  { ?>
<div class="error-info" >Record not found. Please try again.</div>
<?php } ?>