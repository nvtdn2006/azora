<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Manage products'?>

<?php		

	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('checkboxlist');
	JSManager::getInstance()->add('messagebox');
	
	DomainManager::getInstance()->load('Product');
	$productObj = new Product();
	
	$error = array();
	
	if (isset($_POST['delete']) && $_POST['delete'] == 'Remove') {					
		$products = null;
		if (isset($_POST['products']))
			$products = $_POST['products'];
		if (isset($products) && count($products) > 0) {
		
			$_names = '';
			$sp = '';
			foreach($products as $item) {
				$nobj = $productObj->getProduct($item);
				$_names .= $sp . $nobj['product']['product_code'];
				$sp = ', ';
			}
		
			if ($productObj->deleteProduct($products))	{	
				
				//Event Log
				$evtObj = new EventObject();					
				$evtObj->event_id = EventTypes::PRMS_DeleteProduct;
				$evtObj->description = Authentication::getAttribute('login_id') . ' deleted products ('.$_names.').';
				$evtObj->action_by = Authentication::getAttribute('login_id');					
				EventLog::Log($evtObj);
				
				header( 'Location: products.php');
			}
		} else {
			array_push($error, 'Please select at least one item to proceed.');	
		}
	}
	
	$products = $productObj->getAllProductInfo();
?>

<?php
	if (isset($error) && count($error) > 0) {
?>
	<div class="error-info tabular-info">
		<?php foreach ($error as $handle) {
				echo "<p>$handle</p>";
		} ?>
	</div>
<?php
	}
?>


<form name="productlist" id="productlist" action="products.php" method="post"> 
	<table class="tabular">
		<thead>
			<tr>			
				<th><input type="checkbox" name="listselectall" class="listselectall" onclick="javascript:checkSelectAllBox('listselect', this.checked);"/></th>
				<th>Code</th>
				<th>Name</th>	
				<th>Points</th>				
				<th>Balance</th>
				<th>Updated By</th>
				<th>Updated On</th>			
			</tr>
		</thead>
		<tfoot>
			<tr>
				<th colspan="7"><a class="button-primary" href="product-edit.php">New</a>
				<input type="submit" name="delete" id="submit" class="button-secondary" value="Remove" onclick="javascript:return confirmBox('Are you sure you want to delete selected item?');"/></th>		
			</tr>
		</tfoot>
		<tbody>		
			<?php
				foreach($products as $product) {
			?>
			<tr>
				<td><input type="checkbox" name="products[]" class="listselect" onclick="javascript:checkSelectBox('listselectall', 'listselect');" value="<?php echo $product['product_id']; ?>" /></td>
				<td><a class="detailLink" href="<?php echo 'product-edit.php?product='.$product['product_id'] ?>" alt="Edit" title="Edit"><?php echo $product['product_code'] ?></a></td>
				<td><?php echo $product['product_name'] ?></td>		
				<td><?php echo $product['product_points'] ?></td>				
				<td><?php echo $product['product_balance'] ?></td>
				<td><?php echo $product['updated_username'] ?></td>
				<td><?php echo $product['updated_dt'] ?></td>			
			</tr>
			<?php } ?>
		</tbody>
	</table>
</form>
<?php
	$productObj = null;
?>