<?php require_once './includes/application.php'; $this->template = 'blank'; $this->title = 'Sending'?>

<?php	
	DomainManager::getInstance()->load('Marketing');
	$marketingObj = new Marketing();
	
	$mail_id = 0;	
	
	if (isset($_GET['id'])) {		
		$mail_id = $_GET['id'];		
	}
?>

<div id="Sending" class="<?php echo $mail_id > 0 ? 'success' : 'error'; ?>-info">		
	<?php echo $mail_id > 0 ? 'Please wait, sending...' : 'Mail not found.'; ?>
</div>
<input type="hidden" id="mid" value="<?php echo $mail_id; ?>" />

<script type="text/javascript">
	
	
		<?php if ($mail_id > 0) { ?>
		var t=setTimeout("send()",3000);
		
		function send() {
		id = $( "#mid" ).val();
		
		$.post( 'marketing-sendsvc.php', { mid: id, form: 'send' },
			function(data) {				
				if (data.status) {					
					$( '#Sending' ).html('Process done.');
				} else {
					$( '#Sending' ).removeClass('success-info');
					$( '#Sending' ).addClass('error-info');
					$( '#Sending' ).html('Failed to send.');					
				}					
			}
		);
		}
		<?php } ?>
	
</script>