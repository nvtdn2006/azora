<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Billing'?>

<?php		

	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('validation');
	
	DomainManager::getInstance()->load('Store');	
	$storeObj = new Store();
		
	DomainManager::getInstance()->load('Redemption');
	$redemptionObj = new Redemption();
	
	$redemptions = null;
	
	if (isset($_GET['action']) && $_GET['action'] == 'search') {
		
		$month = date('n');
		$year = date('Y');
		$store = 0;
		
		if (isset($_POST['month'])) 
			$month = $_POST['month'];
			
		if (isset($_POST['year'])) 
			$year = $_POST['year'];
			
		if (isset($_POST['store'])) 
			$store = $_POST['store'];
			
		$redemptions = $redemptionObj->getBills($month, $year, $store);			
		
	}
	
	$formvalues = array();
	
	if (isset($_REQUEST['month'])) 			
		$formvalues['month'] = $_REQUEST['month'];
	else
		$formvalues['month'] =  0;
	
	if (isset($_REQUEST['year'])) 			
		$formvalues['year'] = $_REQUEST['year'];
	else
		$formvalues['year'] =  0;
		
	if (isset($_REQUEST['store'])) 			
		$formvalues['store'] = $_REQUEST['store'];
	else
		$formvalues['store'] =  0;
		
?>

<form name="billingform" id="billingform" action="billing.php?action=search" method="post"> 
	<table class="formview" width="100%" border="0">		
		<tr>
			<td>Month</td>		
			<td>				
				<select tabindex="50" name="month" class="GreaterThanZero" tabindex="10">						
					<option value="0" <?php echo $formvalues['month'] == 0 ? 'selected' : ''; ?>>- Select -</option>
					<option value="1" <?php echo $formvalues['month'] == 1 ? 'selected' : ''; ?>>Jan</option>
					<option value="2" <?php echo $formvalues['month'] == 2 ? 'selected' : ''; ?>>Feb</option>
					<option value="3" <?php echo $formvalues['month'] == 3 ? 'selected' : ''; ?>>Mar</option>
					<option value="4" <?php echo $formvalues['month'] == 4 ? 'selected' : ''; ?>>Apr</option>
					<option value="5" <?php echo $formvalues['month'] == 5 ? 'selected' : ''; ?>>May</option>
					<option value="6" <?php echo $formvalues['month'] == 6 ? 'selected' : ''; ?>>Jun</option>
					<option value="7" <?php echo $formvalues['month'] == 7 ? 'selected' : ''; ?>>Jul</option>
					<option value="8" <?php echo $formvalues['month'] == 8 ? 'selected' : ''; ?>>Aug</option>
					<option value="9" <?php echo $formvalues['month'] == 9 ? 'selected' : ''; ?>>Sep</option>
					<option value="10" <?php echo $formvalues['month'] == 10 ? 'selected' : ''; ?>>Oct</option>
					<option value="11" <?php echo $formvalues['month'] == 11 ? 'selected' : ''; ?>>Nov</option>
					<option value="12" <?php echo $formvalues['month'] == 12 ? 'selected' : ''; ?>>Dec</option>						
				</select>
			</td>			
			<td>Year</td>		
			<td>
				<select tabindex="50" name="year" class="GreaterThanZero" tabindex="20">				
					<option value="0">- Select -</option>
					<?php
						$year = date('Y');
						for($i=0;$i<3;$i++) {
							echo '<option value="'.($year - $i).'" '.($formvalues['year'] == ($year - $i) ? 'selected' : '').'  >'.($year - $i).'</option>';							
						}
					?>
				</select>
			</td>			
		</tr>
		<tr>			
			<td>Store</td>
			<td colspan="3">				
				<select tabindex="20" name="store" class="GreaterThanZero">						
					<?php
						$stores = $storeObj->getAllStores();						
						echo '<option value="0">- Select -</option>';
							
						foreach ($stores as $store) {
							echo '<option value='.$store['store_id'].' '.($store['store_id'] == $formvalues['store'] ? 'selected' : '') .'>'.$store['store_branch_name'].'</option>';
						}
					?>
				</select>
			</td>			
		</tr>		
		<tr>			
			<td colspan="4">				
				<input type="submit" name="search" id="search" class="button-primary" value="Search" tabindex="30"/>				
				<?php
					if ($redemptions != null && count($redemptions) > 0)
						echo '&nbsp;&nbsp;<a target="_blank" href="billing-pf.php?store='.$formvalues['store'].'&month='.$formvalues['month'].'&year='.$formvalues['year'].'">Print</a>';
				?>
			</td>			
		</tr>
	</table>
</form>

<div class="SectionTitle" ><b><?php echo ($redemptions != null && count($redemptions) > 0) ? count($redemptions) : '0'; ?> </b> Record(s) found.</div>

<table class="tabular">
	<thead>
		<tr>						
			<th>No.</th>
			<th>Redemption No.</th>
			<th>NRIC</th>
			<th>Product</th>
			<th>Product Cost</th>
			<th>Product Points</th>			
			<th>Quantity</th>
			<th>Redeemed Points</th>						
			<th>Amount</th>
		</tr>
	</thead>
	<tbody>
		<?php
			if ($redemptions != null) {
			$sr = 1;
			$total = 0;
			foreach($redemptions as $redemption) {
		?>
		<tr>						
			<td><?php echo $sr; ?></td>
			<td><?php echo $redemption['redemption_no'];?></td>
			<td><?php echo $redemption['nric']; ?></td>
			<td><?php echo $redemption['product_name']; ?></td>
			<td><?php echo $redemption['product_cost']; ?></td>
			<td><?php echo $redemption['product_points']; ?></td>
			<td><?php echo $redemption['quantity']; ?></td>
			<td><?php echo $redemption['points']; ?></td>	
			<td><?php echo $redemption['amount']; ?></td>	
		</tr>
		<?php 
			$sr++;
			$total += $redemption['amount'];			
		}?>
		<tr>
			<td colspan="7">&nbsp;</td>
			<td><b>Total</b></td>
			<td><?php echo $total; ?></td>
		</tr>
		<?php } ?>
		
	</tbody>
</table>


<script type="text/javascript">
	$(document).ready(function() { 
		loadValidation('billingform');			
	});	
</script>