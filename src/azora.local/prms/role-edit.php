<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'New role'?>

<?php	

	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('validation');

	require_once './includes/role.php';
	$roleObj = new Role();
	
	require_once './includes/feature.php';
	$featureObj = new Feature();
	
	$role_id = 0;
	$role = null;
	
	$error = array();
	
	if (isset($_GET['role'])) {		
		$role_id = $_GET['role'];
		$role = $roleObj->getRole($role_id);		
		$this->title = 'Update role';
	}
	
	if (isset($_GET['action'])) {		
		$isValid = true;
		
		switch ($_GET['action']){
			case 'new':	
				$features = null;
				if (isset($_POST['select_to']))
					$features = $_POST['select_to'];
					
				if ($features == null) {
					array_push($error, 'Please select features.');
					$isValid = false;
				}
					
				if ($isValid) {
					$role = array('role_name' => $_POST['role_name'],
									'role_description' => $_POST['role_description'],								
									'role_created_by' => Authentication::getUserId(),								
									'role_updated_by' => Authentication::getUserId(),
									'features' => $features);								
					if ($roleObj->saveRole($role)) {
					
						//Event Log
						$evtObj = new EventObject();					
						$evtObj->event_id = EventTypes::PRMS_AddRole;
						$evtObj->description = Authentication::getAttribute('login_id') . ' added new role ('.$_POST['role_name'].').';
						$evtObj->action_by = Authentication::getAttribute('login_id');					
						EventLog::Log($evtObj);
						
						header( 'Location: roles.php');
					}
					else 
						array_push($error, 'Saving process failed. Please try again.');
				}
			break;
			case 'update':
				$features = null;
				if (isset($_POST['select_to']))
					$features = $_POST['select_to'];
					
				if ($features == null) {
					array_push($error, 'Please select features.');
					$isValid = false;
				}
					
				if ($isValid) {
					$role = array('role_name' => $_POST['role_name'],
									'role_description' => $_POST['role_description'],																
									'role_updated_by' => Authentication::getUserId(),
									'role_id' => $role_id,
									'features' => $features);
					if ($roleObj->updateRole($role)) {
						
						//Event Log
						$evtObj = new EventObject();					
						$evtObj->event_id = EventTypes::PRMS_UpdateRole;
						$evtObj->description = Authentication::getAttribute('login_id') . ' updated role ('.$_POST['role_name'].').';
						$evtObj->action_by = Authentication::getAttribute('login_id');					
						EventLog::Log($evtObj);
						
						header( 'Location: roles.php');	
					}
					else 
						array_push($error, 'Saving process failed. Please try again.');
				}
			break;
		}		
	}
	
	if (!isset($role) || isset($_GET['action'])) {		
		if (isset($_REQUEST['role_name'])) 			
			$role['role_name'] = $_REQUEST['role_name'];
		else
			$role['role_name'] =  '';
			
		if (isset($_REQUEST['role_description'])) 			
			$role['role_description'] = $_REQUEST['role_description'];
		else
			$role['role_description'] =  '';
	}
	
?>

<?php
	if (isset($error) && count($error) > 0) {
?>
	<div class="error-info form-info">
		<?php foreach ($error as $handle) {
				echo "<p>$handle</p>";
		} ?>
	</div>
<?php
	}
?>

<form name="roleform" id="roleform" action="role-edit.php?action=<?php echo $role_id > 0 ? 'update&role='.$role_id : 'new'; ?>" method="post"> 
	<table class="formview" width="100%" border="0" cellspacing="3px" cellpadding="0">
		<tr>
			<td class="LabelCell Required">Role</td>
			<td><input type="text" name="role_name" id="role_name" maxlength="50" class="input Required" value="<?php echo isset($role) ? $role['role_name'] : '' ?>" size="20" tabindex="10" /></td>
		</tr>
		<tr>
			<td class="LabelCell">Description</td>
			<td><input type="text" name="role_description" id="role_description" maxlength="255" class="input" value="<?php echo isset($role) ? $role['role_description'] : '' ?>" size="20" tabindex="20" /></td>
		</tr>
		
		<tr>			
			<td colspan="2">				
				<table width="100%" border="0" cellspacing="3px" cellpadding="3px">
					<tr>
						<td width="40%">
							<b>Features</b>
						</td>
						<td width="20%">
							
						</td>
						<td width="40%">
							<b>Selected features</b>
						</td>
					</tr>
					<tr>
						<td>
							<select id="select_from" name="select_from[]" size="7" multiple tabindex="30">							
								<?php
									$features = $featureObj->getRoleFeaturesNotSelected($role_id);
									
									foreach($features as $feature) {
										echo '<option value="'.$feature['feature_id'].'">'.$feature['feature_name'].'</option>';
									}
								?>							
							</select>
						</td>
						<td align="center">
							<a id="btn_add" href="#" class="button-secondary">&gt;</a><br/><br/>
							<a id="btn_remove" href="#" class="button-secondary">&lt;</a>
						</td>
						<td>
							<select id="select_to" name="select_to[]" size="7" multiple tabindex="40">
								<?php
									$features = $featureObj->getRoleFeatures($role_id);
									
									foreach($features as $feature) {
										echo '<option value="'.$feature['feature_id'].'">'.$feature['feature_name'].'</option>';
									}
								?>
							</select>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr>
			<td class="BottomToolBar" colspan="2">	
				<input type="submit" name="submit" id="submit" class="button-primary" value="Save" tabindex="50"/>
				<a href="roles.php" class="button-secondary" tabindex="60">Cancel</a>
			</td>			
		</tr>
	</table>
</form>
<?php
	$roleObj = null;
?>
<script type="text/javascript">
	$(document).ready(function() { 
		loadValidation('roleform');
		
		$('#btn_add').click(function(){
			event.preventDefault();
			$('#select_from option:selected').each( function() {
					$('#select_to').append("<option value='"+$(this).val()+"'>"+$(this).text()+"</option>");
				$(this).remove();
			});
		});
		$('#btn_remove').click(function(){
			event.preventDefault();
			$('#select_to option:selected').each( function() {
				$('#select_from').append("<option value='"+$(this).val()+"'>"+$(this).text()+"</option>");
				$(this).remove();
			});
		});
		
		$('#submit').click(function(){			
			$('#select_to option').each(function() {				
				$(this).attr('selected', 'selected');
			});
		});
	});
</script>