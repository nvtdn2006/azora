<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'New marketing mail'?>

<?php		
	$config = Factory::getConfig();	

	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('validation');
	JSManager::getInstance()->add('numeric');	
	
	DomainManager::getInstance()->load('Marketing');
	$marketingObj = new Marketing();
	
	$mail_id = 0;
	$mail = null;
	
	$error = array();
	
	if (isset($_GET['mail'])) {		
		$mail_id = $_GET['mail'];
		$mail = $marketingObj->getMail($mail_id);		
		$this->title = 'Update marketing mail';
	}
	
	if (isset($_GET['action'])) {

		$isValid = false;
		
		if (isset($_POST['mail_subject']) && $_POST['mail_subject'] != '' &&
			isset($_POST['mail_body']) && $_POST['mail_body'] != '') {
			$isValid = true;
		} else {
			array_push($error, 'All field are required.');	
		}
	
		switch ($_GET['action']){
			case 'new':							
				$mail = array('mail_subject' => $_POST['mail_subject'],
								'mail_body' => $_POST['mail_body'],									
								'created_by' => Authentication::getUserId(),								
								'updated_by' => Authentication::getUserId());								
				if ($marketingObj->saveMail($mail)) {	

					//Event Log
					$evtObj = new EventObject();					
					$evtObj->event_id = EventTypes::PRMS_AddMarketing;
					$evtObj->description = Authentication::getAttribute('login_id') . ' added new marketing mail ('.$_POST['mail_subject'].').';
					$evtObj->action_by = Authentication::getAttribute('login_id');					
					EventLog::Log($evtObj);
				
					header( 'Location: marketing.php');				
				}
			break;
			case 'update':				
				$mail = array('mail_subject' => $_POST['mail_subject'],
								'mail_body' => $_POST['mail_body'],									
								'updated_by' => Authentication::getUserId(),
								'id' => $mail_id);
				if ($marketingObj->updateMail($mail)) {			

					//Event Log
					$evtObj = new EventObject();					
					$evtObj->event_id = EventTypes::PRMS_UpdateMarketing;
					$evtObj->description = Authentication::getAttribute('login_id') . ' updated marketing mail ('.$_POST['mail_subject'].').';
					$evtObj->action_by = Authentication::getAttribute('login_id');					
					EventLog::Log($evtObj);
					
					header( 'Location: marketing.php');						
				}
			break;
		}
	}
			
	if (!isset($mail) || isset($_GET['action'])) {				
		if (isset($_REQUEST['mail_subject'])) 			
			$mail['mail_subject'] = $_REQUEST['mail_subject'];
		else
			$mail['mail_subject'] =  '';		
			
		if (isset($_REQUEST['mail_body'])) 			
			$mail['mail_body'] = $_REQUEST['mail_body'];
		else
			$mail['mail_body'] =  '';		
	}
	
?>

<?php
	if (isset($error) && count($error) > 0) {
?>
	<div class="error-info form-info">
		<?php foreach ($error as $handle) {
				echo "<p>$handle</p>";
		} ?>
	</div>
<?php
	}
?>

<form name="mailform" id="mailform" action="marketing-edit.php?action=<?php echo $mail_id > 0 ? 'update&mail='.$mail_id : 'new'; ?>" method="post"> 
	<table class="formview" width="100%" border="0" cellspacing="3px" cellpadding="3px">
		<tr>
			<td class="LabelCell Required">Subject</td>
			<td><input type="text" name="mail_subject" id="mail_subject" class="input Required" value="<?php echo $mail['mail_subject']; ?>" size="20" tabindex="10" /></td>
		</tr>		
		<tr>			
			<td class="SectionBar" colspan="2">				
				Mail Body
			</td>
		</tr>
		<tr>
			<td class="LabelCell Disabled" >Custom Fields</td>			
			<td class="LabelCell Disabled" >Customer Name : {%name%}</td>			
		</tr>		
		<tr>
			<td colspan="2">
				<div style="width: 635px; height: 500px; overflow: auto;">
					<textarea name="mail_body" id="mail_body" tabindex="50" style="width: 635px"><?php echo $mail['mail_body']; ?></textarea>					
				</div>				
			</td>
		</tr>			
		<tr>			
			<td class="BottomToolBar" colspan="2">	
				<input type="submit" name="submit" id="submit" class="button-primary" value="Save" tabindex="50"/>
				<a href="marketing.php" class="button-secondary" tabindex="60">Cancel</a>
			</td>			
		</tr>
	</table>
</form>
<?php
	
?>
<!-- TinyMCE -->
<script type='text/javascript' src='<?php echo $config['PRMSConfig']->live_site; ?>/js/tiny_mce/tiny_mce.js'></script>

<script type="text/javascript">

	tinyMCE.init({
		// General options
		mode : "exact",
		elements : "mail_body",
		theme : "advanced",
		plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave",

		// Theme options
		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr",
		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
		theme_advanced_buttons5 : ",|,link,unlink,anchor,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor,|,print,|,ltr,rtl,|,fullscreen",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : false,
		editor_content_width : 500,
		width: "635",
		height: "480",

	});
	
	$(document).ready(function() { 
		loadValidation('mailform');	
	});	
	
</script>
<!-- /TinyMCE -->