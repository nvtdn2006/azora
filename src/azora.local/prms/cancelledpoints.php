<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Points cancelled'; ?>

<?php
	$points = 0;
	$nric = '';
	if (isset($_GET['nric'])) {
		$nric = $_GET['nric'];
	}
	
	if (isset($_GET['points'])) {
		$points = $_GET['points'];
	}
?>	
 <div class="cprocess">
	Cancellation process successfully done.<br/><br/><br/><br/>
	Points <b><?php echo $points; ?></b> have been deducted from account <b><?php echo $nric; ?></b>.
	<br/><br/><br/><br/><br/><br/>
	<a href="customersearch.php?d=cancel" class="button-secondary" title="Back to cancellation points.">Done</a>
</div>
</div>