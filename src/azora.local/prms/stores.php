<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Manage stores'?>

<?php	

	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('checkboxlist');
	JSManager::getInstance()->add('messagebox');
	
	DomainManager::getInstance()->load('Store');
	$storeObj = new Store();
	
	$error = array();
	
	if (isset($_POST['delete']) && $_POST['delete'] == 'Remove') {			
		$stores = null;
		if (isset($_POST['stores']))
			$stores = $_POST['stores'];
		if (isset($stores) && count($stores) > 0) {
		
			$_names = '';
			$sp = '';
			foreach($stores as $item) {
				$nobj = $storeObj->getStore($item);
				$_names .= $sp . $nobj['store_name'];
				$sp = ', ';
			}
		
			if ($storeObj->deleteStore($stores)) {	
			
				//Event Log
				$evtObj = new EventObject();					
				$evtObj->event_id = EventTypes::PRMS_DeleteStore;
				$evtObj->description = Authentication::getAttribute('login_id') . ' deleted stores ('.$_names.').';
				$evtObj->action_by = Authentication::getAttribute('login_id');					
				EventLog::Log($evtObj);
				
				header( 'Location: stores.php');
			}
		} else {
			array_push($error, 'Please select at least one item to proceed.');	
		}
	}
	
	$stores = $storeObj->getAllStores();
?>

<?php
	if (isset($error) && count($error) > 0) {
?>
	<div class="error-info tabular-info">
		<?php foreach ($error as $handle) {
				echo "<p>$handle</p>";
		} ?>
	</div>
<?php
	}
?>

<form name="storelist" id="storelist" action="stores.php" method="post"> 
	<table class="tabular">
		<thead>
			<tr>
				<th><input type="checkbox" name="listselectall" class="listselectall" onclick="javascript:checkSelectAllBox('listselect', this.checked);"/></th>
				<th>Store</th>
				<th>Address</th>
				<th>Branch</th>
				<th>Collection</th>
				<th>Updated By</th>
				<th>Updated On</th>			
			</tr>
		</thead>
		<tfoot>
			<tr>
				<th colspan="7"><a class="button-primary" href="store-edit.php">New</a>
				<input type="submit" name="delete" id="submit" class="button-secondary" value="Remove" onclick="javascript:return confirmBox('Are you sure you want to delete selected item?');"/></th>		
			</tr>
		</tfoot>
		<tbody>		
			<?php
				foreach($stores as $store) {
			?>
			<tr>
				<td><input type="checkbox" name="stores[]" class="listselect" onclick="javascript:checkSelectBox('listselectall', 'listselect');" value="<?php echo $store['store_id']; ?>" /></td>
				<td><a class="detailLink" href="<?php echo 'store-edit.php?store='.$store['store_id'] ?>" alt="Edit" title="Edit"><?php echo $store['store_name'] ?></a></td>
				<td><?php echo $store['store_address'] ?></td>
				<td><?php echo $store['branch_name'] ?></td>
				<td><?php echo $store['store_collection'] ? 'Yes' : 'No'; ?></td>
				<td><?php echo $store['updated_username'] ?></td>
				<td><?php echo $store['updated_dt'] ?></td>				
			</tr>
			<?php } ?>
		</tbody>
	</table>
</form>
<?php
	$storeObj = null;
?>