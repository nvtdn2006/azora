<?php require_once './includes/application.php'; $this->Auth_Req = false; $this->template = 'login'; $this->title = 'Login'?>

<?php

	JSManager::getInstance()->add('jquery');
	
	if (Authentication::isAuthenticated()) {
		header( 'Location: index.php');
	}
	
	$error = '';
	$info = '';
	$form = 'login';
	
	
	if (isset($_GET['action']) && $_GET['action'] == 'login') {
		$form = 'login';
		if (isset($_POST['user_login']) && isset($_POST['user_pass'])) {
			$username = $_POST['user_login'];
			$password = $_POST['user_pass'];
			
			if ($username != '' && $password != '') {
				require_once './includes/user.php';
				$userObj = new User();
				$user = null; 
				if ($userObj->isValidUser($username, $password, $user)) {			
					Authentication::SignIn($user['user_id']);
					Authentication::setAttribute('login_id', $user['username']);
					
					if ($userObj->isSuperUser($user['user_id']))
						Authentication::setAttribute('su', true);
					else
						Authentication::setAttribute('su', false);
						
					//Event Log
					$evtObj = new EventObject();					
					$evtObj->event_id = EventTypes::PRMS_UserLogin;
					$evtObj->description = $user['username'] . ' logged into PRMS.';
					$evtObj->action_by = $user['username'];					
					EventLog::Log($evtObj);
					
					header( 'Location: index.php');
				} else {
					$error = 'Invalid username.';
				}
			} else {
				$error = 'Please enter username and password.';
			}
		} 
	} else if (isset($_GET['action']) && $_GET['action'] == 'forgot') {
		$form = 'forgot';
		
		if (isset($_POST['user_email'])) {
			$email = $_POST['user_email'];			
			
			if ($email != '') {
				
				require_once './includes/user.php';
				$userObj = new User();
				
				if ($userObj->isExistEmail($email)) {
					if ($userObj->forgotPassword($email, $forgotInfo)) {
					
						DomainManager::getInstance()->load('Company');
						$companyObj = new Company();
						$company = $companyObj->getCompany();	
						$company = $company[0];
	
						$forgotInfo['company_name'] = $company['company_name'];
	
						$mailer = new SiteMailer();
						$mailer->toMail = $forgotInfo['email'] ;
						$mailer->subject = 'Password reset at '.$company['company_name'].' PRMS';				
						$mailer->PrepareMail('sendPasswordResetToUser', $forgotInfo);
						$mailer->Send();						
						$info = 'Password has been sent to your mail.';
						
						//Event Log
						$evtObj = new EventObject();					
						$evtObj->event_id = EventTypes::PRMS_ReqForgotPwd;
						$evtObj->description = $forgotInfo['username'] . ' requested for forgotten password.';
						$evtObj->action_by = $forgotInfo['username'];					
						EventLog::Log($evtObj);
						
					} else {						
						$error = 'Failed to reset.';
					}
				} else {
					$error = 'Invalid email address.';
				}
				
			} else {
				$error = 'Please enter email address.';
			}
		}
	}
	
?>

<div id="login">
	<div id="logo"></div>
	<div id="prms"></div>
	
	<?php if ($error != '') { ?>
		<div id="login_error">
			<?php echo $error; ?>
		</div>
	<?php } ?>
	
	<?php if ($info != '') { ?>
		<div id="info">
			<?php echo $info; ?>
		</div>
	<?php } ?>
	
	<form name="loginform" id="loginform" action="login.php?action=login" method="post" <?php echo $form == 'login' ? 'style="display: block;"' : 'style="display: none;"'; ?>> 
		<p> 
			<label>Username<br /> 
			<input type="text" name="user_login" id="user_login" class="input" value="" size="20" tabindex="10" /></label> 
		</p> 
		<p> 
			<label>Password<br /> 
			<input type="password" name="user_pass" id="user_pass" class="input" value="" size="20" tabindex="20" /></label> 
		</p> 
		<p id="nav"><a href="#" id="forgot" title="Password Lost and Found">Lost your password?</a></p> 
		<p class="submit"> 
			<input type="submit" name="submit" id="submit" class="button-primary" value="Login" tabindex="100"/> 			
		</p> 		
	</form>
	
	<form name="forgotform" id="forgotform" action="login.php?action=forgot" method="post" <?php echo $form == 'forgot' ? 'style="display: block;"' : 'style="display: none;"'; ?>> 
		<p> 
			<label>Email<br /> 
			<input type="text" name="user_email" id="user_email" class="input" value="" size="20" tabindex="1000" /></label> 
		</p> 		
		<p class="submit"> 
			<input type="submit" name="submit" id="submit" class="button-primary" value="Submit" tabindex="1010"/><div class="indicator"></div> 			
		</p> 
		<p id="nav"> 
			<a href="#" id="cancel" title="Cancel">Cancel</a> 
		</p> 
	</form>
	
</div>

<script type="text/javascript">
	
	$(document).ready(function() { 
		
		$('#user_login').focus();
		
		$('#forgot').click(function(e) {
			e.preventDefault();
			$('#loginform').hide();
			$('#forgotform').show();
			$('#user_email').focus();
		});
		$('#cancel').click(function(e) {
			e.preventDefault();
			$('#loginform').show();
			$('#forgotform').hide();
			$('#user_login').focus();
		});

	});	
	
</script>