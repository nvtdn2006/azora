<?php require_once './includes/application.php'; ?>

<?php	

	include 'connectionstring.php';

	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('jquery.ui');			
	CSSManager::getInstance()->add('/js/jqueryui/smoothness/jquery-ui-1.8.16.custom.css');	

	$config = Factory::getConfig();
	$cryptographer = Factory::getCryptographer();

	DomainManager::getInstance()->load('Customer');
	$customerObj = new Customer();

	$customer = array();
	
	$cutid = trim($_GET['custid']);	
	$customer = $customerObj->getCustomerProfile($cutid);

	DomainManager::getInstance()->load('Company');
	$companyObj = new Company();
	$company = $companyObj->getCompany();	
	$company = $company[0];

	$activationcode = mysql_query("SELECT * FROM customers WHERE customer_id='$cutid'"); 
	$getactivationcode = mysql_fetch_array($activationcode);
	$gotactivationcode = $getactivationcode['activation_key'];
			
	$activation_link = $config['PRMSConfig']->live_site . '/activate.php?atk=' . $gotactivationcode . '&id=' . $cutid;
				
	$customer['activation_link'] = $activation_link;
	$customer['company_name'] = $company['company_name'];

	$custemail = $customer['email'] ;
	$custname = $customer['name'] ;
	$custnric = $customer['nric'] ;
	$fromcoy = $customer['company_name'] ;
	$subject = 'Welcome to '.$company['company_name'].' redemption website';	
	$from = 'no-reply@azora.com.sg';

	$body = "Dear " . $custname . ",\n\n" . "Welcome to " . $fromcoy . " redemption website. To complete the registration process, please click on the link below to activate your account.\n\n" . $activation_link . "\n(If the above link does not work, please copy the full address and paste it to your Internet Browser.)" . "\n\nYour account detail:" . "\n\nLogin ID : " . $custnric . "\n\n\nRegards,\n" . $fromcoy . " Redemption Website";

	mail($custemail, $subject, $body, "From: $from");

	header( "Location: customerrptdetail.php?id=$cutid&fup=3" );

?>