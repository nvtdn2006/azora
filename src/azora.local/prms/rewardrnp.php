<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Reward rule & policies'?>

<?php		
	$config = Factory::getConfig();	

	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('validation');
	JSManager::getInstance()->add('numeric');	
	
	DomainManager::getInstance()->load('Reward');
	$rewardObj = new Reward();
	
	$error = array();
	
	if (isset($_GET['action'])) {		
		switch ($_GET['action']){
			case 'new':							
				$reward = array('pt_in_dollars' => $_POST['pt_in_dollars'],
								'rounding' => $_POST['rounding'],	
								'expiration_required' => $_POST['expiration_required'],	
								'expiration_month' => $_POST['expiration_month'],	
								'terms_n_conditions' => $_POST['terms_n_conditions'],	
								'created_by' => Authentication::getUserId(),								
								'updated_by' => Authentication::getUserId());								
				if ($rewardObj->saveReward($reward)) {					
					//DomainManager::getInstance()->load('Product');
					//$productObj = new Product();	
					//$productObj->updateAllProductPointValue($_POST['pt_in_dollars'], $_POST['rounding']);
					
					//Event Log
					$evtObj = new EventObject();					
					$evtObj->event_id = EventTypes::PRMS_UpdateRulePolicies;
					$evtObj->description = Authentication::getAttribute('login_id') . ' updated reward rule & policies.';
					$evtObj->action_by = Authentication::getAttribute('login_id');					
					EventLog::Log($evtObj);
					
					header( 'Location: rewardrnp.php?rst=1');				
				}
			break;
			case 'update':				
				$reward = array('pt_in_dollars' => $_POST['pt_in_dollars'],
								'rounding' => $_POST['rounding'],	
								'expiration_required' => $_POST['expiration_required'],	
								'expiration_month' => $_POST['expiration_month'],	
								'terms_n_conditions' => $_POST['terms_n_conditions'],																
								'updated_by' => Authentication::getUserId(),
								'id' => $_GET['id']);
				if ($rewardObj->updateReward($reward)) {
					//DomainManager::getInstance()->load('Product');
					//$productObj = new Product();	
					//$productObj->updateAllProductPointValue($_POST['pt_in_dollars'], $_POST['rounding']);
					
					//Event Log
					$evtObj = new EventObject();					
					$evtObj->event_id = EventTypes::PRMS_UpdateRulePolicies;
					$evtObj->description = Authentication::getAttribute('login_id') . ' updated reward rule & policies.';
					$evtObj->action_by = Authentication::getAttribute('login_id');					
					EventLog::Log($evtObj);
					
					header( 'Location: rewardrnp.php?rst=1');									
				}
			break;
		}		
	}
	
	$reward = $rewardObj->getReward();
	
	if ((count($reward) == 0) || isset($_GET['action'])) {		
		$reward['id'] = 0 ;
		
		if (isset($_REQUEST['pt_in_dollars'])) 			
			$reward['pt_in_dollars'] = $_REQUEST['pt_in_dollars'];
		else
			$reward['pt_in_dollars'] =  '';

		if (isset($_REQUEST['rounding'])) 			
			$reward['rounding'] = $_REQUEST['rounding'];
		else
			$reward['rounding'] =  0;
			
		if (isset($_REQUEST['expiration_required'])) 			
			$reward['expiration_required'] = $_REQUEST['expiration_required'];
		else
			$reward['expiration_required'] =  1;
		
		if (isset($_REQUEST['expiration_month	'])) 			
			$reward['expiration_month	'] = $_REQUEST['expiration_month	'];
		else
			$reward['expiration_month	'] =  0;
			
		if (isset($_REQUEST['terms_n_conditions'])) 			
			$reward['terms_n_conditions'] = $_REQUEST['terms_n_conditions'];
		else
			$reward['terms_n_conditions'] =  '';		
	} else {
		$reward = $reward[0];
	}
?>

<?php
	if (isset($error) && count($error) > 0) {
?>
	<div class="error-info form-info">
		<?php foreach ($error as $handle) {
				echo "<p>$handle</p>";
		} ?>
	</div>
<?php
	}
?>

<?php
	if (isset($_GET['rst']) && $_GET['rst'] == 1) {
?>
	<div class="success-info form-info">
		Saving process successfully done.
	</div><br/>
<?php
	}
?>

<div class="warning-info form-info">The change in point conversion value will NOT affect any products in the system.</div>
<form name="rewardform" id="rewardform" action="rewardrnp.php?action=<?php echo $reward['id'] > 0 ? 'update&id='.$reward['id'] : 'new'; ?>" method="post"> 
	<table class="formview" width="100%" border="0" cellspacing="3px" cellpadding="3px">
		<tr>
			<td class="LabelCell Required">1 Pt in Dollars</td>
			<td><input type="text" name="pt_in_dollars" id="pt_in_dollars" class="input Required GreaterThanOrEqualZero numeric" value="<?php echo $reward['pt_in_dollars']; ?>" size="20" tabindex="10" /></td>
		</tr>
		<tr>
			<td class="LabelCell">Point rounding</td>
			<td><input type="radio" name="rounding" value="0" tabindex="20" <?php echo $reward['rounding'] == 0 ? 'checked' : ''; ?>/> Round down 
			<input type="radio" name="rounding" value="1" tabindex="20" <?php echo $reward['rounding'] == 0 ? '' : 'checked'; ?>/> Round up</td>
		</tr>
		<tr>			
			<td class="SectionBar" colspan="2">				
				Points expiration
			</td>
		</tr>
		<tr>
			<td class="LabelCell">Expiration</td>
			<td><input type="radio" name="expiration_required" value="1" tabindex="30" <?php echo $reward['expiration_required'] == 1 ? 'checked' : ''; ?>/> Turn on
			<input type="radio" name="expiration_required" value="0" tabindex="30" <?php echo $reward['expiration_required'] == 1 ? '' : 'checked'; ?>/> Turn off</td>
		</tr>
		<tr>
			<td class="LabelCell">Expiration period in months</td>
			<td>				
				<select tabindex="40" name="expiration_month" class="GreaterThanZero">
					<option value="0" <?php echo ($reward['expiration_month'] == 0) ? 'selected' : ''; ?>>- Select -</option>
					<option value="6" <?php echo ($reward['expiration_month'] == 6) ? 'selected' : ''; ?>>6 months</option>
					<option value="12" <?php echo ($reward['expiration_month'] == 12) ? 'selected' : ''; ?>>12 months</option>
					<option value="18" <?php echo ($reward['expiration_month'] == 18) ? 'selected' : ''; ?>>18 months</option>
					<option value="24" <?php echo ($reward['expiration_month'] == 24) ? 'selected' : ''; ?>>24 months</option>
					<option value="30" <?php echo ($reward['expiration_month'] == 30) ? 'selected' : ''; ?>>30 months</option>
					<option value="36" <?php echo ($reward['expiration_month'] == 36)? 'selected' : ''; ?>>36 months</option>
				</select>
			</td>
		</tr>	
		<tr>			
			<td class="SectionBar" colspan="2">				
				Rewards terms & conditions
			</td>
		</tr>	
		<tr>
			<td colspan="2">
				<div style="width: 635px; height: 500px; overflow: auto;">
					<textarea name="terms_n_conditions" id="terms_n_conditions" class="Required" tabindex="50" style="width: 635px"><?php echo $reward['terms_n_conditions']; ?></textarea>					
				</div>				
			</td>
		</tr>			
		<tr>			
			<td class="BottomToolBar" colspan="2">				
				<input type="submit" name="submit" id="submit" class="button-primary" value="Save" tabindex="100"/>				
			</td>			
		</tr>
	</table>
</form>
<?php
	
?>
<!-- TinyMCE -->
<script type='text/javascript' src='<?php echo $config['PRMSConfig']->live_site; ?>/js/tiny_mce/tiny_mce.js'></script>

<script type="text/javascript">

	tinyMCE.init({
		// General options
		mode : "exact",
		elements : "terms_n_conditions",
		theme : "advanced",
		plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave",

		// Theme options
		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr",
		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
		theme_advanced_buttons5 : ",|,link,unlink,anchor,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor,|,print,|,ltr,rtl,|,fullscreen",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : false,
		editor_content_width : 500,
		width: "635",
		height: "480",

	});
	
	$(document).ready(function() { 
		loadValidation('rewardform');				
		$(".numeric").numeric();		
	});	
	
</script>
<!-- /TinyMCE -->