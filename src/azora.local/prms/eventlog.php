<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Event log'?>

<?php	

	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('jquery.ui');			
	CSSManager::getInstance()->add('/js/jqueryui/smoothness/jquery-ui-1.8.16.custom.css');	

	$criteria = array();
	
	$eventlogs = null;
		
	if (isset($_GET['action']) && $_GET['action'] == 'search') {
		
		Loader::load('DateTimeTool');		
		
		if (isset($_POST['eventfrom']) && $_POST['eventfrom'] != '') {
			$criteria['eventfrom'] = DateTimeTool::convertStringToDate($_POST['eventfrom']);
		}
		
		if (isset($_POST['eventto']) && $_POST['eventto'] != '') {
			$criteria['eventto'] = DateTimeTool::convertStringToDate($_POST['eventto']);
		}		
		
		if (isset($_POST['type']) && $_POST['type'] > 0) {
			$criteria['event_id'] = $_POST['type'];
		}

		$eventlogs = EventLog::GetEventLogs($criteria);
		
	}
	
	$formvalues = array();
		
	if (isset($_REQUEST['eventfrom'])) 			
		$formvalues['eventfrom'] = $_REQUEST['eventfrom'];
	else
		$formvalues['eventfrom'] =  '';
	
	if (isset($_REQUEST['eventto'])) 			
		$formvalues['eventto'] = $_REQUEST['eventto'];
	else
		$formvalues['eventto'] =  '';
		
	if (isset($_REQUEST['type'])) 			
		$formvalues['type'] = $_REQUEST['type'];
	else
		$formvalues['type'] =  0;
		
?>

<form name="eventlogform" id="eventlogform" action="eventlog.php?action=search" method="post"> 

	<table class="formview" width="100%" border="0" cellspacing="3px" cellpadding="3px">
		<tr>
			<td class="LabelCell">Event Type</td>
			<td colspan="3">
				<select tabindex="10" name="type">						
					<?php
						$events = EventLog::GetEventTypes();						
						echo '<option value="0">- All -</option>';
							
						foreach ($events as $event) {
							echo '<option value='.$event['id'].' '.($event['id'] == $formvalues['type'] ? 'selected' : '') .'>'.$event['event_name'].'</option>';
						}
					?>
				</select>
			</td>
		</tr>
		<tr>			
			<td>From</td>		
			<td><input type="text" name="eventfrom" id="eventfrom" class="input" value="<?php echo $formvalues['eventfrom']; ?>" size="20" tabindex="20" /></td>	
			<td>To</td>		
			<td><input type="text" name="eventto" id="eventto" class="input" value="<?php echo $formvalues['eventto']; ?>" size="20" tabindex="30" /></td>						
		</tr>			
		<tr>			
			<td colspan="4">				
				<input type="submit" name="search" id="search" class="button-primary" value="Show" tabindex="40"/>				
			</td>			
		</tr>
	</table>
</form>

<div class="SectionTitle" ><b><?php echo ($eventlogs != null && count($eventlogs) > 0) ? count($eventlogs) : '0'; ?> </b> Record(s) found.</div>

<table class="tabular">
	<thead>
		<tr>			
			<th style="width: 150px;">Event</th>
			<th>Action By</th>
			<th>Description</th>
			<th>Date</th>			
		</tr>
	</thead>
	<tbody>
		<?php
			if ($eventlogs != null) {
			foreach($eventlogs as $event) {
		?>
		<tr>						
			<td><?php echo $event['event_name'] ?></td>
			<td><?php echo $event['action_by'] ?></td>
			<td><?php echo $event['description'] ?></td>
			<td><?php echo $event['action_on'] ?></td>			
		</tr>
		<?php } } ?>
	</tbody>
</table>

<script type="text/javascript">
	$(document).ready(function() { 
		$( "#eventfrom" ).datepicker({ dateFormat: 'dd/mm/yy'});	
		$( "#eventto" ).datepicker({ dateFormat: 'dd/mm/yy'});			
	});	
</script>