<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'New image'?>

<?php	

	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('validation');
	JSManager::getInstance()->add('fancybox');	
	CSSManager::getInstance()->add('/js/fancybox/jquery.fancybox-1.3.4.css');
	JSManager::getInstance()->add('numeric');	

	require_once './includes/mod_slider.php';
	$sldObj = new Mod_Slider();
	
	$image_id = 0;
	$image = null;
	
	$error = array();
	
	if (isset($_GET['image'])) {		
		$image_id = $_GET['image'];
		$image = $sldObj->getImage($image_id);		
		$this->title = 'Update image';
	}
	
	if (isset($_GET['action'])) {		
		$isValid = true;
		
		switch ($_GET['action']){
			case 'new':	
				
				if (isset($_FILES)) {										
					foreach ($_FILES as $key => $file) {				
						$filecheck = basename($file['name']);
						if ($filecheck != '') {
							$ext = strtolower(substr($filecheck, strrpos($filecheck, '.') + 1));

							if (!(($ext == "jpg" || $ext == "gif" || $ext == "png") && ($file["type"] == "image/jpeg" || $file["type"] == "image/gif" || $file["type"] == "image/png") && 
								($file["size"] < 2120000))) {
								$isValid = false;
								array_push($error, $imagetypeName[$key] . ' image : jpg, gif, png file types are only allowed to upload. File size must be less than 2MB.');	
							}
						}
					}
				}
				
				if ($isValid) {				
					
					$image = array('description' => $_POST['description'],
									'image_order' => $_POST['order'],																
									'image_status' => $_POST['image_status'],
									'created_by' => Authentication::getUserId(),
									'updated_by' => Authentication::getUserId(),									
									'image_name' => '',
									'sys_file_name' => '');
					
					if (isset($_FILES['image_thumbnailimage'])) {					
						if ($_FILES['image_thumbnailimage']['name'] != '') {							
							$image['image_name'] = $_FILES['image_thumbnailimage']['name'];											
							$sys_file_name = date('YmdHisu') . '_' . $_FILES['image_thumbnailimage']['name'];							
							move_uploaded_file($_FILES['image_thumbnailimage']['tmp_name'], '../modules/mod_slider/images/'.$sys_file_name);
							$image['sys_file_name'] = $sys_file_name;							
						}
					}
					
					if ($sldObj->saveImage($image)) {
					
						//Event Log
						$evtObj = new EventObject();					
						$evtObj->event_id = EventTypes::PRMS_AddSliderImage;
						$evtObj->description = Authentication::getAttribute('login_id') . ' added new slider image ('.$image['image_name'].').';
						$evtObj->action_by = Authentication::getAttribute('login_id');					
						EventLog::Log($evtObj);
					
						header( 'Location: mod_slider_images.php');							
					}
					else 
						array_push($error, 'Saving process failed. Please try again.');
				}
				
			break;
			case 'update':

				if (isset($_FILES)) {										
					foreach ($_FILES as $key => $file) {				
						$filecheck = basename($file['name']);
						if ($filecheck != '') {
							$ext = strtolower(substr($filecheck, strrpos($filecheck, '.') + 1));

							if (!(($ext == "jpg" || $ext == "gif" || $ext == "png") && ($file["type"] == "image/jpeg" || $file["type"] == "image/gif" || $file["type"] == "image/png") && 
								($file["size"] < 2120000))) {
								$isValid = false;
								array_push($error, $imagetypeName[$key] . ' image : jpg, gif, png file types are only allowed to upload. File size must be less than 2MB.');	
							}
						}
					}
				}
					
				if ($isValid) {
					$image = array('description' => $_POST['description'],
									'image_order' => $_POST['order'],																
									'image_status' => $_POST['image_status'],
									'updated_by' => Authentication::getUserId(),
									'id' => $image_id,
									'image_name' => '',
									'sys_file_name' => '');
					
					if (isset($_FILES['image_thumbnailimage'])) {					
						if ($_FILES['image_thumbnailimage']['name'] != '') {							
							$image['image_name'] = $_FILES['image_thumbnailimage']['name'];											
							$sys_file_name = date('YmdHisu') . '_' . $_FILES['image_thumbnailimage']['name'];							
							move_uploaded_file($_FILES['image_thumbnailimage']['tmp_name'], '../modules/mod_slider/images/'.$sys_file_name);
							$image['sys_file_name'] = $sys_file_name;							
						}
					}
					
					if ($sldObj->updateImage($image)) {
					
						//Event Log
						$evtObj = new EventObject();					
						$evtObj->event_id = EventTypes::PRMS_UpdateSliderImage;
						$evtObj->description = Authentication::getAttribute('login_id') . ' updated slider image ('.$image['image_name'].').';
						$evtObj->action_by = Authentication::getAttribute('login_id');					
						EventLog::Log($evtObj);
						
						header( 'Location: mod_slider_images.php');	
					}
					else 
						array_push($error, 'Saving process failed. Please try again.');
				}
			break;
		}		
	}
	
	if (!isset($image) || isset($_GET['action'])) {		
		if (isset($_REQUEST['description'])) 			
			$image['description'] = $_REQUEST['description'];
		else
			$image['description'] =  '';
			
		if (isset($_REQUEST['order'])) 			
			$image['image_order'] = $_REQUEST['order'];
		else
			$image['image_order'] =  '';
			
		if (isset($_REQUEST['image_status'])) 			
			$image['image_status'] = $_REQUEST['image_status'];
		else
			$image['image_status'] =  1;	
		
		$image['image_name'] = '';
		$image['sys_file_name'] = '';
	}
	
?>

<?php
	if (isset($error) && count($error) > 0) {
?>
	<div class="error-info form-info">
		<?php foreach ($error as $handle) {
				echo "<p>$handle</p>";
		} ?>
	</div>
<?php
	}
?>

<form name="imageform" id="imageform" action="mod_slider_images-edit.php?action=<?php echo $image_id > 0 ? 'update&image='.$image_id : 'new'; ?>" method="post" enctype="multipart/form-data"> 
	<table class="formview" width="100%" border="0" cellspacing="3px" cellpadding="0">
		<tr>
			<td class="LabelCell Required">Description</td>
			<td><input type="text" name="description" id="description" maxlength="500" class="input Required" value="<?php echo isset($image) ? $image['description'] : '' ?>" size="20" tabindex="10" /></td>
		</tr>
		<tr>
			<td class="LabelCell Required">Order</td>
			<td><input type="text" name="order" id="order" maxlength="50" class="input Required GreaterThanOrEqualZero numeric" value="<?php echo isset($image) ? $image['image_order'] : '' ?>" size="20" tabindex="20" /></td>
		</tr>
		<tr>
			<td class="LabelCell">Status</td>
			<td><input type="radio" name="image_status" value="1" tabindex="30" <?php echo ( (isset($image) && $image['image_status'] == true)  ? 'checked' : (isset($image) ? '' : 'checked')); ?>/> Active 
			<input type="radio" name="image_status" value="0" tabindex="30" <?php echo ( (isset($image) && $image['image_status'] == false)  ? 'checked' : ''); ?>/> In-active</td>
		</tr>	
		
		<tr>
			<td class="LabelCell Required">Image</td>
			<td>
				<?php
					$display = '';
					if (isset($image) && $image['image_name'] != '') {
						echo '<div><a class="fancybox" href="../modules/mod_slider/images/'.$image['sys_file_name'].'">'.$image['image_name'].'</a>
						&nbsp;[&nbsp;<a href="#div_image_thumbnailimage" class="imagechange">Change</a>&nbsp;&#8226;&nbsp;<a href="#div_image_thumbnailimage" class="imageremove">Remove</a>]<br/><br/></div>';
						$display = 'style="display:none;"';					
					}
				?>
				<div id="div_image_thumbnailimage" <?php echo $display; ?>>
					<span id="image_thumbnailimage_file">
						<input id="image_thumbnailimage" name="image_thumbnailimage" type="file" tabindex="90" class="Required" <?php echo $display; ?> />
					</span> [&nbsp;<a href="#image_thumbnailimage_file" class="imageclear" >Clear</a>&nbsp;]
				</div>
			</td>
		</tr>
		
		<tr>
			<td class="BottomToolBar" colspan="2">	
				<input type="submit" name="submit" id="submit" class="button-primary" value="Save" tabindex="50"/>
				<a href="mod_slider_images.php" class="button-secondary" tabindex="60">Cancel</a>
			</td>			
		</tr>
	</table>
	<input id="fileRemoved" name="fileRemoved" type="hidden" value=""/>
</form>
<?php
	$roleObj = null;
?>
<script type="text/javascript">
	$(document).ready(function() { 
		loadValidation('imageform');
		$('a.fancybox').fancybox();
		
		$('a.imagechange').click(function (event){
			event.preventDefault();
			e = $(this).attr('href');		
			fi = '#' + e.substring(5);
			if ($(this).html() == 'Change') {
				$(this).html('Cancel');				
				$(e).css('display','');
				$(fi).css('display','');
			} else {
				$(this).html('Change');
				$(e).css('display','none');
				$(fi).css('display','none');
			}			
		});
		
		$('a.imageremove').click(function (event){
			event.preventDefault();
			e = $(this).attr('href');		
			fi = '#' + e.substring(5);
						
			$(e).css('display','');
			$(fi).css('display','');
			
			$(this).parent().remove();
			
			$('#fileRemoved').val($('#fileRemoved').val() + e.substring(5) + ',');			
		});
		
		$(".imageclear").click(function(event){
			event.preventDefault();
			e = $(this).attr('href');
			html = $(e).html();
			$(e).html(html);			
		});
		
		$(".numeric").numeric(false);		
	});	
</script>