<?php
class Feature
{
	function getAllFeatures() {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT f.* FROM features f LEFT JOIN feature_groups fg ON f.feature_group_id = fg.feature_group_id
								ORDER BY fg.feature_group_order, f.feature_order');			
		
		$sth->execute();
		$result = $sth->fetchAll(PDO::FETCH_ASSOC);
		$sth = null;
		$dbh = null;	
		return $result;
	}
	
	function getRoleFeaturesNotSelected($role_id) {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT f.* FROM features f LEFT JOIN feature_groups fg ON f.feature_group_id = fg.feature_group_id
								WHERE f.feature_id NOT IN (SELECT feature_id FROM role_permissions rp WHERE rp.role_id = :role_id )
								ORDER BY fg.feature_group_order, f.feature_order');		
								
		$sth->bindParam(':role_id', $role_id);		
		
		$sth->execute();
		$result = $sth->fetchAll(PDO::FETCH_ASSOC);
		$sth = null;
		$dbh = null;	
		return $result;
	}

	function getRoleFeatures($role_id) {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT rp.*, f.feature_group_id, f.feature_name, f.feature_description , f.feature_order FROM role_permissions rp LEFT JOIN features f ON rp.feature_id = f.feature_id
								WHERE rp.role_id = :role_id ORDER BY f.feature_group_id, f.feature_order');			
		
		$sth->bindParam(':role_id', $role_id);
		$sth->execute();
		$result = $sth->fetchAll(PDO::FETCH_ASSOC);
		$sth = null;
		$dbh = null;	
		return $result;
	}		
	
}
?>