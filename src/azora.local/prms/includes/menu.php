<?php
class Menu
{
	function getMenuItems($user_id, $isSuperUser) {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
		
		if ($isSuperUser) {
			$sth = $dbh->prepare('SELECT distinct mg.menu_group_id, mg.menu_group_name, mg.menu_group_description, mg.menu_group_order, m.menu_id, m.feature_id, m.menu_name, 
									m.menu_description, m.menu_url, m.menu_order FROM menus m LEFT JOIN menu_groups mg ON m.menu_group_id = mg.menu_group_id ORDER BY 
									mg.menu_group_order, m.menu_order');			
		} else {
			$sth = $dbh->prepare('SELECT distinct mg.menu_group_id, mg.menu_group_name, mg.menu_group_description, mg.menu_group_order, m.menu_id, m.feature_id, m.menu_name, 
									m.menu_description, m.menu_url, m.menu_order FROM user_roles ur LEFT JOIN role_permissions rp ON ur.role_id = rp.role_id LEFT JOIN menus m ON 
									rp.feature_id = m.feature_id LEFT JOIN menu_groups mg ON m.menu_group_id = mg.menu_group_id WHERE ur.user_id = :user_id ORDER BY 
									mg.menu_group_order, m.menu_order');
			$sth->bindParam(':user_id', $user_id);
		}
		$sth->execute();
		$result = $sth->fetchAll(PDO::FETCH_ASSOC);
		$sth = null;
		$dbh = null;
		return $result;
	}
}
?>