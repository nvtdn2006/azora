<?php
class Mod_Slider
{
	function getAllImages() {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT s.*, cu.username as created_username, mu.username as updated_username FROM mod_slider s
								LEFT JOIN users cu ON s.created_by = cu.user_id
								LEFT JOIN users mu ON s.updated_by = mu.user_id');			
		
		$sth->execute();
		$result = $sth->fetchAll(PDO::FETCH_ASSOC);
		$sth = null;
		$dbh = null;	
		return $result;
	}
	
	function getImage($image_id) {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT s.*, cu.username as created_username, mu.username as updated_username FROM mod_slider s
								LEFT JOIN users cu ON s.created_by = cu.user_id
								LEFT JOIN users mu ON s.updated_by = mu.user_id
								WHERE s.id = :image_id');						
		$sth->bindParam(':image_id', $image_id);
		$sth->execute();
		$result = $sth->fetch(PDO::FETCH_ASSOC);
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function updateImage($image) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		try {
			
			$filesTBD = array();
			
			$dbh->beginTransaction();

			$sth = $dbh->prepare('UPDATE mod_slider SET description = :description, image_order = :image_order, image_status = :image_status,
									updated_dt = NOW(), updated_by = :updated_by WHERE id = :id');			
		
			$sth->bindParam(':description', $image['description']);
			$sth->bindParam(':image_order', $image['image_order']);		
			$sth->bindParam(':image_status', $image['image_status']);
			$sth->bindParam(':updated_by', $image['updated_by']);
			$sth->bindParam(':id', $image['id']);
			$sth->execute();
			
			if (isset($image['image_name']) && $image['image_name'] != '') {
				
				$sth = $dbh->prepare('SELECT * FROM mod_slider WHERE  
															id = :id');		
				$sth->bindParam(':id', $image['id']);											
				$sth->execute();
				
				if ($sth->rowCount() > 0) {
					$imgTBD = $sth->fetch(PDO::FETCH_ASSOC);					
					array_push($filesTBD, dirname(__FILE__).'/../../modules/mod_slider/images/'.$imgTBD['sys_file_name']);
				}
				
				if (isset($image['sys_file_name']) && $image['sys_file_name'] != '') {
					$sth = $dbh->prepare('UPDATE mod_slider SET image_name = :image_name, sys_file_name = :sys_file_name WHERE id = :id');		

					$sth->bindParam(':image_name', $image['image_name']);					
					$sth->bindParam(':sys_file_name', $image['sys_file_name']);					
					$sth->bindParam(':id', $image['id']);
					$sth->execute();
				}
			}			
				
			$dbh->commit();
			
			foreach($filesTBD as $fileTBD)
				unlink($fileTBD);
						
			$result = true;
			
		} catch(PDOException $e) {
			$dbh->rollback();
			throw new Exception($e->getMessage());
		}

		$sth = null;
		$dbh = null;
		return $result;
	}
	
	
	function saveImage($image) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		try {
		
			$image_id = 0;
		
			$dbh->beginTransaction();
			
			$sth = $dbh->prepare('INSERT INTO mod_slider (image_name, sys_file_name, description, image_order, image_status, in_trash, created_dt, created_by, updated_dt, updated_by) VALUES 
														(:image_name, :sys_file_name, :description, :image_order, :image_status, 0, NOW(), :created_by, NOW(), :updated_by)');		

			$sth->bindParam(':image_name', $image['image_name']);					
			$sth->bindParam(':sys_file_name', $image['sys_file_name']);					
			$sth->bindParam(':description', $image['description']);			
			$sth->bindParam(':image_order', $image['image_order']);			
			$sth->bindParam(':image_status', $image['image_status']);			
			$sth->bindParam(':created_by', $image['created_by']);			
			$sth->bindParam(':updated_by', $image['updated_by']);			
			$sth->execute();
			$image_id = $dbh->lastInsertId('id');
			
			$dbh->commit();
			
			$result = true;
			
		} catch(PDOException $e) {
			$dbh->rollback();
			throw new Exception($e->getMessage());
		}
		
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function deleteImage($image) {				
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
		
		$image_ids = implode(',', $image);	
		
		try {
			
			$filesTBD = array();
			
			$dbh->beginTransaction();
			
			$sth = $dbh->prepare('SELECT * FROM mod_slider WHERE id IN (' . $image_ids . ')');			
			$sth->execute();
			$images = $sth->fetchAll(PDO::FETCH_ASSOC);
			
			foreach($images as $image) {				
				array_push($filesTBD, dirname(__FILE__).'/../../modules/mod_slider/images/'.$image['sys_file_name']);
			}
			
			
			$sth = $dbh->prepare('DELETE FROM mod_slider WHERE id IN (' . $image_ids . ')');				
			$sth->execute();
			
			
			$dbh->commit();
			
			foreach($filesTBD as $fileTBD)
				unlink($fileTBD);
				
			$result = true;
			
		} catch(PDOException $e) {
			$dbh->rollback();			
			throw new Exception($e->getMessage());
		}
				
		$sth = null;
		$dbh = null;
		return $result;
	}
}
?>