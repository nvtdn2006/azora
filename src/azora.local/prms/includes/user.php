<?php
class User
{
	function isValidUser($username, $password, &$user) {
		$isValid = false;
		Loader::Load('Database');
		$cryptographer = Factory::getCryptographer();
		$salt = $this->getUserSalt($username);
		$saltedpassword = $cryptographer->MD5($password . $salt);
		$dbh = Database::getInstance()->getConnection();
		
		$sth = $dbh->prepare('SELECT * FROM users WHERE username = :username AND password = :password AND user_status = 1');
		$sth->bindParam(':username', $username);
		$sth->bindParam(':password', $saltedpassword);
		$sth->execute();	
		while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
			$user = $row;	
		}
		$isValid = ($user != null);
		$sth = null;
		$dbh = null;	
		return $isValid;
	}

	function getUserSalt($username) {
		$salt = null;	
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
		
		$sth = $dbh->prepare('SELECT salt FROM users WHERE username = :username');
		$sth->bindParam(':username', $username);
		$sth->execute();	
		while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
			$salt = $row['salt'];	
		}
		$sth = null;
		$dbh = null;
		return $salt;
	}
	
	function getUserProfile($user_id) {	
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
		
		$sth = $dbh->prepare('SELECT * FROM user_profiles WHERE user_id = :user_id');
		$sth->bindParam(':user_id', $user_id);
		$sth->execute();	
		while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
			$result = $row;			
		}
		$sth = null;
		$dbh = null;
		
		if (isset($result)) {return $result;}
	}
	
	function getUserProfileByEmail($email) {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT * FROM user_profiles
								WHERE email = :email');		

		$sth->bindParam(':email', $email);
		$sth->execute();
		$result = $sth->fetch(PDO::FETCH_ASSOC);
		
		$sth = null;
		$dbh = null;
		return $result;
	}

	function isSuperUser($user_id) {	
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
		$return = false;
		$sth = $dbh->prepare('SELECT user_id FROM user_roles ur LEFT JOIN roles r ON ur.role_id = r.role_id WHERE r.role_is_su = 1 AND r.role_is_sys = 1 AND ur.user_id = :user_id');
		$sth->bindParam(':user_id', $user_id);
		$sth->execute();	
		if ($sth->rowCount() > 0)
			$return	= true;
		$sth = null;
		$dbh = null;	
		return $return;
	}

	function getAllUsers() {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT u.*, up.full_name, up.email 
								, cu.username as user_created_username, mu.username as user_updated_username FROM users u
								LEFT JOIN user_profiles up ON u.user_id = up.user_id
								LEFT JOIN users cu ON u.created_by = cu.user_id
								LEFT JOIN users mu ON u.updated_by = mu.user_id');			
		
		$sth->execute();
		$result = $sth->fetchAll(PDO::FETCH_ASSOC);	
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	
	function isExistUsername($username, $user_id = 0) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT 1 FROM users u 								
								WHERE u.username = :username AND u.user_id <> :user_id');		

		$sth->bindParam(':username', $username);	
		$sth->bindParam(':user_id', $user_id);
		$sth->execute();
		$result = ($sth->rowCount() > 0);
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function isExistEmail($email, $user_id = 0) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT 1 FROM user_profiles up 								
								WHERE up.email = :email AND up.user_id <> :user_id');		
		
		$sth->bindParam(':email', $email);
		$sth->bindParam(':user_id', $user_id);
		$sth->execute();
		$result = ($sth->rowCount() > 0);
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function isExistUser($username, $email, $user_id = 0) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT 1 FROM users u LEFT JOIN user_profiles up ON u.user_id = up.user_id
								WHERE u.username = :username AND up.email = :email AND u.user_id <> :user_id');		

		$sth->bindParam(':username', $username);
		$sth->bindParam(':email', $email);
		$sth->bindParam(':user_id', $user_id);
		$sth->execute();
		$result = ($sth->rowCount() > 0);
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function saveUser($user) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		try {
		
			$user_id = 0;			
			
			$cryptographer = Factory::getCryptographer();
			$salt = $cryptographer->GenerateSalt();
			
			$salted_password = $cryptographer->MD5($user['password']. $salt);			
			
			$dbh->beginTransaction();
			
			$sth = $dbh->prepare('INSERT INTO users (username, password, salt, user_status, created_dt, created_by, updated_dt, updated_by) VALUES 
									(:username, :password, :salt, :user_status, NOW(), :created_by, NOW(), :updated_by)');		

			$sth->bindParam(':username', $user['username']);
			$sth->bindParam(':password', $salted_password);
			$sth->bindParam(':salt', $salt);
			$sth->bindParam(':user_status', $user['user_status']);
			$sth->bindParam(':created_by', $user['created_by']);
			$sth->bindParam(':updated_by', $user['updated_by']);
			$sth->execute();
			$user_id = $dbh->lastInsertId('user_id');
			
			if ($user_id > 0) {				
				$sth = $dbh->prepare('INSERT INTO user_profiles (user_id, full_name, email, created_dt, created_by, updated_dt, updated_by) VALUES 
															(:user_id, :full_name, :email, NOW(), :created_by, NOW(), :updated_by)');		

				$sth->bindParam(':user_id', $user_id);			
				$sth->bindParam(':full_name', $user['full_name']);		
				$sth->bindParam(':email', $user['email']);		
				$sth->bindParam(':created_by', $user['created_by']);
				$sth->bindParam(':updated_by', $user['updated_by']);
				$sth->execute();	
				
				foreach($user['roles'] as $role) {
					$sth = $dbh->prepare('INSERT INTO user_roles (user_id, role_id) VALUES (:user_id, :role_id)');			
					$sth->bindParam(':user_id', $user_id);
					$sth->bindParam(':role_id', $role);
					$sth->execute();
				}
				
				if ($user['stores'] != null) {
					foreach($user['stores'] as $store) {
						$sth = $dbh->prepare('INSERT INTO user_stores (user_id, store_id) VALUES (:user_id, :store_id)');			
						$sth->bindParam(':user_id', $user_id);
						$sth->bindParam(':store_id', $store);
						$sth->execute();
					}
				}
			}		
			
			$dbh->commit();
			$result = true;
			
		} catch(PDOException $e) {
			$dbh->rollback();			
			throw new Exception($e->getMessage());
		}
		
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function updateUser($user) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		try {
			
			$dbh->beginTransaction();
			
			
			$sth = $dbh->prepare('UPDATE user_profiles SET full_name = :full_name, email = :email, updated_dt = NOW(), updated_by = :updated_by 
									WHERE user_id = :user_id');		
			
			$sth->bindParam(':full_name', $user['full_name']);		
			$sth->bindParam(':email', $user['email']);					
			$sth->bindParam(':updated_by', $user['updated_by']);
			$sth->bindParam(':user_id',  $user['user_id']);			
			$sth->execute();
			
			$sth = $dbh->prepare('UPDATE users SET user_status = :user_status, updated_dt = NOW(), updated_by = :updated_by 
									WHERE user_id = :user_id');		

			$sth->bindParam(':user_status', $user['user_status']);
			$sth->bindParam(':updated_by', $user['updated_by']);
			$sth->bindParam(':user_id',  $user['user_id']);			
			$sth->execute();

			$sth = $dbh->prepare('DELETE FROM user_roles WHERE user_id = :user_id');
			$sth->bindParam(':user_id', $user['user_id']);			
			$sth->execute();			
			
			foreach($user['roles'] as $role) {
				$sth = $dbh->prepare('INSERT INTO user_roles (user_id, role_id) VALUES (:user_id, :role_id)');			
				$sth->bindParam(':user_id', $user['user_id']);
				$sth->bindParam(':role_id', $role);
				$sth->execute();
			}
			
			$sth = $dbh->prepare('DELETE FROM user_stores WHERE user_id = :user_id');
			$sth->bindParam(':user_id', $user['user_id']);			
			$sth->execute();			
			
			if ($user['stores'] != null) {
				foreach($user['stores'] as $store) {
					$sth = $dbh->prepare('INSERT INTO user_stores (user_id, store_id) VALUES (:user_id, :store_id)');			
					$sth->bindParam(':user_id', $user['user_id']);
					$sth->bindParam(':store_id', $store);
					$sth->execute();
				}
			}
			
			$dbh->commit();
			$result = true;
			
		} catch(PDOException $e) {
			$dbh->rollback();			
			throw new Exception($e->getMessage());
		}
		
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	
	function getUser($user_id) {
		$result = array();
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT u.*, cu.username as created_username, mu.username as updated_username FROM users u 
								LEFT JOIN users cu ON u.created_by = cu.user_id
								LEFT JOIN users mu ON u.updated_by = mu.user_id
								WHERE u.user_id = :user_id');		

		$sth->bindParam(':user_id', $user_id);
		$sth->execute();
		$result['user'] = $sth->fetch(PDO::FETCH_ASSOC);
		
		
		$sth = $dbh->prepare('SELECT u.*, cu.username as created_username, mu.username as updated_username FROM user_profiles u 
								LEFT JOIN users cu ON u.created_by = cu.user_id
								LEFT JOIN users mu ON u.updated_by = mu.user_id
								WHERE u.user_id = :user_id');		

		$sth->bindParam(':user_id', $user_id);
		$sth->execute();
		$result['user_profiles'] = $sth->fetch(PDO::FETCH_ASSOC);
		
		
		$sth = $dbh->prepare('SELECT ur.*, r.role_name FROM user_roles ur LEFT JOIN roles r ON ur.role_id = r.role_id
								WHERE ur.user_id = :user_id ORDER BY r.role_name');			
		
		$sth->bindParam(':user_id', $user_id);
		$sth->execute();				
		$result['user_roles'] = $sth->fetchAll(PDO::FETCH_ASSOC);
				
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function deleteUser($users) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
		
		$user_ids = implode(',', $users);	
		
		try {
			
			$dbh->beginTransaction();
			
			$sth = $dbh->prepare('DELETE FROM user_roles WHERE user_id IN (' . $user_ids . ')');				
			$sth->execute();
			
			$sth = $dbh->prepare('DELETE FROM user_profiles WHERE user_id IN (' . $user_ids . ')');				
			$sth->execute();
			
			$sth = $dbh->prepare('DELETE FROM users WHERE user_id IN (' . $user_ids . ')');				
			$sth->execute();
			
			$dbh->commit();
			$result = true;
			
		} catch(PDOException $e) {
			$dbh->rollback();			
			throw new Exception($e->getMessage());
		}
				
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	
	function resetPassword($newpassword) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		try {
		
			$user_id = $newpassword['user_id'];			
			
			$cryptographer = Factory::getCryptographer();
			$salt = $cryptographer->GenerateSalt();
			
			$salted_password = $cryptographer->MD5($newpassword['password']. $salt);			
			
			$dbh->beginTransaction();
			
			$sth = $dbh->prepare('UPDATE users SET password = :password, salt = :salt, updated_dt = NOW(), updated_by = :updated_by  
									WHERE user_id = :user_id');		

			$sth->bindParam(':user_id', $user_id);
			$sth->bindParam(':password', $salted_password);
			$sth->bindParam(':salt', $salt);			
			$sth->bindParam(':updated_by', $newpassword['updated_by']);
			$sth->execute();
			
			$dbh->commit();
			$result = true;
			
		} catch(PDOException $e) {
			$dbh->rollback();			
			throw new Exception($e->getMessage());
		}
		
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function updateProfile($user) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		try {
			
			$dbh->beginTransaction();
			
			
			$sth = $dbh->prepare('UPDATE user_profiles SET full_name = :full_name, email = :email, updated_dt = NOW(), updated_by = :updated_by 
									WHERE user_id = :user_id');		
			
			$sth->bindParam(':full_name', $user['full_name']);		
			$sth->bindParam(':email', $user['email']);					
			$sth->bindParam(':updated_by', $user['updated_by']);
			$sth->bindParam(':user_id',  $user['user_id']);			
			$sth->execute();
			
			$dbh->commit();
			$result = true;
			
		} catch(PDOException $e) {
			$dbh->rollback();			
			throw new Exception($e->getMessage());
		}
		
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function isAuthorizedUser($user_id, $password) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
		
		$user = $this->getUser($user_id);		
		
		if (isset($user)) {
			
			$cryptographer = Factory::getCryptographer();
			$salted_password = $cryptographer->MD5($password . $user['user']['salt']);
			
			$sth = $dbh->prepare('SELECT 1 FROM users  								
								WHERE user_id = :user_id AND password = :password');		

			$sth->bindParam(':user_id', $user_id);
			$sth->bindParam(':password', $salted_password);
			$sth->execute();			
			
			$result = ($sth->rowCount() > 0);
		}
		
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function forgotPassword($email, &$forgotInfo) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		try {
		
			$user = $this->getUserProfileByEmail($email);
			
			if (isset($user) && count($user) > 0) {
			
				$cryptographer = Factory::getCryptographer();
				$salt = $cryptographer->GenerateSalt();
				$newpassword = $cryptographer->GenerateAlphanumeric(8);
				$salted_password = $cryptographer->MD5($newpassword . $salt);			
						
				$dbh->beginTransaction();
				
				$sth = $dbh->prepare('UPDATE users SET password = :password, salt = :salt WHERE user_id = :user_id');		
				
				$sth->bindParam(':password', $salted_password);
				$sth->bindParam(':salt', $salt);
				$sth->bindParam(':user_id', $user['user_id']);
				$sth->execute();			
				
				$dbh->commit();	
				$userObj = $this->getUser($user['user_id']);
				
				$user['username'] = $userObj['user']['username'];	
				$user['newpassword'] = $newpassword;

				$forgotInfo = $user;
				$result = true;
			
			}
			
		} catch(PDOException $e) {
			$dbh->rollback();			
			throw new Exception($e->getMessage());
		}
		
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	
	function getUserForRedemptionReminder($store_id) {	
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT up.* FROM users u LEFT JOIN user_profiles up ON u.user_id = up.user_id 
				LEFT JOIN user_roles r ON u.user_id = r.user_id LEFT JOIN role_permissions rp ON r.role_id = rp.role_id LEFT JOIN user_stores s ON u.user_id = s.user_id
				WHERE u.user_status = 1
				AND rp.feature_id = 11 AND s.store_id = ' . $store_id);			
		
		$sth->execute();
		$result = $sth->fetchAll(PDO::FETCH_ASSOC);
		$sth = null;
		$dbh = null;
		return $result;		
	}
	
	function getUserForBillingReminder() {	
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT up.* FROM users u LEFT JOIN user_profiles up ON u.user_id = up.user_id 
				LEFT JOIN user_roles r ON u.user_id = r.user_id LEFT JOIN role_permissions rp ON r.role_id = rp.role_id 
				WHERE u.user_status = 1
				AND rp.feature_id = 22
				UNION ALL 
				SELECT up.* FROM users u LEFT JOIN user_profiles up ON u.user_id = up.user_id 
				LEFT JOIN user_roles ur ON u.user_id = ur.user_id LEFT JOIN roles r ON ur.role_id = r.role_id
				WHERE r.role_is_sys = 1 AND r.role_is_su = 1');			
		
		$sth->execute();
		$result = $sth->fetchAll(PDO::FETCH_ASSOC);
		$sth = null;
		$dbh = null;
		return $result;		
	}
}
?>