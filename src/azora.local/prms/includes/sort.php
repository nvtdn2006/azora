<?php

/*
 * Sorting Call Back
 */

// For PTTRAN.php sorting by transaction date
function cmp_by_transactiondate($a, $b) {
    return - (strtotime($a["transacted_date"]) - strtotime($b["transacted_date"]));
}

?>
