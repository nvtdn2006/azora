<?php
require_once dirname(__FILE__) . '/../../libraries/application.php';

class PRMS extends Application
{	
	public $template;
	public $title;
	public $feature_id;
	private $page_content;
	
	public $Auth_Req = true;

	function __construct() {
		parent::__construct();	
		$this->LoadDomains();	
		$this->LoadMailer();
		$this->LoadEventLog();
	}

	public function run() {		
		$this->getFeatureId();		
		$this->getPageContent();		
		$this->checkAuthentication();
		$this->setTheme();
	}
	
	private function LoadDomains() {
		if (!class_exists('DomainManager')) {
			require_once dirname(__FILE__) . '/../../domain/domainmanager.php';			
		}	
	}
	
	private function LoadMailer() {
		if (!class_exists('SiteMailer')) {
			require_once dirname(__FILE__) . '/../../includes/sitemailer.php';
		}
	}
	
	private function LoadEventLog() {
		if (!class_exists('EventLog')) {
			require_once dirname(__FILE__) . '/../../includes/eventlog.php';
		}
	}
	
	private function checkAuthentication() {
		if ($this->Auth_Req) {
			if (!Authentication::isAuthenticated()) {
				header( 'Location: login.php');
			}
		}
	}
	
	private function getFeatureId() {
		if (isset($_GET['fid'])) {			
			$cryptographer = Factory::getCryptographer();
			$this->feature_id = $cryptographer->Decrypt(rawurldecode(urlencode($_GET['fid'])));			
			Factory::getSession()->setValue('fid', $this->feature_id);
		} elseif (Factory::getSession()->isExist('fid')) {
			$this->feature_id = Factory::getSession()->getValue('fid');
		} else {
			$this->feature_id = 0;
		}
	}
	
	private function getPageContent() {		
		ob_start();			
		include(DOC_ROOT.$_SERVER['PHP_SELF']);
		$this->page_content = ob_get_contents();
		ob_end_clean(); 
	}
	
	private function setTheme() {								
		Loader::load('Theme');		
		$theme = new Theme();
		$theme->initWithThemeOptions(array('content' => $this->page_content, 'template' => $this->template, 'theme_side' => 'back', 'title' => $this->title));
		$theme->applyTheme();
	}
}

$application = new PRMS();
$application->run();
exit();
?>