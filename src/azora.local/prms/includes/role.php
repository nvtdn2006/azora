<?php
class Role
{
	function getAllRoles() {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT r.*, cu.username as role_created_username, mu.username as role_updated_username FROM roles r 
								LEFT JOIN users cu ON r.role_created_by = cu.user_id
								LEFT JOIN users mu ON r.role_updated_by = mu.user_id');			
		
		$sth->execute();
		$result = $sth->fetchAll(PDO::FETCH_ASSOC);
		$sth = null;
		$dbh = null;	
		return $result;
	}

	function getRole($role_id) {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT r.*, cu.username as role_created_username, mu.username as role_updated_username FROM roles r 
								LEFT JOIN users cu ON r.role_created_by = cu.user_id
								LEFT JOIN users mu ON r.role_updated_by = mu.user_id
								WHERE r.role_id = :role_id');						
		$sth->bindParam(':role_id', $role_id);
		$sth->execute();
		$result = $sth->fetch(PDO::FETCH_ASSOC);
		$sth = null;
		$dbh = null;
		return $result;
	}

	function saveRole($role) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		try {
			
			$dbh->beginTransaction();			
			
			$sth = $dbh->prepare('INSERT INTO roles (role_name, role_description, role_created_dt, role_created_by, role_updated_dt, role_updated_by) VALUES (:role_name, :role_description, NOW(), :role_created_by, NOW(), :role_updated_by)');			
		
			$sth->bindParam(':role_name', $role['role_name']);
			$sth->bindParam(':role_description', $role['role_description']);	
			$sth->bindParam(':role_created_by', $role['role_created_by']);	
			$sth->bindParam(':role_updated_by', $role['role_updated_by']);				
			$sth->execute();
			$role_id = $dbh->lastInsertId('role_id');
			
			foreach($role['features'] as $feature) {
				$sth = $dbh->prepare('INSERT INTO role_permissions (role_id, feature_id) VALUES (:role_id, :feature_id)');			
				$sth->bindParam(':role_id', $role_id);
				$sth->bindParam(':feature_id', $feature);
				$sth->execute();
			}
				
			$dbh->commit();
			$result = true;
			
		} catch(PDOException $e) {
			$dbh->rollback();
			throw new Exception($e->getMessage());
		}
		
		$sth = null;
		$dbh = null;
		return $result;
	}

	function updateRole($role) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		try {
			
			$dbh->beginTransaction();			
			
			$sth = $dbh->prepare('UPDATE roles SET role_name = :role_name, role_description = :role_description, role_updated_dt = NOW(), role_updated_by = :role_updated_by WHERE role_id = :role_id');			
		
			$sth->bindParam(':role_name', $role['role_name']);
			$sth->bindParam(':role_description', $role['role_description']);		
			$sth->bindParam(':role_updated_by', $role['role_updated_by']);
			$sth->bindParam(':role_id', $role['role_id']);
			$sth->execute();
			
			$sth = $dbh->prepare('DELETE FROM role_permissions WHERE role_id = :role_id');			
			$sth->bindParam(':role_id', $role['role_id']);
			$sth->execute();
			
			foreach($role['features'] as $feature) {
				$sth = $dbh->prepare('INSERT INTO role_permissions (role_id, feature_id) VALUES (:role_id, :feature_id)');			
				$sth->bindParam(':role_id', $role['role_id']);
				$sth->bindParam(':feature_id', $feature);
				$sth->execute();
			}
				
			$dbh->commit();
			$result = true;
			
		} catch(PDOException $e) {
			$dbh->rollback();
			throw new Exception($e->getMessage());
		}

		$sth = null;
		$dbh = null;
		return $result;
	}

	function deleteRole($roles) {				
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
		
		$role_ids = implode(',', $roles);	
		
		try {
			
			$dbh->beginTransaction();
			
			$sth = $dbh->prepare('DELETE FROM role_permissions WHERE role_id IN (' . $role_ids . ')');				
			$sth->execute();
			
			$sth = $dbh->prepare('DELETE FROM roles WHERE role_id IN (' . $role_ids . ')');				
			$sth->execute();
			
			$dbh->commit();
			$result = true;
			
		} catch(PDOException $e) {
			$dbh->rollback();			
			throw new Exception($e->getMessage());
		}
				
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function getUserRolesNotSelected($user_id) {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT r.* FROM roles r 
								WHERE r.role_id NOT IN (SELECT role_id FROM user_roles ur WHERE ur.user_id = :user_id )
								ORDER BY r.role_name');		
								
		$sth->bindParam(':user_id', $user_id);		
		
		$sth->execute();
		$result = $sth->fetchAll(PDO::FETCH_ASSOC);
		$sth = null;
		$dbh = null;	
		return $result;
	}
	
	function getUserRoles($user_id) {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT ur.*, r.role_name FROM user_roles ur LEFT JOIN roles r ON ur.role_id = r.role_id
								WHERE ur.user_id = :user_id ORDER BY r.role_name');			
		
		$sth->bindParam(':user_id', $user_id);
		$sth->execute();
		$result = $sth->fetchAll(PDO::FETCH_ASSOC);
		$sth = null;
		$dbh = null;	
		return $result;
	}
}
?>