<?php
function getFunctions() {
	return array('User' => WEB_ROOT . '/prms/functions/user.php',
				 'Menu' => WEB_ROOT . '/prms/functions/menu.php',
				 'Role' => WEB_ROOT . '/prms/functions/role.php',
				 'Branch' => WEB_ROOT . '/prms/functions/branch.php',
				 'Store' => WEB_ROOT . '/prms/functions/store.php');
}
?>