<?php
class RWS_Contents
{
	function getContent($type) {
		$result = null;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('SELECT c.*, cu.username as created_username, mu.username as updated_username FROM rws_contents c
								LEFT JOIN users cu ON c.created_by = cu.user_id
								LEFT JOIN users mu ON c.updated_by = mu.user_id
								WHERE c.content_type = :content_type');			
		$sth->bindParam(':content_type', $type);
		$sth->execute();
		$result = $sth->fetchAll(PDO::FETCH_ASSOC);
		$sth = null;
		$dbh = null;	
		return $result;
	}
	
	function saveContent($content) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('INSERT INTO rws_contents (content, content_type, created_dt, created_by, updated_dt, updated_by) VALUES 
							(:content, :content_type, NOW(), :created_by, NOW(), :updated_by)');			
		
		$sth->bindParam(':content', $content['content']);
		$sth->bindParam(':content_type', $content['content_type']);			
		$sth->bindParam(':created_by', $content['created_by']);	
		$sth->bindParam(':updated_by', $content['updated_by']);
			
		$sth->execute();
		$result = ($sth->rowCount() > 0);
		$sth = null;
		$dbh = null;
		return $result;
	}
	
	function updateContent($content) {
		$result = false;
		Loader::Load('Database');
		$dbh = Database::getInstance()->getConnection();
			
		$sth = $dbh->prepare('UPDATE rws_contents SET content = :content, content_type = :content_type,
						updated_dt = NOW(), updated_by = :updated_by WHERE id = :id');			
		
		$sth->bindParam(':content', $content['content']);
		$sth->bindParam(':content_type', $content['content_type']);			
		$sth->bindParam(':updated_by', $content['updated_by']);
		$sth->bindParam(':id', $content['id']);
			
		$sth->execute();
		$result = ($sth->rowCount() > 0);
		$sth = null;
		$dbh = null;
		return $result;
	}
}
?>