<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Redemptions'?>

<?php		

	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('jquery.ui');			
	CSSManager::getInstance()->add('/js/jqueryui/smoothness/jquery-ui-1.8.16.custom.css');	
	
	DomainManager::getInstance()->load('Redemption');
	$redemptionObj = new Redemption();
	
	DomainManager::getInstance()->load('Store');
	$storeObj = new Store();
	
	$assignedStores = $storeObj->getAssignedStores(Authentication::getUserId(), Authentication::getAttribute('su'));
	
	$store_ids = '';
	$sp = '';
	foreach($assignedStores as $as){
		$store_ids .= $sp . $as['store_id'];
		$sp = ',';
	}
	
	$criteria = array('store_id' => $store_ids);
	$criteria['redemption_status'] = 1;
	
	if (isset($_GET['action']) && $_GET['action'] == 'search') {
		
		Loader::load('DateTimeTool');		
		
		if (isset($_POST['submissionfrom']) && $_POST['submissionfrom'] != '') {
			$criteria['submissionfrom'] = DateTimeTool::convertStringToDate($_POST['submissionfrom']);
		}
		
		if (isset($_POST['submissionto']) && $_POST['submissionto'] != '') {
			$criteria['submissionto'] = DateTimeTool::convertStringToDate($_POST['submissionto']);
		}
		
		if (isset($_POST['nric']) && $_POST['nric'] != '') {
			$criteria['nric'] = $_POST['nric'];
		}
		
		if (isset($_POST['name']) && $_POST['name'] != '') {
			$criteria['name'] = $_POST['name'];
		}
		
		if (isset($_POST['collectionfrom']) && $_POST['collectionfrom'] != '') {
			$criteria['collectionfrom'] = DateTimeTool::convertStringToDate($_POST['collectionfrom']);
		}
		
		if (isset($_POST['collectionto']) && $_POST['collectionto'] != '') {
			$criteria['collectionto'] = DateTimeTool::convertStringToDate($_POST['collectionto']);
		}
	}
	
	$redemptions = $redemptionObj->getRedemptionInfo($criteria);
	
	$formvalues = array();
	
	if (isset($_REQUEST['submissionfrom'])) 			
		$formvalues['submissionfrom'] = $_REQUEST['submissionfrom'];
	else
		$formvalues['submissionfrom'] =  '';
	
	if (isset($_REQUEST['submissionto'])) 			
		$formvalues['submissionto'] = $_REQUEST['submissionto'];
	else
		$formvalues['submissionto'] =  '';
		
	if (isset($_REQUEST['nric'])) 			
		$formvalues['nric'] = $_REQUEST['nric'];
	else
		$formvalues['nric'] =  '';
		
	if (isset($_REQUEST['name'])) 			
		$formvalues['name'] = $_REQUEST['name'];
	else
		$formvalues['name'] =  '';
		
	if (isset($_REQUEST['collectionfrom'])) 			
		$formvalues['collectionfrom'] = $_REQUEST['collectionfrom'];
	else
		$formvalues['collectionfrom'] =  '';
	
	if (isset($_REQUEST['collectionto'])) 			
		$formvalues['collectionto'] = $_REQUEST['collectionto'];
	else
		$formvalues['collectionto'] =  '';
?>

<form name="redemptionsearchform" id="redemptionsearchform" action="redemptions.php?action=search" method="post"> 
	<table class="formview" width="100%" border="0">		
		<tr>
			<td>Submission</td>		
			<td>From</td>		
			<td><input type="text" name="submissionfrom" id="submissionfrom" class="input" value="<?php echo $formvalues['submissionfrom']; ?>" size="20" tabindex="10" /></td>	
			<td>To</td>		
			<td><input type="text" name="submissionto" id="submissionto" class="input" value="<?php echo $formvalues['submissionto']; ?>" size="20" tabindex="20" /></td>						
		</tr>		
		<tr>
			<td colspan="2">NRIC</td>		
			<td><input type="text" name="nric" id="nric" class="input" value="<?php echo $formvalues['nric']; ?>" size="20" tabindex="30" /></td>			
			<td>Name</td>		
			<td><input type="text" name="name" id="name" class="input" value="<?php echo $formvalues['name']; ?>" size="20" tabindex="40" /></td>			
		</tr>
		<tr>
			<td>Collection</td>		
			<td>From</td>		
			<td><input type="text" name="collectionfrom" id="collectionfrom" class="input" value="<?php echo $formvalues['collectionfrom']; ?>" size="20" tabindex="50" /></td>	
			<td>To</td>		
			<td><input type="text" name="collectionto" id="collectionto" class="input" value="<?php echo $formvalues['collectionto']; ?>" size="20" tabindex="60" /></td>						
		</tr>		
		<tr>			
			<td colspan="5">				
				<input type="submit" name="search" id="search" class="button-primary" value="Search" tabindex="70"/>				
			</td>			
		</tr>
	</table>
</form>

<div class="SectionTitle"><?php echo ($redemptions != null && count($redemptions) > 0) ? count($redemptions) : '0'; ?> Record(s) found.</div>

<table class="tabular">
	<thead>
		<tr>			
			<th>NRIC</th>
			<th>Name</th>
			<th>Collection</th>
			<th>Store</th>
			<th>Points</th>			
			<th>Submitted</th>
		</tr>
	</thead>
	<tbody>
		<?php
			foreach($redemptions as $redemption) {
		?>
		<tr>			
			<td><a class="detailLink" href="redemptiondetail.php?m=<?php echo Factory::getCryptographer()->Encrypt('write'); ?>&id=<?php echo $redemption['redemption_id']; ?>" alt="Select" title="Select"><?php echo $redemption['nric']; ?></a></td>
			<td><?php echo $redemption['name']; ?></td>
			<td><?php echo $redemption['collection_date'] . ' ' .  date('h:i:s  A', strtotime($redemption['collection_time'])); ?></td>
			<td><?php echo $redemption['store_branch_name']; ?></td>
			<td><?php echo $redemption['total_points']; ?></td>			
			<td><?php echo date('Y-m-d h:i:s  A', strtotime($redemption['transaction_dt'])); ?></td>
		</tr>
		<?php } ?>
	</tbody>
</table>


<script type="text/javascript">
	$(document).ready(function() { 
		$( "#submissionfrom" ).datepicker({ dateFormat: 'dd/mm/yy'});	
		$( "#submissionto" ).datepicker({ dateFormat: 'dd/mm/yy'});	
		$( "#collectionfrom" ).datepicker({ dateFormat: 'dd/mm/yy'});	
		$( "#collectionto" ).datepicker({ dateFormat: 'dd/mm/yy'});
	});	
</script>