<?php require_once './includes/service.php'; ?>

<?php

	$return = array();
	$return['status'] = false;
	
	if (isset($_POST['form']) && $_POST['form'] == 'send'
		&& $_POST['mid'] != '') {
		
		$config = Factory::getConfig();
		
		DomainManager::getInstance()->load('Marketing');
		$marketingObj = new Marketing();
		
		$mail_id = $_POST['mid'];	
		$mail = $marketingObj->getMail($mail_id);				
		
		DomainManager::getInstance()->load('Customer');
		$customerObj = new Customer();
		
		$customers = $customerObj->getCustomerProfileByValues();
		
		foreach ($customers as $customer){
			
			$to      = $customer['email'];
			
			$subject = $mail['mail_subject'];
			
			$message = preg_replace('/\{\%name\%\}/', $customer['name'], $mail['mail_body']);
			
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= 'From: ' . $config['PRMSConfig']->noreply_mail . "\r\n";				

			mail($to, $subject, $message, $headers);
		}
		
		$marketingObj->updateLastSent($mail_id);
		
		$return['status'] = true;
		
		//Event Log
		$evtObj = new EventObject();					
		$evtObj->event_id = EventTypes::PRMS_UpdateMarketing;
		$evtObj->description = Authentication::getAttribute('login_id') . ' sent marketing mail ('.$mail['mail_subject'].') to customers.';
		$evtObj->action_by = Authentication::getAttribute('login_id');					
		EventLog::Log($evtObj);
		
	} else {
		$return['status'] = false;		
	}
		
	header('Content-type: application/json');
	echo json_encode($return);
	
?>