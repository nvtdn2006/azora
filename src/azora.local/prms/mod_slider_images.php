<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Slider images'?>

<?php

	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('checkboxlist');
	JSManager::getInstance()->add('messagebox');
	
	require_once './includes/mod_slider.php';
	$sldObj = new Mod_Slider();
	
	$error = array();

	if (isset($_POST['delete']) && $_POST['delete'] == 'Remove') {
		$sliders = null;
		if (isset($_POST['sliders']))
			$sliders = $_POST['sliders'];	
		if (isset($sliders) && count($sliders) > 0) {
			
			$_names = '';
			$sp = '';
			foreach($sliders as $item) {
				$nobj = $sldObj->getImage($item);
				$_names .= $sp . $nobj['image_name'];
				$sp = ', ';
			}
			
			if ($sldObj->deleteImage($sliders))	 {
			
				//Event Log
				$evtObj = new EventObject();					
				$evtObj->event_id = EventTypes::PRMS_DeleteSliderImage;
				$evtObj->description = Authentication::getAttribute('login_id') . ' deleted slider images ('.$_names.').';
				$evtObj->action_by = Authentication::getAttribute('login_id');					
				EventLog::Log($evtObj);
			
				header( 'Location: mod_slider_images.php');			
			}
		} else {
			array_push($error, 'Please select at least one item to proceed.');	
		}
	}
	
	$sliders = $sldObj->getAllImages();
?>

<?php
	if (isset($error) && count($error) > 0) {
?>
	<div class="error-info tabular-info">
		<?php foreach ($error as $handle) {
				echo "<p>$handle</p>";
		} ?>
	</div>
<?php
	}
?>

<form name="sliderlist" id="sliderlist" action="mod_slider_images.php" method="post"> 
	<table class="tabular">
		<thead>
			<tr>
				<th><input type="checkbox" name="listselectall" class="listselectall" onclick="javascript:checkSelectAllBox('listselect', this.checked);"/></th>
				<th>Image</th>
				<th style="width: 200px;">Description</th>
				<th>Order</th>
				<th>Active</th>
				<th>Updated By</th>
				<th>Updated On</th>			
			</tr>
		</thead>
		<tfoot>
			<tr>			
				<th colspan="7"><a class="button-primary" href="mod_slider_images-edit.php">New</a>
				<input type="submit" name="delete" id="submit" class="button-secondary" value="Remove" onclick="javascript:return confirmBox('Are you sure you want to delete selected item?');"/></th>				
			</tr>
		</tfoot>
		<tbody>		
			<?php
				foreach($sliders as $slider) {
			?>
			<tr>
				<td><?php echo '<input type="checkbox" name="sliders[]" class="listselect" onclick="javascript:checkSelectBox(\'listselectall\', \'listselect\');" value="'.$slider['id'].'"/>'; ?> 
				</td>
				<td><?php echo '<a class="detailLink" href="mod_slider_images-edit.php?image='.$slider['id'].'" alt="Edit" title="Edit">'.$slider['image_name'].'</a>'; ?></td>
				<td><?php echo $slider['description'] ?></td>
				<td><?php echo $slider['image_order'] ?></td>
				<td><?php echo ($slider['image_status'] ? 'Yes' : 'No'); ?></td>
				<td><?php echo $slider['updated_username']; ?></td>
				<td><?php echo $slider['updated_dt']; ?></td>					
			</tr>
			<?php } ?>
		</tbody>
	</table>
</form>
<?php
	$sldObj = null;
?>