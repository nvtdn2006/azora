<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?php $this->theTitle(); ?> &lsaquo; Azora - PRMS Administration</title>
<link rel="stylesheet" type="text/css" media="all" href="<?php $this->theThemeURL(); ?>/css/style.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php $this->theThemeURL(); ?>/css/login.css" />
<?php	
	$this->setPlaceHolder('css');
	$this->setPlaceHolder('js');
?>
</head>
<body>
	<div id="login-wrapper">
		<?php $this->theContent(); ?>
	</div>	
</body>
</html>