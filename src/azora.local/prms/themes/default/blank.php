<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?php $this->theTitle(); ?> &lsaquo; Azora - PRMS Administration</title>
<link rel="stylesheet" type="text/css" media="all" href="<?php $this->theThemeURL(); ?>/css/style.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php $this->theThemeURL(); ?>/css/default.css" />
<?php	
	$this->setPlaceHolder('css');
	$this->setPlaceHolder('js');
?>
</head>
<body>
	<?php $this->theContent(); ?>
</body>
</html>