<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?php $this->theTitle(); ?> &lsaquo; Azora - PRMS Administration</title>
<link rel="stylesheet" type="text/css" media="all" href="<?php $this->theThemeURL(); ?>/css/style.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php $this->theThemeURL(); ?>/css/default.css" />
<?php	
	$this->setPlaceHolder('css');
	$this->setPlaceHolder('js');
?>
</head>
<body>
	<div id="container">
		<div id="userpanel">
			<?php $this->loadModule('mod_userpanel'); ?>
		</div>
		<div id="wrapper">
			<div id="header">
				<div id="logo"><a href="<?php $this->theSiteURL(); echo '/prms/'; ?>"></a></div>
				<div id="prms"><a href="<?php $this->theSiteURL(); echo '/prms/'; ?>"></a></div>
			</div>
			<div id="left">
				<?php $this->loadModule('mod_mainmenu'); ?>
			</div>
			<div id="right">
				<div id="righttitle"><?php $this->theTitle(); ?></div>
				<div id="contentcontainer">
					<?php $this->theContent(); ?>					
				</div>				
			</div>
			<br style="clear:both;" />
			<div class="push"></div>
		</div>
		<div id="footer">
			<div id="container">
				<div id="support"><a href="mailto:support@bluecube.com.sg">Technical Support</a></div>
				<div id="powered"><a href="http://www.bluecube.com.sg" target="_blank">Powered by BlueCube Pte Ltd.</a></div>
			</div>
		</div>
	</div>
</body>
</html>