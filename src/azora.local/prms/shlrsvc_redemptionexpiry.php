<?php 
/*
 * 05-04-2013 FX.1304.002 Read Default Redemption CC email
 */
require_once './includes/service.php'; 
?>

<?php

	$return = array();
	$return['status'] = true;
	$return['message'] = 'Process done.';
	
	$manual = false;
	
	
	try {
	
		if (isset($_GET['m']) && $_GET['m'] == 1) {		
			$manual = true;		
		}
		
		$config = Factory::getConfig();
		
		DomainManager::getInstance()->load('Company');
		$companyObj = new Company();
		$company = $companyObj->getCompany();
		$company = $company[0];
		
		DomainManager::getInstance()->load('Redemption');
		$redemptionObj = new Redemption();		
		$customers = $redemptionObj->doRedemptionExpiry();	
				
		foreach($customers as $customer) {
						
			$property = array ('name' => $customer['name'],
								'company_name' => $company['company_name'],
								'link' => $config['PRMSConfig']->live_site,
								'csmail' => $company['company_customersupport_email']);

			$mailer = new SiteMailer();
			$mailer->toMail = $customer['email'];
                        
                        //+START FX.1304.002 Add Default CC email 
                        if (isset($config['PRMSConfig']->PRMS_default_cc_mail) 
                                && $config['PRMSConfig']->PRMS_default_cc_mail != "") {
                            if (is_array($config['PRMSConfig']->PRMS_default_cc_mail)) {
                                foreach ( $config['PRMSConfig']->PRMS_default_cc_mail as $ccMail) {
                                    $mailer->AddCc($ccMail);
                                }
                            } else {
                                $mailer->AddCc($config['PRMSConfig']->PRMS_default_cc_mail);
                            }
                        }
                        //-END FX.1304.002
                        
			$mailer->subject = 'Redemption expired at '.$company['company_name'].' redemption website';				
			$mailer->PrepareMail('sendRedemptionExpiredToCustomer', $property);
			$mailer->Send();
		
		}
	
	} catch(PDOException $e) {
		$return['status'] = false;	
	}

		
	$log = array( 'scheduler_name' => 'Redemption expiration',
					'status' => $return['status'],
					'is_manual' => $manual);
	SchedulerLog::Log($log);
	
	if ($manual) {
		header('Content-type: application/json');
		echo json_encode($return);
	}	
	
?>