<?php
/*
 * 05-05-2013 FX.1305.001 Add "Date" for Point Transaction query, Expiration Transaction
 * 05-05-2013 FX.1305.001-1 Add Redeem & Cancel Point into report
 */
require_once './includes/application.php';
require_once './includes/sort.php';
$this->template = '';
$this->title = 'Point transactions'
?>

<?php
JSManager::getInstance()->add('jquery');
JSManager::getInstance()->add('jquery.ui'); // FX.1305.001 For DatePicker
CSSManager::getInstance()->add('/js/jqueryui/smoothness/jquery-ui-1.8.16.custom.css'); // FX.1305.001 For DatePicker
JSManager::getInstance()->add('validation');

Loader::load('DateTimeTool');

DomainManager::getInstance()->load('Point');
$pointObj = new Point();

DomainManager::getInstance()->load('Customer');
$customerObj = new Customer();

// FX.1305.001-1 Object Include Redemption and Cancellation
DomainManager::getInstance()->load('Redemption');
$redemptionObj = new Redemption();

// FX.1305.001-1 
DomainManager::getInstance()->load('WhereClause');

$customer_id = 0;
$customer = null;

if (isset($_GET['id'])) {
    $customer_id = $_GET['id'];
}

if (isset($_POST['customer_id'])) {
    $customer_id = $_POST['customer_id'];
}

if ($customer_id > 0) {
    $customer = $customerObj->getCustomerProfile($customer_id);


    $purchasehistory = null;
    $redemptionhistory = null; // FX.1305.001-1 
    $expiredhistory = null;    // FX.1305.001

    if (isset($_GET['action']) && $_GET['action'] == 'search') {
        if ($customer_id > 0) {
            $frommonth = date('n');
            $fromyear = date('Y');

            $tomonth = date('n');
            $toyear = date('Y');

            $type = 0;

            // @ Point History
            // FX.1305.001 Add Point Transaction Query by Date
            $fromdate = (isset($_POST['p_datefrom']) && $_POST['p_datefrom'] != "") ?
                    new DateTime(DateTimeTool::convertStringToDate($_POST['p_datefrom'])) : New DateTime();

            $todate = (isset($_POST['p_dateto']) && $_POST['p_dateto'] != "") ?
                    new DateTime(DateTimeTool::convertStringToDate($_POST['p_dateto'])) : New DateTime();

            //echo $fromdate->format('d/m/Y');

            /*
              if (isset($_POST['frommonth']))
              $frommonth = $_POST['frommonth'];

              if (isset($_POST['fromyear']))
              $fromyear = $_POST['fromyear'];

              if (isset($_POST['tomonth']))
              $tomonth = $_POST['tomonth'];

              if (isset($_POST['toyear']))
              $toyear = $_POST['toyear'];


              $fromdate = new DateTime($fromyear . '-' . $frommonth . '-' . '1');
              $todate = new DateTime($toyear . '-' . $tomonth . '-' . '1');
              $todate->modify('+1 month');
              $todate->modify('-1 day');

             */

            if (isset($_POST['type']))
                $type = $_POST['type'];

            $purchasehistory = $pointObj->getPointsTransactionByCustomer($customer_id, $fromdate, $todate, $type);

            // @ Redeem History
            // +Start FX.1305.001-1 Select Redeem History
            if ($type == 0 || $type == 13) {
                $wc = new WhereClause();
                /*
                  'store_id' => 'r.store_id',
                  'transaction_dt' => 'DATE(r.transaction_dt)',
                  'nric' => 'cp.nric',
                  'name' => 'cp.name',
                  'collection_date' => 'DATE(r.collection_date)',
                  'redemption_status' => 'r.redemption_status';
                 */
                /*
                  if ((isset($_POST['p_datefrom']) && $_POST['p_datefrom'] != "") || (isset($_POST['p_dateto']) && $_POST['p_dateto'] != "")) {

                  $submTo = (isset($_POST['p_dateto']) && $_POST['p_dateto'] != '') ? $_POST['p_dateto'] : $_POST['p_datefrom'];
                  $submFrom = (isset($_POST['p_datefrom']) && $_POST['p_datefrom'] != '') ? $_POST['p_datefrom'] : $_POST['p_dateto'];
                 */

                $wc->add('transaction_dt', wcOption::incl, wcSign::between, _sq($fromdate->format('Y-m-d')), _sq($todate->format('Y-m-d')));

                /* } */


                if (isset($customer['nric']) && $customer['nric'] != '') {
                    $wc->add('nric', wcOption::incl, wcSign::like, _sq($customer['nric']));
                }

                /*
                  if (isset($_POST['name']) && $_POST['name'] != '') {
                  $wc->add('name', wcOption::incl, wcSign::like, _sq('%' . $_POST['name'] . '%'));
                  }

                  if ((isset($_POST['collectionfrom']) && $_POST['collectionfrom'] != '') || (isset($_POST['collectionto']) && $_POST['collectionto'] != '')) {

                  $collTo = (isset($_POST['collectionto']) && $_POST['collectionto'] != '') ? $_POST['collectionto'] : $_POST['collectionfrom'];
                  $collFrom = (isset($_POST['collectionfrom']) && $_POST['collectionfrom'] != '') ? $_POST['collectionfrom'] : $_POST['collectionto'];

                  $wc->add('collection_date', wcOption::incl, wcSign::between, _sq(DateTimeTool::convertStringToDate($collFrom)), _sq(DateTimeTool::convertStringToDate($collTo)));
                  }

                  if (isset($_POST['store']) && $_POST['store'] > 0) {
                  $wc->add('store_id', wcOption::incl, wcSign::like, _sq($_POST['store']));
                  }
                 */

                $wc->add('redemption_status', wcOption::excl, wcSign::equal, _sq('4'));

                if ($wc->count() > 0) {
                    $criteria['WHERECLAUSE'] = $wc;
                }

                if (count($criteria) > 0) {
                    $redemptionhistory = $redemptionObj->getRedemptionInfo($criteria);
                }
            }

            // -End FX.1305.001-1 
            // @ Expired History
            // FX.1305.001 
            if ($type == 0 || $type == 14) {
                $expiredhistory = $pointObj->getExpiredPointsByCustomer($customer_id, $fromdate, $todate);
            }
        }
    }

    $formvalues = array();
    /*
      if (isset($_REQUEST['frommonth']))
      $formvalues['frommonth'] = $_REQUEST['frommonth'];
      else
      $formvalues['frommonth'] = 0;

      if (isset($_REQUEST['fromyear']))
      $formvalues['fromyear'] = $_REQUEST['fromyear'];
      else
      $formvalues['fromyear'] = 0;

      if (isset($_REQUEST['tomonth']))
      $formvalues['tomonth'] = $_REQUEST['tomonth'];
      else
      $formvalues['tomonth'] = 0;

      if (isset($_REQUEST['toyear']))
      $formvalues['toyear'] = $_REQUEST['toyear'];
      else
      $formvalues['toyear'] = 0;
     */
    if (isset($_REQUEST['type']))
        $formvalues['type'] = $_REQUEST['type'];
    else
        $formvalues['type'] = 0;

    if (isset($_POST['p_datefrom']))
        $formvalues['p_datefrom'] = $_POST['p_datefrom'];
    else
        $formvalues['p_datefrom'] = '01/01/2012';
    if (isset($_POST['p_dateto']))
        $formvalues['p_dateto'] = $_POST['p_dateto'];
    else
        $formvalues['p_dateto'] = '';
} else {
    header('Location: customersearch.php?d=pttrans');
    exit();
}
?>

<form name="historyform" id="historyform" action="pttrans.php?action=search&id=<?php echo $customer['customer_id']; ?>" method="post"> 

    <table class="formview" width="100%" border="0" cellspacing="3px" cellpadding="3px">
        <tr>
            <td class="LabelCell Disabled">NRIC / FIN</td>
            <td><input type="text" name="nric" id="nric" maxlength="9" class="input" value="<?php echo $customer['nric']; ?>" size="20" tabindex="10" disabled /></td>
        </tr>
        <tr>
            <td class="LabelCell Disabled">Name</td>
            <td><input type="text" name="name" id="name" maxlength="255" class="input" value="<?php echo $customer['name']; ?>" size="20" tabindex="20" disabled /></td>
        </tr>

    </table>
    <table class="formview" width="100%" border="0">	
        <?php /*
          <tr>
          <td><b>From</b></td>
          <td>Month</td>
          <td>
          <select tabindex="50" name="frommonth" class="GreaterThanZero" tabindex="10">
          <option value="0" <?php echo $formvalues['frommonth'] == 0 ? 'selected' : ''; ?>>- Select -</option>
          <option value="1" <?php echo $formvalues['frommonth'] == 1 ? 'selected' : ''; ?>>Jan</option>
          <option value="2" <?php echo $formvalues['frommonth'] == 2 ? 'selected' : ''; ?>>Feb</option>
          <option value="3" <?php echo $formvalues['frommonth'] == 3 ? 'selected' : ''; ?>>Mar</option>
          <option value="4" <?php echo $formvalues['frommonth'] == 4 ? 'selected' : ''; ?>>Apr</option>
          <option value="5" <?php echo $formvalues['frommonth'] == 5 ? 'selected' : ''; ?>>May</option>
          <option value="6" <?php echo $formvalues['frommonth'] == 6 ? 'selected' : ''; ?>>Jun</option>
          <option value="7" <?php echo $formvalues['frommonth'] == 7 ? 'selected' : ''; ?>>Jul</option>
          <option value="8" <?php echo $formvalues['frommonth'] == 8 ? 'selected' : ''; ?>>Aug</option>
          <option value="9" <?php echo $formvalues['frommonth'] == 9 ? 'selected' : ''; ?>>Sep</option>
          <option value="10" <?php echo $formvalues['frommonth'] == 10 ? 'selected' : ''; ?>>Oct</option>
          <option value="11" <?php echo $formvalues['frommonth'] == 11 ? 'selected' : ''; ?>>Nov</option>
          <option value="12" <?php echo $formvalues['frommonth'] == 12 ? 'selected' : ''; ?>>Dec</option>
          </select>
          </td>
          <td>Year</td>
          <td>
          <select tabindex="50" name="fromyear" class="GreaterThanZero" tabindex="20">
          <option value="0">- Select -</option>
          <?php
          $year = date('Y');
          for($i=0;$i<3;$i++) {
          echo '<option value="'.($year - $i).'"  '.($formvalues['fromyear'] == ($year - $i) ? 'selected' : '').'  >'.($year - $i).'</option>';
          }
          ?>
          </select>
          </td>
          </tr>
          <tr>
          <td><b>To</b></td>
          <td>Month</td>
          <td>
          <select tabindex="50" name="tomonth" class="GreaterThanZero" tabindex="30">
          <option value="0" <?php echo $formvalues['tomonth'] == 0 ? 'selected' : ''; ?>>- Select -</option>
          <option value="1" <?php echo $formvalues['tomonth'] == 1 ? 'selected' : ''; ?>>Jan</option>
          <option value="2" <?php echo $formvalues['tomonth'] == 2 ? 'selected' : ''; ?>>Feb</option>
          <option value="3" <?php echo $formvalues['tomonth'] == 3 ? 'selected' : ''; ?>>Mar</option>
          <option value="4" <?php echo $formvalues['tomonth'] == 4 ? 'selected' : ''; ?>>Apr</option>
          <option value="5" <?php echo $formvalues['tomonth'] == 5 ? 'selected' : ''; ?>>May</option>
          <option value="6" <?php echo $formvalues['tomonth'] == 6 ? 'selected' : ''; ?>>Jun</option>
          <option value="7" <?php echo $formvalues['tomonth'] == 7 ? 'selected' : ''; ?>>Jul</option>
          <option value="8" <?php echo $formvalues['tomonth'] == 8 ? 'selected' : ''; ?>>Aug</option>
          <option value="9" <?php echo $formvalues['tomonth'] == 9 ? 'selected' : ''; ?>>Sep</option>
          <option value="10" <?php echo $formvalues['tomonth'] == 10 ? 'selected' : ''; ?>>Oct</option>
          <option value="11" <?php echo $formvalues['tomonth'] == 11 ? 'selected' : ''; ?>>Nov</option>
          <option value="12" <?php echo $formvalues['tomonth'] == 12 ? 'selected' : ''; ?>>Dec</option>
          </select>
          </td>
          <td>Year</td>
          <td>
          <select tabindex="50" name="toyear" class="GreaterThanZero" tabindex="40">
          <option value="0">- Select -</option>
          <?php
          $year = date('Y');
          for($i=0;$i<3;$i++) {
          echo '<option value="'.($year - $i).'"  '.($formvalues['toyear'] == ($year - $i) ? 'selected' : '').' >'.($year - $i).'</option>';
          }
          ?>
          </select>
          </td>
          </tr>

         */ ?>
        <?php //+Start FX.1305.001 ?>
        <tr>
            <td>Date</td>		
            <td>From</td>		
            <td><input type="text" name="p_datefrom" id="p_datefrom" class="input" value="<?php echo $formvalues['p_datefrom']; ?>" size="20" tabindex="10" /></td>	
            <td>To</td>		
            <td><input type="text" name="p_dateto" id="p_dateto" class="input" value="<?php echo $formvalues['p_dateto']; ?>" size="20" tabindex="20" /></td>						
        </tr>	
        <?php //+End FX.1305.001       ?>
        <tr>		
            <td>Transaction type</td>
            <td colspan="4">				
                <select tabindex="50" name="type" tabindex="30">						
                    <option value="0" <?php echo $formvalues['type'] == 0 ? 'selected' : ''; ?>>- All -</option>
                    <option value="1" <?php echo $formvalues['type'] == 1 ? 'selected' : ''; ?>>Accumulation</option>
                    <option value="2" <?php echo $formvalues['type'] == 2 ? 'selected' : ''; ?>>Cancellation</option>
                    <option value="13" <?php echo $formvalues['type'] == 13 ? 'selected' : ''; ?>>Redemption</option>
                    <option value="14" <?php echo $formvalues['type'] == 14 ? 'selected' : ''; ?>>Expiration</option>
                </select>
            </td>			
        </tr>		
        <tr>			
            <td colspan="5">				
                <input type="hidden" name="customer_id" id="customer_id" value="<?php echo $customer['customer_id']; ?>">		
                <input type="submit" name="search" id="search" class="button-primary" value="Search" tabindex="30"/>				
            </td>			
        </tr>
    </table>
</form>

<div class="SectionTitle" ><b><?php echo ($purchasehistory != null && count($purchasehistory) > 0) ? count($purchasehistory) : '0'; ?> </b> Record(s) found.</div>

<?php
$pur_red_history = array();

$total_available_points = 0;
$redemption_points = 0;
$expired_points = 0;

$amount = 0;
$point = 0;

// Accumulation & Cancellation
if (isset($purchasehistory))
    foreach ($purchasehistory as $history) {
        $amount = $history['transaction_type'] == 1 ? $history['sales_amount'] * 1 : $history['sales_amount'] * -1;
        $point = $history['transaction_type'] == 1 ? $history['trans_points'] * 1 : $history['trans_points'] * -1;

        $pur_red_history[] = array(
            'branch_name'      => $history['branch_name'],
            'store_name'       => $history['store_name'],
            'reference_no'     => $history['reference_no'],
            'sales_amount'     => $amount,
            'trans_points'     => $point,
            'transacted_date'  => $history['transacted_date'],
            'transaction_type' => ($history['transaction_type'] == 1 ? 'Accumulation' : 'Cancellation')
        );
        $total_available_points += $point;
    }

// Redemption
if (isset($redemptionhistory))
    foreach ($redemptionhistory as $history) {
        $point = $history['total_points'] * -1;

        $pur_red_history[] = array(
            'branch_name'      => $history['branch_name'],
            'store_name'       => $history['store_name'],
            'reference_no'     => '-', // Redemption-ID:' . $history['redemption_id'],
            'sales_amount'     => 0,
            'trans_points'     => $point,
            'transacted_date'  => $history['transaction_dt'],
            'transaction_type' => 'Redemption'
        );
        $redemption_points += $point;
    }

// Expired Point
if (isset($expiredhistory))
    foreach ($expiredhistory as $header) {
        if (isset($header['balance']))
            foreach ($header['balance'] as $history) {

                $point = $history['accumulated_points'] * -1;

                $pur_red_history[] = array(
                    'branch_name'      => $history['branch_name'],
                    'store_name'       => $history['store_name'],
                    'reference_no'     => "-", // 'Expiration-ID:' . $history['header_id'],
                    'sales_amount'     => 0,
                    'trans_points'     => $point,
                    'transacted_date'  => $header['header']['transaction_on'],
                    'transaction_type' => 'Expiration'
                );
                $expired_points += $point;
            }
    }

$balance_points = $total_available_points + $expired_points + $redemption_points;

// Sort by transaction date
if (isset($pur_red_history)) {
    usort($pur_red_history, "cmp_by_transactiondate");
}
?>

<table class="tabular">
    <thead>
        <tr>			
            <th>Branch</th>
            <th>Store</th>
            <th>Reference No.</th>
            <th>Purchase Amount</th>
            <th>Points</th>
            <th>Transaction On</th>			
            <th>Type</th>			
        </tr>
    </thead>
    <tbody>
        <?php
        if ($pur_red_history != null) {
            foreach ($pur_red_history as $history) {
                ?>
                <tr>						
                    <td><?php echo $history['branch_name']; ?></td>
                    <td><?php echo $history['store_name']; ?></td>
                    <td><?php echo $history['reference_no']; ?></td>
                    <td class="text-right"><?php echo $history['sales_amount']; ?></td>
                    <td class="text-right"><?php echo $history['trans_points']; //($history['transaction_type'] == 1 ? '' : '-') .                ?></td>			
                    <td><?php echo $history['transacted_date']; ?></td>			
                    <td><?php echo $history['transaction_type']; ?></td>			
                </tr>
                <?php
            }
        }
        ?>
    </tbody>
</table>
<hr/>
<table class="formview">
    <thead>
        <tr>
            <th>Total available points</th>
            <th>Redemption points</th>
            <th>Expired points</th>
            <th>Balance points</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td align="right"><?php echo $total_available_points; ?></td>
            <td align="right"><?php echo $redemption_points; ?></td>
            <td align="right"><?php echo $expired_points; ?></td>
            <td align="right"><?php echo $balance_points; ?></td>
        </tr>
    </tbody>
</table>


<script type="text/javascript">
    $(document).ready(function() {
        loadValidation('historyform');
        $("#p_datefrom").datepicker({dateFormat: 'dd/mm/yy'}); // FX.1305.001 
        $("#p_dateto").datepicker({dateFormat: 'dd/mm/yy'});	  // FX.1305.001 
    });

</script>