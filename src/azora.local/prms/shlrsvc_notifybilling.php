<?php 
/*
 * 05-04-2013 FX.1304.002 Read Default CC email
 */
require_once './includes/service.php'; 
?>

<?php

	$return = array();
	$return['status'] = true;
	$return['message'] = 'Process done.';
	
	$manual = false;
        
        $config = Factory::getConfig(); //FX.1304.002 
	
	
	try {
	
		if (isset($_GET['m']) && $_GET['m'] == 1) {		
			$manual = true;		
		}
		
		DomainManager::getInstance()->load('Company');
		$companyObj = new Company();
		$company = $companyObj->getCompany();
		$company = $company[0];
		
		require_once './includes/user.php';
		$userObj = new User();
		
		$today = getdate();
				
		if ($today['mday'] == 27) {
			
			$users = $userObj->getUserForBillingReminder();
			
			if (isset($users)) {
			
				foreach($users as $user) {					
					
					$property = array ('full_name' => $user['full_name'],
										'company_name' => $company['company_name']);

					$mailer = new SiteMailer();
					$mailer->toMail = $user['email'];
                                        
                                        //+START FX.1304.002 Add Default CC email 
                                        if (isset($config['PRMSConfig']->PRMS_default_cc_mail) 
                                                && $config['PRMSConfig']->PRMS_default_cc_mail != "") {
                                            if (is_array($config['PRMSConfig']->PRMS_default_cc_mail)) {
                                                foreach ( $config['PRMSConfig']->PRMS_default_cc_mail as $ccMail) {
                                                    $mailer->AddCc($ccMail);
                                                }
                                            } else {
                                                $mailer->AddCc($config['PRMSConfig']->PRMS_default_cc_mail);
                                            }
                                        }
                                        //-END FX.1304.002
                                        
					$mailer->subject = 'Reminder for billing at '.$company['company_name'].' PRMS';				
					$mailer->PrepareMail('sendReminderforbillingToUser', $property);
					$mailer->Send();
				}
				
			}
		}		
	
	} catch(PDOException $e) {
		$return['status'] = false;	
	}

		
	$log = array( 'scheduler_name' => 'Billing notification',
					'status' => $return['status'],
					'is_manual' => $manual);
	SchedulerLog::Log($log);
	
	if ($manual) {
		header('Content-type: application/json');
		echo json_encode($return);
	}	
	
?>