<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'New store'?>

<?php	

	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('validation');	
	
	DomainManager::getInstance()->load('Branch');
	DomainManager::getInstance()->load('Store');
	$branchObj = new Branch();
	$storeObj = new Store();
	
	$store_id = 0;
	$store = null;
	
	$error = array();
	
	if (isset($_GET['store'])) {		
		$store_id = $_GET['store'];
		$store = $storeObj->getStore($store_id);	
		$this->title = 'Update store';				
	}
	
	if (isset($_GET['action'])) {		
		switch ($_GET['action']){
			case 'new':				
				if (!$storeObj->isExistStore($_POST['store_name'], $_POST['store_branch'], 0)) {
					$store = array('store_name' => $_POST['store_name'],
									'store_description' => $_POST['store_description'],								
									'store_address' => $_POST['store_address'],								
									'store_website' => $_POST['store_website'],								
									'branch_id' => $_POST['store_branch'],
									'store_collection' => $_POST['store_collection'],
									'created_by' => Authentication::getUserId(),								
									'updated_by' => Authentication::getUserId());								
					if ($storeObj->saveStore($store)) {
						
						//Event Log
						$evtObj = new EventObject();					
						$evtObj->event_id = EventTypes::PRMS_AddStore;
						$evtObj->description = Authentication::getAttribute('login_id') . ' added new store ('.$_POST['store_name'].').';
						$evtObj->action_by = Authentication::getAttribute('login_id');					
						EventLog::Log($evtObj);
						
						header( 'Location: stores.php');
					}
				} else {
					array_push($error, 'Store name already exists in system.');	
				}
			break;
			case 'update':		
				if (!$storeObj->isExistStore($_POST['store_name'], $_POST['store_branch'], $store_id)) {
					$store = array('store_name' => $_POST['store_name'],
									'store_description' => $_POST['store_description'],								
									'store_address' => $_POST['store_address'],		
									'store_website' => $_POST['store_website'],										
									'branch_id' => $_POST['store_branch'],		
									'store_collection' => $_POST['store_collection'],
									'updated_by' => Authentication::getUserId(),
									'store_id' => $store_id);
					if ($storeObj->updateStore($store)) {
					
						//Event Log
						$evtObj = new EventObject();					
						$evtObj->event_id = EventTypes::PRMS_UpdateStore;
						$evtObj->description = Authentication::getAttribute('login_id') . ' updated store ('.$_POST['store_name'].').';
						$evtObj->action_by = Authentication::getAttribute('login_id');					
						EventLog::Log($evtObj);
						
						header( 'Location: stores.php');
					}
				} else {
					array_push($error, 'Store name already exists in system.');	
				}
			break;
		}		
	}
	
	if (!isset($store) || isset($_GET['action'])) {		
		if (isset($_REQUEST['store_name'])) 			
			$store['store_name'] = $_REQUEST['store_name'];
		else
			$store['store_name'] =  '';
			
		if (isset($_REQUEST['store_description'])) 			
			$store['store_description'] = $_REQUEST['store_description'];
		else
			$store['store_description'] =  '';
			
		if (isset($_REQUEST['store_address'])) 			
			$store['store_address'] = $_REQUEST['store_address'];
		else
			$store['store_address'] =  '';			
			
		if (isset($_REQUEST['store_website'])) 			
			$store['store_website'] = $_REQUEST['store_website'];
		else
			$store['store_website'] =  '';	
			
		if (isset($_REQUEST['store_branch'])) 			
			$store['branch_id'] = $_REQUEST['store_branch'];
		else
			$store['branch_id'] =  0;
			
		if (isset($_REQUEST['store_collection'])) 			
			$store['store_collection'] = $_REQUEST['store_collection'];
		else
			$store['store_collection'] =  1;
	}
	
?>

<?php
	if (isset($error) && count($error) > 0) {
?>
	<div class="error-info form-info">
		<?php foreach ($error as $handle) {
				echo "<p>$handle</p>";
		} ?>
	</div>
<?php
	}
?>

<form name="storeform" id="storeform" action="store-edit.php?action=<?php echo $store_id > 0 ? 'update&store='.$store_id : 'new'; ?>" method="post"> 
	<table class="formview"  width="100%" border="0" cellspacing="3px" cellpadding="0">
		<tr>
			<td class="LabelCell Required">Store</td>
			<td><input type="text" name="store_name" id="store_name" maxlength="50" class="input Required" value="<?php echo $store['store_name']; ?>" size="20" tabindex="10" /></td>
		</tr>
		<tr>
			<td class="LabelCell">Description</td>
			<td><input type="text" name="store_description" id="store_description" maxlength="255" class="input" value="<?php echo $store['store_description']; ?>" size="20" tabindex="20" /></td>
		</tr>
		<tr>
			<td class="LabelCell Required">Branch</td>
			<td><select tabindex="30" name="store_branch" class="GreaterThanZero">
			<option value="0">- Select -</option>
			<?php
				$branches = $branchObj->getAllBranches();
				foreach ($branches as $branch) {
					echo '<option value='.$branch['branch_id'].' '.($branch['branch_id'] == $store['branch_id'] ? 'selected' : '').'>'.$branch['branch_name'].'</option>';
				}
			?>
			</select></td>
		</tr>
		<tr>
			<td class="LabelCell">Address</td>
			<td><input type="text" name="store_address" id="store_address" maxlength="1000" class="input" value="<?php echo $store['store_address']; ?>" size="20" tabindex="40" /></td>
		</tr>
		<tr>
			<td class="LabelCell">Website</td>
			<td><input type="text" name="store_website" id="store_website" maxlength="5000" class="input" value="<?php echo $store['store_website']; ?>" size="20" tabindex="50" /></td>
		</tr>
		<tr>
			<td class="LabelCell">Collection</td>
			<td><input type="radio" name="store_collection" value="1" tabindex="60" <?php echo ($store['store_collection'] ? 'checked' : ''); ?>/> Yes 
			<input type="radio" name="store_collection" value="0" tabindex="60" <?php echo ($store['store_collection']  ? '' : 'checked'); ?>/> No</td>
		</tr>
		<tr>
			<td class="BottomToolBar" colspan="2">	
				<input type="submit" name="submit" id="submit" class="button-primary" value="Save" tabindex="70"/>
				<a href="stores.php" class="button-secondary" tabindex="60">Cancel</a>
			</td>			
		</tr>
	</table>
</form>
<?php
	$branchObj = null;
	$storeObj = null;
?>
<script type="text/javascript">
	$(document).ready(function() { 
		loadValidation('storeform');
	});
</script>