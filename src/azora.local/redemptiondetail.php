<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Redemption detail'; ?>

<?php	

	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('validation');
		
	DomainManager::getInstance()->load('Redemption');
	$redemptionObj = new Redemption();
	
	$config = Factory::getConfig();

	
	if(isset($_GET['id'])) {
	
		$redemption_id = Factory::getCryptographer()->Decrypt($_GET['id']);
		
		$redemption = $redemptionObj->getRedemption($redemption_id);
		
		if ($redemption == null) {
			header( 'Location: redemptions.php');
			exit();
		}			
		
	} else {
		header( 'Location: redemptions.php');
		exit();
	}

?>

<div id="righttitle">Redemption detail</div>
<div id="contentcontainer">


<form name="rdetailform" id="rdetailform" action="redemptiondetail.php" method="post"> 
	<table class="formview" width="100%" border="0" cellspacing="3px" cellpadding="3px">		
		<tr>			
			<td class="SectionBar" colspan="2">				
				Redemption
			</td>
		</tr>
		
		<tr>
			<td >Total redemption points</td>
			<td><input type="text" name="t_points" id="t_points"class="input" value="<?php echo $redemption['redemption']['total_points']; ?>" size="20" tabindex="30" disabled /></td>
		</tr>
		<tr>
			<td >Store</td>
			<td><input type="text" name="Store" id="Store" class="input" value="<?php echo $redemption['redemption']['store_branch_name']; ?>" size="20" tabindex="40" disabled /></td>			
		</tr>
		<tr>
			<td >Date <span class="hint">(DD/MM/YYYY)</span></td>
			<td><input type="text" name="date" id="date" class="input " value="<?php echo date('d/m/Y', strtotime($redemption['redemption']['collection_date']));  ?>" size="20" tabindex="50" disabled /></td>
		</tr>
		<tr>
			<td >Time</td>
			<td><input type="text" name="Time" id="Time" class="input " value="<?php echo date('h:i A', strtotime($redemption['redemption']['collection_time']));  ?>" size="20" tabindex="60" disabled /></td>
		</tr>
		<tr>			
			<td class="SectionBar" colspan="2">				
				Points
			</td>
		</tr>
		<tr>			
			<td colspan="2">				
				<table width="100%" border="0" cellspacing="3px" cellpadding="3px">						
					<?php							
						$tpoints = 0;
						foreach($redemption['redemption_points'] as $detail) {
					?>
						<tr>						
							<td><?php echo $detail['branch_name']; ?></td>
							<td><?php echo $detail['store_name']; ?></td>								
							<td width="70px"><input type="text" name="points[]" class="input" value="<?php 
									echo $detail['points'];
								?>" size="20" tabindex="70" style="width:50px" disabled /></td>
						</tr>
					<?php 
							$tpoints += $detail['points'];
						} ?>
					<tr>
						<td>&nbsp;</td>
						<td><b>Total</b></td>
						<td><span id="tpoints"><?php echo $tpoints; ?></span>&nbsp;pts</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr>			
			<td class="SectionBar" colspan="2">				
				Products
			</td>
		</tr>
		
		<tr>
			<td colspan="2">
				<table width="100%" border="0" cellspacing="3px" cellpadding="3px">							
					<tbody>		
						<?php							
							foreach($redemption['redemption_products'] as $item) {
								DomainManager::getInstance()->load('Product');
								$productObj = new Product();	
								$product = $productObj->getProduct($item['product_id']);
								
								$images = $product['product_images'];
								$image  = null;

								foreach($images as $img) {
									if ($img['image_type'] == 'S') {
										$image = $img;
									}													
								}
								
								if (!$image) {
									foreach($images as $img) {
										if ($img['image_type'] == 'T') {
											$image = $img;
										}													
									}
								}									
						?>
						<tr>								
							<td width="60px">
								<img src="<?php echo $config['PRMSConfig']->live_site . '/domain/images/products/' . $image['sys_file_name']; ?>" width="50" height="50" alt="<?php echo $product['product']['product_name']; ?>" title="<?php echo $product['product']['product_name']; ?>"/></td>
							<td><b><?php echo $product['product']['product_name']; ?></b></td>
							<td><?php echo $product['product']['product_description']; ?></td>
							<td width="80px">&nbsp;<?php echo number_format($item['product_points']); ?> pts</td>
							<td width="50px"><input type="text" name="quantity[]" class="input Required numeric" value="<?php echo $item['quantity'] ?>" size="20" tabindex="80" style="width:30px" disabled /></td>						
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</td>
		</tr>
		
		<tr>			
			<td class="SectionBar" colspan="2">				
				Redemption Status
			</td>
		</tr>
		
		
		<tr>		
			<td>Status</td>
			<td><?php echo $redemptionObj->getRedemptionStatus($redemption['redemption']['redemption_status']) ?></td>			
		</tr>
		
		<tr>			
			<td class="BottomToolBar" colspan="2">								
				<a href="redemptions.php" class="button-secondary" tabindex="120">Back</a>
			</td>			
		</tr>
	</table>
</form>

</div>
