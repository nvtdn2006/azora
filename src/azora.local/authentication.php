<?php require_once './includes/service.php'; ?>

<?php

	$config = Factory::getConfig();
	DomainManager::getInstance()->load('Customer');
	$customerObj = new Customer();
	
	DomainManager::getInstance()->load('Company');
	$companyObj = new Company();
	$company = $companyObj->getCompany();	
	$company = $company[0];

	
	$return = array();
	if ($_POST['form'] == 'login') {
		if ($_POST['username'] != '' && $_POST['password'] != '') {
		
			$nric = $_POST['username'];
			$password = $_POST['password'];
			
			if ($customerObj->isValidUser($nric)) {
				if ($customerObj->isActivatedUser($nric)) {
					if ($customerObj->isAuthorizedUser($nric, $password, $customer)) {
						$return['status'] = true;
						$return['url'] = $config['PRMSConfig']->live_site . '/index.php';						
						Authentication::SignIn($customer['customer_id']);
						Authentication::setAttribute('login_id', $customer['login_id']);						
						
						//Event Log
						$evtObj = new EventObject();					
						$evtObj->event_id = EventTypes::RWS_UserLogin;
						$evtObj->description = $customer['login_id'] . ' logged into Redemption Website.';
						$evtObj->action_by = $customer['login_id'];					
						EventLog::Log($evtObj);
					
					} else {
						$return['status'] = false;
						$return['message'] = 'Invalid NRIC/FIN or password.';
					}
				} else {
					$return['status'] = false;
					$return['message'] = 'Please activate your account.';
				}
			} else {
				$return['status'] = false;
				$return['message'] = 'Invalid NRIC/FIN.';
			}
		} else {
			$return['status'] = false;
			$return['message'] = 'Please enter NRIC/FIN and password.';
		}
	} else if ($_POST['form'] == 'logout') {
		$return['status'] = true;
		$return['url'] = $config['PRMSConfig']->live_site . '/index.php';						
		
		//Event Log
		$evtObj = new EventObject();					
		$evtObj->event_id = EventTypes::RWS_UserLogout;
		$evtObj->description = Authentication::getAttribute('login_id') . ' logged out from Redemption Website.';
		$evtObj->action_by = Authentication::getAttribute('login_id');					
		EventLog::Log($evtObj);

		Authentication::SignOut();
	} else if ($_POST['form'] == 'forgot') {		
		if ($_POST['email'] != '') {
			$email = $_POST['email'];			
			
			if ($customerObj->isExistEmail($email)) {
				if ($customerObj->resetPassword($email, $resetInfo)) {
					$mailer = new SiteMailer();
					
					$resetInfo['company_name'] = $company['company_name'];
					
					$mailer->toMail = $resetInfo['email'] ;
					$mailer->subject = 'Password reset at '.$company['company_name'].' redemption website';				
					$mailer->PrepareMail('sendPasswordResetToCustomer', $resetInfo);
					$mailer->Send();						
					$return['status'] = true;
					$return['message'] = 'Password has been sent to your mail.';
					
					//Event Log
					$evtObj = new EventObject();					
					$evtObj->event_id = EventTypes::RWS_ReqForgotPwd;
					$evtObj->description = $resetInfo['nric'] . ' requested for forgotten password.';
					$evtObj->action_by = $resetInfo['nric'];					
					EventLog::Log($evtObj);
						
				} else {
					$return['status'] = false;
					$return['message'] = 'Failed to reset.';
				}
			} else {
				$return['status'] = false;
				$return['message'] = 'Invalid email.';
			}
		} else {
			$return['status'] = false;
			$return['message'] = 'Please enter email.';
		}
	} else {
		//Nothing
	}
		
	header('Content-type: application/json');
	echo json_encode($return);
	
?>