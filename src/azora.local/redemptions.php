<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Redemptions'; ?>

<?php	

	if (!Authentication::isAuthenticated()) {
		header( 'Location: index.php');
	}
	
	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('validation');
	
	DomainManager::getInstance()->load('Redemption');
	$redemptionObj = new Redemption();
	
	$customer_id = Authentication::getUserId();		
	
	$redemptions = null;
	
	if (isset($_GET['action']) && $_GET['action'] == 'search') {
		if ($customer_id > 0) {
			$frommonth = date('n');
			$fromyear = date('Y');
			
			$tomonth = date('n');
			$toyear = date('Y');
			
			$status = 0;
			
			if (isset($_POST['frommonth'])) 
				$frommonth = $_POST['frommonth'];
				
			if (isset($_POST['fromyear'])) 
				$fromyear = $_POST['fromyear'];
				
			if (isset($_POST['tomonth'])) 
				$tomonth = $_POST['tomonth'];
				
			if (isset($_POST['toyear'])) 
				$toyear = $_POST['toyear'];
				
			if (isset($_POST['status'])) 
				$status = $_POST['status'];
				
			$fromdate = new DateTime($fromyear . '-' . $frommonth . '-' . '1');
			$todate = new DateTime($toyear . '-' . $tomonth . '-' . '1');
			$todate->modify('+1 month');
			$todate->modify('-1 day');
						
			$redemptions = $redemptionObj->getRedemptionByCustomer($customer_id, $fromdate, $todate, $status);			
		}
	}
	
	$formvalues = array();
	
	if (isset($_REQUEST['frommonth'])) 			
		$formvalues['frommonth'] = $_REQUEST['frommonth'];
	else
		$formvalues['frommonth'] =  0;
	
	if (isset($_REQUEST['fromyear'])) 			
		$formvalues['fromyear'] = $_REQUEST['fromyear'];
	else
		$formvalues['fromyear'] =  0;
		
	if (isset($_REQUEST['tomonth'])) 			
		$formvalues['tomonth'] = $_REQUEST['tomonth'];
	else
		$formvalues['tomonth'] =  0;
		
	if (isset($_REQUEST['toyear'])) 			
		$formvalues['toyear'] = $_REQUEST['toyear'];
	else
		$formvalues['toyear'] =  0;
		
	if (isset($_REQUEST['status'])) 			
		$formvalues['status'] = $_REQUEST['status'];
	else
		$formvalues['status'] =  0;
	
?>

<div id="righttitle">Redemptions</div>
<div id="contentcontainer">


<form name="redemptionsform" id="redemptionsform" action="redemptions.php?action=search" method="post"> 
	<table class="formview" width="100%" border="0">		
		<tr>
			<td><b>From</b></td>		
			<td>Month</td>		
			<td>				
				<select tabindex="50" name="frommonth" class="GreaterThanZero" tabindex="10">						
					<option value="0" <?php echo $formvalues['frommonth'] == 0 ? 'selected' : ''; ?>>- Select -</option>
					<option value="1" <?php echo $formvalues['frommonth'] == 1 ? 'selected' : ''; ?>>Jan</option>
					<option value="2" <?php echo $formvalues['frommonth'] == 2 ? 'selected' : ''; ?>>Feb</option>
					<option value="3" <?php echo $formvalues['frommonth'] == 3 ? 'selected' : ''; ?>>Mar</option>
					<option value="4" <?php echo $formvalues['frommonth'] == 4 ? 'selected' : ''; ?>>Apr</option>
					<option value="5" <?php echo $formvalues['frommonth'] == 5 ? 'selected' : ''; ?>>May</option>
					<option value="6" <?php echo $formvalues['frommonth'] == 6 ? 'selected' : ''; ?>>Jun</option>
					<option value="7" <?php echo $formvalues['frommonth'] == 7 ? 'selected' : ''; ?>>Jul</option>
					<option value="8" <?php echo $formvalues['frommonth'] == 8 ? 'selected' : ''; ?>>Aug</option>
					<option value="9" <?php echo $formvalues['frommonth'] == 9 ? 'selected' : ''; ?>>Sep</option>
					<option value="10" <?php echo $formvalues['frommonth'] == 10 ? 'selected' : ''; ?>>Oct</option>
					<option value="11" <?php echo $formvalues['frommonth'] == 11 ? 'selected' : ''; ?>>Nov</option>
					<option value="12" <?php echo $formvalues['frommonth'] == 12 ? 'selected' : ''; ?>>Dec</option>					
				</select>
			</td>			
			<td>Year</td>		
			<td>
				<select tabindex="50" name="fromyear" class="GreaterThanZero" tabindex="20">				
					<option value="0">- Select -</option>
					<?php
						$year = date('Y');
						for($i=0;$i<3;$i++) {
							echo '<option value="'.($year - $i).'"  '.($formvalues['fromyear'] == ($year - $i) ? 'selected' : '').'  >'.($year - $i).'</option>';							
						}
					?>
				</select>
			</td>			
		</tr>
		<tr>
			<td><b>To</b></td>		
			<td>Month</td>		
			<td>				
				<select tabindex="50" name="tomonth" class="GreaterThanZero" tabindex="30">						
					<option value="0" <?php echo $formvalues['tomonth'] == 0 ? 'selected' : ''; ?>>- Select -</option>
					<option value="1" <?php echo $formvalues['tomonth'] == 1 ? 'selected' : ''; ?>>Jan</option>
					<option value="2" <?php echo $formvalues['tomonth'] == 2 ? 'selected' : ''; ?>>Feb</option>
					<option value="3" <?php echo $formvalues['tomonth'] == 3 ? 'selected' : ''; ?>>Mar</option>
					<option value="4" <?php echo $formvalues['tomonth'] == 4 ? 'selected' : ''; ?>>Apr</option>
					<option value="5" <?php echo $formvalues['tomonth'] == 5 ? 'selected' : ''; ?>>May</option>
					<option value="6" <?php echo $formvalues['tomonth'] == 6 ? 'selected' : ''; ?>>Jun</option>
					<option value="7" <?php echo $formvalues['tomonth'] == 7 ? 'selected' : ''; ?>>Jul</option>
					<option value="8" <?php echo $formvalues['tomonth'] == 8 ? 'selected' : ''; ?>>Aug</option>
					<option value="9" <?php echo $formvalues['tomonth'] == 9 ? 'selected' : ''; ?>>Sep</option>
					<option value="10" <?php echo $formvalues['tomonth'] == 10 ? 'selected' : ''; ?>>Oct</option>
					<option value="11" <?php echo $formvalues['tomonth'] == 11 ? 'selected' : ''; ?>>Nov</option>
					<option value="12" <?php echo $formvalues['tomonth'] == 12 ? 'selected' : ''; ?>>Dec</option>					
				</select>
			</td>			
			<td>Year</td>		
			<td>
				<select tabindex="50" name="toyear" class="GreaterThanZero" tabindex="40">				
					<option value="0">- Select -</option>
					<?php
						$year = date('Y');
						for($i=0;$i<3;$i++) {
							echo '<option value="'.($year - $i).'"  '.($formvalues['toyear'] == ($year - $i) ? 'selected' : '').' >'.($year - $i).'</option>';							
						}
					?>
				</select>
			</td>			
		</tr>
		<tr>		
			<td>Status</td>
			<td colspan="4">				
				<select tabindex="50" name="status" tabindex="30">						
					<option value="0" <?php echo $formvalues['status'] == 0 ? 'selected' : ''; ?>>- All -</option>
					<option value="1" <?php echo $formvalues['status'] == 1 ? 'selected' : ''; ?>>New redemption</option>
					<option value="2" <?php echo $formvalues['status'] == 2 ? 'selected' : ''; ?>>Collected</option>
					<option value="3" <?php echo $formvalues['status'] == 3 ? 'selected' : ''; ?>>Expired</option>
				</select>
			</td>			
		</tr>		
		<tr>			
			<td colspan="5">				
				<input type="submit" name="search" id="search" class="button-primary" value="Search" tabindex="30"/>				
			</td>			
		</tr>
	</table>
</form>

<div class="SectionTitle" ><b><?php echo ($redemptions != null && count($redemptions) > 0) ? count($redemptions) : '0'; ?> </b> Record(s) found.</div>

<table class="tabular">
	<thead>
		<tr>				
			<th>Branch</th>		
			<th>Store</th>
			<th>Points</th>	
			<th>Submitted</th>	
			<th>Collection</th>			
			<th>Status</th>	
		</tr>
	</thead>
	<tbody>
		<?php
			if ($redemptions != null) {
			foreach($redemptions as $redemption) {
		?>
		<tr>			
			<td><a class="detailLink" href="redemptiondetail.php?id=<?php echo Factory::getCryptographer()->Encrypt($redemption['redemption_id']); ?>" alt="Select" title="Select"><?php echo $redemption['store_name']; ?></a></td>						
			<td><?php echo $redemption['branch_name']; ?></td>						
			<td width="50px"><?php echo $redemption['total_points']; ?></td>						
			<td width="150px"><?php echo date('Y-m-d h:i:s A', strtotime($redemption['transaction_dt'])); ?></td>			
			<td width="150px"><?php echo $redemption['collection_date'] . ' ' .  date('h:i:s  A', strtotime($redemption['collection_time'])); ?></td>
			<td><?php echo $redemptionObj->getRedemptionStatus($redemption['redemption_status']) ?></td>
		</tr>
		<?php } } ?>
	</tbody>
</table>

</div>


<script type="text/javascript">
	$(document).ready(function() { 
		loadValidation('redemptionsform');			
	});	
</script>