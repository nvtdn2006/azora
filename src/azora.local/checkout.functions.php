<?php
	function getMinPoint($list) {		
		$min = null;
		$first = true;
		foreach ($list as $li) {
			if ($first) {
				$min = $li;
				$first = false;
			} else {
				if ($min['accumulated_points'] > $li['accumulated_points']) {
					$min = $li;
				}
			}
		}
		return $min;
	}
?>