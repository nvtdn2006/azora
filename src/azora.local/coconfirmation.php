<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Checkout confirmation'; ?>

<?php	

	if (!Authentication::isAuthenticated()) {
		header( 'Location: index.php');
	}
	
        // FX.1304.005 - add validation js
        JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('validation');	
        
	$config = Factory::getConfig();
	
	DomainManager::getInstance()->load('Store');	
	$storeObj = new Store();

	$redemption = array();
	
	require_once dirname(__FILE__) . '/includes/cart.php';			
	$mypoints =	getPointsInCart();
	
	DomainManager::getInstance()->load('Point');
	$pointObj = new Point();
	
	$customer_id = Authentication::getUserId();	
	$pointdetatils = $pointObj->getPointDetailsByCustomer($customer_id);
	
	$validPointsDetails = array();
	foreach($pointdetatils as $detail) {
		if ($detail['accumulated_points'] > 0) {
			array_push($validPointsDetails, $detail);
		}
	}	
	
	if (Factory::getSession()->isExist('myredemption')) {			
		$redemption = Factory::getSession()->getValue('myredemption');
	} else {
		header( 'Location: checkout.php');
	}
	
	require_once dirname(__FILE__) . '/includes/cart.php';			
	$mycart = getCart();
	
	$error = array();
	
	if (isset($_POST['submit']) && $_POST['submit'] == 'Confirm & Submit'
                || (isset($_POST['hidden_submit']) && $_POST['hidden_submit'] == 'Confirm & Submit') // FX.1304.005  - if not find submit, check hidden submit
                ) {			
		$redemptionToSave = array();
					
		$cdate_year = substr($redemption['date'],6,4);
		$cdate_month = substr($redemption['date'],3,2);
		$cdate_day = substr($redemption['date'],0,2);
		$cdate = date("Y-m-d", mktime(0,0,0,$cdate_month,$cdate_day,$cdate_year));
			
		$header = array('customer_id' => $customer_id,
						'store_id' => $redemption['store_id'],
						'collection_date' => $cdate,
						'collection_time' => $redemption['time'] . ':00:00',
						'total_points' => $mypoints,
						'redemption_status' => 1);
		
		$redemptionToSave['redemption'] = $header;
		
		$pointList = array();
		foreach($redemption['points'] as $point) {
			$p = array ('store_id' => $point['store_id'],
						'points' => $point['accumulated_points']);
			array_push($pointList, $p);
		}
		$redemptionToSave['points'] = $pointList;
		
		
		$productList = array();
		foreach($mycart as $pd) {
			$p = array ('product_id' => $pd['id'],
						'product_points' => $pd['points'],
						'product_cost' => $pd['cost'],
						'quantity' => $pd['qty']);
			array_push($productList, $p);
		}
		$redemptionToSave['products'] = $productList;
		
		
		DomainManager::getInstance()->load('Redemption');
		$redemptionObj = new Redemption();
		if ($redemptionObj->submitRedemption($redemptionToSave)) {
		
			//Event Log
			$evtObj = new EventObject();					
			$evtObj->event_id = EventTypes::RWS_SubmitRedemption;
			$evtObj->description = Authentication::getAttribute('login_id') . ' submitted redemption application (No. : '.$redemptionToSave['redemption']['redemption_no'].').';
			$evtObj->action_by = Authentication::getAttribute('login_id');					
			EventLog::Log($evtObj);
			
			
			
			$itemTable = '<table border="1" cellspacing="3px" cellpadding="3px">							
						<thead>
							<tr>			
								<th>Product Name</th>
								<th>Points</th>
								<th>Quantity</th>								
							</tr>
						</thead>
						<tbody>';
						
						
			DomainManager::getInstance()->load('Product');
			$productObj = new Product();	
			foreach($mycart as $item) {
				
				$product = $productObj->getProduct($item['id']);
				
				$images = $product['product_images'];
				$image  = null;

				foreach($images as $img) {
					if ($img['image_type'] == 'S') {
						$image = $img;
					}													
				}
				
				if (!$image) {
					foreach($images as $img) {
						if ($img['image_type'] == 'T') {
							$image = $img;
						}													
					}
				}									
				
				
				$itemTable .= '<tr>																
					<td><b>'.$product['product']['product_name'].'</b></td>								
					<td width="80px">'.number_format($product['product']['product_points']).'pts</td>
					<td width="50px">'.$item['qty'].'</td>						
				</tr>';
				
			} 
							
			$itemTable .= '</tbody>
				</table>';
			
			
			DomainManager::getInstance()->load('Company');
			$companyObj = new Company();
			$company = $companyObj->getCompany();
			$company = $company[0];
			
			DomainManager::getInstance()->load('Customer');
			$customerObj = new Customer();
			$customer = $customerObj->getCustomerProfile($customer_id);
			
			$store = $storeObj->getStore($redemption['store_id']);
			//Send Mail							
			$property = array ('name' => $customer['name'],
								'cdate' => date('j-M-Y, h:i:s A', strtotime($cdate . $redemption['time'] . ':00:00')),
								'store' => $store['store_branch_name'],
								'items' => $itemTable,
								'csmail' => $company['company_customersupport_email'],
								'company_name' => $company['company_name']);

			$mailer = new SiteMailer();
			$mailer->toMail = $customer['email'];
                        
                        //+START FX.1304.002 Add Default CC email 
                        if (isset($config['PRMSConfig']->PRMS_redemption_cc_mail) 
                                && $config['PRMSConfig']->PRMS_redemption_cc_mail != "") {
                            if (is_array($config['PRMSConfig']->PRMS_redemption_cc_mail)) {
                                foreach ( $config['PRMSConfig']->PRMS_redemption_cc_mail as $ccMail) {
                                    $mailer->AddCc($ccMail);
                                }
                            } else {
                                $mailer->AddCc($config['PRMSConfig']->PRMS_default_cc_mail);
                            }
                        }
                        //-END FX.1304.002
                        
			$mailer->subject = 'You have submitted a new redemption application';				
			$mailer->PrepareMail('sendRedemptionSubmittedNotify', $property);
			$mailer->Send();
			
		
			header( 'Location: submitted.php');					
		} else {
			array_push($error, 'Falied to submit redemption application. Please try again.');
		}
	}
	
?>


<div id="righttitle">Checkout confirmation</div>
<div id="contentcontainer">

<?php
	if (isset($error) && count($error) > 0) {
?>
	<div class="error-info form-info">
		<?php foreach ($error as $handle) {
				echo "<p>$handle</p>";
		} ?>
	</div>
<?php
	}
?>

	<form name="checkoutform" id="checkoutform" action="coconfirmation.php" method="post"> 
		<table class="formview" width="100%" border="0" cellspacing="3px" cellpadding="3px">
			<tr>
				<td class="LabelCell Disabled">Total redemption points</td>
				<td><input type="text" name="t_points" id="t_points" maxlength="9" class="input" value="<?php echo $mypoints; ?>" size="20" tabindex="10" disabled /></td>
			</tr>
			<tr>			
				<td class="SectionBar" colspan="2">				
					Please select your prefer location & datetime to collect
				</td>
			</tr>
			<tr>
				<td class="LabelCell Required">Store</td>
				<td>					
					<select tabindex="20" name="store" class="GreaterThanZero" disabled>						
						<?php
							$stores = $storeObj->getCollectionStores();							
							echo '<option value="0">- Select -</option>';
								
							foreach ($stores as $store) {
								echo '<option value='.$store['store_id'].' '.($store['store_id'] == $redemption['store_id'] ? 'selected' : '') .'>'.$store['store_name'].'</option>';
							}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td class="LabelCell Required">Date <span class="hint">(DD/MM/YYYY)</span></td>
				<td><input type="text" name="date" id="date" maxlength="10" class="input Required ValidDate" value="<?php echo $redemption['date']; ?>" size="20" tabindex="30" disabled /></td>
			</tr>
			<tr>
				<td class="LabelCell Required">Time</td>
				<td>
					<select tabindex="30" name="time" class="GreaterThanZero" tabindex="40" disabled>						
						<option value="0" <?php echo (0 == $redemption['time'] ? 'selected' : ''); ?>>- Select -</option>	
						<?php
							$startime = $config['PRMSConfig']->collection_starttime;
							$counter = 0;
							for ($i=0;$i<24;$i++) {
								$time = $startime + $counter;
								if ($time > 23) {
									$time = 0;
									$startime = 0;
									$counter = 0;
								}
								$counter++;
								$timedesc = '';
								if ($time == 0) {
									$timedesc =  '0' . $time . ':00 AM';									
								} else if ($time <= 11) {
									$timedesc =  str_pad($time, 2, "0", STR_PAD_LEFT)  . ':00 AM';
								} else if ($time == 12) {
									$timedesc = $time . ':00 PM';
								} else if ($time > 12) {
									$timedesc = str_pad(($time - 12), 2, "0", STR_PAD_LEFT) . ':00 PM';
								}								
								echo '<option value="'.($time == 0 ? '24' : $time).'" '.(($time == 0 ? 24 : $time) == $redemption['time'] ? 'selected' : '') .'>'.$timedesc.'</option>';								
							}
							
						?>						
					</select>
				</td>
			</tr>
			<tr>			
				<td class="SectionBar" colspan="2">				
					Points will be deducted from
				</td>
			</tr>
			<tr>			
				<td colspan="2">				
					<table width="100%" border="0" cellspacing="3px" cellpadding="3px">						
						<?php							
							$tpoints = 0;
							foreach($validPointsDetails as $detail) {
						?>
							<tr>						
								<td><?php echo $detail['branch_name'] ?></td>
								<td><?php echo $detail['store_name'] ?></td>								
								<td width="70px"><input type="text" name="points[]" class="input Required numeric" value="<?php 
										if (isset($redemption['points'])) {																					
											$points = $redemption['points'];
											foreach ($points as $li) {
												if ($li['store_id'] == $detail['store_id']) {
													echo $li['accumulated_points'];
													$tpoints += $li['accumulated_points'];
												}
											}												
										}
									?>" size="20" tabindex="50" style="width:50px" disabled /></td>
							</tr>
						<?php 
							} ?>
						<tr>
							<td>&nbsp;</td>
							<td><b>Total</b></td>
							<td><span id="tpoints"><?php echo $mypoints; ?></span>&nbsp;pts</td>
						</tr>
					</table>
				</td>
			</tr>
			
			<tr>			
				<td class="SectionBar" colspan="2">				
					Products in cart
				</td>
			</tr>
			
			<tr>
				<td colspan="2">
					<table width="100%" border="0" cellspacing="3px" cellpadding="3px">							
						<tbody>		
							<?php	
								DomainManager::getInstance()->load('Product');
								$productObj = new Product();	
									
								foreach($mycart as $item) {
									
									$product = $productObj->getProduct($item['id']);
									
									$images = $product['product_images'];
									$image  = null;

									foreach($images as $img) {
										if ($img['image_type'] == 'S') {
											$image = $img;
										}													
									}
									
									if (!$image) {
										foreach($images as $img) {
											if ($img['image_type'] == 'T') {
												$image = $img;
											}													
										}
									}									
							?>
							<tr>								
								<td width="60px">
									<img src="<?php echo $config['PRMSConfig']->live_site . '/domain/images/products/' . $image['sys_file_name']; ?>" width="50" height="50" alt="<?php echo $product['product']['product_name']; ?>" title="<?php echo $product['product']['product_name']; ?>"/></td>
								<td><b><?php echo $product['product']['product_name']; ?></b></td>
								<td><?php echo $product['product']['product_description']; ?></td>
								<td width="80px">&nbsp;<?php echo number_format($product['product']['product_points']); ?> pts</td>
								<td width="50px"><input type="text" name="quantity[]" class="input Required numeric" value="<?php echo $item['qty'] ?>" size="20" tabindex="10" style="width:30px" disabled /></td>						
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</td>
			</tr>
			
			<tr>			
				<td class="BottomToolBar" colspan="2">				
					<input type="submit" name="submit" id="submit" class="button-primary" value="Confirm & Submit" tabindex="60"/>
					<a href="checkout.php" class="button-secondary" tabindex="70">Back</a>					
				</td>			
			</tr>
		</table>
	</form>
	
</div>

<? // FX.1304.005  - add script for check out ?>
<script type="text/javascript">
	$(document).ready(function() { 
		loadValidation('checkoutform');
	});
</script>