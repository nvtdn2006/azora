<?php
class CSSManager
{
	private static $instance;
	
	private $css = array();
			
    public static function getInstance() {
        if (!self::$instance)
            self::$instance = new CSSManager();					
        return self::$instance;
    }
	
	public function add($path) {				
		if (!in_array($path, $this->css))
			array_push($this->css, $path);
    }
	
	public function load() {
		$config = Factory::getConfig();
		foreach($this->css as $handle)
			echo  '<link rel="stylesheet" type="text/css" media="all" href="'.$config['PRMSConfig']->live_site . $handle.'" />';
    }
	
	public function get() {
		$config = Factory::getConfig();
		$css = '';
		foreach($this->css as $handle)
			$css .= '<link rel="stylesheet" type="text/css" media="all" href="'.$config['PRMSConfig']->live_site . $handle.'" />';
		return $css;
    }
}
?>