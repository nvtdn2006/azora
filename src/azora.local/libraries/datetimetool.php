<?php
class DateTimeTool
{

	//Need to enhance this library
	
	public static function convertStringToDate($svalue) {	
		$dateValue = null;
		
		$_year = substr($svalue,6,4);
		$_month = substr($svalue,3,2);
		$_day = substr($svalue,0,2);
		$dateValue = date("Y-m-d", mktime(0,0,0,$_month, $_day, $_year));
		
		return $dateValue;
	}
}
?>