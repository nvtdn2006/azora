<?php
abstract class Factory
{
	private static $config = null;	
	private static $cryptographer = null;
	private static $session = null;	
	
	public static function getConfig()
	{
		if (!self::$config) {
			Loader::load('PRMSConfig');
			self::$config = array('PRMSConfig' => new PRMSConfig());
		}

		return self::$config;
	}
	
	public static function getCryptographer()
	{
		if (!self::$cryptographer) {
			Loader::load('Cryptographer');
			self::$cryptographer = new Cryptographer();
		}

		return self::$cryptographer;
	}
	
	public static function getSession()
	{
		if (!self::$session) {
			Loader::load('Session');
			self::$session = new Session();
		}

		return self::$session;
	}
}
?>