<?php
abstract class Application 
{
	function __construct() {	
		$this->setdefines();
		$this->importCoreLibraries();
		$this->startSession();
		$this->loadAuthentication();
		$this->setTimezone();
		$this->loadJSManager();
		$this->loadCSSManager();
	}
	
	private function setdefines() {	
		require_once('defines.php');		
	}
	
	private function importCoreLibraries() {		
		if (!class_exists('Loader')) {
			require_once 'loader.php';
			Loader::registerTheLibraries();
		}
		
		if (!class_exists('Factory'))
			require_once 'factory.php';			
			
		Loader::load('Module');	
	}
	
	private function startSession() {
		Factory::getSession()->start();		
	}
	
	private function loadAuthentication() {
		Loader::load('Authentication');
	}
	
	private function setTimezone() {
		$config = Factory::getConfig();
		$timezone = $config['PRMSConfig']->timezone;
		if(function_exists('date_default_timezone_set')) date_default_timezone_set($timezone);
	}
	
	private function loadJSManager() {
		Loader::load('JSManager');
	}
	
	private function loadCSSManager() {
		Loader::load('CSSManager');
	}
}
?>