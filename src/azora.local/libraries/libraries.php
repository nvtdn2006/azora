<?php
function getLibraries() {
	return array(
				'PRMSConfig' => WEB_ROOT . '/config/PRMSConfig.php',
				'Theme' => WEB_ROOT . '/libraries/theme.php',
				'Module' => WEB_ROOT . '/libraries/module.php',
				'Database' => WEB_ROOT . '/libraries/database.php',
				'Cryptographer' => WEB_ROOT . '/libraries/cryptographer.php',
				'Session' => WEB_ROOT . '/libraries/session.php',
				'Authentication' => WEB_ROOT . '/libraries/authentication.php',
				'JSManager' => WEB_ROOT . '/libraries/jsmanager.php',
				'CSSManager' => WEB_ROOT . '/libraries/cssmanager.php',
				'DateTimeTool' => WEB_ROOT . '/libraries/datetimetool.php',
				);
}
?>