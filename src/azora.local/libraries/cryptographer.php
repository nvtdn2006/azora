<?php
class Cryptographer 
{

	function __construct() {	
		//empty
	}
	
	public function Encrypt($string) {
		$config = Factory::getConfig();
		$key = $config['PRMSConfig']->cryptography_key;
		$encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->MD5($key), $string, MCRYPT_MODE_CBC, $this->MD5($this->MD5($key))));
		return $encrypted;
	}
	
	public function Decrypt($string) {
		$config = Factory::getConfig();
		$key = $config['PRMSConfig']->cryptography_key;
		$decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->MD5($key), base64_decode($string), MCRYPT_MODE_CBC, $this->MD5(self::MD5($key))), "\0");
		return $decrypted;
	}
	
	public function MD5($string) {
		$config = Factory::getConfig();
		$salt = $config['PRMSConfig']->cryptography_key;
		$md5 = md5($string.$salt);
		return $md5;
	}
	
	public function GenerateSalt($length = 3) 
	{		
		$allowsaltChr = "#$%&()*-/0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[]^abcdefghijklmnopqrstuvwxyz{|}~";		
		$salt = '';
		for ($i = 0; $i < $length; $i++) {
			$salt .= $allowsaltChr[rand(0,strlen($allowsaltChr)-1)];
		}		
		return $salt;		
	}
	
	public function GenerateAlphanumeric($length = 3) 
	{			
		$allowedChr = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		$alphanumeric = '';
		for ($i = 0; $i < $length; $i++) {
			$alphanumeric .= $allowedChr[rand(0,strlen($allowedChr)-1)];
		}
		return $alphanumeric;			
	}
}
?>