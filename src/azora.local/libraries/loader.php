<?php
class Loader
{
	private static $libraries = array();
	
	public static function registerTheLibraries() {		
		require_once('libraries.php');
		self::$libraries = getLibraries();
	}
	
	public static function load($key) {
		if (file_exists(self::$libraries[$key]))
			require_once(self::$libraries[$key]);
		else
			throw new Exception('Library could not found.');
	}	
}
?>