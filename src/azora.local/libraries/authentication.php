<?php
class Authentication
{

	public static $process = 'back';
	
	private static $auth_key = "AKY";
	private static $auth_value = "TRUE-PRMS-VU";
	
	private static $user_id = "UID";
	private static $last_login = "LLI";
	
	private static $auth_attributes_key = "AATTK";
	
	function __construct() {	
		//empty
	}
	
	public static function SignIn($user_id) {
		$cryptographer = Factory::getCryptographer();
		Factory::getSession()->setValue(self::$auth_key, $cryptographer->MD5(self::$process .'-'. self::$auth_value));
		Factory::getSession()->setValue(self::$user_id, $user_id);
		Factory::getSession()->setValue(self::$last_login, date("M j, Y, g:i a"));		
	}
	
	public static function SignOut() {
		Factory::getSession()->clear();
	}
	
	public static function isAuthenticated() {
		$cryptographer = Factory::getCryptographer();
		return (Factory::getSession()->getValue(self::$auth_key) == $cryptographer->MD5(self::$process .'-'. self::$auth_value));	
	}
	
	public static function getUserId() {
		$user_id = null;
		if (self::isAuthenticated())
			$user_id = Factory::getSession()->getValue(self::$user_id);		
		return $user_id;
	}
	
	public static function getLoggedInDateTime() {
		$last_login = null;
		if (self::isAuthenticated())
			$last_login = Factory::getSession()->getValue(self::$last_login);		
		return $last_login;
	}
	
	public static function getAttributes() {
		$value = null;
		if (self::isAuthenticated()) 
			$value = Factory::getSession()->getValue(self::$auth_attributes_key);
		return $value;
	}
	
	public static function setAttributes($value) {
		if (self::isAuthenticated()) 
			Factory::getSession()->setValue(self::$auth_attributes_key, $value);
	}
	
	public static function setAttribute($key, $value) {
		if (self::isAuthenticated()) {
			$attributes = self::getAttributes();
			if (isset($attributes)) {
				$attributes[$key] = $value;
			} else {
				$attributes = array();
				$attributes[$key] = $value;
			}			
			self::setAttributes($attributes);
		}
	}
	
	public static function getAttribute($key) {
		$value = null;
		if (self::isAuthenticated()) {
			$attributes = self::getAttributes();			
			if (isset($attributes))
				$value = $attributes[$key];		
		}
		return $value;
	}
}
?>