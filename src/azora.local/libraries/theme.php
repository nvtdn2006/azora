<?php
class Theme
{
	private $config;

	private $the_title;	
	private $the_content;	
		
	private $theme_side;	
	private $theme_name;	
	private $template_name;	
	
	function __construct() {		
		$this->config = Factory::getConfig();		
	}
		
	function initWithThemeOptions($options) {
		$this->the_title = $options['title'];
		$this->the_content = $options['content'];				
		$this->theme_side = $options['theme_side'];
		$this->theme_name = $options['theme_side'] == 'front' ? $this->config['PRMSConfig']->front_theme : $this->config['PRMSConfig']->back_theme;
		$this->template_name = $options['template'] == '' ? 'index' : $options['template'];
	}
	
	private function getThemePath() {
		return WEB_ROOT .DS. ($this->theme_side == 'front' ? '' : 'prms') .DS. 'themes' .DS. $this->theme_name;	
	}
	
	function applyTheme() {
		ob_start();			
		include($this->getThemePath() .DS. $this->template_name .'.php');
		$content = ob_get_contents();
		
		$content = $this->replacePlaceHolder('css', CSSManager::getInstance()->get(), $content);
		$content = $this->replacePlaceHolder('js', JSManager::getInstance()->get(), $content);
		
		ob_end_clean(); 
		echo $content;
	}
	
	function theTitle() {
		echo $this->the_title;		
	}
	
	function theContent() {
		echo $this->the_content;		
	}
	
	function theThemeURL() {
		echo $this->config['PRMSConfig']->live_site .($this->theme_side == 'front' ? '/themes/' : '/prms/themes/'). $this->theme_name;		
	}
	
	function theSiteURL() {
		echo $this->config['PRMSConfig']->live_site;
	}
	
	function loadModule($mod_name) {
		Module::load(WEB_ROOT .DS. ($this->theme_side == 'front' ? '' : 'prms') .DS. 'modules' .DS. $mod_name .DS. $mod_name. '.php');	
	}
	
	function setPlaceHolder($name = '') {
		echo '{%placeholder_'.$name.'%}';	
	}
	
	private function replacePlaceHolder($name = '', $newcontent, $content) {		
		return preg_replace('/\{\%placeholder_'.$name.'\%\}/', $newcontent, $content);
	}
}
?>