<?php
class Session
{
	private $started = false;
	
	function __construct() {	
		//empty		
	}
	
	public function start() {
		session_start();
		$this->started = true;
	}
	
	public function setValue($key, $value) {
		if($this->started) 
			$_SESSION[$key] = $value;
	}
	
	public function getValue($key) {	
		$value = null;
		if ($this->started) {
			if(isset($_SESSION[$key]))
				$value = $_SESSION[$key];
		}
		return $value;		
	}
	
	public function isExist($key) {	
		$isexist = null;
		if ($this->started) {
			if(isset($_SESSION[$key]))
				$isexist = true;
			else
				$isexist = false;
		}
		return $isexist;		
	}
	
	public function removeKey($key) {		
		if ($this->started) {
			if(isset($_SESSION[$key]))
				unset($_SESSION[$key]); 		
		}
	}
	
	public function clear() {		
		if ($this->started)
			session_destroy();	
	}
	
	public function getSessionID() {
		$id = null;
		if ($this->started) {
			$id = SID;
		}
		return $id;
	}
}
?>