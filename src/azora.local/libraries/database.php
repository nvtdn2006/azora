<?php
class Database 
{
	private static $instance;
	
	private static $dbInstance;
	
    public static function getInstance() {
        if (!self::$instance)
            self::$instance = new Database();					
        return self::$instance;
    }
	
	public function getConnection() {
		try {
			if (!self::$dbInstance) {
				$config = Factory::getConfig();
				self::$dbInstance = new PDO($config['PRMSConfig']->DB_DSN, $config['PRMSConfig']->DB_User, $config['PRMSConfig']->DB_Password);
				$this->setSessionTimeZone(self::$dbInstance);
			}
		}
		catch(PDOException $e) {
			throw new Exception($e->getMessage());
		}
        return self::$dbInstance;
    }
	
	private function setSessionTimeZone($dbh) {
		try {
		
			$config = Factory::getConfig();
		
			$sth = $dbh->prepare('SET SESSION time_zone = \''.$config['PRMSConfig']->timezone.'\';');		
			$sth->execute();
			$result = ($sth->rowCount() > 0);
			$sth = null;
			$dbh = null;
		}
		catch(PDOException $e) {
			throw new Exception($e->getMessage());
		}
	}
}
?>