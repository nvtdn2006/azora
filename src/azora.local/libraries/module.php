<?php
class Module
{
	static function load($mod_path) {		
		$module_content = '';
		ob_start();			
		include($mod_path);
		$module_content = ob_get_contents();
		ob_end_clean();
		echo $module_content;
	}
}
?>