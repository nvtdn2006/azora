<?php
class JSManager
{
	private static $instance;
	
	private $scripts = array();
	
	public $compress = 1;
	
    public static function getInstance() {
        if (!self::$instance)
            self::$instance = new JSManager();					
        return self::$instance;
    }
	
	public function add($key) {				
		if (!in_array($key, $this->scripts)) {			
			array_push($this->scripts, $key);
		}
    }
	
	public function load() {
		$js = implode(',', $this->scripts); 
		$config = Factory::getConfig();
		if ($js != '')
			echo '<script type="text/javascript" src="'.$config['PRMSConfig']->live_site.'/includes/loadscripts.php?c='.$this->compress.'&load='.$js.'"></script>';
    }
	
	public function get() {
		$js = implode(',', $this->scripts); 
		$config = Factory::getConfig();
		if ($js != '')
			return '<script type="text/javascript" src="'.$config['PRMSConfig']->live_site.'/includes/loadscripts.php?c='.$this->compress.'&load='.$js.'"></script>';
		else 
			return '';
    }
}
?>