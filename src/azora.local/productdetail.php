<?php require_once './includes/application.php'; $this->template = ''; $this->title = 'Product detail' ?>

<?php	

	if (!Authentication::isAuthenticated()) {
		header( 'Location: index.php');
	}
	
	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('validation');		
	JSManager::getInstance()->add('fancybox');	
	CSSManager::getInstance()->add('/js/fancybox/jquery.fancybox-1.3.4.css');
	JSManager::getInstance()->add('numeric');
	
	$config = Factory::getConfig();
	
	$product = null;
	$quantity = 1;
	
	$error = array();
	
	if(isset($_GET['id']) && $_GET['id'] > 0) {
	
		$product_id = $_GET['id'];
	
		DomainManager::getInstance()->load('Product');
		$productObj = new Product();	
	
		$product = $productObj->getProduct($product_id );		
		
		if (count($product) > 0 && $product['product']['product_id'] != '' && $product['product']['product_status'] == 1) {
		
		} else {
			$product = null;
		}
	}
	
	if(isset($_GET['action']) && $_GET['action'] == 'addtocart') {
		$firstload = false;
		
		$qty = 0;
		
		if (isset($_POST['quantity'])) 	
			$qty = $_POST['quantity'];
			
		if ($qty > 0) {
		
			if ($product['product_balance']['product_balance'] >= $qty) {
			
				DomainManager::getInstance()->load('Point');
				$pointObj = new Point();	
				
				require_once dirname(__FILE__) . '/includes/cart.php';
				
				$pointsinCart = getPointsInCart();
				
				$points = $pointObj->getPointByCustomer(Authentication::getUserId());
				
				if (($points - $pointsinCart) >= ($qty * $product['product']['product_points'])) {
				
					$item = array ('id' => $product['product']['product_id'],
									'qty' => $qty,
									'points' => $product['product']['product_points'],
									'cost' => $product['product']['product_cost']);
					if (addToCart($item)) 
						header( 'Location: addedtocart.php');
					else
						array_push($error, 'Failed to add product to your cart. Plesae try again.');
					
				} else {
					array_push($error, 'Currently, you have insufficient points in your remaining balance.');
				}
				
			} else {
				array_push($error, 'Insufficient product quantity. Maximum '. $product['product_balance']['product_balance']. ' quantity you can redeem for this product.');
			}
		} else {
			array_push($error, 'Please enter number of quantity.');
		}			
	}
	
	if (isset($_REQUEST['quantity'])) 			
		$quantity = $_REQUEST['quantity'];
	else
		$quantity =  1;
	
?>


<div id="righttitle">Product detail</div>
<div id="contentcontainer">
	
	<?php
	if (isset($error) && count($error) > 0) {
	?>
		<div class="error-info form-info">
			<?php foreach ($error as $handle) {
					echo "<p>$handle</p>";
			} ?>
		</div>
	<?php
	}
	?>

	<?php if ($product != null) { 
			
		$images = $product['product_images'];
		$image  = null;
		$largeimage  = null;
		foreach($images as $img) {
			if ($img['image_type'] == 'T') {
				$image = $img;
			}					
			if ($img['image_type'] == 'L') {
				$largeimage = $img;
			}					
		}
	?>
	
		<form name="addtocartform" id="addtocartform" action="productdetail.php?action=addtocart&id=<?php echo $product['product']['product_id']; ?>" method="post"> 
			<table class="formview" width="100%" border="0">		
				<tr>
					<td rowspan="8" width="200px" align="center">
						<img src="<?php echo $config['PRMSConfig']->live_site . '/domain/images/products/' . $image['sys_file_name']; ?>" width="155" height="155" alt="<?php echo $product['product']['product_name']; ?>" title="<?php echo $product['product']['product_name']; ?>"/>
						</br></br>
						<?php if ($largeimage != null) { ?>
							<a class="fancybox" href="<?php echo $config['PRMSConfig']->live_site; ?>/domain/images/products/<?php echo $image['sys_file_name']; ?>">Click to enlarge</a>
						<?php } ?>
					</td>		
					<td class="SectionBar">				
						Product
					</td>
				</tr>
				<tr>
					<td>				
						<?php echo $product['product']['product_name']; ?>
					</td>								
				</tr>				
				<tr>					
					<td class="SectionBar">				
						Description
					</td>								
				</tr>
				<tr>
					<td>				
						<?php echo $product['product']['product_description']; ?>
					</td>								
				</tr>
				<tr>					
					<td class="SectionBar">				
						Points
					</td>								
				</tr>
				<tr>
					<td>				
						<?php echo $product['product']['product_points']; ?>
					</td>								
				</tr>
				<tr>					
					<td class="SectionBar">				
						Quantity
					</td>								
				</tr>
				<tr>
					<td>				
						<input type="text" name="quantity" id="quantity" class="input Required GreaterThanZero numeric" value="<?php echo $quantity; ?>" size="20" tabindex="10" />
					</td>								
				</tr>
				<tr>			
					<td colspan="2" class="BottomToolBar">				
						<input type="submit" name="search" id="search" class="button-primary" value="Add to cart" tabindex="20"/>
						<a class="button-secondary" href="products.php">Cancel</a>						
					</td>			
				</tr>
			</table>
		</form>
	
	<?php } else { ?>
		<div class="cprocess">
			<b>The product you requested temporarily not available in system.</b>
		</div>	
	<?php } ?>
	
</div>

<?php
	$product = null;
	$productObj = null;	
?>
<script type="text/javascript">
	$(document).ready(function() { 
		loadValidation('addtocartform');
		$('a.fancybox').fancybox();	
		$(".numeric").numeric(false);		
	});	
</script>