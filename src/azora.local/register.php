<?php require_once './includes/application.php'; $this->template = 'registration'; $this->title = 'Registration'; ?>

<?php	
	JSManager::getInstance()->add('jquery');
	JSManager::getInstance()->add('validation');
	JSManager::getInstance()->add('jquery.ui');			
	CSSManager::getInstance()->add('/js/jqueryui/smoothness/jquery-ui-1.8.16.custom.css');	
	
	$config = Factory::getConfig();
	$cryptographer = Factory::getCryptographer();
	
	$error = array();
	
	DomainManager::getInstance()->load('Customer');
	$customerObj = new Customer();
	
	$customer = array();
	
	DomainManager::getInstance()->load('Company');
	$companyObj = new Company();
	$company = $companyObj->getCompany();	
	$company = $company[0];
	
	if ( (isset($_POST['register']) && $_POST['register'] == 'Register') 
             || (isset($_POST['hidden_submit']) && $_POST['hidden_submit'] == 'Register')   // FX.1304.005  - if not find submit, check hidden submit
                ) {
		
		$isValid = true;
		
		if (!empty($_POST['captcha'])) {	
			if (empty($_POST['captcha']) || trim(strtolower($_POST['captcha'])) != $_SESSION['captcha']) {
				$isValid = false;
				array_push($error, 'Invalid verification code.');
			} else {
				if ($_POST['email'] != $_POST['reenteremail']) {
					$isValid = false;
					array_push($error, 'Emails do not match.');
				}
				
				if ($_POST['password'] != $_POST['reenterpassword']) {
					$isValid = false;
					array_push($error, 'Passwords do not match.');
				}
			}			
			unset($_SESSION['captcha']);
		} else {
			$isValid = false;
			array_push($error, 'Invalid verification code.');
		}
		
		$dob_year = substr($_POST['dob'],6,4);
		$dob_month = substr($_POST['dob'],3,2);
		$dob_day = substr($_POST['dob'],0,2);
		$dob = date("Y-m-d", mktime(0,0,0,$dob_month,$dob_day,$dob_year));
		
		if ($isValid) {
			if (!checkdate($dob_month, $dob_day, $dob_year)) {		
				$isValid = false;
				array_push($error, 'Invalid date of birth.');	
			}
		}		
		
		if ($isValid) {		
			$today = getdate();
			$today = date("Y-m-d", mktime(0,0,0,$today['mon'],$today['mday'],$today['year']));
			if ( $dob >= $today ) {
				$isValid = false;
				array_push($error, 'Invalid date of birth.');	
			}					
		}		
		
		if ($isValid) {
			if ($customerObj->isExistNRIC($_POST['nric'])) {				
				$isValid = false;
				array_push($error, 'Your NRIC/FIN is already exist in system, please re-enter.');	
			}
		}
		
		if ($isValid) {
			if ($customerObj->isExistEmail($_POST['email'])) {				
				$isValid = false;
				array_push($error, 'Your email is already exist in system, please re-enter.');	
			}
		}
		
		if ($isValid) {
			if ($customerObj->isExistCustomer($_POST['nric'], $_POST['email'])) {				
				$isValid = false;
				array_push($error, 'User already exists in system, please check your NRIC/FIN or Email.');	
			}
		}
		
		
		if ($isValid) {
			
			$customer = array('nric' => $_POST['nric'],
									'name' => $_POST['name'],
									'dob' => $dob,
									'address' => $_POST['address'],
									'company' => $_POST['company'],
									'mobile' => $_POST['mobile'],
									'home' => $_POST['home'],
									'email' => $_POST['email'],
									'password' => $_POST['password']);			
			
			if ($customerObj->registerCustomer($customer)) {			
				//$activation_link = $config['PRMSConfig']->live_site . '/activate.php?atk=' . $customer['activation_key'] . '&id=' . $cryptographer->Encrypt($customer['id']);
				$activation_link = $config['PRMSConfig']->live_site . '/activate.php?atk=' . $customer['activation_key'] . '&id=' . $customer['id'];
				
				$customer['activation_link'] = $activation_link;
				$customer['company_name'] = $company['company_name'];
				
				$mailer = new SiteMailer();
				
				$mailer->toMail = $customer['email'] ;
				$mailer->subject = 'Welcome to '.$company['company_name'].' redemption website';				
				$mailer->PrepareMail('sendActivationToCustomer', $customer);
				if ($mailer->Send())
					header( 'Location: complete.php');		
			}
		}
	
	}
	
	if (isset($_REQUEST['nric'])) 			
		$customer['nric'] = $_REQUEST['nric'];
	else
		$customer['nric'] =  '';
		
	if (isset($_REQUEST['name'])) 			
		$customer['name'] = $_REQUEST['name'];
	else
		$customer['name'] =  '';
		
	if (isset($_REQUEST['dob'])) 			
		$customer['dob'] = $_REQUEST['dob'];
	else
		$customer['dob'] =  '';
		
	if (isset($_REQUEST['address'])) 			
		$customer['address'] = $_REQUEST['address'];
	else
		$customer['address'] =  '';
		
	if (isset($_REQUEST['company'])) 			
		$customer['company'] = $_REQUEST['company'];
	else
		$customer['company'] =  '';
		
	if (isset($_REQUEST['mobile'])) 			
		$customer['mobile'] = $_REQUEST['mobile'];
	else
		$customer['mobile'] =  '';
		
	if (isset($_REQUEST['home'])) 			
		$customer['home'] = $_REQUEST['home'];
	else
		$customer['home'] =  '';
		
	if (isset($_REQUEST['email'])) 			
		$customer['email'] = $_REQUEST['email'];
	else
		$customer['email'] =  '';
		
	if (isset($_REQUEST['reenteremail'])) 			
		$customer['reenteremail'] = $_REQUEST['reenteremail'];
	else
		$customer['reenteremail'] =  '';
		
?>

<div id="registration" >Registration</div>
<div class="register-icon"></div>

<?php
	if (isset($error) && count($error) > 0) {
?>
	<div class="error-info form-info">
		<?php foreach ($error as $handle) {
				echo "<p>$handle</p>";
		} ?>
	</div>
<?php
	}
?>

<form name="registerform" id="registerform" action="register.php" method="post"> 
	<table class="formview" width="100%" border="0" cellspacing="3px" cellpadding="3px">
		<tr>
			<td class="LabelCell Required">NRIC / FIN <span class="hint">(Identity for account)</span></td>
			<td><input type="text" name="nric" id="nric" maxlength="25" class="input Required AlphaNumberic" value="<?php echo $customer['nric']; ?>" size="20" tabindex="10" /></td>
		</tr>
		<tr>
			<td class="LabelCell Required">Name</td>
			<td><input type="text" name="name" id="name" maxlength="255" class="input Required" value="<?php echo $customer['name']; ?>" size="20" tabindex="20" /></td>
		</tr>
		<tr>
			<td class="LabelCell Required">Date of Birth <span class="hint">(DD/MM/YYYY)</span></td>
			<td><input type="text" name="dob" id="dob" maxlength="10" class="input Required ValidDate" value="<?php echo $customer['dob']; ?>" size="20" tabindex="30" /></td>
		</tr>
		<tr>
			<td class="LabelCell">Address</td>
			<td><input type="text" name="address" id="address" maxlength="500" class="input" value="<?php echo $customer['address']; ?>" size="20" tabindex="40" /></td>
		</tr>		
		<tr>
			<td class="LabelCell Required">Company <span class="hint">(N.A. if not applicable)</span></td>
			<td><input type="text" name="company" id="company" maxlength="500" class="input Required" value="<?php echo $customer['company']; ?>" size="20" tabindex="50" /></td>
		</tr>
		<tr>			
			<td class="SectionBar" colspan="2">				
				Contacts
			</td>
		</tr>
		<tr>
			<td class="LabelCell Required">Mobile</td>
			<td><input type="text" name="mobile" id="mobile" maxlength="50" class="input Required" value="<?php echo $customer['mobile']; ?>" size="20" tabindex="60" /></td>
		</tr>
		<tr>
			<td class="LabelCell">Home</td>
			<td><input type="text" name="home" id="home" maxlength="50" class="input" value="<?php echo $customer['home']; ?>" size="20" tabindex="70" /></td>
		</tr>	
		<tr>			
			<td class="SectionBar" colspan="2">				
				Email
			</td>
		</tr>
		<tr>
			<td class="LabelCell Required">Your Email <span class="hint">(due to activation)</span></td>
			<td><input type="text" name="email" id="email" maxlength="50" class="input Required ValidEmail" value="<?php echo $customer['email']; ?>" size="20" tabindex="80" /></td>
		</tr>
		<tr>
			<td class="LabelCell Required">Re-enter Email</td>
			<td><input type="text" name="reenteremail" id="reenteremail" maxlength="50" class="input Required ValidEmail" value="<?php echo $customer['reenteremail']; ?>" size="20" tabindex="90" /></td>
		</tr>	
		<tr>			
			<td class="SectionBar" colspan="2">				
				Passwords
			</td>
		</tr>
		<tr>
			<td class="LabelCell Required">Your Password</td>
			<td><input type="password" name="password" id="password" maxlength="50" class="input Required" value="" size="20" tabindex="100" /></td>
		</tr>
		<tr>
			<td class="LabelCell Required">Re-enter Password</td>
			<td><input type="password" name="reenterpassword" id="reenterpassword" maxlength="50" class="input Required" value="" size="20" tabindex="110" /></td>
		</tr>
		<tr>			
			<td class="SectionBar" colspan="2">				
				Word Verification
			</td>
		</tr>
		<tr>
			<td class="LabelCell Required">Type the characters</td>
			<td>
				<img src="components/cool.php.captcha/captcha.php" id="captcha" />&nbsp;&nbsp;&nbsp;
				<!-- CHANGE TEXT LINK -->
				<a href="#" onclick="
					document.getElementById('captcha').src='components/cool.php.captcha/captcha.php?'+Math.random();
					document.getElementById('captcha-form').focus();"
					id="change-image">Not readable? Change text.</a>&nbsp;<br/><br/>


				<input type="text" name="captcha" id="captcha-form" class="input Required" tabindex="120" /><br/>
			</td>
		</tr>		
		<tr>			
			<td class="BottomToolBar" colspan="2">				
				<input type="submit" name="register" id="register" class="button-primary" value="Register" tabindex="130"/>
				<a href="index.php" class="button-secondary" tabindex="140">Cancel</a>				
			</td>			
		</tr>
	</table>	
</form>

<script type="text/javascript">
	$(document).ready(function() { 
		loadValidation('registerform');
		<?php if ($_SERVER['REQUEST_METHOD'] != "POST") { ?> 
			$("#nric").focus();
		<?php } ?>
		$( "#dob" ).datepicker({ dateFormat: 'dd/mm/yy'});	
	});	
</script>