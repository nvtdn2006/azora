@extends('frontend.template.matter_page')

@section('title')
Home page
@stop

@section('slider-container')
<div id="slider-container">
	<div id="slider">
		<img src="{{ asset('public/asets/images/images_sliders/20120201142220000000_new ipad.jpg') }}" width="950" height="300" alt="" title=""/>	
		
	</div>
</div>
@stop


@section('content')
<div id="right">
	<div id="righttitle">Latest Entries</div>
	<div class="new"></div>
	<div id="latestentries">
		<div class="product">
			<div class="image">
				<img src="http://azora.local/domain/images/products/20160531084109000000_11500 SK II Mens Facial Treatment Essence 30ml x 3.jpg" width="155" height="155" alt="SK II Mens Facial Treatment Essence 30ml x 3" title="SK II Mens Facial Treatment Essence 30ml x 3">
									</div>
			<div class="description"><div>
				<span>SK II Mens Facial Treatment Essence 30ml x 3</span>
				<p title=""></p></div></div>
			<div class="controls">
				<span>11,500 pts</span>
				<a class="button-primary redeem" href="#pleaselogin">Redeem</a>
			</div>
		</div>
		<div class="product">
			<div class="image">
				<img src="http://azora.local/domain/images/products/20160531084109000000_11500 SK II Mens Facial Treatment Essence 30ml x 3.jpg" width="155" height="155" alt="SK II Mens Facial Treatment Essence 30ml x 3" title="SK II Mens Facial Treatment Essence 30ml x 3">
									</div>
			<div class="description"><div>
				<span>SK II Mens Facial Treatment Essence 30ml x 3</span>
				<p title=""></p></div></div>
			<div class="controls">
				<span>11,500 pts</span>
				<a class="button-primary redeem" href="#pleaselogin">Redeem</a>
			</div>
		</div>
		<div class="product">
			<div class="image">
				<img src="http://azora.local/domain/images/products/20160531084109000000_11500 SK II Mens Facial Treatment Essence 30ml x 3.jpg" width="155" height="155" alt="SK II Mens Facial Treatment Essence 30ml x 3" title="SK II Mens Facial Treatment Essence 30ml x 3">
									</div>
			<div class="description"><div>
				<span>SK II Mens Facial Treatment Essence 30ml x 3</span>
				<p title=""></p></div></div>
			<div class="controls">
				<span>11,500 pts</span>
				<a class="button-primary redeem" href="#pleaselogin">Redeem</a>
			</div>
		</div>
		<div class="product">
			<div class="image">
				<img src="http://azora.local/domain/images/products/20160531084109000000_11500 SK II Mens Facial Treatment Essence 30ml x 3.jpg" width="155" height="155" alt="SK II Mens Facial Treatment Essence 30ml x 3" title="SK II Mens Facial Treatment Essence 30ml x 3">
									</div>
			<div class="description"><div>
				<span>SK II Mens Facial Treatment Essence 30ml x 3</span>
				<p title=""></p></div></div>
			<div class="controls">
				<span>11,500 pts</span>
				<a class="button-primary redeem" href="#pleaselogin">Redeem</a>
			</div>
		</div>
		<div class="product">
			<div class="image">
				<img src="http://azora.local/domain/images/products/20160531084109000000_11500 SK II Mens Facial Treatment Essence 30ml x 3.jpg" width="155" height="155" alt="SK II Mens Facial Treatment Essence 30ml x 3" title="SK II Mens Facial Treatment Essence 30ml x 3">
									</div>
			<div class="description"><div>
				<span>SK II Mens Facial Treatment Essence 30ml x 3</span>
				<p title=""></p></div></div>
			<div class="controls">
				<span>11,500 pts</span>
				<a class="button-primary redeem" href="#pleaselogin">Redeem</a>
			</div>
		</div>
		<div class="product">
			<div class="image">
				<img src="http://azora.local/domain/images/products/20160531084109000000_11500 SK II Mens Facial Treatment Essence 30ml x 3.jpg" width="155" height="155" alt="SK II Mens Facial Treatment Essence 30ml x 3" title="SK II Mens Facial Treatment Essence 30ml x 3">
									</div>
			<div class="description"><div>
				<span>SK II Mens Facial Treatment Essence 30ml x 3</span>
				<p title=""></p></div></div>
			<div class="controls">
				<span>11,500 pts</span>
				<a class="button-primary redeem" href="#pleaselogin">Redeem</a>
			</div>
		</div>

		<div class="product">
			<div class="image">
				<img src="http://azora.local/domain/images/products/20160531084109000000_11500 SK II Mens Facial Treatment Essence 30ml x 3.jpg" width="155" height="155" alt="SK II Mens Facial Treatment Essence 30ml x 3" title="SK II Mens Facial Treatment Essence 30ml x 3">
									</div>
			<div class="description"><div>
				<span>SK II Mens Facial Treatment Essence 30ml x 3</span>
				<p title=""></p></div></div>
			<div class="controls">
				<span>11,500 pts</span>
				<a class="button-primary redeem" href="#pleaselogin">Redeem</a>
			</div>
		</div>
	</div>
	<div id="pleaselogin" style="display:none;" class="info-box">	
		Please login first to proceed.
	</div>
	<div id="outofstock" style="display:none;" class="info-box">	
		This item is temporarily out of stock.
	</div>
</div>
@stop