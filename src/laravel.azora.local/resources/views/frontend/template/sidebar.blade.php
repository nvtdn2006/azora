<div id="left">
	<div id="registration" class="left">
		<a id="register" href="{{URL::to('/register')}}" title="Click here to register.">Create an account</a>
	</div>
	<div id="login" class="left">
		<a id="home-login" href="login.php" title="Login your account." class="home-login">Login your account</a>
	</div>
	<div id="site-menu" class="left">
		<ul class="main-menu">
			<li class="menu-group">
				<div class="menu-group-panel-outer">
					<div class="menu-group-panel-bar">
						<span>About Rewards</span>		
						<div id="rewards-menu-icon"></div>								
					</div>
					<ul class="menu-item-list">
						<li><a href="{{URL::to('/overview')}}" title="Overview">Overview</a></li>	
						<li><a href="{{URL::to('/faq')}}" title="Frequently Ask Question">FAQ</a></li>	
					</ul>
				</div>
			</li>
		</ul>
	</div>

	<div id="site-menu" class="left">
		<ul class="main-menu">
			<li class="menu-group">
				<div class="menu-group-panel-outer">
					<div class="menu-group-panel-bar">
						<span>Information</span>		
						<div id="information-menu-icon"></div>								
					</div>
					<ul class="menu-item-list">
						<li><a href="contactus.php" title="Help">Help</a></li>	
						<li><a href="stores.php" title="Stores">Our stores</a></li>	
					</ul>
				</div>
			</li>
		</ul>
	</div>
	<div id="product-categories" class="left">
<ul class="main-menu">
	<li class="menu-group"><div class="menu-group-panel-outer">					
		<div class="menu-group-panel-bar">
			<span>Product Category</span>
			<div id="product-menu-icon"></div>
		</div>
		<ul class="menu-item-list">
			<li><a href="#" title="All products">All Products</a></li>
			<li><a href="#" title="Premium Products">Premium Products</a></li>
			<li><a href="#" title="Luxury Products">Luxury Products</a></li>
			<li><a href="#" title="Apple Products">Apple Products</a></li>
			<li><a href="#" title="Electronics Product">Electronics Product</a></li>
			<li><a href="#" title="Lifestyle Products">Lifestyle Products</a></li>
			<li><a href="#" title="Home Appliances">Home Appliances</a></li>
		</ul>
		</div>
	</li>	
</ul>
</div>
</div>