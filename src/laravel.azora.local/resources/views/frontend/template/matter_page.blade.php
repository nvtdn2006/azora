
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title>@yield('title')</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('public/asets/css/style.css') }}" media="screen">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/asets/css/default.css') }}" media="screen">
	<!-- style css -->
	<link rel="stylesheet" type="text/css" href="{{ asset('public/asets/css/home.css') }}" media="screen">
	

	@yield('style')
</head>
<body>
	<div id="container">
		<!-- user panel -->
		<div id="userpanel">
			<div id="userpanel-wrapper">				
				<div id="user">
					<ul>
						<li><a href="#" id="home"></a></li><li><span id="username">Welcome, Guest</span></li>
						<li><span id="lastlogin"></span></li></ul></div>
				<div id="controls">
					<ul>
						<li><a href="#" id="login" title="Click here to login to your account.">Login</a></li>
					</ul>
				</div>				
			</div>
		</div>
		
		<div id="wrapper">
			<div id="header">
				<div id="logo"><a href="{{URL::to('/')}}"></a></div>
				<div id="prms"><a href="#"></div>
			</div>
			@yield('slider-container')
			@include('frontend.template.sidebar')
			@yield('content')
			<br style="clear:both;" />
			<div class="push"></div>
		</div>

		<div id="footer">
			@include('frontend.template.footer')
		</div>

	</div>
	
	@yield('script')

</body>
</html>